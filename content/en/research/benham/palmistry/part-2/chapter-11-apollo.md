---
title: "The Apollo Line"
# heading: "A Working Hypothesis"
date: 2021-11-20
image: "/photos/objects/hand.jpg"
description: "There is another Minor line, called the Via Lascivia, and supposed to be a sister line to the Mercury line. But I consider it as a chance line, and so it does not have  fixed place among the Minor lines"
linkb: "/research/benham/palmistry/part-2/chapter-02"
linkbtext: "Chapter 2"
linkf: "/research/benham/palmistry/part-2/chapter-03-defects"
linkftext: "Chapter 3"
---



The subject may do something wonderful, or may pass everything by and accomplish nothing. 

If this line becomes strong on the Mount of Apollo, the subject will finally round up with force in some direction. It may be that he becomes a great wanderer, very erratic, or a crank, but he will earn reputation for something. If this line should end in a star (441) the life will end brilliantly, the subject will have achieved wide reputation, and an unsteady though talented subject will have his work finally crowned with success. The direction from which this success will come must be estimated Chirognomically. 

If islands be seen in the line (442) they will prove serious obstructions. Islands appearing in a deep line will show that the realization of wealth and fame will be impeded, and the subject stands in danger of a positive loss of reputation and of money. In whatever world the subject moves, he will attempt things which promise reward, but the failure of which will bring him loss. If the Apollo finger be as long or longer than Saturn, the subject will be a "plunger," he will dazzle the stock market with his daring, and will take desperate chances to win. If his Head and Life lines be widely separated this plunging disposition will be increased by his extreme self confidence.

But with the island in the Apollo line he is in constant danger of a loss of money, and of his reputation as well. If the third phalanx of the Apollo finger be long and thick, with the above combination, this subject will be the common gambler. If the Mercury finger be crooked and twisted, the Heart line thin or absent, and the hand otherwise bad, he will be the card sharper and trickster, who resorts to cheating, and uses the brilliancy afforded by the Apollo line to further the basest ends. Such a line in any hand, no matter how good, should be a constant warning to the subject. Bars cutting the line of Apollo (443) will show constant impediments to the success of the subject. When these are seen, first locate the subject's world of action, and then apply these impediments to it. These bars may arise from various causes, all of which may be located by chance lines, Mounts, Influence lines, or other indications. If the Apollo line cuts through these bars, the subject will overcome the obstacles. If the bars cut the line, the impediments will seriously affect the career. If these bars are little fine lines which seem only to run over the top of the Apollo line, they are annoying interferences, which keep the subject constantly worried and by disturbing his mental peace impede his progress.

Dots on the Apollo line (444) are a menace to the reputation of the subject. If they be small they only indicate the whisperings of enemies, but if they be large and deep, they indicate the actual loss of good name. Chance lines and the usual indications will locate the causes.

The Line Of Apollo Part 4 593
No. 442.

The Line Of Apollo Part 4 594
No. 443.

The Line Of Apollo Part 4 595
No. 444.

The Line Of Apollo Part 4 596
No. 445.

Breaks in the Apollo line (445) indicate setbacks to the ambition and upward course of the subject, and are impediments which destroy the usefulness of the line and render inoperative all of its beneficial influences. Such a line shows that the subject may have a strong liking for art, if that world rules, but that he will never be a producer or a creator of it; he is, if wealthy, the art patron. If the business world be strong, these people will be only partially successful, and even with some of the beneficial effects of the line present, will make many costly mistakes. With breaks in the line. one cannot see more than ordinary success for the subject, and many failures. With all breaks seen in the line, look for repair signs which will tend to overcome the obstacles and bring about better conditions. The usual repair signs will be found in a variety of combinations (446). Such a repaired line is not better in grade than a broad and shallow one.

The Line Of Apollo Part 4 597
No. 446.

The Line Of Apollo Part 4 598
No. 447.

The Line Of Apollo Part 4 599
No. 448.

The termination of the Apollo line will tell the outcome of the line. If it be a deep line at the start and grows thin until it gradually fades away, the best period of the life will be during the time when the line is deep, and the wealth-producing capacity will diminish as the life progresses. In this case the final success of the subject will only be ordinary (447). If a line of Apollo ends in a dot (448) the subject will after a life of prosperity lose his reputation in the end. If the line of Apollo ends in a star (449) it will be an indication of brilliant success. A star on the Mount of Apollo is an electric light ending a good line, and intensifies the entire combination. If the subject be mental he will win great fame and renown as a poet, writer, painter, sculptor, actor, or in other artistic callings. Bernhardt, Nordica, Modjeska, Kathryn Kidder, and many brilliant actresses, vocalists, or instrumentalists have this marking. If the subject belongs to the practical world he will make money fast and easily. His ventures will be uniformly profitable, and he will be celebrated for his success. If the third phalanx of his finger predominates he may not have a high grade of refinement, but will make a great deal of money.

If the line of Apollo have on it a double star the subject will be dazzling in his brilliancy and the greatest farne will come to him (450). Bernhardt has this marking. In these cases the first star will indicate the age at which the subject will first achieve great success, and the star at the end of the line will indicate that this prosperity and renown will continue to the end of life. If the Apollo line begins with a star and ends with one, the subject will be brilliant and successful from the time of birth, throughout the entire life (451). If the line of Apollo ends in a deep bar the life will meet some obstruction near the close which will be iusurmouutable (452). This will be a decided check to the career and with this sign the Saturn line should be closely examined, as well as all other indications which may locate the cause. If with this marking the Life line be defective at near fifty years, and continue so, ending in a tassel, fork, island, star, cross, or other defect, the subject will have ill health and delicacy at that age, from which he never recovers, and which ruins his prospects in life (453). If an island and a star or either be seen in the Head line at age fifty, the subject will fail in mental powers, which will check bis career and put a stop to his success (454). If a rising split from the Head line runs to a bar on the Mount, an error in calculation will cause a check to the subject's career from which he does not recover (455). Often this refers to investments made early in life, and which turn out badly.

The Line Of Apollo Part 4 600
No. 449.

The Line Of Apollo Part 4 601
No. 460.

The Line Of Apollo Part 4 602
No. 451.

The Line Of Apollo Part 4 603
No. 452.

The Line Of Apollo Part 4 604
No. 453.

The Line Of Apollo Part 4 605
No. 454.

The Line Of Apollo Part 4 606
No. 455.

 

