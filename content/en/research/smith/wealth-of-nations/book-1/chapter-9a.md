+++
title=  "Profits"
date=  2020-01-14
image=  "/covers/wn.jpg"
description=  "Interest is the Best Indicator of Profits"
linkb=  "/research/smith/wealth-of-nations/book-1/chapter-8e"
linkbtext=  "Chapter 8e"
linkf=  "/research/smith/wealth-of-nations/book-1/chapter-9b"
linkftext=  "Chapter 9b"
heading=  "Chapter 9a"
icon=  "/icons/smith.png"
# linkbook=  https= //play.google.com/store/books/details/Juan_Dalisay_Jr_The_Simple_Wealth_of_Nations_by_Ad?id=BjnPDwAAQBAJ
# linkbooktext=  Support Superphysics by buying the ebook version
+++


## Interest is the Best Indicator of Profits

{{< s v="1" >}} The rise and fall in profits depend on the increasing or declining movement of the society's wealth.

This is also the cause of the rise and fall in wages, although those causes affect profits and wages very differently.

{{< s v="2" >}} The increase of stock raises wages and lowers profit.

When the stocks of merchants are turned into the same trade, their mutual competition lowers their profit.
Likewise, when stocks increase in all trades, the same competition also lowers profits.

{{< s v="3" >}} It is not easy to determine the average wages even in a particular place and time.

It would be more difficult to determine the average profits of stock.
Profit is so very fluctuating, that the businessman cannot always tell what his average annual profit is.

It is affected by: 
- every variation of the prices of the commodities he deals in
- the good or bad fortune of his rivals and customers
- many other accidents to which goods are liable to during transportation and storage

It varies yearly, daily, and hourly.
It is much more difficult to ascertain the average profit of all businesses in a big country.
It is impossible to judge the average profit in the different times in the past.


{{< s v="4" >}} However, past and present average profits can be estimated from the interest of money.

Wherever a great deal can be made by the use of money, a great deal will be given for the use of it.
Wherever little can be made by it, less will be commonly given for it.
The ordinary profits of stock must rise and fall with the usual market rate of interest.
The progress of interest indicates the progress of profit.

{{< s v="5" >}} By 1545, all interest above 10% was declared unlawful.

Interest rates must have been higher before then.
Back then, religious zeal prohibited all interest.
This prohibition was ineffective.
It probably even increased the evil of usury.

The statute of Henry the 8th was revived by the 13th of Elizabeth, Chapter 8 in 1570.
10% continued as the legal interest rate until 1623 when it was restricted to 8%.
It was reduced to 6% after the restoration in 1660.
By the 12th of Queen Anne in 1713, it was reduced to 5%.
All these regulations were made with great propriety.
They followed the market rate of interest, or the rate at which people of good credit usually borrowed.
Since Queen Anne's time, 5% was above the market rate.
Before the Seven Years' War in 1756, the government borrowed at 3%, while people of good credit in Great Britain borrowed at 3.5-4.5%

{{< s v="6" >}} Since the time of Henry the 8th in 1509, England's revenue has been advancing continually and at an accelerating rate.

Wages have been continually increasing while business profits have been declining.


{{< s v="7" >}} More stock is generally needed to do business in a big town than in a rural village.

The following generally reduce the profit rate in big towns below that of rural villages= 
The great stocks employed in every business.
The number of rich competitors.
But wages are generally higher in a big town than in a rural village.
In a thriving town, the people who have great stocks to employ frequently cannot get the number of workers that they want.
They bid against one another to get as many as they can.
This raises wages and lowers profits.
In the remote parts of the country, there is frequently insufficient stock to employ everyone.
The people bid against each other to get employment.
This lowers wages and raises profits.

8 In Scotland, the market interest rate is higher than the legal interest rate even though the legal interest rate is the same as in England.

Credit-worthy people in Scotland seldom borrow under 5%.
Even private bankers in Edinburgh give 4% loans which can be demanded by the banks at their pleasure.
Private bankers in London pay no interest on deposits.
Some businesses in Scotland need a bigger stock than a similar business in England.
The common rate of profit, therefore, must be greater in Scotland than England.
I have mentioned earlier that wages are lower in Scotland than in England.

Scotland is also much poorer and advancing more slowly than England.

9 According to Jean-Baptiste Denisart, the legal interest rate in France was not always always regulated by the market rate.
- In 1720, interest was reduced from 5% to 2%.
- In 1724, it was raised to 3.33%.
- In 1725, it was again raised to 5%.
- In 1766, during Mr. Laverdy's administration, it was reduced to 4%.
- The Abbé Terray raised it afterwards to 5%.

The reductions were done to reduce the public debts. 

France is not as rich as England. The legal interest rate in France frequently has been lower than in England.
But the market rate has generally been higher.
France has very safe and easy methods to evading the law.
The profits of trade are higher in France than in England.
Many British choose to employ their capitals in a country where trade is in disgrace, than in one where it is highly respected.
Wages are lower in France than in England
The disparity of wealth and revenue between the two countries can be seen in the difference in the dress and food of the common people.
This disparity is seen between Scotland and England, but is more evident in France.
Although France is richer than Scotland, it seems to be advancing more slowly.
People think that France and Scotland are going backwards.
I think France is not going backwards
I think it is impossible that Scotland is going backwards because I have seen it 20-30 years ago and today.

10 Holland is richer than England.

Its government borrows at 2%.
Credit worthy people borrow at 3%.
Wages are higher in Holland than in England.
The Dutch trade with lower profits than any European country.
The trade of Holland was alleged to be decaying
It might be true in some trades.
But the symptoms above show that there is no general decay.
When profits decrease, merchants complain that trade decays, though this reduction is the natural effect of its prosperity, or of more stock being employed in it than before.
During the Seven Years' war, the Dutch gained and still retain a large share of France's total carrying trade.
They own 40 million of combined French and English funds (which might be exaggerated).
Their excess stock is shown by the big amounts they lend out to countries which have higher interest rates.
They are unable to profit from this stock in their own country.
It does not mean that business has decreased in Holland.
A private man's capital may increase to exceed what he can employ himself, while his business increases.
This is the same with a nation.

11 In our North American and West Indian colonies, both wages and interest, and consequently profits, are higher than in England.

Both the legal and market interest rates in the colonies run from 6-8%.
High wages and high profits, however, rarely go together, except in the peculiar circumstances of new colonies.
Compared to other countries, a new colony= 
is more under-stocked relative to its territorial size for some time,
is more under-peopled relative to the size of this stock,
has more land than they have stock to cultivate,
uses its stock to cultivate the most fertile and best situated land with access to waterways.
Such land is frequently bought at a price below the value even of its natural produce.
Stock employed to buy and improve such lands must yield a very large profit and interest.
Its rapid accumulation and high profits enables the planter to increase employment faster than he can find workers in a new settlement.
Those whom he can find, therefore, are very liberally rewarded.
As the colony increases, profits gradually decrease.

When the most fertile and best situated lands have been all occupied, less profit can be made from the remaining inferior land.
This creates less interest.
In most of our colonies, both the legal and the market interest rate have been considerably reduced within the last century.
As riches, improvement, and population have increased, interest has declined.

The wages do not sink with the profits.
The demand for labour increases with the increase of stock, whatever be its profits.
After profits are reduced, stock continues to increase much faster than before.
This holds true for industrious nations advancing in wealth, as with industrious individuals.
A great stock with small profits generally increases faster than a small stock with great profits.
"Money makes money," says the proverb.
When you have the seed money, it is often easy to get more.
The great difficulty is to get that seed money.
In Book 2, Chapter 3, I shall explain the connection between the increase of stock and the increase of the demand for labour, with regard to the accumulation of stock.

12 The following may sometimes raise profits, and with them the interest of money, even in a country fast advancing to wealth= 

the acquisition of new territory, or
the acquisition of new branches of trade.
The limited stock of the country is applied only to those branches which afford the greatest profit.

Part of what was employed in other trades, is withdrawn and turned into some of the new and more profitable ones.
In all those old trades, the competition becomes less.
Less supply becomes available.
It raises prices and yields more profit to the dealers.
They can then afford to borrow at a higher interest.
After the Seven Years' war, credit-worthy people, and some of the biggest companies in London, borrowed at 5%.

They used to borrow at 4-4.5%.
This was caused by the great increase of territory and trade without the reduction in the capital stock of society.
New businesses were carried on by the old stock.
It lowered unemployment and created greater profits because of the reduced competition.
I will show next why Great Britain's capital stock was not reduced even after the enormous cost of that war.
Wages and Stock are Inverse of Profits and Interest

13 The capital stock of society is the fund which maintains industry.

The reduction of this stock lowers wages, raises profits, and consequently, raises the interest of money.
With lower wages, businesses can make products more cheaply than before.
The fewer products in the market allow them to sell their products dearer.
Their goods cost them less, and they get more for them.
Their profits are augmented at both ends.
This affords them a large interest.
The great fortunes so suddenly and easily acquired in Bengal and the British settlements in the East Indies, prove that as wages are very low, so profits and interest are very high in those ruined countries.
In Bengal, money is frequently lent to farmers at 40-60%.
The succeeding crop is mortgaged for the payment.
The interest eats up the profits which eats up landlord's rent.
Before the fall of the Roman republic, the same usury was common in the Roman provinces under the ruinous administration of their proconsuls.
The virtuous Brutus lent money in Cyprus at 48%, based on Cicero's letters.


