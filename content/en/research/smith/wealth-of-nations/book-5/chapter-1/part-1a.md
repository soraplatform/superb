+++
title=  "The Development of Militaries"
description=  "The art of war is certainly the noblest of all arts."
date=  2020-12-30
image=  "/covers/wn.jpg"
linkb=  "/research/smith/wealth-of-nations/book-4/chapter-9b-unproductive"
linkbtext=  "unproductive"
linkf=  "/research/smith/wealth-of-nations/book-4/chapter-9d-foreign-systems"
linkftext=  "foreign-systems"
heading=  "Chapter 1 Part 1a"
icon=  "/icons/smith.png"
# linkbook=  https= //play.google.com/store/books/details/Juan_Dalisay_Jr_The_Simple_Wealth_of_Nations_by_Ad?id=WyYAEAAAQBAJ
# linkbooktext=  Support Superphysics by buying the ebook
+++


## Part 1: Defence Expenditures 

The sovereign's first duty is to protect society from the violence and invasion of other societies.
- This can only be done through a military force.
- The cost of this military force in peacetime and wartime is very different in the different states and periods of the improvement of society.


## The Development of Militaries

{{< s v="1" >}} The hunting nation is the lowest and rudest state of society. An example is the North America native tribes.

In such a society, every man is a warrior and a hunter. When he goes to war, he maintains himself by his own labour in the same way as when he lives at home.

His society does not spend to= 
- prepare him for the field, or
- maintain him while he is in it, because there is no sovereign nor commonwealth.


{{< s v="2" >}} The shepherd nation is a more advanced society. Examples are the Mongols and the Arabs.

Every man there is a warrior. Such nations have commonly no fixed habitation. The people= 
- live in tents or in covered wagons, and
- change their location according to the seasons and other accidents.

When its herds and flocks consume the forage of one part of the country, it moves to another, and then to a third.
- In the dry season, it comes down to the riverbanks.
- In the wet season, it retires to the upper country.

When such a nation goes to war, the warriors take their herds, old men, women and children with them.
- The whole nation is used to a wandering life and so they easily take the field in wartime.
- They go to war together<!--  and everyone does as well as he can -->.

Among the Mongols, even the women have engaged in battle.
- If they conquer, whatever belongs to enemy is the recompense of the victory.
- But if they are vanquished, all is lost -- their their herds, women, and children, become the conqueror's booty.
- Even most of the survivors surrender to him for the sake of immediate subsistence.
- The rest are commonly dispersed in the desert.


{{< s v="3" >}} The ordinary life and exercises of a Mongol or an Arab, prepare him for war.

Running, wrestling, cudgel-playing, javelin-throwing, drawing the bow, etc. are the common pastimes of those who live in the open air.
- All such activities are also war activities.

When a Mongol or Arab goes to war, he is maintained by his own herds and flocks.
- He also carries these with him in peacetime.

His chief does not spend to prepare him for the field.
- The chance of plunder is the only pay he expects or requires.


{{< s v="4" >}} An army of hunters can seldom exceed 200-300 men. The chase affords a precarious subsistence which could seldom sustain more men.

On the contrary, an army of shepherds might sometimes reach 200,000-300,000. There seems no limit on how many can march together as long as= 
- nothing stops their progress, and
- they can go from one district to another while consuming the forage

A nation of hunters can never be formidable to civilized nations. A nation of shepherds may be formidable.

Nothing can be more contemptible than an Indian war in North America.
On the contrary, nothing can be more dreadful than Mongol invasions in Asia.

Thucydides judged that Europe and Asia could not resist the Scythians united.
This has been verified by the experience of all ages.
The people of the extensive but defenceless plains of Russia or Mongolia were frequently united under the chief of some conquering horde or clan.

Asia's devastation has always resulted from their union.
The people of the inhospitable Arabian deserts were only united under Mohammad and his immediate successors.
Their union was more the effect of religious enthusiasm than of conquest.
It also caused the havoc in Asia.
If the hunting nations of America ever become shepherds, they would be much more dangerous to the European colonies.

5 A nation of husbandmen is a more advanced state of society.

Such nations have= 
- little foreign commerce, and
- only coarse and household manufactures which almost every private family prepares for its own use.

Every man of such a nation is a warrior or easily becomes one.
Those who live by agriculture generally pass the whole day in the open air, exposed to the seasons.
The hardiness of their ordinary life and their necessary occupations prepares them for the fatigues of war.

The necessary occupation of a ditcher prepares him to= 
    work in the trenches,
    fortify a camp, and
    enclose a field.

The ordinary pastimes of such husbandmen are the same as those of shepherds.
They are the same activities during war.
Husbandmen have less leisure than shepherds.
Husbandmen are not so frequently employed in those pastimes.
They are soldiers not quite so skilled in their exercise.
It seldom costs the sovereign any expence to prepare them for the field.

6 Agriculture requires a settlement even in its rudest and lowest state.

It requires a fixed habitation which cannot be abandoned without great loss.
When a nation of mere husbandmen goes to war, the whole people cannot take the field together.
The old men, women, and children, must remain at home to take care of the habitation.
All the men of military age may take the field.
In small nations of husbandmen, they have frequently done so.
In every nation, the men of military age are supposed be around 1/4 or 1/5 of total population.
If the campaign should begin after seed-time and end before harvest, the husbandman and his labourers can be spared from the farm without much loss.
He is willing to serve without pay during a short campaign.
It frequently costs the sovereign as little to maintain him in the field as to prepare him for it.
The work in the fields can be executed by the women, children, and old men in the meantime.

All the ancient Greek citizens served in this way until after the second Persian war.

The people of Peloponnesus served in this way until after the Peloponnesian war.
Thucydides observes that the Peloponnesians= 
left the field in the summer, and
returned home to reap the harvest.

During the first ages of the republic, the Romans did the same.

Starting from the siege of Veii, those who stayed at home began to contribute towards maintaining those who went to war.
In the European monarchies before and after the establishment of the feudal law, the great lords served the crown at their own expence.
In the field and at home, they maintained themselves by their own revenue, and not by any stipend or pay from the king.


{{< s v="7" >}} In a more advanced society, two causes render it impossible for those who take the field to maintain themselves at their own expence= 
- The progress of manufactures
- The improvement in the art of war

8 When a husbandman goes to war which starts after seed-time and ends before harvest, his revenue is not reduced with his work's interruption.

Nature does most of the husbandman's remaining work.
But when an artificer, smith, carpenter, or weaver quits his workhouse, his revenue completely dries up.
Nature does nothing for him.
    He does all for himself.
When he takes the field to defend the public, he has no revenue to maintain himself.
    He must be maintained by the public.
But in a country where most of the people are artificers and manufacturers, the people who go to war must be drawn from those classes.
Therefore, they must be maintained by the public as long as they are employed in its service.


9 It becomes universally necessary for the public to maintain those who serve the public in war, at least while they are employed to do so when= 

the art of war has grown to be a very intricate and complicated science,
war ceases to be determined by a single irregular skirmish or battle, and
war is made up of several different campaigns, each lasts for most of the year.

Military service would have been very tedious and expensive for those who served in war.

After the second Persian war, the armies of Athens were composed of mercenary troops.
They consisted partly of citizens and foreigners.
All of them were equally hired and paid by the state.
From the time of the siege of Veii, the Roman armies received pay while they remained in the field.
Under the feudal governments, the military service of the great lords and of their dependants was universally exchanged for a payment in money which maintained those who served.


10 Fewer people can go to war in a civilized society than one in a rude state.

In a civilized society, the soldiers are maintained by those who are not soldiers.
The number of soldiers can never exceed what the non-soldiers can maintain.
In the little agrarian states of ancient Greece, 1/4 or 1/5 of the people are soldiers.
In modern Europe, not more than 1% of the people of any country can be soldiers without ruining the country which pays their expences.


11 The cost of an army was not considerable in any nation until long after it fell entirely on the sovereign.

In all the ancient Greek republics, learning military exercises was a necessary part of education imposed by the state on every free citizen.
In every city, there was a public field under the protection of the public magistrate where young people were taught exercises by different masters.
This was the whole expence any Greek state ever had to prepare its citizens for war.
The exercises of the Campus Martius in ancient Rome answered the same purpose of the Gymnasium in ancient Greece.
Under the feudal governments, there many public ordinances promoting the practise archery and other military exercises to the citizens.

They were universally neglected= 
- from the lack of interest of the officers executing those ordinances, or
- from some other cause.

In all those governments, military exercises went gradually into disuse among the people.


12 The trade of a soldier was not a separate distinct trade.

It was not the sole or principal occupation of any citizen.
This was true from the ancient Greek and Roman republics to the first feudal governments.
Every subject of the state considered himself as a soldier during many extraordinary occasions.


13 The art of war is certainly the noblest of all arts.

In the progress of improvement, it necessarily becomes one of the most complicated.
The mechanical and other arts connected to it determines its perfection.
To make it perfect, it should be the sole or principal occupation of a class of citizens.
The division of labour is necessary for the improvement of this art as in every other art.
The division of labour is naturally introduced by the prudence of individuals.
They find that they promote their private interest better by confining themselves to a particular trade than by exercising many.
Only the state's wisdom can render the soldier's trade distinct from all others.
A private citizen who spends his time in military exercises might= 
    improve himself very much in them and
    amuse himself very well.
But he certainly would not promote his own interest.
Only the state's wisdom can render it for his interest to give up most of his time to be a soldier.
    States did not always have this wisdom even when their existence required it.


14 A shepherd has much leisure. An artificer or manufacturer has none at all.

<!-- In the rude state of husbandry, a husbandman has some leisure. -->

The shepherd may spend much of his time in martial exercises without any loss. But an artificier or manufacturer cannot spend a single hour in martial exercises without some loss. This leads him to neglect martial exercises. 
<!-- A husbandman may employ some of his time without any loss. -->

<!-- His attention to his own interest naturally leads him to  -->
The improvements in husbandry leave the husbandman as little leisure as the artificer.
Military exercises become neglected by people in towns and in the countryside.
The people becomes unwarlike.

That wealth always follows the improvements of agriculture and manufactures.
It is the accumulated produce of those improvements.
It provokes the invasion of all their neighbours.

An industrious and wealthy nation is the most likely to be attacked.Unless the state takes some new measures for its defence, the natural habits of the people render them incapable of defending themselves.
