+++
title=  "Part 2b-Fees on Justice"
description=  "Justice never was in reality administered gratis in any country"
date=  2020-12-26
image=  "/covers/wn.jpg"
linkb=  "/research/smith/wealth-of-nations/book-5/chapter-1/part-2a"
linkbtext=  "Part 2a"
linkf=  "/research/smith/wealth-of-nations/book-5/chapter-1/part-3a"
linkftext=  "Part 3a"
heading=  "Part 2b"
icon=  "/icons/smith.png"
# linkbook=  https= //play.google.com/store/books/details/Juan_Dalisay_Jr_The_Simple_Wealth_of_Nations_by_Ad?id=WyYAEAAAQBAJ
# linkbooktext=  Support Superphysics by buying the ebook
+++


{{< s v="55" >}} The second period of society is the age of shepherds.

The inequality of fortune first begins in this period.
This period introduces a new degree of authority and subordination.
It introduces some civil government necessary for its own preservation.
It seems to do this naturally and independent of that necessity.
    The consideration of that necessity comes afterwards.
    It contributes very much to maintain and secure that authority and subordination.
The rich are necessarily interested to support those who secure them in their own advantages.
Poorer men combine to defend the property of richer men so that those richer men may combine to defend the property of the poorer men.

All the inferior shepherds feel that their herd's security depend on the security of the great shepherd's herds.

Their lesser authority depends on his greater authority.
Their subordination to the great shepherd keeps their inferiors, in turn, subordinate to them.
They constitute a little nobility.
They support their sovereign so that he can defend their property and support their own authority.
Regarding the security of property, civil government is in reality instituted for the defence of the rich against the poor.

56 For a long time, the judicial authority of such a sovereign was his source of revenue.

The persons who applied to him for justice were always willing to pay for it.
    A gift never failed to accompany a petition.
After the authority of the sovereign was thoroughly established, the person found guilty was forced to pay a fine to= 
    the injured party, and
    the sovereign.
A fine was due because= 
    he had given trouble, and
    he had disturbed and broke the peace of his king.

In the ancient Mongol, German, Scythian governments, the administration of justice was a big source of revenue to the sovereign and his lesser chiefs.

Originally, they exercised this jurisdiction in their own persons.
Afterwards, they universally found it convenient to delegate it to some substitute, bailiff, or judge.
    However, this substitute was still obliged to account to his principal or constituent for the profits of the jurisdiction.

The judges of Henry II were itinerant agents sent around the country to levy the king's revenue.

In those days, the administration of justice afforded a revenue to the sovereign.
Procuring this revenue was one of the principal advantages the sovereign obtained by the administration of justice.

57 This scheme of making a revenue out of the administration of justice was prone to gross abuses.

The person who applied for justice with a large gift in his hand was likely to get something more than justice.
    The person who applied for it with a small gift was likely to get something less.
Justice might also be frequently delayed so that this gift might be repeated.
The fine of the person accused might be a very strong reason for finding him in the wrong even when he was not really so.
    Such abuses were common in the ancient history of every European country.

58 It was impossible to get any redress when the sovereign exercised his judicial authority in his own person and abused it.

There could seldom be anybody powerful enough to call the sovereign to account.
When the sovereign exercised judicial authority through a bailiff, there might be some redress only the bailiff had been guilty of injustice for the bailiff's benefit.
    The sovereign might be willing= 
        to punish him, or
        to oblige him to repair the wrong.
But if the bailiff committed any act of oppression for the sovereign's benefit, redress would usually be as impossible as if the sovereign had committed it himself.
    For a long time, the administration of justice was extremely corrupt in= 
        all barbarous governments, and
        all ancient European governments founded on the ruins of the Roman empire.
It was far from being equal and impartial even under the best monarchs.
    It was altogether profligate under the worst monarchs.

59 Among nations of shepherds, the sovereign or chief is the only greatest shepherd.

He is maintained by the increase of his own herds or flocks in the same way as his subjects.
Examples of nations of husbandmen who have just come out of the shepherd state were= 
    The Greek tribes during the Trojan war, and
    Our German and Scythian ancestors when they first settled on the ruins of the western empire.
The sovereign of those nations is their greatest landlord.
    He is maintained in the same way as any other landlord= 
        by a revenue from his own private estate, and
        the demesne of the crown in modern Europe.
    His subjects contributed nothing to his support except when they need his protection.
        The gifts they give him on such occasions make up his whole ordinary revenue or emoluments.

In Homer, Agamemnon offers the sovereignty of seven Greek cities to Achilles for his friendship.

The sole advantage he mentions was the gifts the people would give him in honour.
The sovereign could not be expected to give up such gifts and the fees of court if they were his whole revenue.
He might regulate and ascertain them.
    But after they had been so regulated and ascertained, it was still very difficult or impossible to hinder him from extending those gifts beyond those regulations.
    The corruption of justice naturally resulted from the arbitrary and uncertain nature of those gifts.
        There was no effective remedy to it.

60 The sovereign's private estate became insufficient for defraying the cost of his sovereignty chiefly because of the increasing expences of national defence.

When it became necessary for the people to contribute taxes for their own security, it was commonly stipulated that no gift for the administration of justice should be accepted by the sovereign or his judges, bailiffs, and substitutes.
    Those gifts could more easily be abolished than regulated and ascertained.
Fixed salaries were appointed to the judges.
    It was supposed to compensate to them for the loss of their ancient emoluments of justice, in the same way the taxes more than compensated the sovereign for the loss of his emoluments.
    "Justice was then said to be administered gratis."

61 "Justice never was in reality administered gratis in any country."

Lawyers and attorneys must always be paid by the parties.
    If not, they would perform their duty worse.
The fees annually paid to lawyers and attorneys are higher than the judges' salaries.
The salaries paid by the crown did not much reduce a lawsuit's cost.
Judges were prohibited from receiving any gift or fee from the parties to prevent the corruption of justice, not to reduce the cost.

62 The office of judge is so very honourable that men are willing to accept it with very small emoluments.

The inferior office of justice of peace is attended with a good deal of trouble.
    In most cases, it has no emoluments at all.
    It is an object of ambition for most of our country gentlemen.
The salaries of all judges and the total cost of the administration and execution of justice makes a very small part of government expences in any civilized country.

63 The whole expence of justice might be easily defrayed by the fees of court.

The public revenue might thus be discharged from a small incumbrance without exposing justice to corruption.
When the sovereign has a share in the fees of court, it is= 
    difficult to regulate the fees effectively, and
    difficult for the sovereign to derive any considerable revenue from them.
It is very easy where the judge is the principal person who can reap any benefit from the fees.
    The law can very easily oblige the judge to respect the regulation.
    It might not always be able to make the sovereign respect it.
There is no additional danger of corruption when= 
    the court fees are precisely regulated and ascertained, and
    the fees are paid all at once in every process into the cashier or receiver.
        The fees are distributed by the cashier in known proportions among the judges after the ruling is made, and not before.
        Those fees do not increase the lawsuit's cost considerably.
            It might be enough to defray the total cost of justice.
By not paying the fees to the judges until the process is determined, they might be incited to diligence in examining and deciding it.
    In courts with many judges, those fees might encourage the diligence of each judge by proportioning the share of each judge to the number of hours and days he examined the process.

"Public services are never better performed than when their reward comes only in consequence of their being performed, and is proportioned to the diligence employed in performing them."

In the French parliaments, the fees of court are called Epicès and vacations.
    They make up most of the emoluments of the judges.
The Toulouse parliament is second in rank and dignity in France.
    After all the deductions, the net salary paid by the crown to a judge there is only 150 livres, about 1,572 pence a year.
    Around seven years ago, that was the yearly wage of a common footman in Toulouse.
    The distribution of those Epicès is also according to the judge's diligence.
        A diligent judge gains a comfortable, moderate revenue.
        An idle one gets little more than his salary.
Those parliaments are perhaps not very convenient courts of justice.
    They were never accused nor suspected of corruption.

64 The court fees were originally the principal support of the English courts of justice.

Each court tried to draw as much business as it could to itself.
    It was willing to take many suits which did not fall under its jurisdiction.
The court of king's bench was instituted for criminal trials only.
    It took civil suits.
    The plaintiff pretended that the defendant was guilty of some crime in not giving him civil justice.
The court of exchequer was instituted to= 
    levy the king's revenue, and
    enforce the payment of the debts due to the king.
It took all other suits on contract debts.
    The plaintiff alleged that he could not pay the king because the defendant would not pay him.
Because of such fictions, the courts came to depend on the parties instead of the people depending on the court.
    Each court tried to draw to itself as many cases as it could through superior dispatch and impartiality.

The present admirable constitution of the courts of justice in England was, perhaps, originally formed by this emulation which anciently took place between their respective judges.

In his own court, each judge endeavoured to give the speediest and most effective remedy for every sort of injustice.
Originally, the courts of law gave damages only for breach of contract.
    The court of chancery was a court of conscience.
        It first enforced the specific performance of agreements.
    When the breach of contract consisted in the non-payment of money, the damage could only be compensated by ordering payment.
        The payment was equal to a specific performance of the agreement.
        In such cases, the remedy of the courts of law was sufficient.
            In other cases, it was insufficient.
When the tenant sued his landlord for outing him unjustly of his lease, the damages he recovered were not equal to the possession of the land.
    For some time, such causes went all to the court of chancery.
    It was no small loss of the courts of law.
The artificial and fictitious writ of ejectment is the most effective remedy for an unjust dispossession of land.
    It was invented by the courts of law to draw back such cases to themselves.

65 A stamp-duty on the law proceedings of each court might afford enough revenue to defray the cost of administering justice without burdening the society's revenue.

It would be levied by that court and paid to maintain its judges and officers.
    The judges might be tempted to unnecessarily multiply the proceedings on every case to increase the stamp-duty.
It was the custom in modern Europe to regulate the payment of the attorneys and clerks of court according to the number of pages they wrote.
    The court required each page to have a certain number of lines.
        Each line should have a certain number of words.
    To increase their payment, the attorneys and clerks contrived to unnecessarily multiply words.
        It corrupted the law language of every European court of justice.
A similar temptation might perhaps create a similar corruption in law proceedings.

66 It is unnecessary that the executive branch should manage the fund for justice.

That fund might come from the rent of landed estates.
    The management of each estate could be entrusted to the court to be maintained by the estate.
    That fund might even come from the interest of money.
        Its lending out could be entrusted to the court to be maintained by the interest earned.
A small part of the court judges' salary in Scotland comes from the interest of money.
    The instability of such a fund makes it an improper fund to maintain a perpetual institution.


67 The separation of the judicial from the executive power originally arose from the increasing business of society from its increasing improvement.

The administration of justice became so laborious and complicated.
    It required the undivided attention of the judges and clerks.
The executive did not have the leisure to attend to private cases.
    He appointed a deputy to decide for him.
In Rome, the consul was too much occupied with political affairs to attend to the administration of justice.
    A prætor was appointed to administer it.
In the European monarchies after the Roman empire, the sovereigns and great lords universally came to consider the administration of justice as too laborious and ignoble for themselves.
    They universally appointed a deputy, bailiff, or judge.


68 When the judicial is united to the executive power, justice is frequently sacrificed to polities.

Even without any corrupt views, the executive might think it necessary to sacrifice the rights of a private man to the interests of the state.
The liberty of every person depends on the impartial administration of justice.
To make every person feel perfectly secure in his rights, the judicial must be separated and made independent from the executive power
    The judge should not be liable to be removed according to the caprice of that executive power.
        His salary should not depend on the goodwill or even on the good economy of that power.

