+++
title=  "Superiority of Armies vs Militia"
description=  "History shows the superiority of a well-regulated standing army over a militia."
date=  2020-12-28
image=  "/covers/wn.jpg"
linkb=  "/research/smith/wealth-of-nations/book-5/chapter-1/part-1b"
linkbtext=  "Part 1b"
linkf=  "/research/smith/wealth-of-nations/book-5/chapter-1/part-2a"
linkftext=  "Part 2a"
heading=  "Part 1c"
icon=  "/icons/smith.png"
# linkbook=  https= //play.google.com/store/books/details/Juan_Dalisay_Jr_The_Simple_Wealth_of_Nations_by_Ad?id=WyYAEAAAQBAJ
# linkbooktext=  Support Superphysics by buying the ebook
+++


{{< s v="27" >}} History shows the superiority of a well-regulated standing army over a militia.

{{< s v="28" >}} The army of Philip of Macedon is one of the first standing armies documented. In the beginning, his troops were probably a militia.
- Frequent wars with the Thracians, Illyrians, Thessalians, and some Greek cities near Macedon gradually formed his troops to the exact discipline of a standing army.
- He was careful not to disband that army when he was at peace, which was very seldom.

His army vanquished and subdued= 
- the well exercised militias of ancient Greece after a long and violent struggle
- the effeminate and ill-exercised militia of the Persian empire, with very little struggle

The fall of the Greek republics and the Persian empire was due to the superiority of a standing army over a militia. It is the first great revolution in human affairs which was recorded in history.


{{< s v="29" >}} The fall of Carthage and the elevation of Rome is the second great revolution in human affairs.
- The varieties in the fortune of those two republics was caused by the superiority of the standing army.

30 From the end of the first to the beginning of the second Carthaginian war, the armies of Carthage were continually in the field.

They were employed under three great generals who succeeded one another= 
- Hamilcar,
- Hasdrubal, Hamilcar's son-in-law, and
- Hannibal, Hamilcar's son.

They chastised their own rebellious slaves. Then, they subdued the revolted nations of Africa. Lastly, they conquered Spain.

The army which Hannibal led from Spain into Italy gradually formed the discipline of a standing army. 

The Romans then were not engaged in any great war.
- Their military discipline was very relaxed.
- The Roman military Hannibal encountered at Trebia, Thrasymenus, and Cannæ were militia.
  - This probably contributed more to determine the fate of those battles.


31 The standing army which Hannibal left in Spain were superior to the Roman militia sent to oppose it.
- In a few years, under the command of his younger brother Hasdrubal, they expelled the Romans almost entirely from Spain.

32 Hannibal was ill-supplied from home.

The Roman militia was continually in the field. They became a well disciplined standing army. The superiority of Hannibal grew less everyday.

Hasdrubal judged it necessary to lead his whole army in Spain to assist Hannibal in Italy.
- He was misled by his guides.
- He was in an unfamiliar country and was surprised and attacked by another army equal or superior to his own.
- He was entirely defeated.


33 When Hasdrubal left Spain, the great Scipio was opposed only by a militia inferior to his own.

He conquered and subdued that militia.
His own militia became a well-disciplined and well-exercised standing army.
His army was afterwards carried to Africa, where it found only a militia to oppose it.
To defend Carthage, Hannibal's army had to be recalled.
The disheartened and frequently defeated African militia joined it.
They composed most of Hannibal's troops at the battle of Zama.
That battle determined the fate of the two republics.

34 From the end of the second Carthaginian war until the fall of the Roman republic, the Roman armies were standing armies.

Macedon's standing army made some resistance to them. In the height of Roman grandeur, that little kingdom's defeat cost Rome= 
- two great wars and
- three great battles.

The conquest would probably have been more difficult if not for the cowardice of its last king, Perseus.

The militias of all the ancient civilized nations made but a feeble resistance to the Roman armies.

The militias of some barbarous nations defended themselves much better.
The Scythian or Tartar militia were the most formidable enemies of the Romans after the second Carthaginian war.
Mithridates VI of Pontus drew them from countries north of the Black and Caspian seas.

The Parthian and German militias were always respectable.

On several occasions, they gained very considerable advantages over the Roman armies.
When the Roman armies were well commanded, they appeared very much superior.
The Romans did not pursue the final conquest of Parthia or Germany probably because they judged that it was not worthwhile to add those two barbarous countries to an empire already too large.

The ancient Parthians (Iran) were a nation of Scythian or Tartar extraction.

    They have always retained their ancestors' manners.

The ancient Germans were like the Scythians or Tartars.

    They were a nation of wandering shepherds.
    They went to war under the same chiefs whom they followed in peace.
    Their militia was the same kind with the Scythians or Tartars from whom they were probably descended.

35 Many causes contributed to relax the Roman armies' discipline.

    The extreme severity of such discipline was perhaps one of those causes.
    In the days of their grandeur, no enemy could oppose them.
    Their heavy armour was laid aside as unnecessarily burdensome.
    Their labourious exercises were neglected as unnecessarily toilsome.

Under the Roman emperors, the Roman armies, particularly those which guarded the German and Pannonian frontiers, became dangerous to their masters.

    Frequently, the armies used to set up their own generals against the emperors.
        They were encamped in the frontier in great bodies of two or three legions each.
    To render them less formidable, Dioclesian and Constantine withdrew them from the frontier.
        Those emperors dispersed them in small bodies through the provincial towns.
        Those armies were only removed from those towns to repel invasions.
        The soldiers became tradesmen, artificers, and manufacturers themselves.
            The civil character came to predominate over the military character.
        The Roman armies degenerated into a corrupt, neglected, and undisciplined militia.
            They were incapable of resisting the attack of German and Scythian militias which soon invaded the western empire.
        The emperors were able to defend themselves for some time by hiring foreign militias to fight other foreign militias.

The fall of the western empire is the third great revolution in human affairs recorded in ancient history.

    It was caused by the superiority of the militia of a barbarous nation over the militia of a civilized nation.
        It is the superiority of the militia of shepherd nations over the militia a nation of husbandmen, artificers, and manufacturers.
    The victories gained by militias were, not over standing armies, but over other militias inferior in exercise and discipline.
        Such were the victories of= 
            the Greek militia over the Persian militia, and
            the Swiss militia over the Austrian and Burgundian militia of later times.

36 The German and Scythian nations established themselves on the ruins of the western empire.

    For some time, their military force continued to be a militia of shepherds and husbandmen.
    In wartime, it took the field under the command of the same chieftains whom it obeyed in peacetime.
        It was tolerably well-exercised and well-disciplined.
    As arts and industry advanced, the authority of the chieftains gradually decayed.
        The people had less time to spare for military exercises.
        The discipline and the exercise of the feudal militia went gradually to ruin.
        Standing armies were gradually introduced to replace it.

When a standing army has been adopted by a civilized nation, it became necessary for its neighbours to follow their example.

    They soon found that their own militia was incapable of resisting the attack of such an army.

37 The soldiers of a standing army can possess the courage of veteran troops even if they may never have seen an enemy.

    Once they took the field they can be fit to face the hardiest and most experienced veterans.

In 1756, when the Russian army marched into Poland, the valour of the Russian soldiers was not inferior to the valour of the Prussians.

    At that time, the Prussians were the hardiest and most experienced veterans in Europe.
    The Russian empire enjoyed a profound peace 20 years before.
        They had very few soldiers at that time who had ever seen an enemy.

When the Spanish war broke out in 1739, England enjoyed a profound peace for 28 years.

    The valour of British soldiers was not corrupted by that long peace.
    The attempt on Carthagena was the first unfortunate exploit of that war.
    The valour of British soldiers were most distingushed in that exploit.

In a long peace, perhaps the generals may sometimes forget their skills.

    But where a well-regulated standing army has been kept up, the soldiers seem never to forget their valour.

38 A civilized nation is always exposed to be conquered by any nearby barbarous nation when it depends on a militia for its defence.

    The frequent Mongol conquests of the civilized Asian countries demonstrates the natural superiority of a barbarous nation's militia over a civilized nation's militia.
    A well-regulated standing army is superior to every militia.
        Such an army can best be maintained by an opulent and civilized nation.
    Only a well-regulated standing army can= 
        defend such a nation against the invasion of a poor and barbarous neighbour, and
        perpetuate or preserve its civilization.

39 Only through a well-regulated standing army can= 

    a civilized country be defended, and
    a barbarous country can be civilized.

A standing army establishes the sovereign's law through the remotest provinces with an irresistible force.

    It maintains some regular government in countries which otherwise could not have any.
    Peter the Great's improvements in the Russian empire were almost all for the establishment of a well-regulated standing army.
    It is the instrument which executes and maintains all his other regulations.
    Russia's peace and order since then is due to that army.

40 Men of republican principles have been jealous of a standing army as dangerous to liberty.

    An army is certainly dangerous wherever the interest of the army general and principal officers are not connected with the constitution.
        Cæsar's standing army destroyed the Roman republic.
        Cromwell's standing army turned the long parliament out of doors.
    A standing army can never be dangerous to liberty when= 
        the sovereign himself is the general, and
        the principal nobility and gentry are the chief officers.
    The military is commanded by those who have the greatest interest in supporting the civil authority because they themselves have the greatest share of that authority.
        In some cases, a standing army may be favourable to liberty.
        The security which it gives to the sovereign renders that troublesome jealousy unnecessary.

The jealousy in some modern republics watches over the minutest actions.

    At all times, it is ready to disturb the peace of every citizen.
    The government's whole authority must suppress and punish every murmur and complaint against it when= 
        the security of the magistrate is endangered by every popular discontent, and
        a small tumult can bring about a great revolution in a few hours.
    The rudest, most groundless, and most licentious remonstrances cannot much disturb a sovereign who feels supported by= 
        the natural aristocracy of the country, and
        a well-regulated standing army.
    His consciousness of his own superiority naturally disposes him to safely pardon or neglect them.
        Licentious liberty can only be tolerated only in countries where the sovereign is secured by a well-regulated standing army.
    Only in such countries does public safety not require the sovereign to have any discretionary power for suppressing the impertinent wantonness of this licentious liberty.

41 The sovereign's first duty of defending society gradually grows more expensive as society advances in civilization.

    Its military force originally did not cost him anything in peacetime or wartime.
    In the progress of improvement, he first maintains it in wartime, then afterwards, in peacetime.

42 The great change introduced into the art of war by firearms further enhanced the cost of= 

    exercising and disciplining soldiers in peacetime, and
    employing them in wartime.

Their arms and ammunition become more expensive.

    A musket is a more expensive machine than a javelin or a bow and arrows.
        A cannon or a mortar is more expensive than a balista or a catapult.
    The powder spent in a modern battle is lost irrecoverably.
        It creates a very considerable expence.
    The javelins and arrows thrown or shot in an ancient battle= 
        had very little value, and
        could be easily picked up again.
    The cannon and the mortar are much dearer and heavier than the balista or catapult.
        They are more costly to prepare and carry them to the field.

The superiority of the modern artillery over ancient artillery is very great.

    It became much more difficult and expensive to fortify a town against modern artillery.
    In modern times, society's defence is more expensive because of many different causes.
    The accidental invention of gunpowder caused a great revolution in the art of war.
        It greatly enhanced the effects of the natural progress of improvement of societies.

43 In modern war, the great cost of firearms gives an advantage to the opulent and civilized nation over a poor and barbarous nation.

    In ancient times, the opulent and civilized nations found it difficult to defend themselves against poor and barbarous nations.
    In modern times, the poor and barbarous find it difficult to defend themselves against the opulent and civilized nations.
    The invention of firearms is so harmful at first sight.
        But it is certainly favourable to the permanency and extension of civilization.

