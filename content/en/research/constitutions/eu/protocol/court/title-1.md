<!-- THE HIGH CONTRACTING PARTIES
DESIRING to lay down the Statute of the Court of Justice of the European Union provided for in Article III-381 of the
Constitution,
HAVE AGREED upon the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe and the Treaty establishing the European Atomic Energy Community: -->

Article 1

The Court of Justice of the European Union shall be constituted and shall function in accordance with
the Constitution, with the Treaty establishing the European Atomic Energy Community
(EAEC Treaty) and with this Statute.

TITLE I

JUDGES AND ADVOCATES-GENERAL
Article 2
Before taking up his duties each Judge shall, before the Court of Justice sitting in open court, take an
oath to perform his duties impartially and conscientiously and to preserve the secrecy of the
deliberations of the Court.
Article 3
The Judges shall be immune from legal proceedings. After they have ceased to hold office, they shall
continue to enjoy immunity in respect of acts performed by them in their official capacity, including
words spoken or written.
The Court of Justice, sitting as a full Court, may waive the immunity. If the decision concerns a
member of the General Court or of a specialised court, the Court shall decide after consulting the
court concerned.
Where immunity has been waived and criminal proceedings are instituted against a Judge, he shall be
tried, in any of the Member States, only by the court competent to judge the members of the highest
national judiciary.
Articles 11 to 14 and Article 17 of the Protocol on the privileges and immunities of the Union shall
apply to the Judges, Advocates-General, Registrars and Assistant Rapporteurs of the Court of Justice
of the European Union, without prejudice to the provisions relating to immunity from legal
proceedings of Judges which are set out in the first, second and third paragraphs of this Article.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 218
218
Part IV
Article 4
The Judges may not hold any political or administrative office.
They may not engage in any occupation, whether gainful or not, unless exemption is exceptionally
granted by a European decision of the Council, acting by a simple majority.
When taking up their duties, they shall give a solemn undertaking that, both during and after their
term of office, they will respect the obligations arising therefrom, in particular the duty to behave
with integrity and discretion as regards the acceptance, after they have ceased to hold office, of
certain appointments or benefits.
Any doubt on this point shall be settled by decision of the Court of Justice. If the decision concerns a
member of the General Court or of a specialised court, the Court shall decide after consulting the
court concerned.
Article 5
Apart from normal replacement, or death, the duties of a Judge shall end when he resigns.
Where a Judge resigns, his letter of resignation shall be addressed to the President of the Court of
Justice for transmission to the President of the Council. Upon this notification a vacancy shall arise
on the bench.
Save where Article 6 applies, a Judge shall continue to hold office until his successor takes up his
duties.
Article 6
A Judge may be deprived of his office or of his right to a pension or other benefits in its stead only if,
in the unanimous opinion of the Judges and Advocates-General of the Court of Justice, he no longer
fulfils the requisite conditions or meets the obligations arising from his office. The Judge concerned
shall not take part in any such deliberations. If the person concerned is a member of the General
Court or of a specialised court, the Court shall decide after consulting the court concerned.
The Registrar of the Court of Justice shall communicate the decision of the Court of Justice to the
President of the European Parliament and to the President of the Commission and shall notify it to
the President of the Council.
In the case of a decision depriving a Judge of his office, a vacancy shall arise on the bench upon this
latter notification.
Article 7
A Judge who is to replace a member of the Court of Justice whose term of office has not expired shall
be appointed for the remainder of his predecessor's term.
Article 8
The provisions of Articles 2 to 7 shall apply to the Advocates-General.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 219
Treaty establishing a Constitution for Europe
219
TITLE II
ORGANISATION OF THE COURT OF JUSTICE
Article 9
When, every three years, the Judges are partially replaced, thirteen and twelve Judges shall be replaced
alternately.
When, every three years, the Advocates-General are partially replaced, four Advocates-General shall
be replaced on each occasion.
Article 10
The Registrar shall take an oath before the Court of Justice to perform his duties impartially and
conscientiously and to preserve the secrecy of the deliberations of the Court of Justice.
Article 11
The Court of Justice shall arrange for replacement of the Registrar on occasions when he is prevented
from attending the Court of Justice.
Article 12
Officials and other servants shall be attached to the Court of Justice to enable it to function. They
shall be responsible to the Registrar under the authority of the President.
Article 13
A European law may provide for the appointment of Assistant Rapporteurs and lay down the rules
governing their service. It shall be adopted at the request of the Court of Justice. The Assistant
Rapporteurs may be required, under conditions laid down in the Rules of Procedure, to participate in
preparatory inquiries in cases pending before the Court of Justice and to cooperate with the Judge
who acts as Rapporteur.
The Assistant Rapporteurs shall be chosen from persons whose independence is beyond doubt and
who possess the necessary legal qualifications; they shall be appointed by a European decision of the
Council, acting by a simple majority. They shall take an oath before the Court of Justice to perform
their duties impartially and conscientiously and to preserve the secrecy of the deliberations of the
Court of Justice.
Article 14
The Judges, the Advocates-General and the Registrar shall be required to reside at the place where the
Court of Justice has its seat.
Article 15
The Court of Justice shall remain permanently in session. The duration of the judicial vacations shall
be determined by the Court of Justice with due regard to the needs of its business.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 220
220
Part IV
Article 16
The Court of Justice shall form chambers consisting of three and five Judges. The Judges shall elect
the Presidents of the chambers from among their number. The Presidents of the chambers of
five Judges shall be elected for three years. They may be re-elected once.
The Grand Chamber shall consist of thirteen Judges. It shall be presided over by the President of the
Court of Justice. The Presidents of the chambers of five Judges and other Judges appointed in
accordance with the conditions laid down in the Rules of Procedure shall also form part of the Grand
Chamber.
The Court of Justice shall sit in a Grand Chamber when a Member State or an institution of the
Union that is party to the proceedings so requests.
The Court of Justice shall sit as a full Court where cases are brought before it pursuant to Article
III-335(2), the second paragraph of Article III-347, Article III-349 or Article III-385(6) of the
Constitution.
Moreover, where it considers that a case before it is of exceptional importance, the Court of Justice
may decide, after hearing the Advocate-General, to refer the case to the full Court.
Article 17
Decisions of the Court of Justice shall be valid only when an uneven number of its members is sitting
in the deliberations.
Decisions of the chambers consisting of either three or five Judges shall be valid only if they are taken
by three Judges.
Decisions of the Grand Chamber shall be valid only if nine Judges are sitting.
Decisions of the full Court shall be valid only if fifteen Judges are sitting.
In the event of one of the Judges of a chamber being prevented from attending, a Judge of another
chamber may be called upon to sit in accordance with conditions laid down in the Rules of
Procedure.
Article 18
No Judge or Advocate-General may take part in the disposal of any case in which he has previously
taken part as agent or adviser or has acted for one of the parties, or in which he has been called upon
to pronounce as a member of a court or tribunal, of a commission of inquiry or in any other
capacity.
If, for some special reason, any Judge or Advocate-General considers that he should not take part in
the judgment or examination of a particular case, he shall so inform the President. If, for some special
reason, the President considers that any Judge or Advocate-General should not sit or make
submissions in a particular case, he shall notify him accordingly.
Any difficulty arising as to the application of this Article shall be settled by decision of the Court of
Justice.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 221
Treaty establishing a Constitution for Europe
221
A party may not apply for a change in the composition of the Court of Justice or of one of its
chambers on the grounds of either the nationality of a Judge or the absence from the Court or from
the chamber of a Judge of the nationality of that party.
TITLE III
PROCEDURE BEFORE THE COURT OF JUSTICE
Article 19
The Member States and the institutions of the Union shall be represented before the Court of Justice
by an agent appointed for each case. The agent may be assisted by an adviser or by a lawyer.
The States, other than the Member States, which are parties to the Agreement on the European
Economic Area and also the European Free Trade Association (EFTA) Surveillance Authority referred
to in that Agreement shall be represented in same manner.
Other parties must be represented by a lawyer.
Only a lawyer authorised to practise before a court of a Member State or of another State which is a
party to the Agreement on the European Economic Area may represent or assist a party before the
Court of Justice.
Such agents, advisers and lawyers shall, when they appear before the Court of Justice, enjoy the rights
and immunities necessary to the independent exercise of their duties, under conditions laid down in
the Rules of Procedure.
As regards such advisers and lawyers who appear before it, the Court of Justice shall have the powers
normally accorded to courts of law, under conditions laid down in the Rules of Procedure.
University teachers being nationals of a Member State whose law accords them a right of audience
shall have the same rights before the Court of Justice as are accorded by this Article to lawyers.
Article 20
The procedure before the Court of Justice shall consist of two parts: written and oral.
The written procedure shall consist of the communication to the parties and to the institutions,
bodies, offices or agencies of the Union whose acts are in dispute, of applications, statements of case,
defences and observations, and of replies, if any, as well as of all papers and documents in support or
of certified copies of them.
Communications shall be made by the Registrar in the order and within the time laid down in the
Rules of Procedure.
The oral procedure shall consist of the reading of the report presented by a Judge acting as
Rapporteur, the hearing by the Court of Justice of agents, advisers and lawyers and of the submissions
of the Advocate General, as well as the hearing, if any, of witnesses and experts.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 222
222
Part IV
Where it considers that the case raises no new point of law, the Court of Justice may decide, after
hearing the Advocate-General, that the case shall be determined without a submission from the
Advocate General.
Article 21
A case shall be brought before the Court of Justice by a written application addressed to the Registrar.
The application shall contain the applicant's name and permanent address and the description of the
signatory, the name of the party or names of the parties against whom the application is made, the
subject matter of the dispute, the form of order sought and a brief statement of the pleas in law on
which the application is based.
The application shall be accompanied, where appropriate, by the measure the annulment of which is
sought or, in the circumstances referred to in Article III-367 of the Constitution, by documentary
evidence of the date on which an institution was, in accordance with that Article, requested to act. If
the documents are not submitted with the application, the Registrar shall ask the party concerned to
produce them within a reasonable period, but in that event the rights of the party shall not lapse even
if such documents are produced after the time limit for bringing proceedings.
Article 22
A case governed by Article 18 of the EAEC Treaty shall be brought before the Court of Justice by an
appeal addressed to the Registrar. The appeal shall contain the name and permanent address of the
applicant and the description of the signatory, a reference to the decision against which the appeal is
brought, the names of the respondents, the subject matter of the dispute, the submissions and a brief
statement of the grounds on which the appeal is based.
The appeal shall be accompanied by a certified copy of the decision of the Arbitration Committee
which is contested.
If the Court rejects the appeal, the decision of the Arbitration Committee shall become final.
If the Court annuls the decision of the Arbitration Committee, the matter may be reopened, where
appropriate, on the initiative of one of the parties in the case, before the Arbitration Committee. The
latter shall conform to any decisions on points of law given by the Court.
Article 23
In the cases governed by Article III-369 of the Constitution, the decision of the court or tribunal of a
Member State which suspends its proceedings and refers a case to the Court of Justice shall be
notified to the Court of Justice by the court or tribunal concerned. The decision shall then be notified
by the Registrar of the Court of Justice to the parties, to the Member States and to the Commission,
and to the institution, body, office or agency which adopted the act the validity or interpretation of
which is in dispute.
Within two months of this notification, the parties, the Member States, the Commission and, where
appropriate, the institution, body, office or agency which adopted the act the validity or
interpretation of which is in dispute, shall be entitled to submit statements of case or written
observations to the Court of Justice.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 223
Treaty establishing a Constitution for Europe
223
The decision of the national court or tribunal shall, moreover, be notified by the Registrar of the
Court of Justice to the States, other than the Member States, which are parties to the Agreement on
the European Economic Area and also to the EFTA Surveillance Authority referred to in that
Agreement which may, within two months of notification, where one of the fields of application of
that Agreement is concerned, submit statements of case or written observations to the Court of
Justice. This paragraph shall not apply to questions falling within the scope of the EAEC Treaty.
Where an agreement relating to a specific subject matter, concluded by the Council and one or more
non-member countries, provides that those countries are to be entitled to submit statements of case
or written observations where a court or tribunal of a Member State refers to the Court of Justice for
a preliminary ruling on a question falling within the scope of the agreement, the decision of the
national court or tribunal containing that question shall also be notified to the non-member
countries concerned. Within two months from such notification, those countries may lodge at the
Court of Justice statements of case or written observations.
Article 24
The Court of Justice may require the parties to produce all documents and to supply all information
which the Court of Justice considers desirable. Formal note shall be taken of any refusal.
The Court of Justice may also require the Member States and institutions, bodies, offices and agencies
of the Union not being parties to the case to supply all information which the Court of Justice
considers necessary for the proceedings.
Article 25
The Court of Justice may at any time entrust any individual, body, authority, committee or other
organisation it chooses with the task of giving an expert opinion.
Article 26
Witnesses may be heard under conditions laid down in the Rules of Procedure.
Article 27
With respect to defaulting witnesses the Court of Justice shall have the powers generally granted to
courts and tribunals and may impose pecuniary penalties under conditions laid down in the Rules of
Procedure.
Article 28
Witnesses and experts may be heard on oath taken in the form laid down in the Rules of Procedure
or in the manner laid down by the law of the country of the witness or expert.
Article 29
The Court of Justice may order that a witness or expert be heard by the judicial authority of his place
of permanent residence.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 224
224
Part IV
The order shall be sent for implementation to the competent judicial authority under conditions laid
down in the Rules of Procedure. The documents drawn up in compliance with the letters rogatory
shall be returned to the Court of Justice under the same conditions.
The Court of Justice shall defray the expenses, without prejudice to the right to charge them, where
appropriate, to the parties.
Article 30
A Member State shall treat any violation of an oath by a witness or expert in the same manner as if
the offence had been committed before one of its courts with jurisdiction in civil proceedings. At the
instance of the Court of Justice, the Member State concerned shall prosecute the offender before its
competent court.
Article 31
The hearing in court shall be public, unless the Court of Justice, of its own motion or on application
by the parties, decides otherwise for serious reasons.
Article 32
During the hearings the Court of Justice may examine the experts, the witnesses and the parties
themselves. The latter, however, may address the Court of Justice only through their representatives.
Article 33
Minutes shall be made of each hearing and signed by the President and the Registrar.
Article 34
The case list shall be established by the President.
Article 35
The deliberations of the Court of Justice shall be and shall remain secret.
Article 36
Judgments shall state the reasons on which they are based. They shall contain the names of the Judges
who took part in the deliberations.
Article 37
Judgments shall be signed by the President and the Registrar. They shall be read in open court.
Article 38
The Court of Justice shall adjudicate upon costs.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 225
Treaty establishing a Constitution for Europe
225
Article 39
The President of the Court of Justice may, by way of summary procedure, which may, insofar as
necessary, differ from some of the rules contained in this Statute and which shall be laid down in the
Rules of Procedure, adjudicate upon applications to suspend execution, as provided for in Article III-
379(1) of the Constitution and Article 157 of the EAEC Treaty, or to prescribe interim measures in
pursuance of Article III-379(2) of the Constitution, or to suspend enforcement in accordance with
the fourth paragraph of Article III-401 of the Constitution or the third paragraph of Article 164 of
the EAEC Treaty.
Should the President be prevented from attending, his place shall be taken by another Judge under
conditions laid down in the Rules of Procedure.
The ruling of the President or of the Judge replacing him shall be provisional and shall in no way
prejudice the decision of the Court of Justice on the substance of the case.
Article 40
Member States and institutions of the Union may intervene in cases before the Court of Justice.
The same right shall be open to the bodies, offices and agencies of the Union and to any other person
which can establish an interest in the result of a case submitted to the Court of Justice. Natural or
legal persons shall not intervene in cases between Member States, between institutions of the Union
or between Member States and institutions of the Union.
Without prejudice to the second paragraph, the States, other than the Member States, which are
parties to the Agreement on the European Economic Area, and also the EFTA Surveillance Authority
referred to in that Agreement, may intervene in cases before the Court of Justice where one of the
fields of application of that Agreement is concerned.
An application to intervene shall be limited to supporting the form of order sought by one of the
parties.
Article 41
Where the defending party, after having been duly summoned, fails to file written submissions in
defence, judgment shall be given against that party by default. An objection may be lodged against
the judgment within one month of it being notified. The objection shall not have the effect of staying
enforcement of the judgment by default unless the Court of Justice decides otherwise.
Article 42
Member States, institutions, bodies, offices and agencies of the Union and any other natural or legal
persons may, in cases and under conditions laid down in the Rules of Procedure, institute third-party
proceedings to contest a judgment rendered without their being heard, where the judgment is
prejudicial to their rights.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 226
226
Part IV
Article 43
If the meaning or scope of a judgment is in doubt, the Court of Justice shall construe it on application
by any party or any institution of the Union establishing an interest therein.
Article 44
An application for revision of a judgment may be made to the Court of Justice only on discovery of a
fact which is of such a nature as to be a decisive factor, and which, when the judgment was given, was
unknown to the Court and to the party claiming the revision.
The revision shall be opened by a judgment of the Court expressly recording the existence of a new
fact, recognising that it is of such a character as to lay the case open to revision and declaring the
application admissible on this ground.
No application for revision may be made after the lapse of ten years from the date of the judgment.
Article 45
Periods of grace based on considerations of distance shall be laid down in the Rules of Procedure.
No right shall be prejudiced in consequence of the expiry of a time-limit if the party concerned
proves the existence of unforeseeable circumstances or of force majeure.
Article 46
Proceedings against the Union in matters arising from non-contractual liability shall be barred after a
period of five years from the occurrence of the event giving rise thereto. The period of limitation
shall be interrupted if proceedings are instituted before the Court of Justice or if prior to such
proceedings an application is made by the aggrieved party to the relevant institution of the Union. In
the latter event the proceedings must be instituted within the period of two months provided for in
Article III-365 of the Constitution. The second paragraph of Article III-367 of the Constitution shall
apply.
This Article shall also apply to proceedings against the European Central Bank regarding non-
contractual liability.
TITLE IV
THE GENERAL COURT
Article 47
The first paragraph of Article 9, Articles 14 and 15, the first, second, fourth and fifth paragraphs of
Article 17 and Article 18 shall apply to the General Court and its members.
Articles 10, 11 and 14 shall apply to the Registrar of the General Court mutatis mutandis.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 227
Treaty establishing a Constitution for Europe
227
Article 48
The General Court shall consist of twenty-five Judges.
Article 49
The members of the General Court may be called upon to perform the task of an Advocate-General.
It shall be the duty of the Advocate-General, acting with complete impartiality and independence, to
make, in open court, reasoned submissions on certain cases brought before the General Court in
order to assist that Court in the performance of its task.
The criteria for selecting such cases, as well as the procedures for designating the Advocates-General,
shall be laid down in the Rules of Procedure of the General Court.
A member called upon to perform the task of Advocate-General in a case may not take part in the
judgment of the case.
Article 50
The General Court shall sit in chambers of three or five Judges. The Judges shall elect the Presidents of
the chambers from among their number. The Presidents of the chambers of five Judges shall be
elected for three years. They may be re-elected once.
The composition of the chambers and the assignment of cases to them shall be governed by the Rules
of Procedure. In certain cases governed by the Rules of Procedure, the General Court may sit as a full
court or be constituted by a single Judge.
The Rules of Procedure may also provide that the General Court may sit in a Grand Chamber in cases
and under the conditions specified therein.
Article 51
By way of derogation from the rule laid down in Article III-358(1) of the Constitution, jurisdiction
shall be reserved to the Court of Justice in the actions referred to in Articles III-365 and III-367 of the
Constitution when they are brought by a Member State against:
(a) an act of or failure to act by the European Parliament or the Council, or both those institutions
acting jointly, except for:
— European decisions adopted by the Council under the third subparagraph of Article III-168(2)
of the Constitution;
— acts of the Council adopted pursuant to a Council act concerning measures to protect trade
within the meaning of Article III-315 of the Constitution;
— acts of the Council by which the Council exercises implementing powers in accordance with
Article I-37(2) of the Constitution;954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 228
228
Part IV
(b) against an act of or failure to act by the Commission under Article III-420(1) of the Constitution.
Jurisdiction shall also be reserved to the Court of Justice in the actions referred to in the same Articles
when they are brought by an institution of the Union against an act of or failure to act by the
European Parliament or the Council or both of those institutions acting jointly or the Commission,
or brought by an institution against an act of or failure to act by the European Central Bank.
Article 52
The President of the Court of Justice and the President of the General Court shall determine, by
common accord, the conditions under which officials and other servants attached to the Court of
Justice shall render their services to the General Court to enable it to function. Certain officials or
other servants shall be responsible to the Registrar of the General Court under the authority of the
President of the General Court.
Article 53
The procedure before the General Court shall be governed by Title III.
Such further and more detailed provisions as may be necessary shall be laid down in its Rules of
Procedure. The Rules of Procedure may derogate from the fourth paragraph of Article 40 and from
Article 41 in order to take account of the specific features of litigation in the field of intellectual
property.
Notwithstanding the fourth paragraph of Article 20, the Advocate-General may make his reasoned
submissions in writing.
Article 54
Where an application or other procedural document addressed to the General Court is lodged by
mistake with the Registrar of the Court of Justice, it shall be transmitted immediately by that Registrar
to the Registrar of the General Court. Likewise, where an application or other procedural document
addressed to the Court of Justice is lodged by mistake with the Registrar of the General Court, it shall
be transmitted immediately by that Registrar to the Registrar of the Court of Justice.
Where the General Court finds that it does not have jurisdiction to hear and determine an action in
respect of which the Court of Justice has jurisdiction, it shall refer that action to the Court of Justice.
Likewise, where the Court of Justice finds that an action falls within the jurisdiction of the General
Court, it shall refer that action to the General Court, whereupon that Court may not decline
jurisdiction.
Where the Court of Justice and the General Court are seised of cases in which the same relief is
sought, the same issue of interpretation is raised or the validity of the same act is called in question,
the General Court may, after hearing the parties, stay the proceedings before it until such time as the
Court of Justice has delivered judgment, or, where the action is one brought pursuant to Article III-
365 of the Constitution or pursuant to Article 146 of the EAEC Treaty, may decline jurisdiction so as
to allow the Court of Justice to rule on such actions. In the same circumstances, the Court of Justice
may also decide to stay the proceedings before it. In that event, the proceedings before the General
Court shall continue.
Where a Member State and an institution are challenging the same act, the General Court shall
decline jurisdiction so that the Court of Justice may rule on those applications.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 229
Treaty establishing a Constitution for Europe
229
Article 55
Final decisions of the General Court, decisions disposing of the substantive issues in part only or
disposing of a procedural issue concerning a plea of lack of competence or inadmissibility, shall be
notified by the Registrar of the General Court to all parties as well as all Member States and the
institutions of the Union even if they did not intervene in the case before the General Court.
Article 56
An appeal may be brought before the Court of Justice, within two months of the notification of the
decision appealed against, against final decisions of the General Court and decisions of that Court
disposing of the substantive issues in part only or disposing of a procedural issue concerning a plea
of lack of competence or inadmissibility.
Such an appeal may be brought by any party which has been unsuccessful, in whole or in part, in its
submissions. However, interveners other than the Member States and the institutions of the Union
may bring such an appeal only where the decision of the General Court directly affects them.
With the exception of cases relating to disputes between the Union and its servants, an appeal may
also be brought by Member States and institutions of the Union which did not intervene in the
proceedings before the General Court. Such Member States and institutions shall be in the same
position as Member States or institutions which intervened at first instance.
Article 57
Any person whose application to intervene has been dismissed by the General Court may appeal to
the Court of Justice within two weeks from the notification of the decision dismissing the
application.
The parties to the proceedings may appeal to the Court of Justice against any decision of the General
Court made pursuant to Article III-379(1) or (2) or the fourth paragraph of Article III-401 of the
Constitution, or Article 157 or the third paragraph of Article 164 of the EAEC Treaty, within two
months from their notification.
The appeal referred to in the first two paragraphs shall be heard and determined under the procedure
referred to in Article 39.
Article 58
An appeal to the Court of Justice shall be limited to points of law. It shall lie on the grounds of lack of
competence of the General Court, a breach of procedure before it which adversely affects the interests
of the appellant as well as the infringement of Union law by the General Court.
No appeal shall lie regarding only the amount of the costs or the party ordered to pay them.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 230
230
Part IV
Article 59
Where an appeal is brought against a decision of the General Court, the procedure before the Court
of Justice shall consist of a written part and an oral part. In accordance with conditions laid down in
the Rules of Procedure, the Court of Justice, having heard the Advocate-General and the parties, may
dispense with the oral procedure.
Article 60
Without prejudice to Article III-379(1) and (2) of the Constitution or Article 157 of the EAEC Treaty,
an appeal shall not have suspensory effect.
By way of derogation from Article III-380 of the Constitution, decisions of the General Court
declaring a European law or European regulation binding in its entirety and directly applicable in all
Member States to be void shall take effect only as from the date of expiry of the period referred to in
the first paragraph of Article 56 of this Statute or, if an appeal shall have been brought within that
period, as from the date of dismissal of the appeal, without prejudice, however, to the right of a party
to apply to the Court of Justice, pursuant to Article III-379(1) and (2) of the Constitution, and
Article 157 of the EAEC Treaty for the suspension of the effects of the European law or European
regulation which has been declared void or for the prescription of any other interim measure.
Article 61
If the appeal is well founded, the Court of Justice shall quash the decision of the General Court. It may
itself give final judgment in the matter, where the state of the proceedings so permits, or refer the
case back to the General Court for judgment.
Where a case is referred back to the General Court, that Court shall be bound by the decision of the
Court of Justice on points of law.
When an appeal brought by a Member State or an institution of the Union, which did not intervene
in the proceedings before the General Court, is well founded, the Court of Justice may, if it considers
this necessary, state which of the effects of the decision of the General Court which has been quashed
shall be considered as definitive in respect of the parties to the litigation.
Article 62
In the cases provided for in Article III-358(2) and (3) of the Constitution, where the First Advocate-
General considers that there is a serious risk of the unity or consistency of Union law being affected,
he may propose that the Court of Justice review the decision of the General Court.
The proposal must be made within one month of delivery of the decision by the General Court.
Within one month of receiving the proposal made by the First Advocate-General, the Court of Justice
shall decide whether or not the decision should be reviewed.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 231
Treaty establishing a Constitution for Europe
231
TITLE V
FINAL PROVISIONS
Article 63
The Rules of Procedure of the Court of Justice and of the General Court shall contain any provisions
necessary for applying and, where required, supplementing this Statute.
Article 64
The rules governing the language arrangements applicable at the Court of Justice of the European
Union shall be laid down by a European regulation of the Council acting unanimously. This
regulation shall be adopted either at the request of the Court of Justice and after consultation of the
Commission and the European Parliament, or on a proposal from the Commission and after
consultation of the Court of Justice and of the European Parliament.
Until those rules have been adopted, the provisions of the Rules of Procedure of the Court of Justice
and of the Rules of Procedure of the General Court governing language arrangements shall apply. By
way of derogation from Articles III-355 and III-356 of the Constitution, those provisions may only
be amended or repealed with the unanimous consent of the Council.
Article 65
1. By way of derogation from Article IV-437 of the Constitution, any amendment to the Protocol
on the Statute of the Court of Justice, annexed to the Treaty on European Union, to the Treaty
establishing the European Community and to the EAEC Treaty, which is adopted between the signing
and the entry into force of the Treaty establishing a Constitution for Europe, shall remain in force.
2. In order to incorporate them into the enacting terms of this Statute, the amendments referred to
in paragraph 1 shall be subject to official codification by a European law of the Council, adopted at
the request of the Court of Justice. When such codifying European law enters into force, this Article
shall be repealed.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 232
232
Part IV
4.
PROTOCOL ON THE STATUTE OF THE EUROPEAN SYSTEM
OF CENTRAL BANKS AND OF THE EUROPEAN CENTRAL BANK
THE HIGH CONTRACTING PARTIES,
DESIRING to lay down the Statute of the European System of Central Banks and of the European Central Bank provided
for in Articles I‑30 and III‑187(2) of the Constitution,
HAVE AGREED upon the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe:
CHAPTER I
THE EUROPEAN SYSTEM OF CENTRAL BANKS
Article 1
The European System of Central Banks
1. In accordance with Article I-30(1) of the Constitution, the European Central Bank and the
national central banks shall constitute the European System of Central Banks. The European Central
Bank and the national central banks of those Member States whose currency is the euro shall
constitute the Eurosystem.
2. The European System of Central Banks and the European Central Bank shall perform their tasks
and carry on their activities in accordance with the Constitution and this Statute.
CHAPTER II
OBJECTIVES AND TASKS OF THE EUROPEAN SYSTEM OF CENTRAL BANKS
Article 2
Objectives
In accordance with Articles I-30(2) and III-185(1) of the Constitution, the primary objective of the
European System of Central Banks shall be to maintain price stability. Without prejudice to that
objective, it shall support the general economic policies in the Union in order to contribute to the
achievement of the latter's objectives as laid down in Article I‑3 of the Constitution. The European
System of Central Banks shall act in accordance with the principle of an open market economy with
free competition, favouring an efficient allocation of resources, and in compliance with the principles
set out in Article III-177 of the Constitution.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 233
Treaty establishing a Constitution for Europe
233
Article 3
Tasks
1. In accordance with Article III-185(2) of the Constitution, the basic tasks to be carried out
through the European System of Central Banks shall be:
(a) to define and implement the Union's monetary policy;
(b) to conduct foreign-exchange operations consistent with Article III-326 of the Constitution;
(c) to hold and manage the official foreign reserves of the Member States;
(d) to promote the smooth operation of payment systems.
2. In accordance with Article III-185(3) of the Constitution, paragraph 1(c) of this Article shall be
without prejudice to the holding and management by the governments of Member States of
foreign‑exchange working balances.
3. In accordance with Article III-185(5) of the Constitution, the European System of Central Banks
shall contribute to the smooth conduct of policies pursued by the competent authorities relating to
the prudential supervision of credit institutions and the stability of the financial system.
Article 4
Advisory functions
In accordance with Article III-185(4) of the Constitution, the European Central Bank shall be
consulted:
(a) on any proposed Union act in areas within its powers;
(b) by national authorities regarding any draft legislative provision in its fields of competence, but
within the limits and under the conditions set out by the Council in accordance with the
procedure laid down in Article 41.
The European Central Bank may submit opinions to the Union institutions, bodies, offices or
agencies or to national authorities on matters within its powers.
Article 5
Collection of statistical information
1. In order to undertake the tasks of the European System of Central Banks, the European Central
Bank, assisted by the national central banks, shall collect the necessary statistical information either
from the competent national authorities or directly from economic agents. For these purposes it shall954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 234
234
Part IV
cooperate with Union institutions, bodies, offices or agencies and with the competent authorities of
the Member States or third countries and with international organisations.
2. The national central banks shall carry out, to the extent possible, the tasks referred to in
paragraph 1.
3. The European Central Bank shall contribute to the harmonisation, where necessary, of the rules
and practices governing the collection, compilation and distribution of statistics in the areas within
its powers.
4. The Council, in accordance with the procedure laid down in Article 41, shall define the natural
and legal persons subject to reporting requirements, the confidentiality regime and the appropriate
provisions for enforcement.
Article 6
International cooperation
1. In the field of international cooperation involving the tasks entrusted to the European System of
Central Banks, the European Central Bank shall decide how the European System of Central Banks
shall be represented.
2. The European Central Bank and, subject to its approval, the national central banks may
participate in international monetary institutions.
3.
Paragraphs 1 and 2 shall be without prejudice to Article III-196 of the Constitution.
CHAPTER III
ORGANISATION OF THE EUROPEAN SYSTEM OF CENTRAL BANKS
Article 7
Independence
In accordance with Article III-188 of the Constitution, when exercising the powers and carrying out
the tasks and duties conferred upon them by the Constitution and this Statute, neither the European
Central Bank, nor a national central bank, nor any member of their decision‑making bodies shall seek
or take instructions from Union institutions, bodies, offices or agencies, from any government of a
Member State or from any other body. The Union institutions, bodies, offices and agencies and the
governments of the Member States undertake to respect this principle and not to seek to influence
the members of the decision-making bodies of the European Central Bank or of the national central
banks in the performance of their tasks.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 235
Treaty establishing a Constitution for Europe
235
Article 8
General principle
The European System of Central Banks shall be governed by the decision‑making bodies of the
European Central Bank.
Article 9
The European Central Bank
1. The European Central Bank, which, in accordance with Article I-30(3) of the Constitution, has
legal personality, shall enjoy in each of the Member States the most extensive legal capacity accorded
to legal persons under its law. It may, in particular, acquire or dispose of movable and immovable
property and may be a party to legal proceedings.
2. The European Central Bank shall ensure that the tasks conferred upon the European System of
Central Banks under Article III-185(2), (3) and (5) of the Constitution are implemented either by itself
pursuant to this Statute or through the national central banks pursuant to Article 12(1) and
Article 14.
3. In accordance with Article III-187(1) of the Constitution, the decision-making bodies of the
European Central Bank shall be the Governing Council and the Executive Board.
Article 10
The Governing Council
1. In accordance with Article III-382(1) of the Constitution, the Governing Council shall comprise
the members of the Executive Board of the European Central Bank and the Governors of the national
central banks of the Member States without a derogation as referred to in Article III-197 of the
Constitution.
2. Each member of the Governing Council shall have one vote. As from the date on which the
number of members of the Governing Council exceeds 21, each member of the Executive Board shall
have one vote and the number of governors with a voting right shall be 15. The latter voting rights
shall be assigned and shall rotate as follows:
(a) as from the date on which the number of governors exceeds 15 and until it reaches 22, the
governors shall be allocated to two groups, according to a ranking of the size of the share of their
national central bank's Member State in the aggregate gross domestic product at market prices
and in the total aggregated balance sheet of the monetary financial institutions of the Member
States whose currency is the euro. The shares in the aggregate gross domestic product at market
prices and in the total aggregated balance sheet of the monetary financial institutions shall be
assigned weights of 5/6 and 1/6, respectively. The first group shall be composed of five governors
and the second group of the remaining governors. The frequency of voting rights of the
governors allocated to the first group shall not be lower than the frequency of voting rights of
those of the second group. Subject to the previous sentence, the first group shall be assigned four
voting rights and the second group eleven voting rights;954393_TRAITE_EN_201_250
236
12-01-2005
14:59
Pagina 236
Part IV
(b) as from the date on which the number of governors reaches 22, the governors shall be allocated
to three groups according to a ranking based on the criteria laid down in (a). The first group shall
be composed of five governors and shall be assigned four voting rights. The second group shall
be composed of half of the total number of governors, with any fraction rounded up to the
nearest integer, and shall be assigned eight voting rights. The third group shall be composed of
the remaining governors and shall be assigned three voting rights;
(c) within each group, the governors shall have their voting rights for equal amounts of time;
(d) for the calculation of the shares in the aggregate gross domestic product at market prices
Article 29(2) shall apply. The total aggregated balance sheet of the monetary financial institutions
shall be calculated in accordance with the statistical framework applying in the Union at the time
of the calculation;
(e) whenever the aggregate gross domestic product at market prices is adjusted in accordance with
Article 29(3), or whenever the number of governors increases, the size and/or composition of the
groups shall be adjusted in accordance with the principles laid down in this subparagraph;
(f) the Governing Council, acting by a two-thirds majority of all its members, with and without a
voting right, shall take all measures necessary for the implementation of the principles laid down
in this subparagraph and may decide to postpone the start of the rotation system until the date
on which the number of governors exceeds 18.
The right to vote shall be exercised in person. By way of derogation from this rule, the Rules of
Procedure referred to in Article 12(3) may lay down that members of the Governing Council may cast
their vote by means of teleconferencing. These Rules shall also provide that a member of the
Governing Council who is prevented from attending meetings of the Governing Council for a
prolonged period may appoint an alternate as a member of the Governing Council.
The first and second subparagraphs are without prejudice to the voting rights of all members of the
Governing Council, with and without a voting right, under paragraph 3 and Article 40(2) and (3).
Save as otherwise provided for in this Statute, the Governing Council shall act by a simple majority of
the members having a voting right. In the event of a tie, the President shall have the casting vote.
In order for the Governing Council to vote, there shall be a quorum of two thirds of the members
having a voting right. If the quorum is not met, the President may convene an extraordinary meeting
at which decisions may be taken without regard to the quorum.
3. For any decisions to be taken under Articles 28, 29, 30, 32, 33 and 49, the votes in the
Governing Council shall be weighted according to the national central banks' shares in the subscribed
capital of the European Central Bank. The weighting of the votes of the members of the Executive
Board shall be zero. A decision requiring a qualified majority shall be adopted if the votes cast in
favour represent at least two thirds of the subscribed capital of the European Central Bank and
represent at least half of the shareholders. If a Governor is unable to be present, he may nominate an
alternate to cast his weighted vote.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 237
Treaty establishing a Constitution for Europe
237
4. The proceedings of the meetings shall be confidential. The Governing Council may decide to
make the outcome of its deliberations public.
5.
The Governing Council shall meet at least ten times a year.
Article 11
The Executive Board
1. In accordance with the first subparagraph of Article III-382(2) of the Constitution, the Executive
Board shall comprise the President, the Vice‑President and four other members.
The members shall perform their duties on a full-time basis. No member shall engage in any
occupation, whether gainful or not, unless exemption is exceptionally granted by the Governing
Council.
2. In accordance with Article III-382(2) of the Constitution, the President, the Vice-President and
the other members of the Executive Board shall be appointed by the European Council, acting by a
qualified majority, from among persons of recognised standing and professional experience in
monetary or banking matters, on a recommendation from the Council and after consulting the
European Parliament and the Governing Council.
Their term of office shall be eight years and shall not be renewable.
Only nationals of Member States may be members of the Executive Board.
3. The terms and conditions of employment of the members of the Executive Board, in particular
their salaries, pensions and other social security benefits shall be the subject of contracts with the
European Central Bank and shall be fixed by the Governing Council on a proposal from a Committee
comprising three members appointed by the Governing Council and three members appointed by
the Council. The members of the Executive Board shall not have the right to vote on matters referred
to in this paragraph.
4. If a member of the Executive Board no longer fulfils the conditions required for the performance
of his duties or if he has been guilty of serious misconduct, the Court of Justice may, on application
by the Governing Council or the Executive Board, compulsorily retire him.
5. Each member of the Executive Board present in person shall have the right to vote and shall
have, for that purpose, one vote. Save as otherwise provided, the Executive Board shall act by a simple
majority of the votes cast. In the event of a tie, the President shall have the casting vote. The voting
arrangements shall be specified in the Rules of Procedure referred to in Article 12(3).
6.
The Executive Board shall be responsible for the current business of the European Central Bank.
7. Any vacancy on the Executive Board shall be filled by the appointment of a new member in
accordance with paragraph 2.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 238
238
Part IV
Article 12
Responsibilities of the decision‑making bodies
1. The Governing Council shall adopt the guidelines and take the decisions necessary to ensure the
performance of the tasks entrusted to the European System of Central Banks under the Constitution
and this Statute. The Governing Council shall formulate the monetary policy of the Union including,
as appropriate, decisions relating to intermediate monetary objectives, key interest rates and the
supply of reserves in the European System of Central Banks, and shall establish the necessary
guidelines for their implementation.
The Executive Board shall implement monetary policy in accordance with the guidelines and
decisions laid down by the Governing Council. In doing so the Executive Board shall give the
necessary instructions to national central banks. In addition the Executive Board may have certain
powers delegated to it where the Governing Council so decides.
To the extent deemed possible and appropriate and without prejudice to the provisions of this
Article, the European Central Bank shall have recourse to the national central banks to carry out
operations which form part of the tasks of the European System of Central Banks.
2. The Executive Board shall have responsibility for the preparation of meetings of the Governing
Council.
3. The Governing Council shall adopt Rules of Procedure which determine the internal organisation
of the European Central Bank and its decision‑making bodies.
4. The Governing Council shall exercise the advisory functions referred to in Article 4.
5. The Governing Council shall take the decisions referred to in Article 6.
Article 13
The President
1. The President or, in his absence, the Vice‑President shall chair the Governing Council and the
Executive Board of the European Central Bank.
2. Without prejudice to Article 38, the President or his nominee shall represent the European
Central Bank externally.
Article 14
National central banks
1. In accordance with Article III-189 of the Constitution, each Member State shall ensure that its
national legislation, including the statutes of its national central bank, is compatible with the
Constitution and this Statute.
2. The statutes of the national central banks shall, in particular, provide that the term of office of a
Governor of a national central bank shall be no less than five years.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 239
Treaty establishing a Constitution for Europe
239
A Governor may be relieved from office only if he no longer fulfils the conditions required for the
performance of his duties or if he has been guilty of serious misconduct. A decision to this effect may
be referred to the Court of Justice by the Governor concerned or the Governing Council on grounds
of infringement of the Constitution or of any rule of law relating to its application. Such proceedings
shall be instituted within two months of the publication of the decision or of its notification to the
plaintiff or, in the absence thereof, of the day on which it came to the knowledge of the latter, as the
case may be.
3. The national central banks are an integral part of the European System of Central Banks and
shall act in accordance with the guidelines and instructions of the European Central Bank. The
Governing Council shall take the necessary steps to ensure compliance with the guidelines and
instructions of the European Central Bank, and shall require that any necessary information be given
to it.
4. National central banks may perform functions other than those specified in this Statute unless
the Governing Council finds, by a majority of two thirds of the votes cast, that these interfere with
the objectives and tasks of the European System of Central Banks. Such functions shall be performed
on the responsibility and liability of national central banks and shall not be regarded as being part of
the functions of the European System of Central Banks.
Article 15
Reporting commitments
1. The European Central Bank shall draw up and publish reports on the activities of the European
System of Central Banks at least quarterly.
2. A consolidated financial statement of the European System of Central Banks shall be published
each week.
3. In accordance with Article III-383(3) of the Constitution, the European Central Bank shall
address an annual report on the activities of the European System of Central Banks and on the
monetary policy of both the previous and the current year to the European Parliament, the European
Council, the Council and the Commission.
4. The reports and statements referred to in this Article shall be made available to interested parties
free of charge.
Article 16
Banknotes
In accordance with Article III-186(1) of the Constitution, the Governing Council shall have the
exclusive right to authorise the issue of euro banknotes within the Union. The European Central Bank
and the national central banks may issue such notes. The banknotes issued by the European Central
Bank and the national central banks shall be the only such notes to have the status of legal tender
within the Union.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 240
240
Part IV
The European Central Bank shall respect as far as possible existing practices regarding the issue and
design of banknotes.
CHAPTER IV
MONETARY FUNCTIONS AND OPERATIONS OF THE EUROPEAN SYSTEM OF CENTRAL BANKS
Article 17
Accounts with the European Central Bank and the national central banks
In order to conduct their operations, the European Central Bank and the national central banks may
open accounts for credit institutions, public entities and other market participants, and accept assets,
including book entry securities, as collateral.
Article 18
Open market and credit operations
1. In order to achieve the objectives of the European System of Central Banks and to carry out its
tasks, the European Central Bank and the national central banks may:
(a) operate in the financial markets by buying and selling outright (spot and forward) or under
repurchase agreement and by lending or borrowing claims and marketable instruments, whether
in euro or other currencies, as well as precious metals;
(b) conduct credit operations with credit institutions and other market participants, with lending
being based on adequate collateral.
2. The European Central Bank shall establish general principles for open market and credit
operations carried out by itself or the national central banks, including for the announcement of
conditions under which they stand ready to enter into such transactions.
Article 19
Minimum reserves
1. Subject to Article 2, the European Central Bank may require credit institutions established in
Member States to hold minimum reserves on accounts with the European Central Bank and national
central banks in pursuance of monetary policy objectives. Detailed rules concerning the calculation
and determination of the required minimum reserves may be established by the Governing Council.
In cases of non‑compliance the European Central Bank shall be entitled to levy penalty interest and to
impose other sanctions with comparable effect.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 241
Treaty establishing a Constitution for Europe
241
2. For the application of this Article, the Council shall, in accordance with the procedure laid down
in Article 41, define the basis for minimum reserves and the maximum permissible ratios between
those reserves and their basis, as well as the appropriate sanctions in cases of non-compliance.
Article 20
Other instruments of monetary control
The Governing Council may, by a majority of two thirds of the votes cast, decide upon the use of
such other operational methods of monetary control as it sees fit, respecting Article 2.
The Council shall, in accordance with the procedure laid down in Article 41, define the scope of such
methods if they impose obligations on third parties.
Article 21
Operations with public entities
1. In accordance with Article III-181 of the Constitution, overdrafts or any other type of credit
facility with the European Central Bank or with the national central banks in favour of Union
institutions, bodies, offices or agencies, central governments, regional, local or other public
authorities, other bodies governed by public law, or public undertakings of Member States shall be
prohibited, as shall the purchase directly from them by the European Central Bank or national central
banks of debt instruments.
2. The European Central Bank and national central banks may act as fiscal agents for the entities
referred to in paragraph 1.
3. The provisions of this Article shall not apply to publicly owned credit institutions which, in the
context of the supply of reserves by central banks, shall be given the same treatment by national
central banks and the European Central Bank as private credit institutions.
Article 22
Clearing and payment systems
The European Central Bank and national central banks may provide facilities, and the European
Central Bank may make regulations, to ensure efficient and sound clearing and payment systems
within the Union and with other countries.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 242
242
Part IV
Article 23
External operations
The European Central Bank and national central banks may:
(a) establish relations with central banks and financial institutions in other countries and, where
appropriate, with international organisations;
(b) acquire and sell spot and forward all types of foreign exchange assets and precious metals; the
term ‘foreign exchange asset’ shall include securities and all other assets in the currency of any
country or units of account and in whatever form held;
(c) hold and manage the assets referred to in this Article;
(d) conduct all types of banking transactions in relations with third countries and international
organisations, including borrowing and lending operations.
Article 24
Other operations
In addition to operations arising from their tasks, the European Central Bank and national central
banks may enter into operations for their administrative purposes or for their staff.
CHAPTER V
PRUDENTIAL SUPERVISION
Article 25
Prudential supervision
1. The European Central Bank may offer advice to and be consulted by the Council, the
Commission and the competent authorities of the Member States on the scope and implementation
of legally binding acts of the Union relating to the prudential supervision of credit institutions and to
the stability of the financial system.
2. In accordance with any European law adopted under Article III-185(6) of the Constitution, the
European Central Bank may perform specific tasks concerning policies relating to the prudential
supervision of credit institutions and other financial institutions with the exception of insurance
undertakings.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 243
Treaty establishing a Constitution for Europe
243
CHAPTER VI
FINANCIAL PROVISIONS OF THE EUROPEAN SYSTEM OF CENTRAL BANKS
Article 26
Financial accounts
1. The financial year of the European Central Bank and national central banks shall begin on the
first day of January and end on the last day of December.
2. The annual accounts of the European Central Bank shall be drawn up by the Executive Board, in
accordance with the principles established by the Governing Council. The accounts shall be approved
by the Governing Council and shall thereafter be published.
3. For analytical and operational purposes, the Executive Board shall draw up a consolidated
balance sheet of the European System of Central Banks, comprising those assets and liabilities of the
national central banks that fall within the European System of Central Banks.
4. For the application of this Article, the Governing Council shall establish the necessary rules for
standardising the accounting and reporting of operations undertaken by the national central banks.
Article 27
Auditing
1. The accounts of the European Central Bank and national central banks shall be audited by
independent external auditors recommended by the Governing Council and approved by the Council.
The auditors shall have full power to examine all books and accounts of the European Central Bank
and national central banks and obtain full information about their transactions.
2. Article III-384 of the Constitution shall only apply to an examination of the operational
efficiency of the management of the European Central Bank.
Article 28
Capital of the European Central Bank
1. The capital of the European Central Bank, shall be 5 000 million euro. The capital may be
increased by such amounts as stipulated by a European decision by the Governing Council acting by
the qualified majority provided for in Article 10(3), within the limits and under the conditions set by
the Council under the procedure laid down in Article 41.
2. The national central banks shall be the sole subscribers to and holders of the capital of the
European Central Bank. The subscription of capital shall be according to the key established in
accordance with Article 29.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 244
244
Part IV
3. The Governing Council, acting by the qualified majority provided for in Article 10(3), shall
determine the extent to which and the form in which the capital shall be paid up.
4. Subject to paragraph 5, the shares of the national central banks in the subscribed capital of the
European Central Bank may not be transferred, pledged or attached.
5. If the key referred to in Article 29 is adjusted, the national central banks shall transfer among
themselves capital shares to the extent necessary to ensure that the distribution of capital shares
corresponds to the adjusted key. The Governing Council shall determine the terms and conditions of
such transfers.
Article 29
Key for capital subscription
1. The key for subscription of the European Central Bank's capital, fixed for the first time in 1998
when the European System of Central Banks was established, shall be determined by assigning to
each national central bank a weighting in this key equal to the sum of:
— 50 % of the share of its respective Member State in the population of the Union in the
penultimate year preceding the establishment of the European System of Central Banks;
— 50 % of the share of its respective Member State in the Union's gross domestic product at market
prices as recorded in the last five years preceding the penultimate year before the establishment of
the European System of Central Banks.
The percentages shall be rounded up or down to the nearest multiple of 0,0001 percentage points.
2. The statistical data to be used for the application of this Article shall be provided by the
Commission in accordance with the rules laid down by the Council under the procedure provided for
in Article 41.
3. The weightings assigned to the national central banks shall be adjusted every five years after the
establishment of the European System of Central Banks by analogy with paragraph 1. The adjusted
key shall apply with effect from the first day of the following year.
4.
The Governing Council shall take all other measures necessary for the application of this Article.
Article 30
Transfer of foreign reserve assets to the European Central Bank
1. Without prejudice to Article 28, the European Central Bank shall be provided by the national
central banks with foreign reserve assets, other than Member States' currencies, euro,
International Monetary Fund reserve positions and special drawing rights, up to an amount
equivalent to 50 000 million euro. The Governing Council shall decide upon the proportion to be
called up by the European Central Bank. The European Central Bank shall have the full right to hold
and manage the foreign reserves that are transferred to it and to use them for the purposes set out in
this Statute.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 245
Treaty establishing a Constitution for Europe
245
2. The contributions of each national central bank shall be fixed in proportion to its share in the
subscribed capital of the European Central Bank.
3. Each national central bank shall be credited by the European Central Bank with a claim
equivalent to its contribution. The Governing Council shall determine the denomination and
remuneration of such claims.
4. Further calls of foreign reserve assets beyond the limit set in paragraph 1 may be effected by the
European Central Bank, in accordance with paragraph 2, within the limits and under the conditions
laid down by the Council in accordance with the procedure laid down in Article 41.
5. The European Central Bank may hold and manage International Monetary Fund reserve
positions and special drawing rights and provide for the pooling of such assets.
6.
The Governing Council shall take all other measures necessary for the application of this Article.
Article 31
Foreign reserve assets held by national central banks
1. The national central banks shall be allowed to perform transactions in fulfilment of their
obligations towards international organisations in accordance with Article 23.
2. All other operations in foreign reserve assets remaining with the national central banks after the
transfers referred to in Article 30, and Member States' transactions with their foreign exchange
working balances shall, above a certain limit to be established within the framework of paragraph 3,
be subject to approval by the European Central Bank in order to ensure consistency with the
exchange rate and monetary policies of the Union.
3.
The Governing Council shall issue guidelines with a view to facilitating such operations.
Article 32
Allocation of monetary income of national central banks
1. The income accruing to the national central banks in the performance of the monetary policy
function of the European System of Central Banks (hereinafter referred to as ‘monetary income’) shall
be allocated at the end of each financial year in accordance with the provisions of this Article.
2. The amount of each national central bank's monetary income shall be equal to its annual income
derived from its assets held against notes in circulation and deposit liabilities to credit institutions.
These assets shall be earmarked by national central banks in accordance with guidelines to be
established by the Governing Council.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 246
246
Part IV
3. If, after the start of the third stage, the balance sheet structures of the national central banks do
not, in the judgment of the Governing Council, permit the application of paragraph 2, the Governing
Council, acting by a qualified majority, may decide that, by way of derogation from paragraph 2,
monetary income shall be measured according to an alternative method for a period of not more
than five years.
4. The amount of each national central bank's monetary income shall be reduced by an amount
equivalent to any interest paid by that central bank on its deposit liabilities to credit institutions in
accordance with Article 19.
The Governing Council may decide that national central banks shall be indemnified against costs
incurred in connection with the issue of banknotes or in exceptional circumstances for specific losses
arising from monetary policy operations undertaken for the European System of Central Banks.
Indemnification shall be in a form deemed appropriate in the judgment of the Governing Council.
These amounts may be offset against the national central banks' monetary income.
5. The sum of the national central banks' monetary income shall be allocated to the national central
banks in proportion to their paid-up shares in the capital of the European Central Bank, subject to
any decision taken by the Governing Council pursuant to Article 33(2).
6. The clearing and settlement of the balances arising from the allocation of monetary income shall
be carried out by the European Central Bank in accordance with guidelines established by the
Governing Council.
7.
The Governing Council shall take all other measures necessary for the application of this Article.
Article 33
Allocation of net profits and losses of the European Central Bank
1.
The net profit of the European Central Bank shall be transferred in the following order:
(a) an amount to be determined by the Governing Council, which may not exceed 20 % of the net
profit, shall be transferred to the general reserve fund subject to a limit equal to 100 % of the
capital;
(b) the remaining net profit shall be distributed to the shareholders of the European Central Bank in
proportion to their paid‑up shares.
2. In the event of a loss incurred by the European Central Bank, the shortfall may be offset against
the general reserve fund of the European Central Bank and, if necessary, following a decision by the
Governing Council, against the monetary income of the relevant financial year in proportion and up
to the amounts allocated to the national central banks in accordance with Article 32(5).954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 247
Treaty establishing a Constitution for Europe
247
CHAPTER VII
GENERAL PROVISIONS
Article 34
Legal acts
1.
In accordance with Article III‑190 of the Constitution, the European Central Bank shall adopt:
(a) European regulations to the extent necessary to implement the tasks defined in Article 3(1)(a),
Article 19(1), Article 22 or Article 25(2) of this Statute and in cases which shall be laid down in
the European regulations and decisions referred to in Article 41;
(b) the European decisions necessary for carrying out the tasks entrusted to the European System of
Central Banks under the Constitution and this Statute;
(c) recommendations and opinions.
2. The European Central Bank may decide to publish its European decisions, recommendations and
opinions.
3. Within the limits and under the conditions adopted by the Council under the procedure laid
down in Article 41, the European Central Bank shall be entitled to impose fines or periodic penalty
payments on undertakings for failure to comply with obligations under its European regulations and
decisions.
Article 35
Judicial control and related matters
1. The acts or omissions of the European Central Bank shall be open to review or interpretation by
the Court of Justice of the European Union in the cases and under the conditions laid down in the
Constitution. The European Central Bank may institute proceedings in the cases and under the
conditions laid down in the Constitution.
2. Disputes between the European Central Bank, on the one hand, and its creditors, debtors or any
other person, on the other, shall be decided by the competent national courts, save where jurisdiction
has been conferred upon the Court of Justice of the European Union.
3. The European Central Bank shall be subject to the liability regime provided for in Article III-431
of the Constitution. The national central banks shall be liable according to their respective national
laws.
4. The Court of Justice of the European Union shall have jurisdiction to give judgment pursuant to
any arbitration clause contained in a contract concluded by or on behalf of the European Central
Bank, whether that contract be governed by public or private law.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 248
248
Part IV
5. A decision of the European Central Bank to bring an action before the Court of Justice of the
European Union shall be taken by the Governing Council.
6. The Court of Justice of the European Union shall have jurisdiction in disputes concerning the
fulfilment by a national central bank of obligations under the Constitution and this Statute. If the
European Central Bank considers that a national central bank has failed to fulfil an obligation under
the Constitution and this Statute, it shall deliver a reasoned opinion on the matter after giving the
national central bank concerned the opportunity to submit its observations. If the national central
bank concerned does not comply with the opinion within the period laid down by the European
Central Bank, the latter may bring the matter before the Court of Justice of the European Union.
Article 36
Staff
1. The Governing Council, on a proposal from the Executive Board, shall lay down the conditions
of employment of the staff of the European Central Bank.
2. The Court of Justice of the European Union shall have jurisdiction in any dispute between the
European Central Bank and its servants within the limits and under the conditions laid down in the
conditions of employment.
Article 37
Professional secrecy
1. Members of the governing bodies and the staff of the European Central Bank and the national
central banks shall be required, even after their duties have ceased, not to disclose information of the
kind covered by the obligation of professional secrecy.
2. Persons having access to data covered by a legally binding Union act imposing an obligation of
secrecy shall be subject to that obligation.
Article 38
Signatories
The European Central Bank shall be legally committed to third parties by the President or by two
members of the Executive Board or by the signatures of two members of the staff of the European
Central Bank who have been duly authorised by the President to sign on behalf of the European
Central Bank.
Article 39
Privileges and immunities
The European Central Bank shall enjoy in the territories of the Member States such privileges and
immunities as are necessary for the performance of its tasks, under the conditions laid down in the
Protocol on the privileges and immunities of the European Union.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 249
Treaty establishing a Constitution for Europe
249
CHAPTER VIII
AMENDMENT OF THE STATUTE AND COMPLEMENTARY RULES
Article 40
Simplified amendment procedures
1. In accordance with Article III-187(3) of the Constitution, Articles 5(1), (2) and (3), 17, 18, 19(1),
22, 23, 24, 26, 32(2), (3), (4) and (6), 33(1)(a) and 36 of this Statute may be amended by European
laws:
(a) on a proposal from the Commission and after consulting the European Central Bank, or
(b) on a recommendation from the European Central Bank and after consulting the Commission.
2. Article 10(2) may be amended by a European decision of the European Council, acting
unanimously, either on a recommendation from the European Central Bank and after consulting the
European Parliament and the Commission, or on a recommendation from the Commission and after
consulting the European Parliament and the European Central Bank. These amendments shall not
enter into force until they are approved by the Member States in accordance with their respective
constitutional requirements.
3. A recommendation made by the European Central Bank under this Article shall require a
unanimous decision by the Governing Council.
Article 41
Complementary rules
In accordance with Article III-187(4) of the Constitution, the Council shall adopt European
regulations and decisions establishing the measures referred to in Articles 4, 5(4), 19(2), 20, 28(1), 29
(2), 30(4) and 34(3) of this Statute. It shall act after consulting the European Parliament either:
(a) on a proposal from the Commission and after consulting the European Central Bank or
(b) on a recommendation from the European Central Bank and after consulting the Commission.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 250
250
Part IV
CHAPTER IX
TRANSITIONAL AND OTHER PROVISIONS FOR THE EUROPEAN SYSTEM OF CENTRAL BANKS
Article 42
General provisions
1. A derogation as referred to in Article III-197(1) of the Constitution shall entail that the following
Articles of this Statute shall not confer any rights or impose any obligations on the Member State
concerned: 3, 6, 9(2), 12(1), 14(3), 16, 18, 19, 20, 22, 23, 26(2), 27, 30, 31, 32, 33, 34 and 50.
2. The central banks of Member States with a derogation as specified in Article III-197(1) of the
Constitution shall retain their powers in the field of monetary policy according to national law.
3. In accordance with the second subparagraph of Article III-197(2) of the Constitution, in
Articles 3, 11(2) and 19 of this Statute ‘Member States’ shall mean Member States whose currency is
the euro.
4. In Articles 9(2), 10(2) and (3), 12(1), 16, 17, 18, 22, 23, 27, 30, 31, 32, 33(2) and 50 of this
Statute, ‘national central banks’ shall mean central banks of Member States whose currency is the
euro.
5. In Articles 10(3) and 33(1), ‘shareholders’ shall mean national central banks of Member States
whose currency is the euro.
6. In Articles 10(3) and 30(2), ‘subscribed capital’ shall mean capital of the European Central Bank
subscribed by the national central banks of Member States whose currency is the euro.
Article 43
Transitional tasks of the European Central Bank
The European Central Bank shall take over the former functions of the European Monetary Institute
referred to in Article III-199(2) of the Constitution which, because of the derogations of one or more
Member States, still have to be performed after the introduction of the euro.
The European Central Bank shall give advice in the preparations for the abrogation of the derogations
referred to in Article III-198 of the Constitution.954393_TRAITE_EN_201_250
12-01-2005
14:59
Pagina 251
Treaty establishing a Constitution for Europe
251
Article 44
The General Council of the European Central Bank
1. Without prejudice to Article III-187(1) of the Constitution, the General Council shall be
constituted as a third decision‑making body of the European Central Bank.
2. The General Council shall comprise the President and Vice-President of the European Central
Bank and the Governors of the national central banks. The other members of the Executive Board
may participate, without having the right to vote, in meetings of the General Council.
3.
The responsibilities of the General Council are listed in full in Article 46.
Article 45
Functioning of the General Council
1. The President or, in his absence, the Vice‑President of the European Central Bank shall chair the
General Council of the European Central Bank.
2. The President of the Council and a Member of the Commission may participate, without having
the right to vote, in meetings of the General Council.
3. The President shall prepare the meetings of the General Council.
4. By way of derogation from Article 12(3), the General Council shall adopt its Rules of Procedure.
5. The Secretariat of the General Council shall be provided by the European Central Bank.
Article 46
Responsibilities of the General Council
1.
The General Council shall:
(a) perform the tasks referred to in Article 43;
(b) contribute to the advisory functions referred to in Articles 4 and 25(1).
2.
The General Council shall contribute to:
(a) the collection of statistical information as referred to in Article 5;
(b) the reporting activities of the European Central Bank as referred to in Article 15;
(c) the establishment of the necessary rules for the application of Article 26 as referred to in
Article 26(4);
(d) the taking of all other measures necessary for the application of Article 29 as referred to in
Article 29(4);954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 252
252
Part IV
(e) the laying down of the Conditions of employment of the staff of the European Central Bank as
referred to in Article 36.
3. The General Council shall contribute to the necessary preparations for irrevocably fixing the
exchange rates of the currencies of Member States with a derogation against the euro as referred to in
Article III-198(3) of the Constitution.
4. The General Council shall be informed by the President of the European Central Bank of
decisions of the Governing Council.
Article 47
Transitional provisions for the capital of the European Central Bank
In accordance with Article 29, each national central bank shall be assigned a weighting in the key for
subscription of the European Central Bank's capital. By way of derogation from Article 28(3), central
banks of Member States with a derogation shall not pay up their subscribed capital unless the General
Council, acting by a majority representing at least two thirds of the subscribed capital of the
European Central Bank and at least half of the shareholders, decides that a minimal percentage has to
be paid up as a contribution to the operational costs of the European Central Bank.
Article 48
Deferred payment of capital, reserves and provisions of the European Central Bank
1. The central bank of a Member State whose derogation has been abrogated shall pay up its
subscribed share of the capital of the European Central Bank to the same extent as the central banks
of other Member States whose currency is the euro, and shall transfer to the European Central Bank
foreign reserve assets in accordance with Article 30(1). The sum to be transferred shall be determined
by multiplying the euro value at current exchange rates of the foreign reserve assets which have
already been transferred to the European Central Bank in accordance with Article 30(1), by the ratio
between the number of shares subscribed by the national central bank concerned and the number of
shares already paid up by the other national central banks.
2. In addition to the payment to be made in accordance with paragraph 1, the national central bank
concerned shall contribute to the reserves of the European Central Bank, to those provisions
equivalent to reserves, and to the amount still to be appropriated to the reserves and provisions
corresponding to the balance of the profit and loss account as at 31 December of the year prior to
the abrogation of the derogation. The sum to be contributed shall be determined by multiplying the
amount of the reserves, as defined above and as stated in the approved balance sheet of the European
Central Bank, by the ratio between the number of shares subscribed by the central bank concerned
and the number of shares already paid up by the other central banks.
3. Upon one or more countries becoming Member States and their respective national central
banks becoming part of the European System of Central Banks, the subscribed capital of the
European Central Bank and the limit on the amount of foreign reserve assets that may be transferred
to the European Central Bank shall be automatically increased. The increase shall be determined by
multiplying the respective amounts then prevailing by the ratio, within the expanded capital key,954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 253
Treaty establishing a Constitution for Europe
253
between the weighting of the entering national central banks concerned and the weighting of the
national central banks already members of the European System of Central Banks. Each national
central bank's weighting in the capital key shall be calculated by analogy with Article 29(1) and in
compliance with Article 29(2). The reference periods to be used for the statistical data shall be
identical to those applied for the latest quinquennial adjustment of the weightings under Article 29
(3).
Article 49
Derogation from Article 32
1. If, after the start of the third stage, the Governing Council decides that the application of
Article 32 results in significant changes in national central banks' relative income positions, the
amount of income to be allocated pursuant to Article 32 shall be reduced by a uniform percentage
which shall not exceed 60 % in the first financial year after the start of the third stage and which shall
decrease by at least 12 percentage points in each subsequent financial year.
2. Paragraph 1 shall be applicable for not more than five financial years after the start of the third
stage.
Article 50
Exchange of banknotes in Member States' currencies
Following the irrevocable fixing of exchange rates in accordance with Article III-198(3) of the
Constitution, the Governing Council shall take the necessary measures to ensure that banknotes
denominated in the currencies of Member States with irrevocably fixed exchange rates are exchanged
by the national central banks at their respective par values.
Article 51
Applicability of the transitional provisions
If and as long as there are Member States with a derogation Articles 42 to 47 shall be applicable.954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 254
254
Part IV
5.
PROTOCOL ON THE STATUTE OF THE EUROPEAN INVESTMENT BANK
THE HIGH CONTRACTING PARTIES,
DESIRING to lay down the Statute of the European Investment Bank provided for in Article III‑393 of the Constitution,
HAVE AGREED upon the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe:
Article 1
The European Investment Bank referred to in Article III‑393 of the Constitution (hereinafter called
the ‘Bank’) is hereby constituted; it shall perform its functions and carry on its activities in accordance
with the provisions of the Constitution and of this Statute.
Article 2
The task of the Bank shall be that defined in Article III‑394 of the Constitution.
Article 3
In accordance with Article III‑393 of the Constitution, the Bank's members shall be the Member
States.
Article 4
1. The capital of the Bank shall be 163 653 737 000 euro, subscribed by the Member States as
follows:
Germany 26 649 532 500
France 26 649 532 500
Italy 26 649 532 500
United Kingdom 26 649 532 500
Spain 15 989 719 500
Belgium 7 387 065 000
Netherlands 7 387 065 000
Sweden 4 900 585 500
Denmark 3 740 283 000
Austria 3 666 973 500
Poland 3 411 263 500
Finland 2 106 816 000954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 255
Treaty establishing a Constitution for Europe
255
Greece 2 003 725 500
Portugal 1 291 287 000
Czech Republic 1 258 785 500
Hungary 1 190 868 500
Ireland 935 070 000
Slovakia 428 490 500
Slovenia 397 815 000
Lithuania 249 617 500
Luxembourg 187 015 500
Cyprus 183 382 000
Latvia 152 335 000
Estonia 117 640 000
Malta 69 804 000
The Member States shall be liable only up to the amount of their share of the capital subscribed and
not paid up.
2. The admission of a new member shall entail an increase in the subscribed capital corresponding
to the capital brought in by the new member.
3. The Board of Governors may, acting unanimously, decide to increase the subscribed capital.
4. The share of a member in the subscribed capital may not be transferred, pledged or attached.
Article 5
1. The subscribed capital shall be paid in by Member States to the extent of 5 % on average of the
amounts laid down in Article 4(1).
2. In the event of an increase in the subscribed capital, the Board of Governors, acting unanimously,
shall fix the percentage to be paid up and the arrangements for payment. Cash payments shall be
made exclusively in euro.
3. The Board of Directors may require payment of the balance of the subscribed capital, to such
extent as may be required for the Bank to meet its obligations.
Each Member State shall make this payment in proportion to its share of the subscribed capital.954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 256
256
Part IV
Article 6
The Bank shall be directed and managed by a Board of Governors, a Board of Directors and a
Management Committee.
Article 7
1.
The Board of Governors shall consist of the ministers designated by the Member States.
2. The Board of Governors shall lay down general directives for the credit policy of the Bank, in
accordance with the Union's objectives.
The Board of Governors shall ensure that these directives are implemented.
3.
The Board of Governors shall in addition:
(a) decide whether to increase the subscribed capital in accordance with Article 4(3) and Article 5(2);
(b) for the purposes of Article 9(1), determine the principles applicable to financing operations
undertaken within the framework of the Bank's task;
(c) exercise the powers provided in Articles 9 and 11 in respect of the appointment and the
compulsory retirement of the members of the Board of Directors and of the Management
Committee, and those powers provided for in the second subparagraph of Article 11(1);
(d) take decisions in respect of the granting of finance for investment operations to be carried out, in
whole or in part, outside the territories of the Member States in accordance with Article 16(1);
(e) approve the annual report of the Board of Directors;
(f) approve the annual balance sheet and profit and loss account;
(g) approve the Rules of Procedure of the Bank;
(h) exercise the other powers conferred by this Statute.
4. Within the framework of the Constitution and this Statute, the Board of Governors, acting
unanimously, may take any decisions concerning the suspension of the operations of the Bank and,
should the event arise, its liquidation.
Article 8
1. Save as otherwise provided in this Statute, decisions of the Board of Governors shall be taken by
a majority of its members. This majority must represent at least 50 % of the subscribed capital.
A qualified majority shall require eighteen votes in favour and 68 % of the subscribed capital.954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 257
Treaty establishing a Constitution for Europe
257
2. Abstentions by members present in person or represented shall not prevent the adoption of
decisions requiring unanimity.
Article 9
1. The Board of Directors shall take decisions in respect of granting finance, in particular in the
form of loans and guarantees, and raising loans; it shall fix the interest rates on loans granted and the
commission and other charges. It may, on the basis of a decision taken by a qualified majority,
delegate some of its functions to the Management Committee. It shall determine the terms and
conditions for such delegation and shall supervise its execution.
The Board of Directors shall see that the Bank is properly run; it shall ensure that the Bank is
managed in accordance with the Constitution and this Statute and with the general directives laid
down by the Board of Governors.
At the end of the financial year the Board of Directors shall submit a report to the Board of
Governors and shall publish it when approved.
2.
The Board of Directors shall consist of twenty‑six directors and sixteen alternate directors.
The directors shall be appointed by the Board of Governors for five years, one nominated by each
Member State. One shall also be nominated by the Commission.
The alternate directors shall be appointed by the Board of Governors for five years as shown below:
— two alternates nominated by the Federal Republic of Germany,
— two alternates nominated by the French Republic,
— two alternates nominated by the Italian Republic,
— two alternates nominated by the United Kingdom of Great Britain and Northern Ireland,
— one alternate nominated by common accord between the Kingdom of Spain and
the Portuguese Republic,
— one alternate nominated by common accord between the Kingdom of Belgium, the Grand Duchy
of Luxembourg and the Kingdom of the Netherlands,
— one alternate nominated by common accord between the Kingdom of Denmark,
the Hellenic Republic and Ireland,
— one alternate nominated by common accord between the Republic of Austria, the Republic of
Finland and the Kingdom of Sweden,954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 258
258
Part IV
— three alternates nominated by common accord between the Czech Republic, the Republic of
Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of
Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak
Republic,
— one alternate director nominated by the Commission.
The Board of Directors shall co‑opt six non‑voting experts: three as members and three as alternates.
The appointments of the directors and the alternates shall be renewable.
The Rules of Procedure shall lay down the arrangements for participating in the meetings of the
Board of Directors and the provisions applicable to alternates and co‑opted experts.
The President of the Management Committee or, in his absence, one of the Vice‑Presidents, shall
preside over meetings of the Board of Directors but shall not vote.
Members of the Board of Directors shall be chosen from persons whose independence and
competence are beyond doubt. They shall be responsible only to the Bank.
3. A director may be compulsorily retired by the Board of Governors only if he no longer fulfils the
conditions required for the performance of his duties; the Board must act by a qualified majority.
If the annual report is not approved, the Board of Directors shall resign.
4. Any vacancy arising as a result of death, voluntary resignation, compulsory retirement or
collective resignation shall be filled in accordance with paragraph 2. A member shall be replaced for
the remainder of his term of office, save where the entire Board of Directors is being replaced.
5. The Board of Governors shall determine the remuneration of members of the Board of Directors.
The Board of Governors shall lay down what activities are incompatible with the duties of a director
or an alternate.
Article 10
1. Each director shall have one vote on the Board of Directors. He may delegate his vote in all cases,
according to procedures to be laid down in the Rules of Procedure of the Bank.
2. Save as otherwise provided in this Statute, decisions of the Board of Directors shall be taken by at
least one third of the members entitled to vote, representing at least 50 % of the subscribed capital. A
qualified majority shall require eighteen votes and 68 % of the subscribed capital in favour. The Rules
of Procedure of the Bank shall lay down how many members of the Board of Directors constitute the
quorum needed for the adoption of decisions.954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 259
Treaty establishing a Constitution for Europe
259
Article 11
1. The Management Committee shall consist of a President and eight Vice‑Presidents appointed for
a period of six years by the Board of Governors on a proposal from the Board of Directors. Their
appointments shall be renewable.
The Board of Governors, acting unanimously, may vary the number of members on the Management
Committee.
2. On a proposal from the Board of Directors adopted by a qualified majority, the Board of
Governors may, acting by a qualified majority, compulsorily retire a member of the Management
Committee.
3. The Management Committee shall be responsible for the current business of the Bank, under the
authority of the President and the supervision of the Board of Directors.
It shall prepare the decisions of the Board of Directors, including decisions on the raising of loans
and the granting of finance, in particular in the form of loans and guarantees. It shall ensure that
these decisions are implemented.
4. The Management Committee, acting by a majority, shall adopt opinions on proposals for raising
loans or granting finance, in particular in the form of loans and guarantees.
5. The Board of Governors shall determine the remuneration of members of the Management
Committee and shall lay down what activities are incompatible with their duties.
6. The President or, if he is prevented, a Vice‑President shall represent the Bank in judicial and other
matters.
7. The staff of the Bank shall be under the authority of the President. They shall be engaged and
discharged by him. In the selection of staff, account shall be taken not only of personal ability and
qualifications but also of an equitable representation of nationals of Member States. The Rules of
Procedure shall determine which organ is competent to adopt the provisions applicable to staff.
8. The Management Committee and the staff of the Bank shall be responsible only to the Bank and
shall be completely independent in the performance of their duties.
Article 12
1. A Committee consisting of six members, appointed on the grounds of their competence by the
Board of Governors, shall verify that the activities of the Bank conform to best banking practice and
shall be responsible for the auditing of its accounts.
2. The Committee referred to in paragraph 1 shall annually ascertain that the operations of the
Bank have been conducted and its books kept in a proper manner. To this end, it shall verify that the
Bank's operations have been carried out in compliance with the formalities and procedures laid down
by this Statute and the Rules of Procedure.954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 260
260
Part IV
3. The Committee referred to in paragraph 1 shall confirm that the financial statements, as well as
any other financial information contained in the annual accounts drawn up by the Board of
Directors, give a true and fair view of the financial position of the Bank in respect of its assets and
liabilities, and of the results of its operations and its cash flows for the financial year under review.
4. The Rules of Procedure shall specify the qualifications required of the members of the
Committee and lay down the terms and conditions for the Committee's activity.
Article 13
The Bank shall deal with each Member State through the authority designated by that State. In the
conduct of financial operations the Bank shall have recourse to the national central bank of the
Member State concerned or to other financial institutions approved by that State.
Article 14
1.
The Bank shall cooperate with all international organisations active in fields similar to its own.
2. The Bank shall seek to establish all appropriate contacts in the interests of cooperation with
banking and financial institutions in the countries to which its operations extend.
Article 15
At the request of a Member State or of the Commission, or on its own initiative, the Board of
Governors shall, in accordance with the same provisions as governed their adoption, interpret or
supplement the directives laid down by it under Article 7.
Article 16
1. Within the framework of the task set out in Article III‑394 of the Constitution, the Bank shall
grant finance, in particular in the form of loans and guarantees to its members or to private or public
undertakings for investments to be carried out in the territories of Member States, to the extent that
funds are not available from other sources on reasonable terms.
However, by decision of the Board of Governors, acting by a qualified majority on a proposal from
the Board of Directors, the Bank may grant financing for investment to be carried out, in whole or in
part, outside the territories of Member States.
2. As far as possible, loans shall be granted only on condition that other sources of finance are also
used.
3. When granting a loan to an undertaking or to a body other than a Member State, the Bank shall
make the loan conditional either on a guarantee from the Member State in whose territory the
investment will be carried out, on adequate guarantees, or on the financial strength of the debtor.
Furthermore, in accordance with the principles established by the Board of Governors pursuant to
Article 7(3)(b), and where the implementation of projects provided for in Article III‑394 of the
Constitution so requires, the Board of Directors shall, acting by a qualified majority, lay down the954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 261
Treaty establishing a Constitution for Europe
261
terms and conditions of any financing operation presenting a specific risk profile and thus considered
to be a special activity.
4. The Bank may guarantee loans contracted by public or private undertakings or other bodies for
the purpose of carrying out projects provided for in Article III‑394 of the Constitution.
5. The aggregate amount outstanding at any time of loans and guarantees granted by the Bank shall
not exceed 250 % of its subscribed capital, reserves, non‑allocated provisions and profit and loss
account surplus. The latter aggregate amount shall be reduced by an amount equal to the amount
subscribed (whether or not paid in) for any equity participation of the Bank.
The amount of the Bank's disbursed equity participations shall not exceed at any time an amount
corresponding to the total of its paid‑in subscribed capital, reserves, non‑allocated provisions and
profit and loss account surplus.
By way of exception, the special activities of the Bank, as decided by the Board of Governors and the
Board of Directors in accordance with paragraph 3, will have a specific allocation of reserve.
This paragraph shall also apply to the consolidated accounts of the Bank.
6. The Bank shall protect itself against exchange risks by including in contracts for loans and
guarantees such clauses as it considers appropriate.
Article 17
1. Interest rates on loans to be granted by the Bank and commission and other charges shall be
adjusted to conditions prevailing on the capital market and shall be calculated in such a way that the
income therefrom shall enable the Bank to meet its obligations, to cover its expenses and risks and to
build up a reserve fund as provided for in Article 22.
2. The Bank shall not grant any reduction in interest rates. Where a reduction in the interest rate
appears desirable in view of the nature of the investment to be financed, the Member State concerned
or some other agency may grant aid towards the payment of interest to the extent that this is
compatible with Article III‑167 of the Constitution.
Article 18
In its financing operations, the Bank shall observe the following principles:
1.
It shall ensure that its funds are employed in the most rational way in the interests of the Union.
It may grant loans or guarantees only:
(a) where, in the case of investments by undertakings in the production sector, interest and
amortisation payments are covered out of operating profits or, in the case of other
investments, either by a commitment entered into by the State in which the investment is
made or by some other means; and,954393_TRAITE_EN_251_300
12-01-2005
15:20
Pagina 262
262
Part IV
(b) where the execution of the investment contributes to an increase in economic productivity in
general and promotes the establishment or functioning of the internal market.
2. It shall neither acquire any interest in an undertaking nor assume any responsibility in its
management unless this is required to safeguard the rights of the Bank in ensuring recovery of
funds lent.
However, in accordance with the principles determined by the Board of Governors pursuant to
Article 7(3)(b), and where the implementation of operations provided for in Article III‑394 of the
Constitution so requires, the Board of Directors shall, acting by a qualified majority, lay down the
terms and conditions for taking an equity participation in a commercial undertaking, normally as
a complement to a loan or a guarantee, insofar as this is required to finance an investment or
programme.
3. It may dispose of its claims on the capital market and may, to this end, require its debtors to issue
bonds or other securities.
4. Neither the Bank nor the Member States shall impose conditions requiring funds lent by the Bank
to be spent within a specified Member State.
5. The Bank may make its loans conditional on international invitations to tender being arranged.
6. The Bank shall not finance, in whole or in part, any investment opposed by the Member State in
whose territory it is to be carried out.
7. As a complement to its lending activity, the Bank may provide technical assistance services in
accordance with the terms and conditions laid down by the Board of Governors, acting by a
qualified majority, and in compliance with this Statute.
Article 19
1. Any undertaking or public or private entity may apply directly to the Bank for financing.
Applications to the Bank may also be made either through the Commission or through the Member
State on whose territory the investment will be carried out.
2. Applications made through the Commission shall be submitted for an opinion to the
Member State in whose territory the investment will be carried out. Applications made through a
Member State shall be submitted to the Commission for an opinion. Applications made direct by an
undertaking shall be submitted to the Member State concerned and to the Commission.
The Member State concerned and the Commission shall deliver their opinions within two months. If
no reply is received within this period, the Bank may assume that there is no objection to the
investment in question.
3. The Board of Directors shall rule on financing operations submitted to it by the Management
Committee.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 263
Treaty establishing a Constitution for Europe
263
4. The Management Committee shall examine whether financing operations submitted to it comply
with the provisions of this Statute, in particular with Articles 16 and 18. Where the Management
Committee is in favour of the financing operation, it shall submit the corresponding proposal to the
Board of Directors. The Committee may make its favourable opinion subject to such conditions as it
considers essential. Where the Management Committee is against granting the finance, it shall submit
the relevant documents together with its opinion to the Board of Directors.
5. Where the Management Committee delivers an unfavourable opinion, the Board of Directors
may not grant the finance concerned unless its decision is unanimous.
6. Where the Commission delivers an unfavourable opinion, the Board of Directors may not grant
the finance concerned unless its decision is unanimous, the director nominated by the Commission
abstaining.
7. Where both the Management Committee and the Commission deliver an unfavourable opinion,
the Board of Directors may not grant the finance.
8. In the event that a financing operation relating to an approved investment has to be restructured
in order to safeguard the Bank's rights and interests, the Management Committee shall take without
delay the emergency measures which it deems necessary, subject to immediate reporting thereon to
the Board of Directors.
Article 20
1. The Bank shall borrow on the capital markets the funds necessary for the performance of its
tasks.
2. The Bank may borrow on the capital markets of the Member States in accordance with the legal
provisions applying to those markets.
The competent authorities of a Member State with a derogation within the meaning of Article
III‑197(1) of the Constitution may oppose this only if there is reason to fear serious disturbances on
the capital market of that State.
Article 21
1. The Bank may employ any available funds which it does not immediately require to meet its
obligations in the following ways:
(a) it may invest on the money markets;
(b) it may, subject to the provisions of Article 18(2), buy and sell securities;
(c) it may carry out any other financial operation linked with its objectives.
2. Without prejudice to the provisions of Article 23, the Bank shall not, in managing its
investments, engage in any currency arbitrage not directly required to carry out its lending operations
or fulfil commitments arising out of loans raised or guarantees granted by it.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 264
264
Part IV
3. The Bank shall, in the fields covered by this Article, act in agreement with the competent
authorities or with the national central bank of the Member State concerned.
Article 22
1. A reserve fund of up to 10 % of the subscribed capital shall be built up progressively. If the state
of the liabilities of the Bank should so justify, the Board of Directors may decide to set aside
additional reserves. Until such time as the reserve fund has been fully built up, it shall be fed by:
(a) interest received on loans granted by the Bank out of sums to be paid up by the Member States
pursuant to Article 5;
(b) interest received on loans granted by the Bank out of funds derived from repayment of the loans
referred to in (a);
to the extent that this income is not required to meet the obligations of the Bank or to cover its
expenses.
2. The resources of the reserve fund shall be so invested as to be available at any time to meet the
purpose of the fund.
Article 23
1. The Bank shall at all times be entitled to transfer its assets into the currency of a Member State
whose currency is not the euro in order to carry out financial operations corresponding to the task
set out in Article III‑394 of the Constitution, taking into account the provisions of Article 21 of this
Statute. The Bank shall, as far as possible, avoid making such transfers if it has cash or liquid assets in
the currency required.
2. The Bank may not convert its assets in the currency of a Member State whose currency is not the
euro into the currency of a third country without the agreement of the Member State concerned.
3. The Bank may freely dispose of that part of its capital which is paid up and of any currency
borrowed on markets outside the Union.
4. The Member States undertake to make available to the debtors of the Bank the currency needed
to repay the capital and pay the interest on loans or commission on guarantees granted by the Bank
for investment to be carried out in their territory.
Article 24
If a Member State fails to meet the obligations of membership arising from this Statute, in particular
the obligation to pay its share of the subscribed capital or to service its borrowings, the granting of
loans or guarantees to that Member State or its nationals may be suspended by a decision of the
Board of Governors, acting by a qualified majority.
Such decision shall not release either the Member State or its nationals from their obligations towards
the Bank.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 265
Treaty establishing a Constitution for Europe
265
Article 25
1. If the Board of Governors decides to suspend the operations of the Bank, all its activities shall
cease forthwith, except those required to ensure the due realisation, protection and preservation of its
assets and the settlement of its liabilities.
2. In the event of liquidation, the Board of Governors shall appoint the liquidators and give them
instructions for carrying out the liquidation. It shall ensure that the rights of the members of staff are
safeguarded.
Article 26
1. In each of the Member States, the Bank shall enjoy the most extensive legal capacity accorded to
legal persons under their laws. It may, in particular, acquire or dispose of movable or immovable
property and may be a party to legal proceedings.
2.
The property of the Bank shall be exempt from all forms of requisition or expropriation.
Article 27
1. Disputes between the Bank on the one hand, and its creditors, debtors or any other person on
the other, shall be decided by the competent national courts, save where jurisdiction has been
conferred on the Court of Justice of the European Union. The Bank may provide for arbitration in any
contract.
2. The Bank shall have an address for service in each Member State. It may, however, in any
contract, specify a particular address for service.
3. The property and assets of the Bank shall not be liable to attachment or to seizure by way of
execution except by decision of a court.
Article 28
1. The Board of Governors may, acting unanimously, decide to establish subsidiaries or other
entities, which shall have legal personality and financial autonomy.
2. The Board of Governors, acting unanimously, shall establish the Statutes of the bodies referred to
in paragraph 1, defining, in particular, their objectives, structure, capital, membership, the location of
their seat, their financial resources, means of intervention and auditing arrangements, as well as their
relationship with the organs of the Bank.
3. The Bank may participate in the management of these bodies and contribute to their subscribed
capital up to the amount determined by the Board of Governors, acting unanimously.
4. The Protocol on the privileges and immunities of the European Union shall apply to the bodies
referred to in paragraph 1 insofar as they are incorporated under Union law, to the members of their
organs in the performance of their duties as such and to their staff, under the same terms and
conditions as those applicable to the Bank.954393_TRAITE_EN_251_300
266
12-01-2005
15:21
Pagina 266
Part IV
Those dividends, capital gains or other forms of revenue stemming from such bodies to which the
members, other than the European Union and the Bank, are entitled, shall however remain subject to
the fiscal provisions of the applicable legislation.
5. The Court of Justice of the European Union shall, within the limits hereinafter laid down, hear
disputes concerning measures adopted by organs of a body incorporated under Union law.
Proceedings against such measures may be instituted by any member of such a body in its capacity as
such or by Member States under the conditions laid down in Article III‑365 of the Constitution.
6. The Board of Governors may, acting unanimously, decide to admit the staff of bodies
incorporated under Union law to joint schemes with the Bank, in compliance with the respective
internal procedures.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 267
Treaty establishing a Constitution for Europe
6.
267
PROTOCOL ON THE LOCATION OF THE SEATS OF THE INSTITUTIONS AND OF CERTAIN
BODIES, OFFICES, AGENCIES AND DEPARTMENTS OF THE EUROPEAN UNION
THE HIGH CONTRACTING PARTIES,
HAVING REGARD to Article III‑432 of the Constitution,
RECALLING AND CONFIRMING the Decision of 8 April 1965, and without prejudice to the decisions concerning the
seat of future institutions, bodies, offices, agencies and departments,
HAVE AGREED UPON the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe and to the Treaty establishing the European Atomic Energy Community:
Sole article
1. The European Parliament shall have its seat in Strasbourg, where the 12 periods of monthly
plenary sessions, including the budget session, shall be held. The periods of additional plenary
sessions shall be held in Brussels. The committees of the European Parliament shall meet in Brussels.
The General Secretariat of the European Parliament and its departments shall remain in Luxembourg.
2. The Council shall have its seat in Brussels. During the months of April, June and October, the
Council shall hold its meetings in Luxembourg.
3. The Commission shall have its seat in Brussels. The departments listed in Articles 7, 8 and 9 of
the Decision of 8 April 1965 shall be established in Luxembourg.
4. The Court of Justice of the European Union shall have its seat in Luxembourg.
5. The European Central Bank shall have its seat in Frankfurt.
6. The Court of Auditors shall have its seat in Luxembourg.
7. The Committee of the Regions shall have its seat in Brussels.
8. The Economic and Social Committee shall have its seat in Brussels.
9. The European Investment Bank shall have its seat in Luxembourg.
10.
Europol shall have its seat in The Hague.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 268
268
Part IV
7.
PROTOCOL ON THE PRIVILEGES AND IMMUNITIES OF THE EUROPEAN UNION
THE HIGH CONTRACTING PARTIES,
CONSIDERING that, in accordance with Article III-434 of the Constitution, the Union shall enjoy in the territories of
the Member States such privileges and immunities as are necessary for the performance of its tasks,
HAVE AGREED upon the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe and to the Treaty establishing the European Atomic Energy Community:
CHAPTER I
PROPERTY, FUNDS, ASSETS AND OPERATIONS OF THE UNION
Article 1
The premises and buildings of the Union shall be inviolable. They shall be exempt from search,
requisition, confiscation or expropriation. The property and assets of the Union shall not be the
subject of any administrative or legal measure of constraint without the authorisation of the Court of
Justice.
Article 2
The archives of the Union shall be inviolable.
Article 3
The Union, its assets, revenues and other property shall be exempt from all direct taxes.
The governments of the Member States shall, wherever possible, take the appropriate measures to
remit or refund the amount of indirect taxes or sales taxes included in the price of movable or
immovable property, where the Union makes, for its official use, substantial purchases the price of
which includes taxes of this kind. These provisions shall not be applied, however, so as to have the
effect of distorting competition within the Union.
No exemption shall be granted in respect of taxes and dues which amount merely to charges for
public utility services.
Article 4
The Union shall be exempt from all customs duties, prohibitions and restrictions on imports and
exports in respect of articles intended for its official use. Articles so imported shall not be disposed of,
whether or not in return for payment, in the territory of the State into which they have been
imported, except under conditions approved by the government of that State.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 269
Treaty establishing a Constitution for Europe
269
The Union shall also be exempt from any customs duties and any prohibitions and restrictions on
import and exports in respect of its publications.
CHAPTER II
COMMUNICATIONS AND LAISSEZ-PASSER
Article 5
For their official communications and the transmission of all their documents, the institutions of the
Union shall enjoy in the territory of each Member State the treatment accorded by that State to
diplomatic missions.
Official correspondence and other official communications of the institutions of the Union shall not
be subject to censorship.
Article 6
Laissez-passer in a form to be prescribed by a European regulation of the Council acting by a simple
majority, which shall be recognised as valid travel documents by the authorities of the Member States,
may be issued to members and servants of the institutions of the Union by the Presidents of these
institutions. These laissez-passer shall be issued to officials and other servants under conditions laid
down in the Staff Regulations of officials and the Conditions of employment of other servants of the
Union.
The Commission may conclude agreements for these laissez-passer to be recognised as valid travel
documents within the territory of third States.
CHAPTER III
MEMBERS OF THE EUROPEAN PARLIAMENT
Article 7
No administrative or other restriction shall be imposed on the free movement of members of the
European Parliament travelling to or from the place of meeting of the European Parliament.
Members of the European Parliament shall, in respect of customs and exchange control, be accorded:
(a) by their own governments, the same facilities as those accorded to senior officials travelling
abroad on temporary official missions;
(b) by the governments of other Member States, the same facilities as those accorded to
representatives of foreign governments on temporary official missions.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 270
270
Part IV
Article 8
Members of the European Parliament shall not be subject to any form of inquiry, detention or legal
proceedings in respect of opinions expressed or votes cast by them in the performance of their duties.
Article 9
During the sessions of the European Parliament, its members shall enjoy:
(a) in the territory of their own State, the immunities accorded to members of their Parliament;
(b) in the territory of any other Member State, immunity from any measure of detention and from
legal proceedings.
Immunity shall likewise apply to members while they are travelling to and from the place of meeting
of the European Parliament.
Immunity cannot be claimed when a member is found in the act of committing an offence and shall
not prevent the European Parliament from exercising its right to waive the immunity of one of its
members.
CHAPTER IV
REPRESENTATIVES OF MEMBER STATES TAKING PART IN THE WORK OF THE INSTITUTIONS OF THE
UNION
Article 10
Representatives of Member States taking part in the work of the institutions of the Union, their
advisers and technical experts shall, in the performance of their duties and during their travel to and
from the place of meeting, enjoy the customary privileges, immunities and facilities.
This Article shall also apply to members of the advisory bodies of the Union.
CHAPTER V
OFFICIALS AND OTHER SERVANTS OF THE UNION
Article 11
In the territory of each Member State and whatever their nationality, officials and other servants of
the Union shall:
(a) subject to the provisions of the Constitution relating, on the one hand, to the rules on the liability
of officials and other servants towards the Union and, on the other hand, to the jurisdiction of
the Court of Justice of the European Union in disputes between the Union and its officials and
other servants, be immune from legal proceedings in respect of acts performed by them in their954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 271
Treaty establishing a Constitution for Europe
271
official capacity, including their words spoken or written. They shall continue to enjoy this
immunity after they have ceased to hold office;
(b) together with their spouses and dependent members of their families, not be subject to
immigration restrictions or to formalities for the registration of aliens;
(c) in respect of currency or exchange regulations, be accorded the same facilities as are customarily
accorded to officials of international organisations;
(d) enjoy the right to import free of duty their furniture and effects at the time of first taking up their
post in the State concerned, and the right to re‑export free of duty their furniture and effects, on
termination of their duties in that State, subject in either case to the conditions considered to be
necessary by the government of the State in which this right is exercised;
(e) have the right to import free of duty a motor car for their personal use, acquired either in the
State of their last residence or in the State of which they are nationals on the terms ruling in the
home market in that State, and to re‑export it free of duty, subject in either case to the conditions
considered to be necessary by the government of the State concerned.
Article 12
Officials and other servants of the Union shall be liable to a tax, for the benefit of the Union, on
salaries, wages and emoluments paid to them by the Union, in accordance with the conditions and
procedure laid down by a European law. That law shall be adopted after consultation of the
institutions concerned.
Officials and other servants of the Union shall be exempt from national taxes on salaries, wages and
emoluments paid by the Union.
Article 13
In the application of income tax, wealth tax and death duties and in the application of conventions
on the avoidance of double taxation concluded between Member States of the Union, officials and
other servants of the Union who, solely by reason of the performance of their duties in the service of
the Union, establish their residence in the territory of a Member State other than their State of
domicile for tax purposes at the time of entering the service of the Union, shall be considered, both
in the State of their actual residence and in the State of domicile for tax purposes, as having
maintained their domicile in the latter State provided that it is a member of the Union. This provision
shall also apply to a spouse, to the extent that the latter is not separately engaged in a gainful
occupation, and to children dependent on and in the care of the persons referred to in this Article.
Movable property belonging to persons referred to in the first paragraph and situated in the territory
of the State where they are staying shall be exempt from death duties in that State. Such property
shall, for the assessment of such duty, be considered as being in the State of domicile for tax
purposes, subject to the rights of third States and to the possible application of provisions of
international conventions on double taxation.
Any domicile acquired solely by reason of the performance of duties in the service of other
international organisations shall not be taken into consideration in applying the provisions of this
Article.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 272
272
Part IV
Article 14
The scheme of social security benefits for officials and other servants of the Union shall be laid down
by a European law. That law shall be adopted after consultation of the institutions concerned.
Article 15
The categories of officials and other servants of the Union to whom Article 11, the second paragraph
of Article 12, and Article 13 shall apply, in whole or in part, shall be determined by a European law.
That law shall be adopted after consultation of the institutions concerned.
The names, grades and addresses of officials and other servants included in such categories shall be
communicated periodically to the governments of the Member States.
CHAPTER VI
PRIVILEGES AND IMMUNITIES OF MISSIONS OF THIRD STATES ACCREDITED TO THE UNION
Article 16
The Member State in whose territory the Union has its seat shall accord the customary diplomatic
privileges and immunities to missions of third States accredited to the Union.
CHAPTER VII
GENERAL PROVISIONS
Article 17
Privileges, immunities and facilities shall be accorded to officials and other servants of the Union
solely in the interests of the Union.
Each institution of the Union shall be required to waive the immunity accorded to an official or other
servant wherever that institution considers that the waiver of such immunity is not contrary to the
interests of the Union.
Article 18
The institutions of the Union shall, for the purpose of applying this Protocol, cooperate with the
responsible authorities of the Member States concerned.
Article 19
Articles 11 to 14 and Article 17 shall apply to members of the Commission.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 273
Treaty establishing a Constitution for Europe
273
Article 20
Articles 11 to 14 and Article 17 shall apply to the Judges, the Advocates‑General, the Registrars and
the Assistant Rapporteurs of the Court of Justice of the European Union, without prejudice to the
provisions of Article 3 of the Protocol on the Statute of the Court of Justice of the European Union
concerning immunity from legal proceedings of Judges and Advocates-General.
Articles 11 to 14 and Article 17 shall also apply to the members of the Court of Auditors.
Article 21
This Protocol shall also apply to the European Central Bank, to the members of its organs and to its
staff, without prejudice to the Protocol on the Statute of the European System of Central Banks and
of the European Central Bank.
The European Central Bank shall, in addition, be exempt from any form of taxation or imposition of
a like nature on the occasion of any increase in its capital and from the various formalities which may
be connected therewith in the State where the Bank has its seat. The activities of the Bank and of its
organs carried on in accordance with the Statute of the European System of Central Banks and of the
European Central Bank shall not be subject to any turnover tax.
Article 22
This Protocol shall also apply to the European Investment Bank, to the members of its organs, to its
staff and to the representatives of the Member States taking part in its activities, without prejudice to
the Protocol on the Statute of the Bank.
The European Investment Bank shall in addition be exempt from any form of taxation or imposition
of a like nature on the occasion of any increase in its capital and from the various formalities which
may be connected therewith in the State where the Bank has its seat. Similarly, its dissolution or
liquidation shall not give rise to any imposition. Finally, the activities of the Bank and of its organs
carried on in accordance with its Statute shall not be subject to any turnover tax.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 274
274
Part IV
8.
PROTOCOL ON THE TREATIES AND ACTS OF ACCESSION OF THE KINGDOM OF
DENMARK, IRELAND AND THE UNITED KINGDOM OF GREAT BRITAIN AND NORTHERN
IRELAND, OF THE HELLENIC REPUBLIC, OF THE KINGDOM OF SPAIN AND THE PORTUGUESE
REPUBLIC, AND OF THE REPUBLIC OF AUSTRIA, THE REPUBLIC OF FINLAND
AND THE KINGDOM OF SWEDEN
THE HIGH CONTRACTING PARTIES,
RECALLING that the Kingdom of Denmark, Ireland and the United Kingdom of Great Britain and Northern Ireland
acceded to the European Communities on 1 January 1973; that the Hellenic Republic acceded to the European
Communities on 1 January 1981; that the Kingdom of Spain and the Portuguese Republic acceded to the European
Communities on 1 January 1986; that the Republic of Austria, the Republic of Finland and the Kingdom of Sweden
acceded to the European Communities and to the European Union established by the Treaty on European Union on
1 January 1995;
CONSIDERING THAT Article IV‑437(2) of the Constitution provides that the Treaties concerning the accessions
referred to above shall be repealed;
CONSIDERING THAT certain provisions appearing in those Accession Treaties and in the Acts annexed thereto remain
relevant; and that Article IV‑437(2) of the Constitution provides that such provisions must be set out or referred to in a
Protocol, so that they remain in force and that their legal effects are preserved;
WHEREAS the provisions in question require the technical adjustments necessary to bring them into line with the
Constitution without altering their legal effect,
HAVE AGREED upon the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe and the Treaty establishing the European Atomic Energy Community:
TITLE I
COMMON PROVISIONS
Article 1
The rights and obligations resulting from the Accession Treaties referred to in Article IV‑437(2)(a) to
(d) of the Constitution took effect, under the conditions laid down in those Treaties, on the following
dates:
(a) 1 January 1973, for the Treaty concerning the accession of the Kingdom of Denmark, Ireland and
the United Kingdom of Great Britain and Northern Ireland;
(b) 1 January 1981, for the Treaty concerning the accession of the Hellenic Republic;
(c) 1 January 1986, for the Treaty concerning the accession of the Kingdom of Spain and the
Portuguese Republic;954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 275
Treaty establishing a Constitution for Europe
275
(d) 1 January 1995, for the Treaty concerning the accession of the Republic of Austria, the Republic
of Finland and the Kingdom of Sweden.
Article 2
1. The acceding States referred to in Article 1 shall be required to accede to the following
agreements or conventions concluded before their respective accessions, insofar as such agreements
or conventions are still in force:
(a) agreements or conventions concluded between the other Member States which are based on the
Treaty establishing the European Community, the Treaty establishing the European Atomic
Energy Community or the Treaty on European Union, or which are inseparable from the
attainment of the objectives of those Treaties, or which relate to the functioning of the
Communities or of the Union or which are connected with the activities thereof;
(b) agreements or conventions concluded by the other Member States, acting jointly with the
European Communities, with one or more third States or with an international organisation, and
the agreements which are related to those agreements or conventions. The Union and the other
Member States shall assist the acceding States referred to in Article 1 in this respect.
2. The acceding States referred to in Article 1 shall take appropriate measures, where necessary, to
adjust their position in relation to international organisations, and in relation to those international
agreements to which the Union or the European Atomic Energy Community or other Member States
are also parties, to the rights and obligations arising from their accession.
Article 3
Provisions of the Acts of Accession, as interpreted by the Court of Justice of the European
Communities and the Court of First Instance, the purpose or effect of which is to repeal or amend,
otherwise than as a transitional measure, acts adopted by the institutions, bodies, offices or agencies
of the European Communities or of the European Union established by the Treaty on European
Union shall remain in force subject to the second paragraph.
The provisions referred to in the first paragraph shall have the same status in law as the acts which
they repeal or amend and shall be subject to the same rules as those acts.
Article 4
The texts of the acts of the institutions, bodies, offices and agencies of the European Communities or
of the European Union established by the Treaty on European Union which were adopted before the
accessions referred to in Article 1 and which were subsequently drawn up successively in the English
and Danish languages, in the Greek language, in the Spanish and Portuguese languages, and in the
Finnish and Swedish languages, shall be authentic from the date of the respective accessions referred
to in Article 1, under the same conditions as the texts drawn up and authentic in the other languages.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 276
276
Part IV
Article 5
A European law of the Council may repeal the transitional provisions set out in this Protocol, when
they are no longer applicable. The Council shall act unanimously after consulting the European
Parliament.
TITLE II
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS OF ACCESSION OF THE
KINGDOM OF DENMARK, IRELAND AND THE UNITED KINGDOM OF GREAT BRITAIN
AND NORTHERN IRELAND
SECTION 1
Provisions on Gibraltar
Article 6
1. Acts of the institutions relating to the products in Annex I to the Constitution and the products
subject, on importation into the Union, to specific rules as a result of the implementation of the
common agricultural policy, as well as the acts on the harmonisation of legislation of Member States
concerning turnover taxes, shall not apply to Gibraltar unless the Council adopts a European decision
which provides otherwise. The Council shall act unanimously on a proposal from the Commission.
2. The situation of Gibraltar defined in point VI of Annex II ( 1 ) to the Act concerning the
conditions of accession of the Kingdom of Denmark, Ireland and the United Kingdom of
Great Britain and Northern Ireland shall be maintained.
SECTION 2
Provisions on the Faroe Islands
Article 7
Danish nationals resident in the Faroe Islands shall be considered to be nationals of a Member State
within the meaning of the Constitution only from the date on which the Constitution becomes
applicable to those islands.
( 1 )
OJ L 73, 27.3.1972, p. 47.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 277
Treaty establishing a Constitution for Europe
277
SECTION 3
Provisions on the Channel Islands and the Isle of Man
Article 8
1. The Union rules on customs matters and quantitative restrictions, in particular customs duties,
charges having equivalent effect and the Common Customs Tariff, shall apply to the Channel Islands
and the Isle of Man under the same conditions as they apply to the United Kingdom.
2. In respect of agricultural products and products processed therefrom which are the subject of a
special trade regime, the levies and other import measures laid down in Union rules and applicable by
the United Kingdom shall be applied to third countries.
Such provisions of Union rules as are necessary to allow free movement and observance of normal
conditions of competition in trade in these products shall also be applicable.
The Council, on a proposal from the Commission, shall adopt the European regulations or decisions
establishing the conditions under which the provisions referred to in the first and second
subparagraphs shall be applicable to these territories.
Article 9
The rights enjoyed by Channel Islanders or Manxmen in the United Kingdom shall not be affected by
Union law. However, such persons shall not benefit from provisions of Union law relating to the free
movement of persons and services.
Article 10
The provisions of the Treaty establishing the European Atomic Energy Community applicable to
persons or undertakings within the meaning of Article 196 of that Treaty shall apply to those persons
or undertakings when they are established in the territories referred to in Article 8 of this Protocol.
Article 11
The authorities of the territories referred to in Article 8 shall apply the same treatment to all natural
and legal persons of the Union.
Article 12
If, during the application of the arrangements defined in this Section, difficulties appear on either side
in relations between the Union and the territories referred to in Article 8, the Commission shall
without delay propose to the Council such safeguard measures as it believes necessary, specifying
their terms and conditions of application.
The Council shall adopt the appropriate European regulations or decisions within one month.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 278
278
Part IV
Article 13
In this Section, Channel Islander or Manxman shall mean any British citizen who holds that
citizenship by virtue of the fact that he, a parent or grandparent was born, adopted, naturalised or
registered in the island in question; but such a person shall not for this purpose be regarded as a
Channel Islander or Manxman if he, a parent or a grandparent was born, adopted, naturalised or
registered in the United Kingdom. Nor shall he be so regarded if he has at any time been ordinarily
resident in the United Kingdom for five years.
The administrative arrangements necessary to identify these persons will be notified to the
Commission.
SECTION 4
Provisions on the implementation of the policy of industrialisation
and economic development in Ireland
Article 14
The Member States take note of the fact that the Government of Ireland has embarked upon the
implementation of a policy of industrialisation and economic development designed to align the
standards of living in Ireland with those of the other Member States and to eliminate
underemployment while progressively evening out regional differences in levels of development.
They recognise it to be in their common interest that the objectives of this policy be so attained and
agree to recommend to this end that the institutions implement all the means and procedures laid
down by the Constitution, particularly by making adequate use of the Union resources intended for
the realisation of its objectives.
The Member States recognise in particular that, in the application of Articles III‑167 and III‑168 of
the Constitution, it will be necessary to take into account the objectives of economic expansion and
the raising of the standard of living of the population.
SECTION 5
Provisions on the exchange of information with Denmark in the field
of nuclear energy
Article 15
1. From 1 January 1973, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of Denmark, which shall give it limited
distribution within its territory under the conditions laid down in that Article.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 279
Treaty establishing a Constitution for Europe
279
2. From 1 January 1973, Denmark shall place at the disposal of the European Atomic Energy
Community an equivalent volume of information in the sectors specified in paragraph 3. This
information shall be set forth in detail in a document transmitted to the Commission. The
Commission shall communicate this information to Community undertakings under the conditions
laid down in Article 13 of the Treaty establishing the European Atomic Energy Community.
3. The sectors in which Denmark shall make information available to the European Atomic Energy
Community are as follows:
(a) DOR heavy water moderated organic cooled reactor;
(b) DT‑350, DK‑400 heavy water pressure vessel reactors;
(c) high temperature gas loop;
(d) instrumentation systems and special electronic equipment;
(e) reliability;
(f) reactor physics, reactor dynamics and heat exchange;
(g) in‑pile testing of materials and equipment.
4. Denmark shall undertake to supply the European Atomic Energy Community with any
information complementary to the reports which it shall communicate, in particular during visits by
European Atomic Energy Community personnel or personnel from the Member States to the
Risö Centre, under conditions to be determined by mutual agreement in each case.
Article 16
1. In those sectors in which Denmark places information at the disposal of the European Atomic
Energy Community, the competent authorities shall grant upon request licences on commercial
terms to Member States, persons and undertakings of the Community where they possess exclusive
rights to patents filed in Member States and insofar as they have no obligation or commitment in
respect of third parties to grant or offer to grant an exclusive or partially exclusive licence to the
rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, Denmark shall encourage and
facilitate the granting of sublicences on commercial terms to Member States, persons and
undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 280
280
Part IV
SECTION 6
Provisions on the exchange of information with Ireland in the field of nuclear energy
Article 17
1. From 1 January 1973, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of Ireland, which shall give it limited distribution
within its territory under the conditions laid down in that Article.
2. From 1 January 1973, Ireland shall place at the disposal of the European Atomic Energy
Community information obtained in the nuclear field in Ireland, which is given limited distribution,
insofar as strictly commercial applications are not involved. The Commission shall communicate this
information to Community undertakings under the conditions laid down in Article 13 of the Treaty
establishing the European Atomic Energy Community.
3. The information referred to in paragraphs 1 and 2 shall mainly concern studies for the
development of a power reactor and work on radioisotopes and their application in medicine,
including the problems of radiation protection.
Article 18
1. In those sectors in which Ireland places information at the disposal of the European Atomic
Energy Community, the competent authorities shall grant upon request licences on commercial
terms to Member States, persons and undertakings of the Community where they possess exclusive
rights to patents filed in Member States and insofar as they have no obligation or commitment in
respect of third parties to grant or offer to grant an exclusive or partially exclusive licence to the
rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, Ireland shall encourage and
facilitate the granting of sublicences on commercial terms to Member States, persons and
undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
SECTION 7
Provisions on the exchange of information with the United Kingdom in the field
of nuclear energy
Article 19
1. From 1 January 1973, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 281
Treaty establishing a Constitution for Europe
281
Energy Community, shall be placed at the disposal of the United Kingdom, which shall give it limited
distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1973, the United Kingdom shall place at the disposal of the European Atomic
Energy Community an equivalent volume of information in the sectors set out in the list contained in
the Annex ( 1 ) to Protocol No 28 to the Act concerning the conditions of accession of the Kingdom
of Denmark, Ireland and the United Kingdom of Great Britain and Northern Ireland. This
information shall be set forth in detail in a document transmitted to the Commission. The
Commission shall communicate this information to Community undertakings under the conditions
laid down in Article 13 of the Treaty establishing the European Atomic Energy Community.
3. In view of the European Atomic Energy Community's greater interest in certain sectors, the
United Kingdom shall lay special emphasis on the transmission of information in the following
sectors:
(a) fast reactor research and development (including safety);
(b) fundamental research (applicable to reactor types);
(c) reactor safety (other than fast reactors);
(d) metallurgy, steel, zirconium alloys and concrete;
(e) compatibility of structural materials;
(f) experimental fuel fabrication;
(g) thermohydrodynamics;
(h) instrumentation.
Article 20
1. In those fields in which the United Kingdom places information at the disposal of the European
Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in the Member States of the Community and insofar as they
have no obligation or commitment in respect of third parties to grant or offer to grant an exclusive
or partially exclusive licence to the rights in these patents.
( 1 )
OJ L 73, 27.3.1972, p. 84.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 282
282
Part IV
2. Where an exclusive or partially exclusive licence has been granted, the United Kingdom shall
encourage and facilitate the granting of sublicences on commercial terms to the Member States,
persons and undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
TITLE III
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS
OF ACCESSION OF THE HELLENIC REPUBLIC
SECTION 1
Provisions on the granting by Greece of exemption from customs duties
on the import of certain goods
Article 21
Article III‑151 of the Constitution shall not prevent the Hellenic Republic from maintaining measures
of exemption granted before 1 January 1979 pursuant to:
(a) Law No 4171/61 (General measures to aid development of the country's economy),
(b) Decree Law No 2687/53 (Investment and protection of foreign capital),
(c) Law No 289/76 (Incentives with a view to promoting the development of frontier regions and
governing all pertinent questions),
until the expiry of the agreements concluded by the Hellenic Government with those persons
benefiting from these measures.
SECTION 2
Provisions on taxation
Article 22
The acts listed in point II.2 of Annex VIII ( 1 ) to the Act concerning the conditions of accession of the
Hellenic Republic shall apply in respect of the Hellenic Republic under the conditions laid down in
that Annex, with the exception of the references to points 9 and 18(b) thereof.
( 1 )
OJ L 291, 19.11.1979, p. 163.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 283
Treaty establishing a Constitution for Europe
283
SECTION 3
Provisions on cotton
Article 23
1. This Section concerns cotton, not carded or combed, falling within subheading 5201 00 of the
Combined Nomenclature.
2.
A system shall be introduced in the Union particularly to:
(a) support the production of cotton in regions of the Union where it is important for the
agricultural economy,
(b) permit the producers concerned to earn a fair income,
(c) stabilise the market by structural improvements at the level of supply and marketing.
3.
The system referred to in paragraph 2 shall include the grant of an aid to production.
4. In order to allow cotton producers to concentrate supply and to adapt production to market
requirements, a system shall be introduced to encourage the formation of producer groups and
federations of such groups.
This system shall provide for the grant of aids with a view to providing incentives for the formation
and facilitating the functioning of producer groups.
The only groups that may benefit from this system must:
(a) be formed on the initiative of the producers themselves,
(b) offer a sufficient guarantee for the duration and effectiveness of their action,
(c) be recognised by the Member State concerned.
5. The Union trading system with third countries shall not be affected. In this respect, in particular,
no measure restricting imports may be laid down.
6. A European law of the Council shall establish the adjustments necessary to the system
introduced pursuant to this Section.
The Council, on a proposal from the Commission, shall adopt the European regulations and
decisions establishing the general rules necessary for implementing the provisions of this Section.
The Council shall act after consulting the European Parliament.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 284
284
Part IV
SECTION 4
Provisions on the economic and industrial development of Greece
Article 24
The Member States take note of the fact that the Hellenic Government has embarked upon the
implementation of a policy of industrialisation and economic development designed to align the
standards of living in Greece with those of the other Member States and to eliminate
underemployment while progressively evening out regional differences in levels of development.
They recognise it to be in their common interest that the objectives of this policy be so attained.
To this end, the institutions shall implement all the means and procedures laid down by the
Constitution, particularly by making adequate use of the Union resources intended for the realisation
of its objectives.
In particular, in the application of Articles III‑167 and III‑168 of the Constitution, it will be necessary
to take into account the objectives of economic expansion and the raising of the standard of living of
the population.
SECTION 5
Provisions on the exchange of information with Greece in the field of nuclear energy
Article 25
1. From 1 January 1981, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of the Hellenic Republic, which shall give it limited
distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1981, the Hellenic Republic shall place at the disposal of the European Atomic
Energy Community information obtained in the nuclear field in Greece which is given limited
distribution, insofar as strictly commercial applications are not involved. The Commission shall
communicate this information to Community undertakings under the conditions laid down in
Article 13 of the Treaty establishing the European Atomic Energy Community.
3.
The information referred to in paragraphs 1 and 2 shall mainly concern:
(a) studies on the application of radioisotopes in the following fields: medicine, agriculture,
entomology and environmental protection,
(b) the application of nuclear technology to archeometry,
(c) the development of electronic medical apparatus,
(d) the development of methods of radioactive ore prospecting.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 285
Treaty establishing a Constitution for Europe
285
Article 26
1. In those sectors in which the Hellenic Republic places information at the disposal of the
European Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in Member States of the Community and insofar as they have
no obligation or commitment in respect of third parties to grant or offer to grant an exclusive or
partially exclusive licence to the rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, the Hellenic Republic shall
encourage and facilitate the granting of sublicences on commercial terms to Member States, persons
and undertakings of the European Atomic Energy Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
TITLE IV
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS OF ACCESSION
OF THE KINGDOM OF SPAIN AND THE PORTUGUESE REPUBLIC
SECTION 1
Financial provisions
Article 27
The own resources accruing from value added tax shall be calculated and checked as if the Canary
Islands and Ceuta and Melilla were included in the territorial field of application of Sixth Council
Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States
relating to turnover taxes — Common system of value added tax: uniform basis of assessment.
SECTION 2
Provisions on patents
Article 28
The provisions of Spanish national law relating to the burden of proof, which were adopted under
paragraph 2 of Protocol No 8 to the Act concerning the conditions of accession of the Kingdom of
Spain and the Portuguese Republic, shall not apply if the infringement proceedings are brought
against the holder of another process patent for the manufacture of a product identical to that954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 286
286
Part IV
obtained as the result of the patented process of the plaintiff, if that other patent was issued before
1 January 1986.
In cases where shifting the burden of proof does not apply, the Kingdom of Spain shall continue to
require the patent holder to adduce proof of infringement. In all these cases the Kingdom of Spain
shall apply a judicial procedure known as ‘distraint‑description’.
‘Distraint‑description’ means a procedure forming part of the system referred to in the first and
second paragraphs by which any person entitled to bring an action for infringement may, after
obtaining a court order, granted on his application, cause a detailed description to be made, at the
premises of the alleged infringer, by a bailiff assisted by experts, of the processes in question, in
particular by photocopying technical documents, with or without actual distraint. This court order
may order the payment of a security, intended to grant damages to the alleged infringer in case of
injury caused by the ‘distraint‑description’.
Article 29
The provisions of Portuguese national law relating to the burden of proof, which were adopted under
paragraph 2 of Protocol No 19 to the Act concerning the conditions of accession of the Kingdom of
Spain and the Portuguese Republic, shall not apply if the infringement proceedings are brought
against the holder of another process patent for the manufacture of a product identical to that
obtained as the result of the patented process of the plaintiff, if that other patent was issued before
1 January 1986.
In cases where shifting the burden of proof does not apply, the Portuguese Republic shall continue to
require the patent holder to adduce proof of infringement. In all these cases, the Portuguese Republic
shall apply a judicial procedure known as ‘distraint‑description’.
‘Distraint‑description’ means a procedure forming part of the system referred to in the first and
second paragraphs by which any person entitled to bring an action for infringement may, after
obtaining a court order, granted on his application, cause a detailed description to be made, at the
premises of the alleged infringer, by a bailiff assisted by experts, of the processes in question, in
particular by photocopying technical documents, with or without actual distraint. This court order
may order the payment of a security, intended to grant damages to the alleged infringer in case of
injury caused by the ‘distraint‑description’.
SECTION 3
Provisions on the mechanism for additional responsibilities within the framework
of fisheries agreements concluded by the Union with third countries
Article 30
1. A specific system is hereby established for the execution of operations carried out as a
complement to fishing activities undertaken by vessels flying the flag of a Member State in waters
falling under the sovereignty or within the jurisdiction of a third country within the framework of
responsibilities created under fisheries agreements concluded by the Union with the third countries in
question.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 287
Treaty establishing a Constitution for Europe
287
2. Operations considered likely to occur by way of addition to fishing activities subject to the
conditions and within the limits referred to in paragraphs 3 and 4 relate to:
(a) the processing, in the territory of the third country concerned, of fishery products caught by
vessels flying the flag of a Member State in the waters of that third country in the course of
fishing activities carried out by virtue of a fisheries agreement, with a view to those products
being put on the Union market under tariff headings falling within Chapter 3 of the Common
Customs Tariff,
(b) the loading or transhipment aboard a vessel flying the flag of a Member State occurring within
the framework of activities provided for under such a fisheries agreement, of fishery products
falling within Chapter 3 of the Common Customs Tariff with a view to their transport and any
processing for the purpose of being put on the Union market.
3. The import into the Union of products having been the subject of the operations referred to in
paragraph 2 shall be carried out subject to suspension, in part or in whole, of the Common Customs
Tariff duties or subject to a special system of charges, under the conditions and within the limits of
additionality fixed annually in relation to the volume of fishing possibilities deriving from the
agreements in question and from their accompanying detailed rules.
4. European laws or framework laws shall lay down the general rules of application of this system
and in particular the criteria for fixing and apportioning the quantities concerned.
The detailed implementing rules of this system and the quantities concerned shall be adopted in
accordance with the procedure laid down in Article 37 of Regulation (EC) No 104/2000.
SECTION 4
Provisions on Ceuta and Melilla
Subsection 1
General provisions
Article 31
1. The Constitution and the acts of the institutions shall apply to Ceuta and to Melilla, subject to the
derogations referred to in paragraphs 2 and 3 and to the other provisions of this Section.
2. The conditions under which the provisions of the Constitution concerning the free movement of
goods, and the acts of the institutions concerning customs legislation and commercial policy, shall
apply to Ceuta and to Melilla are set out in Subsection 3 of this Section.
3. Without prejudice to the specific provisions of Article 32, the acts of the institutions concerning
the common agricultural policy and the common fisheries policy shall not apply to Ceuta or to
Melilla.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 288
288
4.
Part IV
At the request of the Kingdom of Spain, a European law or framework law of the Council may:
(a) include Ceuta and Melilla in the customs territory of the Union;
(b) define the appropriate measures aimed at extending to Ceuta and to Melilla the provisions of
Union law in force.
On a proposal from the Commission acting on its own initiative or at the request of a Member State,
the Council may adopt a European law or framework law adjusting the arrangements applicable to
Ceuta and to Melilla if necessary.
The Council shall act unanimously after consulting the European Parliament.
Subsection 2
Provisions relating to the Common Fisheries Policy
Article 32
1. Subject to paragraph 2 and without prejudice to Subsection 3, the common fisheries policy shall
not apply to Ceuta or to Melilla.
2. The Council, on a proposal from the Commission, shall adopt the European laws, framework
laws, regulations or decisions which:
(a) determine the structural measures which may be adopted in favour of Ceuta and Melilla;
(b) determine the procedures appropriate to take into consideration all or part of the interests of
Ceuta and Melilla when it adopts acts, case by case, with a view to the negotiations by the Union
aimed at the resumption or conclusion of fisheries agreements with third countries and to the
specific interests of Ceuta and Melilla within international conventions concerning fisheries, to
which the Union is a contracting party.
3. The Council, on a proposal from the Commission, shall adopt the European laws, framework
laws, regulations or decisions which determine, where appropriate, the possibilities and conditions of
mutual access to respective fishing zones and to the resources thereof. It shall act unanimously.
4. The European laws and framework laws referred to in paragraphs 2 and 3 shall be adopted after
consultation of the European Parliament.
Subsection 3
Provisions on free movement of goods, customs legislation and commercial policy
Article 33
1. Products originating in Ceuta or in Melilla and products coming from third countries imported
into Ceuta or into Melilla under the arrangements which are applicable there to them shall not be954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 289
Treaty establishing a Constitution for Europe
289
deemed, when released for free circulation in the customs territory of the Union, to be goods
fulfilling the conditions of paragraphs 1 to 3 of Article III‑151 of the Constitution.
2.
The customs territory of the Union shall not include Ceuta and Melilla.
3. Except where otherwise provided for in this Subsection, the acts of the institutions regarding
customs legislation for foreign trade shall apply under the same conditions to trade between the
customs territory of the Union, on the one hand, and Ceuta and Melilla, on the other.
4. Except where otherwise provided for in this Subsection, the acts of the institutions regarding the
common commercial policy, be they autonomous or enacted by agreement, directly linked to the
import or export of goods, shall not be applicable to Ceuta or to Melilla.
5. Except where otherwise provided for in this Title, the Union shall apply in its trade with Ceuta
and Melilla, for products falling within Annex I to the Constitution, the general arrangements which
it applies in its foreign trade.
Article 34
Subject to Article 35, customs duties on the import into the customs territory of the Union of
products originating in Ceuta or in Melilla shall be abolished.
Article 35
1. Fishery products falling within headings 0301, 0302, 0303, 1604, 1605 and subheadings
0511 91 and 2301 20 of the Common Customs Tariff and originating in Ceuta or in Melilla, shall,
within the limit of tariff quotas calculated by product and on the average quantities actually disposed
of during 1982, 1983 and 1984, qualify for exemption from customs duties throughout the customs
territory of the Union.
The release for free circulation of products imported into the customs territory of the Union, under
these tariff quotas, shall be subject to compliance with the rules laid down by the common
organisation of markets and in particular with respect to reference prices.
2. The Council, on a proposal from the Commission, shall each year adopt European regulations or
decisions opening and allocating tariff quotas in accordance with the detailed rules laid down in
paragraph 1.
Article 36
1. Where application of Article 34 could lead to a substantial increase in the import of certain
products originating in Ceuta or in Melilla such as might prejudice Union producers, the Council, on
a proposal from the Commission, may adopt European regulations or decisions to subject the access
of these products to the customs territory of the Union to special conditions.
2. Where, because the common commercial policy and the Common Customs Tariff are not
applied to the import of raw materials or intermediate products into Ceuta or into Melilla, imports of
a product originating in Ceuta or in Melilla cause, or may cause, serious injury to a producer activity954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 290
290
Part IV
exercised in one or more Member States, the Commission, at the request of a Member State or on its
own initiative, may take the appropriate measures.
Article 37
The customs duties on import into Ceuta and into Melilla of products originating in the customs
territory of the Union, and charges having equivalent effect, shall be abolished.
Article 38
The customs duties and charges having an effect equivalent to such duties and the trade arrangements
applied on the import into Ceuta and into Melilla of goods coming from a third country may not be
less favourable than those applicable by the Union in accordance with its international commitments
or its preferential arrangements with regard to such third country, providing that the same
third country grants, to imports from Ceuta and from Melilla, the same treatment as that which it
grants to the Union. However, the arrangements applied to imports into Ceuta and into Melilla with
regard to goods coming from such third country may not be more favourable than those applied
with regard to the imports of products originating in the customs territory of the Union.
Article 39
The Council, on a proposal from the Commission, shall adopt European regulations or decisions
laying down the rules for the application of this Subsection and in particular the rules of origin
applicable to trade, as referred to in Articles 34, 35 and 37, including the provisions concerning the
identification of originating products and the control of origin.
The rules will include, in particular, provisions on marking and/or labelling of products, on the
conditions of registration of vessels, on the application of the rule on mixed origin for fishery
products, and also provisions enabling the origin of products to be determined.
SECTION 5
Provisions on the regional development of Spain
Article 40
The Member States take note of the fact that the Spanish Government has embarked upon the
implementation of a policy of regional development designed in particular to stimulate economic
growth in the less‑developed regions and areas of Spain.
They recognise it to be in their common interest that the objectives of this policy be attained.
They agree, in order to help the Spanish Government to accomplish this task, to recommend that the
institutions use all the means and procedures laid down by the Constitution, particularly by making
adequate use of the Union resources intended for the realisation of its objectives.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 291
Treaty establishing a Constitution for Europe
291
The Member States recognise in particular that, in the application of Articles III‑167 and III‑168 of
the Constitution, it will be necessary to take into account the objectives of economic expansion and
the raising of the standard of living of the population of the less‑developed regions and areas of
Spain.
SECTION 6
Provisions on the economic and industrial development of Portugal
Article 41
The Member States take note of the fact that the Portuguese Government has embarked upon the
implementation of a policy of industrialisation and economic development designed to align the
standard of living in Portugal with that of the other Member States and to eliminate
underemployment while progressively evening out regional differences in levels of development.
They recognise it to be in their common interest that the objectives of this policy be attained.
They agree to recommend to this end that the institutions use all the means and procedures laid
down by the Constitution, particularly by making adequate use of the Union resources intended for
the realisation of its objectives.
The Member States recognise in particular that, in the application of Articles III-167 and III-168 of
the Constitution, it will be necessary to take into account the objectives of economic expansion and
the raising of the standard of living of the population.
SECTION 7
Provisions on the exchange of information with the Kingdom of Spain
in the field of nuclear energy
Article 42
1. From 1 January 1986, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of the Kingdom of Spain, which shall give it
limited distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1986, the Kingdom of Spain shall place at the disposal of the European Atomic
Energy Community information obtained in the nuclear field in Spain which is given limited
distribution, insofar as strictly commercial applications are not involved. The Commission shall
communicate this information to Community undertakings under the conditions laid down in
Article 13 of the Treaty establishing the European Atomic Energy Community.
3.
The information referred to in paragraphs 1 and 2 shall mainly concern:
(a) nuclear physics (low- and high-energy);
(b) radiation protection;954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 292
292
Part IV
(c) isotope applications, in particular those of stable isotopes;
(d) research reactors and relevant fuels;
(e) research into the field of the fuel cycle (more especially the mining and processing of low‑grade
uranium ore; optimisation of fuel elements for power reactors).
Article 43
1. In those sectors in which the Kingdom of Spain places information at the disposal of the
European Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in Member States and insofar as they have no obligation or
commitment in respect of third parties to grant or offer to grant an exclusive or partially exclusive
licence to the rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, the Kingdom of Spain shall
encourage and facilitate the granting of sublicences on commercial terms to Member States, persons
and undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
SECTION 8
Provisions on the exchange of information with the Portuguese Republic in the field
of nuclear energy
Article 44
1. From 1 January 1986, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of the Portuguese Republic, which shall give it
limited distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1986, the Portuguese Republic shall place at the disposal of the European
Atomic Energy Community information obtained in the nuclear field in Portugal which is given
limited distribution, insofar as strictly commercial applications are not involved. The Commission
shall communicate this information to Community undertakings under the conditions laid down in
Article 13 of the Treaty establishing the European Atomic Energy Community.
3.
The information referred to in paragraphs 1 and 2 shall mainly concern:
(a) reactor dynamics;
(b) radiation protection;954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 293
Treaty establishing a Constitution for Europe
293
(c) application of nuclear measuring techniques (in the industrial, agricultural, archaeological and
geological fields);
(d) atomic physics (effective measuring of cross sections, pipeline techniques);
(e) extractive metallurgy of uranium.
Article 45
1. In those sectors in which the Portuguese Republic places information at the disposal of the
European Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in Member States and insofar as they have no obligation or
commitment in respect of third parties to grant or offer to grant an exclusive or partially exclusive
licence to the rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, the Portuguese Republic shall
encourage and facilitate the granting of sublicences on commercial terms to Member States, persons
and undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
TITLE V
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS OF ACCESSION OF THE
REPUBLIC OF AUSTRIA, THE REPUBLIC OF FINLAND AND THE KINGDOM OF SWEDEN
SECTION 1
Financial provisions
Article 46
Own resources accruing from value added tax shall be calculated and checked as though
the Åland Islands were included in the territorial scope of Sixth Council Directive 77/388/EEC of
17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes —
Common system of value added tax: uniform basis of assessment.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 294
294
Part IV
SECTION 2
Provisions on agriculture
Article 47
Where there are serious difficulties resulting from accession which remain after full utilisation of
Article 48 and of the other measures resulting from the rules existing in the Union, the Commission
may adopt a European decision authorising Finland to grant national aids to producers so as to
facilitate their full integration into the common agricultural policy.
Article 48
1. The Commission shall adopt European decisions authorising Finland and Sweden to grant
long‑term national aids with a view to ensuring that agricultural activity is maintained in specific
regions. These regions should cover the agricultural areas situated to the north of the 62nd Parallel
and some adjacent areas south of that parallel affected by comparable climatic conditions rendering
agricultural activity particularly difficult.
2. The regions referred to in paragraph 1 shall be determined by the Commission, taking into
consideration in particular:
(a) the low population density;
(b) the portion of agricultural land in the overall surface area;
(c) the portion of agricultural land devoted to arable crops intended for human consumption, in the
agricultural surface area used.
3. The national aids provided for in paragraph 1 may be related to physical factors of production,
such as hectares of agricultural land or heads of animal taking account of the relevant limits laid
down in the common organisations of the market, as well as the historical production patterns of
each farm, but must not:
(a) be linked to future production;
(b) or lead to an increase in production or in the level of overall support recorded during a reference
period preceding 1 January 1995, to be determined by the Commission.
These aids may be differentiated by region.
These aids must be granted in particular in order to:
(a) maintain traditional primary production and processing naturally suited to the climatic
conditions of the regions concerned;
(b) improve the structures for the production, marketing and processing of agricultural products;954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 295
Treaty establishing a Constitution for Europe
295
(c) facilitate the disposal of the said products;
(d) ensure that the environment is protected and the countryside preserved.
Article 49
1. The aids provided for in Articles 47 and 48 and any other national aid subject to Commission
authorisation under this Title shall be notified to the Commission. They may not be applied until
such authorisation has been given.
2. As regards the aids provided for in Article 48, the Commission shall submit to the Council every
five years as from 1 January 1996 a report on:
(a) the authorisations granted;
(b) the results of the aid granted under such authorisations.
In preparation for drawing up such reports, Member States in receipt of such authorisations shall
supply the Commission in good time with information on the effects of the aids granted, illustrating
the development noted in the agricultural economy of the regions in question.
Article 50
In the field of the aids provided for in Articles III‑167 and III‑168 of the Constitution:
(a) among the aids applied in Austria, Finland and Sweden prior to 1 January 1995, only those
notified to the Commission by 30 April 1995 will be deemed to be existing aids within the
meaning of Article III‑168(1) of the Constitution;
(b) existing aids and plans intended to grant or alter aids which were notified to the Commission
prior to 1 January 1995 shall be deemed to have been notified on that date.
Article 51
1. Unless otherwise stipulated in specific cases, the Council, on a proposal from the Commission,
shall adopt the necessary European regulations or decisions to implement this Section.
2. A European law of the Council may make the adaptations to the provisions appearing in this
Section which may prove necessary as a result of a modification in Union law. The Council shall act
unanimously after consulting the European Parliament.
Article 52
1. If transitional measures are necessary to facilitate the transition from the existing regime in
Austria, Finland and Sweden to that resulting from application of the common organisation of the
markets under the conditions set out in the Act concerning the conditions of accession of the
Republic of Austria, the Republic of Finland and the Kingdom of Sweden, such measures shall be
adopted in accordance with the procedure laid down in Article 38 of Regulation No 136/66/EEC or,
as appropriate, in the corresponding Articles of the other Regulations on the common organisation954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 296
296
Part IV
of agricultural markets. These measures may be taken during a period expiring on 31 December 1997
and their application shall be limited to that date.
2. A European law of the Council may extend the period referred to in paragraph 1. The Council
shall act unanimously after consulting the European Parliament.
Article 53
Articles 51 and 52 shall be applicable to fishery products.
SECTION 3
Provisions on transitional measures
Article 54
The Acts listed in points VII.B.I, VII.D.1, VII.D.2.c, IX.2.b, c, f, g, h, i, j, l, m, n, x, y, z and aa, and X.a, b
and c of Annex XV ( 1 ) to the Act concerning the conditions of accession of the Republic of Austria,
the Republic of Finland and the Kingdom of Sweden shall apply in respect of Austria, Finland and
Sweden under the conditions laid down in that Annex.
With regard to point IX.2.x of Annex XV referred to in the first paragraph, the reference to the
provisions of the Treaty establishing the European Community, in particular to Articles 90 and 91
thereof, must be understood as referring to the provisions of the Constitution, in particular to
Article III‑170(1) and (2) thereof.
SECTION 4
Provisions on the applicability of certain acts
Article 55
1. Any individual exemption decisions taken and negative clearance decisions taken before
1 January 1995 under Article 53 of the Agreement on the European Economic Area (EEA) or
Article 1 of Protocol 25 to that Agreement, whether by the Surveillance Authority of the European
Free Trade Association (EFTA) or the Commission, and which concern cases which fall under
Article 81 of the Treaty establishing the European Community as a result of accession shall remain
valid for the purposes of Article III‑161 of the Constitution until the time limit specified therein
expires or until the Commission adopts a duly motivated European decision to the contrary, in
accordance with Union law.
2. All decisions taken by the EFTA Surveillance Authority before 1 January 1995 pursuant to
Article 61 of the EEA Agreement and which fall under Article 87 of the Treaty establishing the
European Community as a result of accession shall remain valid with respect to Article III‑167 of the
Constitution unless the Commission adopts a European decision to the contrary pursuant to
( 1 )
OJ C 241, 29.8.1994, p. 322.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 297
Treaty establishing a Constitution for Europe
297
Article III‑168 of the Constitution. This paragraph shall not apply to decisions subject to the
proceedings provided for in Article 64 of the EEA Agreement.
3. Without prejudice to paragraphs 1 and 2, the decisions taken by the EFTA Surveillance Authority
remain valid after 1 January 1995 unless the Commission takes a duly motivated decision to the
contrary in accordance with Union law.
SECTION 5
Provisions on the Åland Islands
Article 56
The provisions of the Constitution shall not preclude the application of the existing provisions in
force on 1 January 1994 on the Åland islands on:
(a) restrictions, on a non-discriminatory basis, on the right of natural persons who do not enjoy
hembygdsrätt/kotiseutuoikeus (regional citizenship) in Åland, and for legal persons, to acquire
and hold real property on the Åland islands without permission by the competent authorities of
the Åland islands;
(b) restrictions, on a non-discriminatory basis, on the right of establishment and the right to provide
services by natural persons who do not enjoy hembygdsrätt/kotiseutuoikeus (regional citizenship)
in Åland, or by legal persons without permission by the competent authorities of the Åland
islands.
Article 57
1. The territory of the Åland Islands — being considered as a third territory, as defined in the third
indent of Article 3(1) of Council Directive 77/388/EEC, and as a national territory falling outside the
field of application of the excise harmonisation directives as defined in Article 2 of Council Directive
92/12/EEC — shall be excluded from the territorial application of Union law in the fields of
harmonisation of the laws of the Member States on turnover taxes and on excise duties and other
forms of indirect taxation.
This paragraph shall not apply to the provisions of Council Directive 69/335/EEC relating to capital
duty.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 298
298
Part IV
2. The derogation provided for in paragraph 1 is aimed at maintaining a viable local economy in
the islands and shall not have any negative effects on the interests of the Union nor on its common
policies. If the Commission considers that the provisions in paragraph 1 are no longer justified,
particularly in terms of fair competition or own resources, it shall submit appropriate proposals to
the Council, which shall adopt the necessary acts in accordance with the pertinent articles of the
Constitution.
Article 58
The Republic of Finland shall ensure that the same treatment applies to all natural and legal persons
of the Member States in the Åland islands.
Article 59
The provisions of this Section shall apply in the light of the Declaration on the Åland Islands, which
incorporates, without altering its legal effect, the wording of the preamble to Protocol No 2 to the Act
concerning the conditions of accession of the Republic of Austria, the Republic of Finland and the
Kingdom of Sweden.
SECTION 6
Provisions on the Sami people
Article 60
Notwithstanding the provisions of the Constitution, exclusive rights to reindeer husbandry within
traditional Sami areas may be granted to the Sami people.
Article 61
This Section may be extended to take account of any further development of exclusive Sami rights
linked to their traditional means of livelihood. A European law of the Council may make the
necessary amendments to this Section. The Council shall act unanimously after consulting the
European Parliament and the Committee of the Regions.
Article 62
The provisions of this Section shall apply in the light of the Declaration on the Sami people, which
incorporates, without altering its legal effect, the wording of the preamble to Protocol 3 to the Act
concerning the conditions of accession of the Republic of Austria, the Republic of Finland and the
Kingdom of Sweden.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 299
Treaty establishing a Constitution for Europe
299
SECTION 7
Special provisions in the framework of the Structural Funds in Finland and Sweden
Article 63
Areas covered by the objective of promoting the development and structural adjustment of regions
with an extremely low population density shall in principle represent or belong to regions at NUTS
level II with a population density of 8 persons per km 2 or less. Union assistance may, subject to the
requirement of concentration, also extend to adjacent and contiguous smaller areas fulfilling the same
population density criterion. The regions and areas referred to in this Article, are listed in Annex 1 ( 1 )
to Protocol 6 to the Act concerning the conditions of accession of the Republic of Austria, the
Republic of Finland and the Kingdom of Sweden.
SECTION 8
Provisions on rail and combined transport in Austria
Article 64
1.
For the purposes of this Section, the following definitions shall apply:
(a) ‘heavy goods vehicle’ shall mean any motor vehicle with a maximum authorised weight of over
7,5 tonnes registered in a Member State designed to carry goods or haul trailers, including
semi‑trailer tractor units, and trailers with a maximum authorised weight of over 7,5 tonnes and
hauled by a motor vehicle registered in a Member State with a maximum authorised weight of
7,5 tonnes or less;
(b) ‘combined transport’ shall mean the carriage of goods by heavy goods vehicles or loading units
which complete part of their journey by rail and either begin or end the journey by road,
whereby transit traffic may under no circumstances cross Austrian territory on its way to or from
a rail terminal by road alone.
2. Articles 65 to 71 shall apply to measures relating to the provision of rail and combined transport
crossing the territory of Austria.
Article 65
The Union and the Member States concerned shall, within the framework of their respective
competences, adopt and closely coordinate measures for the development and promotion of rail and
combined transport for the trans‑Alpine carriage of goods.
( 1 )
OJ C 241, 29.8.1994, p. 355.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 300
300
Part IV
Article 66
When establishing the guidelines provided for in Article III‑247 of the Constitution, the Union shall
ensure that the axes defined in Annex 1 ( 1 ) to Protocol 9 to the Act concerning the conditions of
accession of the Republic of Austria, the Republic of Finland and the Kingdom of Sweden form part
of the trans‑European networks for rail and combined transport and are furthermore identified as
projects of common interest.
Article 67
The Union and the Member States concerned shall, within the framework of their respective
competences, implement the measures listed in Annex 2 ( 2 ) to Protocol 9 to the Act concerning the
conditions of accession of the Republic of Austria, the Republic of Finland and the Kingdom of
Sweden.
Article 68
The Union and the Member States concerned shall use their best endeavours to develop and utilise
the additional railway capacity referred to in Annex 3 ( 3 ) to Protocol 9 to the Act concerning the
conditions of accession of the Republic of Austria, the Republic of Finland and the Kingdom of
Sweden.
Article 69
The Union and the Member States concerned shall take measures to enhance the provision of rail and
combined transport. Where appropriate, and subject to the provisions of the Constitution, such
measures shall be established in close consultation with railway companies and other railway service
providers. Priority should be given to those measures set out in the provisions of Union law on
railways and combined transport. In implementing any measures, particular attention shall be
attached to the competitiveness, effectiveness and cost transparency of rail and combined transport.
In particular, the Member States concerned shall endeavour to take such measures so as to ensure that
prices for combined transport are competitive with those for other modes of transport. Any aid
granted to these ends shall comply with Union law.
Article 70
The Union and the Member States concerned shall, in the event of a serious disturbance in rail transit,
such as a natural disaster, take all possible concerted action to maintain the flow of traffic. Priority
shall be given to sensitive loads, such as perishable foods.
( 1 ) OJ C 241, 29.8.1994, p. 364.
( 2 ) OJ C 241, 29.8.1994, p. 365.
( 3 ) OJ C 241, 29.8.1994, p. 367.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 301
Treaty establishing a Constitution for Europe
301
Article 71
The Commission, acting in accordance with the procedure laid down in Article 73(2), shall review the
operation of this Section.
Article 72
1. This Article shall apply to the carriage of goods by road on journeys carried out within the
territory of the Community.
2. For journeys which involve transit of goods by road through Austria, the regime established for
journeys on own account and for journeys for hire or reward under the First Council Directive of
23 July 1962 and Council Regulation (EEC) No 881/92 shall apply subject to the provisions of this
Article.
3.
Until 1 January 1998, the following provisions shall apply:
(a) The total of NOx emissions from heavy goods vehicles crossing Austria in transit shall be reduced
by 60 % in the period between 1 January 1992 and 31 December 2003, according to the table in
Annex 4.
(b) The reductions in total NOx emissions from heavy goods vehicles shall be administered
according to an ecopoints system. Under that system any heavy goods vehicle crossing Austria in
transit shall require a number of ecopoints equivalent to its NOx emissions (authorised under the
Conformity of Production (COP) value or type‑approval value). The method of calculation and
administration of such points is described in Annex 5.
(c) If the number of transit journeys in any year exceeds the reference figure established for 1991 by
more than 8 %, the Commission, acting in accordance with the procedure laid down in
Article 16, shall adopt appropriate measures in accordance with paragraph 3 of Annex 5.
(d) Austria shall issue and make available in good time the ecopoints cards required for the
administration of the ecopoints system, pursuant to Annex 5, for heavy goods vehicles crossing
Austria in transit.
(e) The ecopoints shall be distributed by the Commission among Member States in accordance with
provisions to be established in accordance with paragraph 7.
4. Before 1 January 1998, the Council, on the basis of a report by the Commission, shall review the
operation of provisions concerning transit of goods by road through Austria. The review shall take
place in conformity with basic principles of Community law, such as the proper functioning of the
internal market, in particular the free movement of goods and freedom to provide services,
protection of the environment in the interest of the Community as a whole, and traffic safety. Unless
the Council, acting unanimously on a proposal from the Commission and after consulting the
European Parliament, decides otherwise, the transitional period shall be extended to 1 January 2001,
during which the provisions of paragraph 3 shall apply.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 302
302
Part IV
5. Before 1 January 2001, the Commission, in cooperation with the European Environment
Agency, shall make a scientific study of the degree to which the objective concerning reduction of
pollution set out in paragraph 3(a) has been achieved. If the Commission concludes that this objective
has been achieved on a sustainable basis, the provisions of paragraph 3 shall cease to apply on
1 January 2001. If the Commission concludes that this objective has not been achieved on a
sustainable basis, the Council, acting in accordance with Article 75 of the EC Treaty, may adopt
measures, within a Community framework, which ensure equivalent protection of the environment,
in particular a 60 % reduction of pollution. If the Council does not adopt such measures, the
transitional period shall be automatically extended for a final period of three years, during which the
provisions of paragraph 3 shall apply.
6.
At the end of the transitional period, the Community acquis in its entirety shall be applied.
7. The Commission, acting in accordance with the procedure laid down in Article 16, shall adopt
detailed measures concerning the procedures relating to the ecopoints system, the distribution of
ecopoints and technical questions concerning the application of this Article, which shall enter into
force on the date of accession of Austria.
The measures referred to in the first subparagraph shall ensure that the factual situation for the
present Member States resulting from the application of Council Regulation (EEC) No 3637/92 and
of the Administrative Arrangement, signed on 23 December 1992, setting the date of entry into force
and the procedures for the introduction of the ecopoints system referred to in the Transit Agreement,
is maintained. All necessary efforts shall be made to ensure that the share of ecopoints allocated to
Greece takes sufficient account of Greek needs in this context.
Article 73
1.
The Commission shall be assisted by a Committee.
2. In cases where reference is made to this paragraph, Articles 3 and 7 of Decision 1999/468/EC
shall apply.
3.
The Committee shall adopt its Rules of Procedure.
SECTION 9
Provisions on the use of specific Austrian terms of the German language in the framework
of the European Union
Article 74
1. The specific Austrian terms of the German language contained in the Austrian legal order and
listed in the Annex ( 1 ) to Protocol No 10 to the Act concerning the conditions of accession of the
Republic of Austria, the Republic of Finland and the Kingdom of Sweden shall have the same status
( 1 )
OJ C 241, 29.8.1994, p. 370.954393_TRAITE_EN_301_350
12-01-2005
15:56
Treaty establishing a Constitution for Europe
Pagina 303
303
and may be used with the same legal effect as the corresponding terms used in Germany listed in that
Annex.
2. In the German language version of new legal acts the specific Austrian terms referred to in the
Annex to Protocol No 10 to the Act concerning the conditions of accession of the Republic of
Austria, the Republic of Finland and the Kingdom of Sweden shall be added in appropriate form to
the corresponding terms used in Germany.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 304
304
9.
Part IV
PROTOCOL ON THE TREATY AND THE ACT OF ACCESSION OF THE CZECH REPUBLIC,
THE REPUBLIC OF ESTONIA, THE REPUBLIC OF CYPRUS, THE REPUBLIC OF LATVIA,
THE REPUBLIC OF LITHUANIA, THE REPUBLIC OF HUNGARY, THE REPUBLIC OF MALTA,
THE REPUBLIC OF POLAND, THE REPUBLIC OF SLOVENIA AND THE SLOVAK REPUBLIC
THE HIGH CONTRACTING PARTIES,
RECALLING that the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the
Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia
and the Slovak Republic acceded to the European Communities and to the European Union established by the Treaty on
European Union on 1 May 2004;
CONSIDERING that Article IV‑437(2)(e) of the Constitution provides that the Treaty of 16 April 2003 concerning the
accessions referred to above shall be repealed;
CONSIDERING that many of the provisions of the Act annexed to that Treaty of Accession remain relevant; that Article
IV‑437(2) of the Constitution provides that those provisions must be set out or referred to in a Protocol, so that they
remain in force and their legal effects are preserved;
CONSIDERING that some of those provisions require the technical adjustments necessary to bring them into line with
the Constitution without altering their legal effect,
HAVE AGREED UPON the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe and to the Treaty establishing the European Atomic Energy Community:
PART ONE
PROVISIONS RELATING TO THE ACT OF ACCESSION OF 16 APRIL 2003
TITLE I
PRINCIPLES
Article 1
For the purposes of this Protocol:
(a) the expression ‘Act of Accession of 16 April 2003’ means the Act concerning the conditions of
accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of
Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of
Poland, the Republic of Slovenia and the Slovak Republic and the adjustments to the Treaties on
which the European Union is founded;954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 305
Treaty establishing a Constitution for Europe
305
(b) the expressions ‘Treaty establishing the European Community’ (EC Treaty) and ‘Treaty
establishing the European Atomic Energy Community’ (EAEC Treaty) mean those Treaties as
supplemented or amended by treaties or other acts which entered into force before 1 May 2004;
(c) the expression ‘Treaty on European Union’ (EU Treaty) means that Treaty as supplemented or
amended by treaties or other acts which entered into force before 1 May 2004;
(d) the expression ‘the Community’ means one or both of the Communities referred to in (b) as the
case may be;
(e) the expression ‘present Member States’ means the following Member States: the Kingdom of
Belgium, the Kingdom of Denmark, the Federal Republic of Germany, the Hellenic Republic, the
Kingdom of Spain, the French Republic, Ireland, the Italian Republic, the Grand Duchy of
Luxembourg, the Kingdom of the Netherlands, the Republic of Austria, the Portuguese Republic,
the Republic of Finland, the Kingdom of Sweden and the United Kingdom of Great Britain and
Northern Ireland;
(f) the expression ‘new Member States’ means the following Member States: the Czech Republic, the
Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the
Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and
the Slovak Republic.
Article 2
The rights and obligations resulting from the Treaty on the Accession of the Czech Republic, the
Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the
Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the
Slovak Republic, referred to in Article IV‑437(2)(e) of the Constitution, took effect, under the
conditions laid down in that Treaty, as from 1 May 2004.
Article 3
1. The provisions of the Schengen acquis integrated into the framework of the Union by the
Protocol annexed to the Treaty establishing a Constitution for Europe (hereinafter referred to as the
‘Schengen Protocol’) and the acts building upon it or otherwise related to it, listed in Annex I to the
Act of Accession of 16 April 2003, as well as any further such acts adopted before 1 May 2004, shall
be binding on and applicable in the new Member States from 1 May 2004.
2. Those provisions of the Schengen acquis as integrated into the framework of the Union and the
acts building upon it or otherwise related to it not referred to in paragraph 1, while binding on the
new Member States from 1 May 2004, shall apply in a new Member State only pursuant to a
European decision of the Council to that effect after verification in accordance with the applicable
Schengen evaluation procedures that the necessary conditions for the application of all parts of the
acquis concerned have been met in that new Member State.
The Council shall take its decision, after consulting the European Parliament, acting with the
unanimity of its members representing the Governments of the Member States in respect of which
the provisions referred to in the present paragraph have already been put into effect and of the
representative of the Government of the Member State in respect of which those provisions are to be954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 306
306
Part IV
put into effect. The members of the Council representing the Governments of Ireland and of the
United Kingdom of Great Britain and Northern Ireland shall take part in such a decision insofar as it
relates to the provisions of the Schengen acquis and the acts building upon it or otherwise related to it
in which these Member States participate.
3. The Agreements concluded by the Council under Article 6 of the Schengen Protocol shall be
binding on the new Member States from 1 May 2004.
4. The new Member States shall be required in respect of those conventions or instruments in the
field of justice and home affairs which are inseparable from the attainment of the objectives of the EU
Treaty:
(a) to accede to those which, by 1 May 2004, have been opened for signature by the present Member
States, and to those which have been drawn up by the Council in accordance with Title VI of the
EU Treaty and recommended to the Member States for adoption;
(b) to introduce administrative and other arrangements, such as those adopted by 1 May 2004 by the
present Member States or by the Council, to facilitate practical cooperation between the Member
States' institutions and organisations working in the field of justice and home affairs.
Article 4
Each of the new Member States shall participate in Economic and Monetary Union from 1 May 2004
as a Member State with a derogation within the meaning of Article III‑197 of the Constitution.
Article 5
1. The new Member States, which have acceded by the Act of Accession of 16 April 2003 to the
decisions and agreements adopted by the Representatives of the Governments of the Member States,
meeting within the Council, shall be required to accede to all other agreements concluded by the
present Member States relating to the functioning of the Union or connected with the activities
thereof.
2. The new Member States shall be required to accede to the conventions provided for in Article
293 of the EC Treaty and to those that are inseparable from the attainment of the objectives of the EC
Treaty, insofar as they are still in force, and also to the protocols on the interpretation of those
conventions by the Court of Justice of the European Communities, signed by the present Member
States, and to this end they shall be required to enter into negotiations with the present Member
States in order to make the necessary adjustments thereto.
Article 6
1. The new Member States shall be required to accede, under the conditions laid down in this
Protocol, to the agreements or conventions concluded or provisionally applied by the present
Member States and the Union or the European Atomic Energy Community, acting jointly, and to the
agreements concluded by those States which are related to those agreements or conventions.954393_TRAITE_EN_301_350
12-01-2005
15:56
Treaty establishing a Constitution for Europe
Pagina 307
307
The accession of the new Member States to the agreements or conventions mentioned in paragraph
4, as well as the agreements with Belarus, China, Chile, Mercosur and Switzerland which have been
concluded or signed by the Community and its present Member States jointly shall be agreed by the
conclusion of a protocol to such agreements or conventions between the Council, acting
unanimously on behalf of the Member States, and the third country or countries or international
organisation concerned. This procedure is without prejudice to the Union's and the European Atomic
Energy Community's own competences and does not affect the allocation of powers between the
Union and the European Atomic Energy Community and the Member States as regards the
conclusion of such agreements in the future or any other amendments not related to accession. The
Commission shall negotiate these protocols on behalf of the Member States on the basis of
negotiating directives approved by the Council, acting by unanimity, and in consultation with a
committee comprised of the representatives of the Member States. It shall submit a draft of the
protocols for conclusion to the Council.
2. Upon acceding to the agreements and conventions referred to in paragraph 1 the new Member
States shall acquire the same rights and obligations under those agreements and conventions as the
present Member States.
3. The new Member States shall be required to accede, under the conditions laid down in this
Protocol, to the Agreement on the European Economic Area ( 1 ), in accordance with Article 128 of
that Agreement.
4. As from 1 May 2004, and, where appropriate, pending the conclusion of the necessary protocols
referred to in paragraph 1, the new Member States shall apply the provisions of the Agreements
concluded by the present Member States and, jointly, the Community, with Algeria, Armenia,
Azerbaijan, Bulgaria, Croatia, Egypt, the former Yugoslav Republic of Macedonia, Georgia, Israel,
Jordan, Kazakhstan, Kyrgyzstan, Lebanon, Mexico, Moldova, Morocco, Romania, the Russian
Federation, San Marino, South Africa, South Korea, Syria, Tunisia, Turkey, Turkmenistan, Ukraine and
Uzbekistan as well as the provisions of other agreements concluded jointly by the present Member
States and the Community before 1 May 2004.
Any adjustments to these Agreements shall be the subject of protocols concluded with the
co‑contracting countries in conformity with the provisions of the second subparagraph of paragraph
1. Should the protocols not have been concluded by 1 May 2004, the Union, the European Atomic
Energy Community and the Member States shall take, in the framework of their respective
competences, the necessary measures to deal with that situation.
5. As from 1 May 2004, the new Member States shall apply the bilateral textile agreements and
arrangements concluded by the Community with third countries.
The quantitative restrictions applied by the Union on imports of textile and clothing products shall
be adjusted to take account of the accession of the new Member States.
Should the amendments to the bilateral textile agreements and arrangements not have entered into
force by 1 May 2004, the Union shall make the necessary adjustments to its rules for the import of
textile and clothing products from third countries to take into account the accession of the new
Member States.
( 1 )
OJ L 1, 3.1.1994, p. 3.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 308
308
Part IV
6. The quantitative restrictions applied by the Union on imports of steel and steel products shall be
adjusted on the basis of imports by new Member States during the years immediately preceding the
signing of the Accession Treaty of steel products originating in the supplier countries concerned.
7. Fisheries agreements concluded before 1 May 2004 by the new Member States with third
countries shall be managed by the Union.
The rights and obligations resulting for the new Member States from those agreements shall not be
affected during the period in which the provisions of those agreements are provisionally maintained.
As soon as possible, and in any event before the expiry of the agreements referred to in the first
subparagraph, appropriate European decisions for the continuation of fishing activities resulting from
those agreements shall be adopted in each case by the Council on a proposal from the Commission,
including the possibility of extending certain agreements for periods not exceeding one year.
8. With effect from 1 May 2004, the new Member States shall withdraw from any free trade
agreements with third countries, including the Central European Free Trade Agreement.
To the extent that agreements between one or more of the new Member States on the one hand, and
one or more third countries on the other, are not compatible with the obligations arising from the
Constitution and in particular from this Protocol, the new Member States shall take all appropriate
steps to eliminate the incompatibilities established. If a new Member State encounters difficulties in
adjusting an agreement concluded with one or more third countries before accession, it shall,
according to the terms of the agreement, withdraw from that agreement.
9. The new Member States shall take appropriate measures, where necessary, to adjust their
position in relation to international organisations, and in relation to those international agreements
to which the Union or the European Atomic Energy Community or other Member States are also
parties, to the rights and obligations arising from their accession to the Union.
They shall in particular withdraw at 1 May 2004 or the earliest possible date thereafter from
international fisheries agreements and organisations to which the Union is also a party, unless their
membership relates to matters other than fisheries.
Article 7
Acts adopted by the institutions to which the transitional provisions laid down in this Protocol relate
shall retain their status in law; in particular, the procedures for amending those acts shall continue to
apply.
Article 8
Provisions of the Act of Accession of 16 April 2003, as interpreted by the Court of Justice of the
European Communities and the Court of First Instance, the purpose or effect of which is to repeal or
amend, otherwise than as a transitional measure, acts adopted by the institutions, bodies, offices or
agencies of the Community or of the European Union established by the Treaty on European Union
shall remain in force subject to the application of the second paragraph.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 309
Treaty establishing a Constitution for Europe
309
These provisions shall have the same status in law as the acts which they repeal or amend and shall be
subject to the same rules as those acts.
Article 9
The texts of the acts of the institutions, bodies, offices and agencies of the Community or of the
European Union established by the Treaty on European Union and the texts of acts of the European
Central Bank which were adopted before 1 May 2004 and which were drawn up in the Czech,
Estonian, Latvian, Lithuanian, Hungarian, Maltese, Polish, Slovenian and Slovak languages shall be
authentic from that date, under the same conditions as the texts drawn up and authentic in the other
languages.
Article 10
A European law of the Council may repeal the transitional provisions set out in this Protocol, when
they are no longer applicable. The Council shall act unanimously after consulting the European
Parliament.
Article 11
The application of the Constitution and acts adopted by the institutions shall, as a transitional
measure, be subject to the derogations provided for in this Protocol.
TITLE II
PERMANENT PROVISIONS
Article 12
The adaptations to the acts listed in Annex III to the Act of Accession of 16 April 2003 made
necessary by accession shall be drawn up in conformity with the guidelines set out in that Annex and
in accordance with the procedure and under the conditions laid down in Article 36.
Article 13
The measures listed in Annex IV to the Act of Accession of 16 April 2003 shall be applied under the
conditions laid down in that Annex.
Article 14
A European law of the Council may make the adaptations to the provisions of this Protocol relating
to the common agricultural policy which may prove necessary as a result of a modification of Union
law. The Council shall act unanimously after consulting the European Parliament.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 310
310
Part IV
TITLE III
TEMPORARY PROVISIONS
Article 15
The measures listed in Annexes V, VI, VII, VIII, IX, X, XI, XII, XIII and XIV to the Act of Accession of
16 April 2003 shall apply in respect of the new Member States under the conditions laid down in
those Annexes.
Article 16
1. The revenue designated as ‘Common Customs Tariff duties and other duties’ referred to in Article
2(1)(b) of Council Decision 2000/597/EC, Euratom of 29 September 2000 on the system of the
European Communities' own resources ( 1 ), or the corresponding provision in any Decision replacing
it, shall include the customs duties calculated on the basis of the rates resulting from the Common
Customs Tariff and any tariff concession relating thereto applied by the Union in the new Member
States' trade with third countries.
2. For the year 2004, the harmonised VAT assessment base and the GNI (gross national income)
base of each new Member State, referred to in Article 2(1)(c) and (d) of Council Decision
2000/597/EC, Euratom shall be equal to two thirds of the annual base. The GNI base of each new
Member State to be taken into account for the calculation of the financing of the correction in
respect of budgetary imbalances granted to the United Kingdom, referred to in Article 5(1) of Council
Decision 2000/597/EC, Euratom shall likewise be equal to two thirds of the annual base.
3. For the purposes of determining the frozen rate for 2004 according to Article 2(4)(b) of Council
Decision 2000/597/EC, Euratom the capped VAT bases of the new Member States shall be calculated
on the basis of two thirds of their uncapped VAT base and two thirds of their GNI.
Article 17
1. The budget of the Union for the financial year 2004 shall be adapted to take into account the
accession of the new Member States through an amending budget taking effect on 1 May 2004.
2. The twelve monthly twelfths of VAT and GNI‑based resources to be paid by the new Member
States under the amending budget referred to in paragraph 1, as well as the retroactive adjustment of
the monthly twelfths for the period January—April 2004 that only apply to the present Member
States, shall be converted into eighths to be called during the period May—December 2004. The
retroactive adjustments that result from any subsequent amending budget adopted in 2004 shall
likewise be converted into equal parts to be called during the remainder of the year.
( 1 )
OJ L 253, 7.10.2000, p. 42.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 311
Treaty establishing a Constitution for Europe
311
Article 18
On the first working day of each month the Union shall pay the Czech Republic, Cyprus, Malta and
Slovenia, as an item of expenditure under the Union budget, one eighth in 2004, as of 1 May 2004,
and one twelfth in 2005 and 2006 of the following amounts of temporary budgetary compensation:
(EUR million, 1999 prices)
Czech Republic
2004 2005
2006
125,4 178,0 85,1
Cyprus 68,9 119,2 112,3
Malta 37,8 65,6 62,9
Slovenia 29,5 66,4 35,5
Article 19
On the first working day of each month the Union shall pay the Czech Republic, Estonia, Cyprus,
Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, as an item of expenditure under the
Union budget, one eighth in 2004, as of 1 May 2004, and one twelfth in 2005 and 2006 of the
following amounts of a special lump‑sum cash‑flow facility:
(EUR million, 1999 prices)
2004 2005 2006
174,70 91,55 91,55
Estonia 15,80 2,90 2,90
Cyprus 27,70 5,05 5,05
Latvia 19,50 3,40 3,40
Lithuania 34,80 6,30 6,30
Hungary 155,30 27,95 27,95
Malta 12,20 27,15 27,15
Poland 442,80 550,00 450,00
Slovenia 65,40 17,85 17,85
Slovakia 63,20 11,35 11,35
Czech Republic954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 312
312
Part IV
One thousand million euro for Poland and 100 million euro for the Czech Republic included in the
special lump‑sum cash‑flow facility shall be taken into account for any calculations on the
distribution of Structural Funds for the years 2004, 2005 and 2006.
Article 20
1. The new Member States listed below shall pay the following amounts to the Research Fund for
Coal and Steel referred to in Decision 2002/234/ECSC of the Representatives of the Governments of
the Member States, meeting within the Council, of 27 February 2002 on the financial consequences
of the expiry of the ECSC Treaty and on the Research Fund for Coal and Steel ( 1 ):
(EUR million, current prices)
Czech Republic
39,88
Estonia 2,50
Latvia 2,69
Hungary 9,93
Poland
92,46
Slovenia 2,36
Slovakia 20,11
2. The contributions to the Research Fund for Coal and Steel shall be made in four instalments
starting in 2006 and paid as follows, in each case on the first working day of the first month of each
year:
2006: 15 %
2007: 20 %
2008: 30 %
2009: 35 %
Article 21
1. Save as otherwise provided for in this Protocol, no financial commitments shall be made under
the Phare programme ( 2 ), the Phare cross‑border cooperation programme ( 3 ), pre‑accession funds
for Cyprus and Malta ( 4 ), the ISPA programme ( 5 ) and the Sapard programme ( 6 ) in favour of the
( 1 )
2
OJ L 79, 22.3.2002, p. 42.
( ) Regulation (EEC) No 3906/89 (OJ L 375, 23.12.1989, p. 11).
( 3 ) Regulation (EC) No 2760/98 (OJ L 345, 19.12.1998, p. 49).
( 4 ) Regulation (EC) No 555/2000 (OJ L 68, 16.3.2000, p. 3).
( 5 ) Regulation (EC) No 1267/1999 (OJ L 161, 26.6.1999, p. 73).
( 6 ) Regulation (EC) No 1268/1999 (OJ L 161, 26.6.1999, p. 87).954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 313
Treaty establishing a Constitution for Europe
313
new Member States after 31 December 2003. The new Member States shall receive the same
treatment as the present Member States as regards expenditure under the first three Headings of the
Financial Perspective, as defined in the Interinstitutional Agreement of 6 May 1999 ( 1 ), as from
1 January 2004, subject to the individual specifications and exceptions below or as otherwise
provided for in this Protocol. The maximum additional appropriations for headings 1, 2, 3 and 5 of
the Financial Perspective related to enlargement are set out in Annex XV to the Act of Accession of
16 April 2003. However, no financial commitment under the 2004 budget for any programme or
agency concerned may be made before the accession of the relevant new Member State has taken
place.
2. Paragraph 1 shall not apply to expenditure under the European Agricultural Guidance and
Guarantee Fund, Guarantee Section, according to Articles 2(1), 2(2), and 3(3) of Council Regulation
(EC) No 1258/1999 of 17 May 1999 on the financing of the common agricultural policy ( 2 ), which
will become eligible for Community funding only from 1 May 2004, in accordance with Article 2 of
this Protocol.
However, paragraph 1 of this Article shall apply to expenditure for rural development under the
European Agricultural Guidance and Guarantee Fund, Guarantee Section, according to Article 47a of
Council Regulation (EC) No 1257/1999 of 17 May 1999 on support for rural development from the
European Agricultural Guidance and Guarantee Fund (EAGGF) and amending and repealing certain
regulations ( 3 ), subject to the conditions set out in the amendment of that Regulation in Annex II to
the Act of Accession of 16 April 2003.
3. Subject to the last sentence of paragraph 1, as of 1 January 2004, the new Member States shall
participate in Union programmes and agencies according to the same terms and conditions as the
present Member States with funding from the general budget of the Union.
4. If any measures are necessary to facilitate the transition from the pre‑accession regime to that
resulting from the application of this Article, the Commission shall adopt the required measures.
Article 22
1. Tendering, contracting, implementation and payments for pre‑accession assistance under the
Phare programme, the Phare cross-border cooperation programme and pre‑accession funds for
Cyprus and Malta shall be managed by implementing agencies in the new Member States as from
1 May 2004.
The Commission shall adopt European decisions to waive the ex ante control by the Commission over
tendering and contracting following a positively assessed Extended Decentralised Implementation
( 1 )
Interinstitutional Agreement of 6 May 1999 between the European Parliament, the Council and the Commission on budgetary discipline and
improvement of the budgetary procedure (OJ C 172, 18.6.1999, p. 1).
( 2 ) OJ L 160, 26.6.1999, p. 103.
( 3 ) OJ L 160, 26.6.1999, p. 80.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 314
314
Part IV
System (EDIS) in accordance with the criteria and conditions laid down in the Annex to Council
Regulation (EC) No 1266/1999 of 21 June 1999 on coordinating aid to the applicant countries in the
framework of the pre‑accession strategy and amending Regulation (EEC) No 3906/89 ( 1 ).
If these decisions to waive ex ante control have not been adopted before 1 May 2004, any contracts
signed between 1 May 2004 and the date on which the Commission decisions are taken shall not be
eligible for pre‑accession assistance.
However, exceptionally, if the Commission decisions to waive ex‑ante control are delayed beyond
1 May 2004 for reasons not attributable to the authorities of a new Member State, the Commission
may accept, in duly justified cases, eligibility for pre‑accession assistance of contracts signed between
1 May 2004 and the date of these decisions, and the continued implementation of pre‑accession
assistance for a limited period, subject to ex ante control by the Commission over tendering and
contracting.
2. Global budget commitments made before 1 May 2004 under the pre‑accession financial
instruments referred to in paragraph 1, including the conclusion and registration of subsequent
individual legal commitments and payments made after 1 May 2004, shall continue to be governed
by the rules and regulations of the pre‑accession financing instruments and be charged to the
corresponding budget chapters until closure of the programmes and projects concerned.
Notwithstanding this, public procurement procedures initiated after 1 May 2004 shall be carried
out in accordance with the relevant Union acts.
3. The last programming exercise for the pre‑accession assistance referred to in paragraph 1 shall
take place in the last full calendar year preceding 1 May 2004. Actions under these programmes will
have to be contracted within the following two years and disbursements made as provided for in the
Financing Memorandum ( 2 ), usually by the end of the third year after the commitment. No
extensions shall be granted for the contracting period. Exceptionally and in duly justified cases,
limited extensions in terms of duration may be granted for disbursement.
4. In order to ensure the necessary phasing out of the pre‑accession financial instruments referred
to in paragraph 1 as well as the ISPA programme, and a smooth transition from the rules applicable
before and after 1 May 2004, the Commission may take all appropriate measures to ensure that the
necessary statutory staff is maintained in the new Member States for a maximum of fifteen months
following that date.
During this period, officials assigned to posts in the new Member States before accession and who are
required to remain in service in those States after 1 May 2004 shall benefit, as an exception, from the
same financial and material conditions as were applied by the Commission before 1 May 2004
in accordance with Annex X to the Staff Regulations of officials and the conditions of employment
of other servants of the European Communities laid down in Regulation (EEC, Euratom, ECSC)
( 1 ) OJ L 232, 2.9.1999, p. 34.
( 2 ) As set out in the Phare Guidelines (SEC (1999) 1596, updated on 6.9.2002 by C 3303/2).954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 315
Treaty establishing a Constitution for Europe
315
No 259/68 ( 1 ). The administrative expenditure, including salaries for other staff, necessary for the
management of the pre‑accession assistance shall be covered, for all of 2004 and until the end of July
2005, under the heading ‘support expenditure for operations’ (former part B of the budget) or
equivalent headings for the financial instruments referred to in paragraph 1 as well as the ISPA
programme, of the relevant pre‑accession budgets.
5. Where projects approved under Regulation (EC) No 1258/1999 can no longer be funded under
that instrument, they may be integrated into rural development programming and financed under the
European Agricultural Guidance and Guarantee Fund. Should specific transitional measures be
necessary in this regard, these shall be adopted by the Commission in accordance with the procedures
laid down in Article 50(2) of Council Regulation (EC) No 1260/1999 of 21 June 1999 laying down
general provisions on the Structural Funds ( 2 ).
Article 23
1. Between 1 May 2004 and the end of 2006, the Union shall provide temporary financial
assistance, hereinafter referred to as the ‘Transition Facility’, to the new Member States to develop and
strengthen their administrative capacity to implement and enforce Union and European Atomic
Energy Community law and to foster exchange of best practice among peers.
2. Assistance shall address the continued need for strengthening institutional capacity in certain
areas through action which cannot be financed by the Structural Funds, in particular in the following
areas:
(a) justice and home affairs (strengthening of the judicial system, external border controls,
anti‑corruption strategy, strengthening of law enforcement capacities);
(b) financial control;
(c) protection of the financial interests of the Union and of the European Atomic Energy
Community and the fight against fraud;
(d) internal market, including customs union;
(e) environment;
(f) veterinary services and administrative capacity‑building relating to food safety;
(g) administrative and control structures for agriculture and rural development, including the
Integrated Administration and Control System (IACS);
( 1 ) OJ L 56, 4.3.1968, p. 1.
( 2 ) OJ L 161, 26.6.1999, p. 1.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 316
316
Part IV
(h) nuclear safety (strengthening the effectiveness and competence of nuclear safety authorities and
their technical support organisations as well as public radioactive waste management agencies);
(i) statistics;
(j) strengthening public administration according to needs identified in the Commission's
comprehensive monitoring report which are not covered by the Structural Funds.
3. Assistance under the Transition Facility shall be decided in accordance with the procedure laid
down in Article 8 of Council Regulation (EEC) No 3906/89 of 18 December 1989 on economic aid
to certain countries of Central and Eastern Europe ( 1 ).
4. The programme shall be implemented in accordance with Article 53(1)(a) and (b) of the
Financial Regulation applicable to the general budget of the European Communities ( 2 ) or the
European law replacing it. For twinning projects between public administrations for the purpose of
institution‑building, the procedure for call for proposals through the network of contact points in the
Member States shall continue to apply, as established in the Framework Agreements with the present
Member States for the purpose of pre‑accession assistance.
The commitment appropriations for the Transition Facility, at 1999 prices, shall be 200 million euro
in 2004, 120 million euro in 2005 and 60 million euro in 2006. The annual appropriations shall be
authorised by the budgetary authority within the limits of the Financial Perspective as defined by the
Interinstitutional Agreement of 6 May 1999.
Article 24
1. A Schengen Facility is hereby created as a temporary instrument to help beneficiary Member
States between 1 May 2004 and the end of 2006 to finance actions at the new external borders of the
Union for the implementation of the Schengen acquis and external border control.
In order to address the shortcomings identified in the preparation for participation in Schengen, the
following types of action shall be eligible for financing under the Schengen Facility:
(a) investment in construction, renovation or upgrading of border‑crossing infrastructure and
related buildings;
(b) investments in any kind of operating equipment (e.g. laboratory equipment, detection tools,
Schengen Information System — SIS II hardware and software, means of transport);
(c) training of border guards;
( 1 ) OJ L 375, 23.12.1989, p. 11.
( 2 ) Regulation (EC, Euratom) No 1605/2002 (OJ L 248, 16.9.2002, p. 1).954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 317
Treaty establishing a Constitution for Europe
317
(d) support to costs for logistics and operations.
2. The following amounts shall be made available under the Schengen Facility in the form of
lump‑sum grant payments as of 1 May 2004 to the beneficiary Member States listed below:
(million euro, 1999 prices)
2004 2005 2006
Estonia 22,90 22,90 22,90
Latvia 23,70 23,70 23,70
Lithuania 44,78 61,07 29,85
Hungary 49,30 49,30 49,30
Poland 93,34 93,33 93,33
Slovenia 35,64 35,63 35,63
Slovakia 15,94 15,93 15,93
3. The beneficiary Member States shall be responsible for selecting and implementing individual
operations in compliance with this Article. They shall also be responsible for coordinating use of the
Schengen Facility with assistance from other Union instruments, ensuring compatibility with Union
policies and measures and compliance with the Financial Regulation applicable to the general budget
of the European Communities or with the European law replacing it.
The lump‑sum grant payments shall be used within three years from the first payment and any
unused or unjustifiably spent funds shall be recovered by the Commission. The beneficiary Member
States shall submit, no later than six months after expiry of the three‑year deadline, a comprehensive
report on the financial execution of the lump‑sum grant payments with a statement justifying the
expenditure.
The beneficiary State shall exercise this responsibility without prejudice to the Commission's
responsibility for the implementation of the Union's budget and in accordance with the provisions
applicable to decentralised management in the said Financial Regulation or in the European law
replacing it.
4. The Commission retains the right of verification, through the Anti‑Fraud Office (OLAF). The
Commission and the Court of Auditors may also carry out on‑the‑spot checks in accordance with the
appropriate procedures.
5. The Commission may adopt any technical provisions necessary for the operation of the
Schengen Facility.
Article 25
The amounts referred to in Articles 18, 19, 23 and 24 shall be adjusted each year, as part of the
technical adjustment provided for in paragraph 15 of the Interinstitutional Agreement of 6 May
1999.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 318
318
Part IV
Article 26
1. If, until the end of a period of up to three years after 1 May 2004, difficulties arise which are
serious and liable to persist in any sector of the economy or which could bring about serious
deterioration in the economic situation of a given area, a new Member State may apply for
authorisation to take protective measures in order to rectify the situation and adjust the sector
concerned to the economy of the internal market.
In the same circumstances, any present Member State may apply for authorisation to take protective
measures with regard to one or more of the new Member States.
2. Upon request by the State concerned, the Commission shall, by emergency procedure, adopt the
European regulations or decisions establishing the protective measures which it considers necessary,
specifying the conditions and modalities under which they are to be put into effect.
In the event of serious economic difficulties and at the express request of the Member State
concerned, the Commission shall act within five working days of the receipt of the request
accompanied by the relevant background information. The measures thus decided on shall be
applicable forthwith, shall take account of the interests of all parties concerned and shall not entail
frontier controls.
3. The measures authorised under paragraph 2 may involve derogations from the rules of the
Constitution, and in particular from this Protocol, to such an extent and for such periods as are
strictly necessary in order to attain the objectives referred to in paragraph 1. Priority shall be given to
such measures as will least disturb the functioning of the internal market.
Article 27
If a new Member State has failed to implement commitments undertaken in the context of the
accession negotiations, causing a serious breach of the functioning of the internal market, including
any commitments in all sectoral policies which concern economic activities with cross‑border effect,
or an imminent risk of such breach, the Commission may, until the end of a period of up to three
years after 1 May 2004, upon the motivated request of a Member State or on its own initiative, adopt
European regulations or decisions establishing appropriate measures.
Measures shall be proportional and priority shall be given to measures which least disturb the
functioning of the internal market and, where appropriate, to the application of the existing sectoral
safeguard mechanisms. Such safeguard measures shall not be invoked as a means of arbitrary
discrimination or a disguised restriction on trade between Member States. The measures shall be
maintained no longer than strictly necessary, and, in any case, will be lifted when the relevant
commitment is implemented. They may however be applied beyond the period specified in the first
paragraph as long as the relevant commitments have not been fulfilled. In response to progress made
by the new Member State concerned in fulfilling its commitments, the Commission may adapt the
measures as appropriate. The Commission shall inform the Council in good time before revoking the
European regulations or decisions establishing the safeguard measures, and it shall take duly into
account any observations of the Council in this respect.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 319
Treaty establishing a Constitution for Europe
319
Article 28
If there are serious shortcomings or any imminent risks of such shortcomings in a new Member State
in the transposition, state of implementation or the application of the framework decisions or any
other relevant commitments, instruments of cooperation and decisions relating to mutual
recognition in the area of criminal law under Title VI of the EU Treaty, Directives and Regulations
relating to mutual recognition in civil matters under Title IV of the EC Treaty, and European laws and
framework laws adopted on the basis of Sections 3 and 4 of Chapter IV of Title III of Part III of the
Constitution, the Commission may, until the end of a period of up to three years after 1 May 2004,
upon the motivated request of a Member State or on its own initiative and after consulting the
Member States, adopt European regulations or decisions establishing appropriate measures and
specify the conditions and modalities under which these measures are put into effect.
These measures may take the form of temporary suspension of the application of relevant provisions
and decisions in the relations between a new Member State and any other Member State or Member
States, without prejudice to the continuation of close judicial cooperation. The measures shall be
maintained no longer than strictly necessary, and, in any case, will be lifted when the shortcomings
are remedied. They may however be applied beyond the period specified in the first paragraph as long
as these shortcomings persist. In response to progress made by the new Member State concerned in
rectifying the identified shortcomings, the Commission may adapt the adopted measures as
appropriate after consulting the Member States. The Commission shall inform the Council in good
time before revoking safeguard measures, and it shall take duly into account any observations of the
Council in this respect.
Article 29
In order not to hamper the proper functioning of the internal market, the enforcement of the new
Member States' national rules during the transitional periods referred to in Annexes V to XIV to the
Act of Accession of 16 April 2003 shall not lead to border controls between Member States.
Article 30
If transitional measures are necessary to facilitate the transition from the existing regime in the new
Member States to that resulting from the application of the common agricultural policy under the
conditions set out in this Protocol, such measures shall be adopted by the Commission in accordance
with the procedure referred to in Article 42(2) of Council Regulation (EC) No 1260/2001 of 19 June
2001 on the common organisation of the markets in the sugar sector ( 1 ), or as appropriate, in the
corresponding Articles of the other Regulations on the common organisation of agricultural markets
or of the European laws replacing them or the relevant procedure as determined in the applicable
legislation. The transitional measures referred to in this Article may be adopted during a period of
three years after 1 May 2004 and their application shall be limited to that period. A European law of
the Council may extend this period. The Council shall act unanimously after consulting the European
Parliament.
( 1 )
OJ L 178, 30.6.2001, p. 1.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 320
320
Part IV
Article 31
If transitional measures are necessary to facilitate the transition from the existing regime in the new
Member States to that resulting from the application of the Union veterinary and phytosanitary rules,
such measures shall be adopted by the Commission in accordance with the relevant procedure as
determined in the applicable legislation. These measures shall be taken during a period of three years
after 1 May 2004 and their application shall be limited to that period.
Article 32
1. The terms of office of the new members of the Committees, groups and other bodies listed in
Annex XVI to the Act of Accession of 16 April 2003 shall expire at the same time as those of the
members in office on 1 May 2004.
2. The terms of office of the new members of the Committees and groups set up by the
Commission which are listed in Annex XVII to the Act of Accession of 16 April 2003 shall expire at
the same time as those of the members in office on 1 May 2004.
TITLE IV
APPLICABILITY OF THE ACTS OF THE INSTITUTIONS
Article 33
As from 1 May 2004, the new Member States shall be considered as being addressees of directives
and decisions within the meaning of Article 249 of the EC Treaty and of Article 161 of the EAEC
Treaty, provided that those directives and decisions have been addressed to all the present Member
States. Except with regard to directives and decisions which enter into force pursuant to Article 254
(1) and (2) of the EC Treaty, the new Member States shall be considered as having received
notification of such directives and decisions upon 1 May 2004.
Article 34
The new Member States shall put into effect the measures necessary for them to comply, from 1 May
2004, with the provisions of directives and decisions within the meaning of Article 249 of the EC
Treaty and of Article 161 of the EAEC Treaty, unless another time‑limit is provided for in the Annexes
referred to in Article 15 or in any other provisions of this Protocol.
Article 35
Unless otherwise stipulated, the Council, on a proposal from the Commission, shall adopt the
necessary European regulations and decisions to implement the provisions contained in Annexes III
and IV to the Act of Accession of 16 April 2003 referred to in Articles 12 and 13 of this Protocol.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 321
Treaty establishing a Constitution for Europe
321
Article 36
1. Where acts of the institutions prior to 1 May 2004 require adaptation by reason of accession,
and the necessary adaptations have not been provided for in this Protocol, those adaptations shall be
made in accordance with the procedure laid down by paragraph 2. Those adaptations shall enter into
force as from 1 May 2004.
2. The Council, on a proposal from the Commission, or the Commission, according to which of
these two institutions adopted the original acts, shall to this end adopt the necessary acts.
Article 37
Provisions laid down by law, regulation or administrative action designed to ensure the protection of
the health of workers and the general public in the territory of the new Member States against the
dangers arising from ionising radiations shall, in accordance with Article 33 of the EAEC Treaty, be
communicated by those States to the Commission within three months from 1 May 2004.
PART TWO
PROVISIONS ON THE PROTOCOLS
ANNEXED TO THE ACT OF ACCESSION OF 16 APRIL 2003
TITLE I
TRANSITIONAL PROVISIONS ON THE EUROPEAN INVESTMENT BANK
Article 38
The Kingdom of Spain shall pay the amount of EUR 309 686 775 as its share of the capital paid in
for the subscribed capital increase. This contribution shall be paid in eight equal instalments
falling due on 30 September 2004, 30 September 2005, 30 September 2006, 31 March 2007,
30 September 2007, 31 March 2008, 30 September 2008 and 31 March 2009.
The Kingdom of Spain shall contribute, in eight equal instalments falling due on those dates, to the
reserves and provisions equivalent to reserves, as well as to the amount still to be appropriated to the
reserves and provisions, comprising the balance of the profit and loss account, established at the end
of the month of April 2004, as entered on the balance sheet of the Bank, in amounts corresponding
to 4,1292 % of the reserves and provisions.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 322
322
Part IV
Article 39
From 1 May 2004, the new Member States shall pay the following amounts corresponding to their
share of the capital paid in for the subscribed capital as defined in Article 4 of the Statute of the
European Investment Bank.
Poland
EUR 170 563 175
Czech Republic EUR 62 939 275
Hungary EUR 59 543 425
Slovakia EUR 21 424 525
Slovenia EUR 19 890 750
Lithuania EUR 12 480 875
Cyprus EUR 9 169 100
Latvia EUR 7 616 750
Estonia EUR 5 882 000
Malta EUR 3 490 200
These contributions shall be paid in eight equal instalments falling due on 30 September 2004,
30 September 2005, 30 September 2006, 31 March 2007, 30 September 2007, 31 March 2008,
30 September 2008 and 31 March 2009.
Article 40
The new Member States shall contribute, in eight equal instalments falling due on the dates referred
to in Article 39, to the reserves and provisions equivalent to reserves, as well as to the amount still to
be appropriated to the reserves and provisions, comprising the balance of the profit and loss account,
established at the end of the month of April 2004, as entered on the balance sheet of the European
Investment Bank, in amounts corresponding to the following percentages of the reserves and
provisions:
Poland 2,2742 %
Czech Republic 0,8392 %
Hungary 0,7939 %
Slovakia 0,2857 %
Slovenia 0,2652 %
Lithuania 0,1664 %
Cyprus 0,1223 %
Latvia 0,1016 %
Estonia 0,0784 %
Malta 0,0465 %954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 323
Treaty establishing a Constitution for Europe
323
Article 41
The capital and payments provided for in Articles 38, 39 and 40 shall be paid in by the Kingdom of
Spain and the new Member States in cash in euro, save by way of derogation decided unanimously by
the Board of Governors.
TITLE II
PROVISIONS ON THE RESTRUCTURING OF THE CZECH STEEL INDUSTRY
Article 42
1. Notwithstanding Articles III‑167 and III‑168 of the Constitution, State aid granted by the Czech
Republic for restructuring purposes to specified parts of the Czech steel industry from 1997 to 2003
shall be deemed to be compatible with the internal market provided that:
(a) the period provided for in Article 8(4) of Protocol 2 on ECSC products to the Europe Agreement
establishing an association between the European Communities and their Member States, of the
one part, and the Czech Republic, of the other part ( 1 ), has been extended until 1 May 2004;
(b) the terms set out in the restructuring plan on the basis of which the abovementioned Protocol
was extended are adhered to throughout the period 2002—2006;
(c) the conditions set out in this Title are met, and
(d) no State aid for restructuring is to be paid to the Czech steel industry after 1 May 2004.
2. Restructuring of the Czech steel sector, as described in the individual business plans of the
companies listed in Annex 1 to Protocol 2 to the Act of Accession of 16 April 2003 (hereinafter
referred to as ‘benefiting companies’), and in line with the conditions set out in this Title, shall be
completed no later than 31 December 2006 (hereinafter referred to as ‘the end of the restructuring
period’).
3. Only benefiting companies shall be eligible for State aid in the framework of the Czech steel
restructuring programme.
4.
A benefiting company may not:
(a) in the case of a merger with a company not included in Annex 1 to Protocol 2 to the Act of
Accession of 16 April 2003, pass on the benefit of the aid granted to the benefiting company;
( 1 )
OJ L 360, 31.12.1994, p. 2.954393_TRAITE_EN_301_350
324
12-01-2005
15:56
Pagina 324
Part IV
(b) take over the assets of any company not included in Annex 1 to Protocol 2 to the Act of
Accession of 16 April 2003 which is declared bankrupt in the period up to 31 December 2006.
5. Any subsequent privatisation of any of the benefiting companies shall respect the conditions and
principles regarding viability, State aid and capacity reduction defined in this Title.
6. The total restructuring aid to be granted to the benefiting companies shall be determined by the
justifications set out in the approved Czech steel restructuring plan and individual business plans as
approved by the Council. But in any case, the aid paid out in the period 1997—2003 is limited to a
maximum amount of CZK 14 147 425 201. Of this total figure, Nová Huť receives a maximum of
CZK 5 700 075 201, Vítkovice Steel receives a maximum of CZK 8 155 350 000 and Válcovny
Plechu Frýdek Místek receives a maximum of CZK 292 000 000 depending on the requirements as
set out in the approved restructuring plan. The aid shall only be granted once. No further State aid
shall be granted by the Czech Republic for restructuring purposes to the Czech steel industry.
7. The net capacity reduction to be achieved by the Czech Republic for finished products during the
period 1997—2006 shall be 590 000 tonnes.
Capacity reduction shall be measured only on the basis of permanent closure of production facilities
by physical destruction such that the facilities cannot be restored to service. A declaration of
bankruptcy of a steel company shall not qualify as capacity reduction.
The above level of net capacity reduction, together with any other capacity reductions identified as
necessary in the restructuring programmes, shall be completed in line with the timetable in Annex 2
to Protocol 2 to the Act of Accession of 16 April 2003.
8. The Czech Republic shall remove trade barriers in the coal market in accordance with the acquis
by accession, enabling Czech steel companies to obtain access to coal at international market prices.
9.
The business plan for the benefiting company Nová Huť shall be implemented. In particular:
(a) the Vysoké Pece Ostrava (VPO) plant shall be brought into the organisational framework of Nová
Huť by acquisition of full ownership. A target date shall be set for this merger, including
assignation of responsibility for its implementation;954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 325
Treaty establishing a Constitution for Europe
325
(b) restructuring efforts shall concentrate on the following:
(i) evolving Nová Huť from being production‑oriented to being marketing‑oriented and
improving the efficiency and effectiveness of its business management, including greater
transparency on costs;
(ii) Nová Huť reviewing its product mix and entry into higher added‑value markets;
(iii) Nová Huť making the necessary investments in order to achieve a higher quality of finished
products in the short term;
(c) employment restructuring shall be implemented; levels of productivity comparable to those
obtained by the Union's steel industry product groups shall be reached as at 31 December 2006,
on the basis of the consolidated figures of the benefiting companies concerned;
(d) compliance with the relevant Community acquis in the field of environmental protection shall be
achieved by 1 May 2004 including the necessary investments addressed in the business plan. In
accordance with the business plan the necessary future IPPC‑related investment shall also be
made, in order to ensure compliance with Council Directive 96/61/EC of 24 September 1996
concerning integrated pollution prevention and control ( 1 ) by 1 November 2007.
10. The business plan for the benefiting company Vítkovice Steel shall be implemented. In
particular:
(a) the Duo Mill shall be permanently closed no later than 31 December 2006. In the event of
purchase of the company by a strategic investor, the purchase contract shall be made conditional
on this closure by this date;
(b) restructuring efforts shall concentrate on the following:
(i) an increase in direct sales and a greater focus on cost reduction, this being essential for more
efficient business management,
(ii) adapting to market demand and shifting towards higher value‑added products,
(iii) bringing forward the proposed investment in the secondary steel‑making process from 2004
to 2003, in order to allow the company to compete on quality rather than on price;
(c) compliance with the relevant Community acquis in the field of environmental protection shall be
achieved by 1 May 2004 including the necessary investments addressed in the business plan,
which include the need for future IPPC‑related investment.
( 1 )
OJ L 257, 10.10.1996, p. 26.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 326
326
Part IV
11. The business plan for the benefiting company Válcovny Plechu Frýdek Místek (VPFM) shall be
implemented. In particular:
(a) Hot Rolling Mills Nos 1 and 2 shall be permanently closed at the end of 2004;
(b) restructuring efforts shall concentrate on the following:
(i) making the necessary investment in order to reach a higher quality of finished product in the
short term after the signing of the Treaty of Accession,
(ii) giving priority to the implementation of key identified profit improvement opportunities
(including employment restructuring, cost reductions, yield improvements and distribution
reorientation).
12. Any subsequent changes in the overall restructuring plan and the individual plans must be
agreed by the Commission and, where appropriate, by the Council.
13. The implementation of the restructuring shall take place under conditions of full transparency
and on the basis of sound market economy principles.
14. The Commission and the Council shall closely monitor the implementation of the restructuring
and the fulfilment of the conditions set out in this Title concerning viability, State aid and capacity
reductions before and after 1 May 2004 until the end of the restructuring period, in accordance with
paragraphs 15 to 18. For this purpose the Commission shall report to the Council.
15. The Commission and the Council shall monitor the restructuring benchmarks set out in Annex
3 to Protocol 2 to the Act of Accession of 16 April 2003. The references in that Annex to paragraph
16 of the said Protocol shall be construed as being made to paragraph 16 of this Article.
16. Monitoring shall include an independent evaluation to be carried out in 2003, 2004, 2005 and
2006. The Commission's viability test shall be an important element in ensuring that viability is
achieved.
17. The Czech Republic shall cooperate fully with all the arrangements for monitoring. In
particular:
(a) the Czech Republic shall supply the Commission with six‑monthly reports concerning the
restructuring of the benefiting companies, no later than 15 March and 15 September of each year,
until the end of the restructuring period,
(b) the first report shall reach the Commission by 15 March 2003 and the last report by 15 March
2007, unless the Commission decides otherwise,954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 327
Treaty establishing a Constitution for Europe
327
(c) the reports shall contain all the information necessary to monitor the restructuring process and
the reduction and use of capacity and shall provide sufficient financial data to allow an
assessment to be made of whether the conditions and requirements contained in this Title have
been fulfilled. The reports shall at the least contain the information set out in Annex 4 to
Protocol 2 to the Act of Accession of 16 April 2003, which the Commission reserves the right to
modify in line with its experiences during the monitoring process. In addition to the individual
business reports of the benefiting companies, there shall also be a report on the overall situation
of the Czech steel sector, including recent macroeconomic developments,
(d) the Czech Republic shall oblige the benefiting companies to disclose all relevant data which
might, under other circumstances, be considered as confidential. In its reporting to the Council,
the Commission shall ensure that company‑specific confidential information is not disclosed.
18. The Commission may at any time decide to mandate an independent consultant to evaluate the
monitoring results, undertake any research necessary and report to the Commission and the Council.
19. If the Commission establishes, on the basis of the reports referred to in paragraph 17, that
substantial deviations from the financial data on which the viability assessment has been made have
occurred, it may require the Czech Republic to take appropriate measures to reinforce the
restructuring measures of the benefiting companies concerned.
20.
Should the monitoring show that:
(a) the conditions for the transitional arrangements contained in this Title have not been fulfilled, or
that
(b) the commitments made in the framework of the extension of the period during which the Czech
Republic may exceptionally grant State support for the restructuring of its steel industry under
the Europe Agreement establishing an association between the European Communities and their
Member States, of the one part, and the Czech Republic, of the other part ( 1 ) have not been
fulfilled, or that
(c) the Czech Republic in the course of the restructuring period has granted additional incompatible
State aid to the steel industry and to the benefiting companies in particular,
the transitional arrangements contained in this Title shall not have effect.
The Commission shall take appropriate steps requiring any company concerned to reimburse any aid
granted in breach of the conditions laid down in this Title.
( 1 )
OJ L 360, 31.12.1994, p. 2.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 328
328
Part IV
TITLE III
PROVISIONS ON THE SOVEREIGN BASE AREAS OF THE UNITED KINGDOM OF GREAT BRITAIN
AND NORTHERN IRELAND IN CYPRUS
Article 43
1. The Sovereign Base Areas shall be included within the customs territory of the Union and, for
this purpose, the customs and common commercial policy acts of the Union listed in Part One of the
Annex to Protocol 3 to the Act of Accession of 16 April 2003 shall apply to the Sovereign Base Areas
with the amendments set out in that Annex. In that Annex, reference to ‘this Protocol’ shall be
construed as being to this Title.
2. The Union acts on turnover taxes, excise duties and other forms of indirect taxation listed in Part
Two of the Annex to Protocol 3 to the Act of Accession of 16 April 2003 shall apply to the Sovereign
Base Areas with the amendments set out in that Annex as well as the relevant provisions applying to
Cyprus as set out in this Protocol.
3. The Union acts listed in Part Three of the Annex to Protocol 3 to the Act of Accession of
16 April 2003 shall be amended as set out in that Annex to enable the United Kingdom to maintain
the reliefs and exemptions from duties and taxes on supplies to its forces and associated personnel
which are granted by the Treaty concerning the Establishment of the Republic of Cyprus (hereinafter
referred to as the ‘Treaty of Establishment’).
Article 44
Articles III‑225 to III‑232 of the Constitution, together with the provisions adopted on that basis,
and the provisions adopted in accordance with Article III‑278(4)(b) of the Constitution shall apply to
the Sovereign Base Areas.
Article 45
Persons resident or employed in the territory of the Sovereign Base Areas who, under arrangements
made pursuant to the Treaty of Establishment and the associated Exchange of Notes dated 16 August
1960, are subject to the social security legislation of the Republic of Cyprus shall be treated for the
purposes of Council Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social
security schemes to employed persons, to self‑employed persons and to members of their families
moving within the Community ( 1 ) as if they were resident or employed in the territory of the
Republic of Cyprus.
( 1 )
OJ L 149, 5.7.1971, p. 2.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 329
Treaty establishing a Constitution for Europe
329
Article 46
1. The Republic of Cyprus shall not be required to carry out checks on persons crossing its land and
sea boundaries with the Sovereign Base Areas and any Union restrictions on the crossing of external
borders shall not apply in relation to such persons.
2. The United Kingdom shall exercise controls on persons crossing the external borders of the
Sovereign Base Areas in accordance with the undertakings set out in Part Four of the Annex to
Protocol 3 to the Act of Accession of 16 April 2003.
Article 47
The Council, on a proposal from the Commission, may, in order to ensure effective implementation
of the objectives of this Title, adopt a European decision amending Articles 43 to 46, including the
Annex to Protocol 3 to the Act of Accession of 16 April 2003, or applying other provisions of the
Constitution and Union acts to the Sovereign Base Areas on such terms and subject to such
conditions as it may specify. The Council shall act unanimously. The Commission shall consult the
United Kingdom and the Republic of Cyprus before bringing forward a proposal.
Article 48
1. Subject to paragraph 2, the United Kingdom shall be responsible for the implementation of this
Title in the Sovereign Base Areas. In particular:
(a) the United Kingdom shall be responsible for the application of the Union measures specified in
this Title in the fields of customs, indirect taxation and the common commercial policy in
relation to goods entering or leaving the island of Cyprus through a port or airport within the
Sovereign Base Areas;
(b) customs controls on goods imported into or exported from the island of Cyprus by the forces of
the United Kingdom through a port or airport in the Republic of Cyprus may be carried out
within the Sovereign Base Areas;
(c) the United Kingdom shall be responsible for issuing any licences, authorisations or certificates
which may be required under any applicable Union measure in respect of goods imported into or
exported from the island of Cyprus by the forces of the United Kingdom.
2. The Republic of Cyprus shall be responsible for the administration and payment of any Union
funds to which persons in the Sovereign Base Areas may be entitled pursuant to the application of the
common agricultural policy in the Sovereign Base Areas under Article 44, and the Republic of
Cyprus shall be accountable to the Commission for such expenditure.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 330
330
Part IV
3. Without prejudice to paragraphs 1 and 2, the United Kingdom may delegate to the competent
authorities of the Republic of Cyprus, in accordance with arrangements made pursuant to the Treaty
of Establishment, the performance of any functions imposed on a Member State by or under any
provision referred to in Articles 43 to 46.
4. The United Kingdom and the Republic of Cyprus shall cooperate to ensure the effective
implementation of this Title in the Sovereign Base Areas and, where appropriate, shall conclude
further arrangements concerning the delegation of the implementation of any of the provisions
referred to in Articles 43 to 46. A copy of any such arrangements shall be submitted to the
Commission.
Article 49
The arrangements provided for in this Title shall have the sole purpose of regulating the particular
situation of the Sovereign Base Areas of the United Kingdom in Cyprus and shall not apply to any
other territory of the Union, nor serve as a precedent, in whole or in part, for any other special
arrangements which either already exist or which might be set up in another European territory
provided for in Article IV‑440 of the Constitution.
Article 50
The Commission shall report to the European Parliament and the Council every five years as from
1 May 2004 on the implementation of the provisions of this Title.
Article 51
The provisions of this Title shall apply in the light of the Declaration on the Sovereign Base Areas of
the United Kingdom of Great Britain and Northern Ireland in Cyprus, which incorporates, without
altering its legal effect, the wording of the preamble to Protocol 3 to the Act of Accession of 16 April
2003.
TITLE IV
PROVISIONS ON THE IGNALINA NUCLEAR POWER PLANT IN LITHUANIA
Article 52
Acknowledging the readiness of the Union to provide adequate additional assistance to the efforts by
Lithuania to decommission the Ignalina nuclear power plant and highlighting this expression of
solidarity, Lithuania has undertaken to close Unit 1 of the Ignalina nuclear power plant before 2005
and Unit 2 of this plant by 31 December 2009 at the latest and subsequently decommission these
units.
Article 53
1. During the period 2004—2006, the Union shall provide Lithuania with additional financial
assistance in support of its efforts to decommission, and to address the consequences of the closure954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 331
Treaty establishing a Constitution for Europe
331
and decommissioning of, the Ignalina nuclear power plant (hereinafter ‘the Ignalina Programme’).
2. Measures under the Ignalina Programme shall be decided and implemented in accordance with
the provisions laid down in Council Regulation (EEC) No 3906/89 of 18 December 1989 on
economic aid to certain countries of Central and Eastern Europe ( 1 ).
3. The Ignalina Programme shall, inter alia, cover: measures in support of the decommissioning of
the Ignalina nuclear power plant; measures for the environmental upgrading in line with the acquis
and modernisation measures of conventional production capacity to replace the production capacity
of the two Ignalina nuclear power plant reactors; and other measures which are consequential to the
decision to close and decommission this plant and which contribute to the necessary restructuring,
environmental upgrading and modernisation of the energy production, transmission and distribution
sectors in Lithuania as well as to enhancing the security of energy supply and improving energy
efficiency in Lithuania.
4. The Ignalina Programme shall include measures to support plant personnel in maintaining a
high level of operational safety at the Ignalina nuclear power plant in the periods prior to the closure
and during the decommissioning of the said reactor units.
5. For the period 2004—2006 the Ignalina Programme shall amount to 285 million euro in
commitment appropriations, to be committed in equal annual tranches.
6. The contribution under the Ignalina Programme may, for certain measures, amount to up to
100 % of the total expenditure. Every effort should be made to continue the co‑financing practice
established under the pre‑accession assistance for Lithuania's decommissioning effort as well as to
attract co‑financing from other sources, as appropriate.
7. The assistance under the Ignalina Programme, or parts thereof, may be made available as a Union
contribution to the Ignalina International Decommissioning Support Fund, managed by the
European Bank for Reconstruction and Development.
8.
Public aid from national, Union and international sources:
(a) for the environmental upgrading in line with the acquis and modernisation measures of the
Lithuanian Thermal Power Plant in Elektrenai as the key replacement for the production capacity
of the two Ignalina nuclear power plant reactors; and
(b) for the decommissioning of the Ignalina nuclear power plant
shall be compatible with the internal market as defined in the Constitution.
( 1 )
OJ L 375, 23.12.1989, p. 11.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 332
332
Part IV
9. Public aid from national, Union and international sources in support of Lithuania's efforts to
address the consequences of the closure and of the decommissioning of the Ignalina nuclear power
plant may, on a case by case basis, be considered to be compatible — under the Constitution — with
the internal market, in particular public aid provided for enhancing the security of energy supply.
Article 54
1. Recognising that the decommissioning of the Ignalina nuclear power plant is of a long‑term
nature and represents for Lithuania an exceptional financial burden not commensurate with its size
and economic strength, the Union shall, in solidarity with Lithuania, provide adequate additional
assistance to the decommissioning effort beyond 2006.
2. The Ignalina Programme shall be, for this purpose, seamlessly continued and extended beyond
2006. Implementing provisions for the extended Ignalina Programme shall be adopted in accordance
with the procedure laid down in Article 35 of this Protocol and enter into force, at the latest, by the
date of expiry of the Financial Perspective as defined in the Interinstitutional Agreement of 6 May
1999.
3. The Ignalina Programme, as extended in accordance with the provisions of paragraph 2, shall be
based on the same elements and principles as described in Article 53.
4. For the period of the subsequent Financial Perspective, the overall average appropriations under
the extended Ignalina Programme shall be appropriate. Programming of these resources will be based
on actual payment needs and absorption capacity.
Article 55
Without prejudice to the provisions of Article 52, the general safeguard clause referred to in Article
26 shall apply until 31 December 2012 if energy supply is disrupted in Lithuania.
Article 56
This Title shall apply in the light of the Declaration on the Ignalina nuclear power plant in Lithuania
which incorporates, without altering its legal effect, the wording of the preamble to Protocol 4 to the
Act of Accession of 16 April 2003.
TITLE V
PROVISIONS ON THE TRANSIT OF PERSONS BY LAND BETWEEN THE REGION OF KALININGRAD
AND OTHER PARTS OF THE RUSSIAN FEDERATION
Article 57
The Union rules and arrangements on transit of persons by land between the region of Kaliningrad
and other parts of the Russian Federation, and in particular the Council Regulation (EC) No 693/
2003 of 14 April 2003 establishing a specific Facilitated Transit Document (FTD), a Facilitated Rail954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 333
Treaty establishing a Constitution for Europe
333
Transit Document (FRTD) and amending the Common Consular Instructions and the Common
Manual ( 1 ), shall not in themselves delay or prevent the full participation of Lithuania in the
Schengen acquis, including the removal of internal border controls.
Article 58
The Union shall assist Lithuania in implementing the rules and arrangements for the transit of
persons between the region of Kaliningrad and the other parts of the Russian Federation with a view
to Lithuania's full participation in the Schengen area as soon as possible.
The Union shall assist Lithuania in managing the transit of persons between the region of Kaliningrad
and the other parts of the Russian Federation and shall, notably, bear any additional costs incurred by
implementing the specific provisions of the acquis provided for such transit.
Article 59
Without prejudice to the sovereign rights of Lithuania, any further act concerning the transit of
persons between the region of Kaliningrad and other parts of the Russian Federation shall be adopted
by the Council on a proposal from the Commission. The Council shall act unanimously.
Article 60
This Title shall apply in the light of the Declaration on the transit of persons by land between the
region of Kaliningrad and other parts of the Russian Federation, which incorporates, without altering
its legal affect, the wording of the preamble to Protocol 5 to the Act of Accession of 16 April 2003.
TITLE VI
PROVISIONS ON THE ACQUISITION OF SECONDARY RESIDENCES IN MALTA
Article 61
Bearing in mind the very limited number of residences in Malta and the very limited land available for
construction purposes, which can only cover the basic needs created by the demographic
development of the present residents, Malta may on a non‑discriminatory basis maintain in force the
rules on the acquisition and holding of immovable property for secondary residence purposes by
nationals of the Member States who have not legally resided in Malta for at least five years laid down
in the Immovable Property (Acquisition by Non‑Residents) Act (Chapter 246).
( 1 )
OJ L 99, 17.4.2003, p. 8.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 334
334
Part IV
Malta shall apply authorisation procedures for the acquisition of immovable property for secondary
residence purposes in Malta, which shall be based on published, objective, stable and transparent
criteria. These criteria shall be applied in a non‑discriminatory manner and shall not differentiate
between nationals of Malta and of other Member States. Malta shall ensure that in no instance shall a
national of a Member State be treated in a more restrictive way than a national of a third country.
In the event that the value of one such property bought by a national of a Member State exceeds the
thresholds provided for in Malta's legislation, namely 30 000 Maltese lira for apartments and 50 000
Maltese lira for any type of property other than apartments and property of historical importance,
authorisation shall be granted. Malta may revise the thresholds established by such legislation to
reflect changes in prices in the property market in Malta.

TITLE VII

PROVISIONS ON ABORTION IN MALTA

Article 62
Nothing in the Treaty establishing a Constitution for Europe or in the Treaties and Acts modifying or
supplementing it shall affect the application in the territory of Malta of national legislation relating to
abortion.
TITLE VIII
PROVISIONS ON THE RESTRUCTURING OF THE POLISH STEEL INDUSTRY
Article 63
1. Notwithstanding Articles III‑167 and III‑168 of the Constitution, State aid granted by Poland for
restructuring purposes to specified parts of the Polish steel industry shall be deemed to be compatible
with the internal market provided that:
(a) the period provided for in Article 8(4) of Protocol 2 on ECSC products to the Europe Agreement
establishing an association between the European Communities and their Member States, of the
one part, and Poland, of the other part ( 1 ), has been extended until 1 May 2004,
(b) the terms set out in the restructuring plan, on the basis of which the abovementioned Protocol
was extended are adhered to throughout the period 2002—2006,
(c) the conditions set out in this Title are met, and
( 1 )
OJ L 348, 31.12.1993, p. 2.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 335
Treaty establishing a Constitution for Europe
335
(d) no State aid for restructuring is to be paid to the Polish steel industry after 1 May 2004.
2. Restructuring of the Polish steel sector, as described in the individual business plans of the
companies listed in Annex 1 to Protocol 8 to the Act of Accession of 16 April 2003 (hereinafter
referred to as ‘benefiting companies’), and in line with the conditions set out in this Title, shall be
completed no later than 31 December 2006 (hereinafter referred to as ‘the end of the restructuring
period’).
3. Only benefiting companies shall be eligible for State aid in the framework of the Polish steel
restructuring programme.
4.
A benefiting company may not:
(a) in the case of a merger with a company not included in Annex 1 to Protocol 8 to the Act of
Accession of 16 April 2003, pass on the benefit of the aid granted to the benefiting company;
(b) take over the assets of any company not included in Annex 1 to Protocol 8 to the Act of
Accession of 16 April 2003 which is declared bankrupt in the period up to 31 December 2006.
5. Any subsequent privatisation of any of the benefiting companies shall take place on a basis that
respects the need for transparency and shall respect the conditions and principles regarding viability,
State aids and capacity reduction defined in this Title. No further State aid shall be granted as part of
the sale of any company or individual assets.
6. The restructuring aid granted to the benefiting companies shall be determined by the
justifications set out in the approved Polish steel restructuring plan and individual business plans as
approved by the Council. But, in any case, the aid paid out in the period of 1997‑2003 in its total
amount shall not exceed PLN 3 387 070 000.
Of this total figure:
(a) as regards Polskie Huty Stali (hereinafter referred to as ‘PHS’), the restructuring aid already granted
or to be granted from 1997 until the end of 2003 shall not exceed PLN 3 140 360 000. PHS has
already received PLN 62 360 000 of restructuring aid in the period 1997‑2001; it shall receive
further restructuring aid of no more than PLN 3 078 000 000 in 2002 and 2003 depending on
the requirements set out in the approved restructuring plan (to be entirely paid out in 2002 if the
extension of the grace period under Protocol 2 of the Europe Agreement establishing an
association between the European Communities and their Member States, of the one part, and
Poland, of the other part, is granted by the end of 2002, or otherwise in 2003);
(b) as regards Huta Andrzej S.A., Huta Bankowa Sp. z o.o., Huta Batory S.A., Huta Buczek S.A., Huta
L.W. Sp. z o.o., Huta Łabędy S.A., and Huta Pokój S.A. (hereinafter referred to as ‘other benefiting
companies’), the steel restructuring aid already granted or to be granted from 1997 until the end
of 2003 shall not exceed PLN 246 710 000. These firms have already received PLN 37 160 000
of restructuring aid in the period 1997‑2001; they shall receive further restructuring aid of no
more than PLN 210 210 000 depending on the requirements set out in the approved954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 336
336
Part IV
restructuring plan (of which PLN 182 170 000 in 2002 and PLN 27 380 000 in 2003 if the
extension of the grace period under Protocol 2 of the Europe Agreement establishing an
association between the European Communities and their Member States, of the one part, and
Poland, of the other part, is granted by the end of 2002, or otherwise PLN 210 210 000 in
2003).
No further State aid shall be granted by Poland for restructuring purposes to the Polish steel
industry.
7. The net capacity reduction to be achieved by Poland for finished products during the period
1997‑2006 shall be a minimum of 1 231 000 tonnes. This overall amount includes net capacity
reductions of at least 715 000 tpy in hot‑rolled products and 716 000 tpy in cold‑rolled products, as
well as an increase of at most 200 000 tpy of other finished products.
Capacity reduction shall be measured only on the basis of permanent closure of production facilities
by physical destruction such that the facilities cannot be restored to service. A declaration of
bankruptcy of a steel company shall not qualify as capacity reduction.
The net capacity reductions shown in Annex 2 to Protocol 8 to the Act of Accession of 16 April
2003 are minima and actual net capacity reductions to be achieved and the time frame for doing so
shall be established on the basis of Poland's final restructuring programme and individual business
plans under the Europe Agreement establishing an association between the European Communities
and their Member States, of the one part, and Poland, of the other part, taking into account the
objective to ensure the viability of benefiting companies as at 31 December 2006.
8.
The business plan for the benefiting company PHS shall be implemented. In particular:
(a) restructuring efforts shall concentrate on the following:
(i) reorganising PHS production facilities on a product basis and ensuring horizontal
organisation by function (purchasing, production, sales),
(ii) establishing in PHS a unified management structure enabling full realisation of synergies in
the consolidation,
(iii) evolving the strategic focus of PHS from being production‑oriented to being
marketing‑oriented,
(iv) improving the efficiency and effectiveness of PHS business management and also ensuring
better control of direct sales,
(v) PHS reviewing, on the basis of sound economic considerations, the strategy of spin‑off
companies and, where appropriate, reintegrating services into the parent company,954393_TRAITE_EN_301_350
12-01-2005
15:57
Pagina 337
Treaty establishing a Constitution for Europe
337
(vi) PHS reviewing its product mix, reducing over‑capacity on long semi‑finished products and
generally moving further into the higher value-added product market,
(vii) PHS investing in order to achieve a higher quality of finished products; special attention shall
be given to attaining by the date set in the timetable for the implementation of the PHS
restructuring programme and at the latest by the end of 2006 3‑Sigma production quality
level at the PHS site in Kraków;
(b) cost savings shall be maximised in PHS during the restructuring period through energy efficiency
gains, improved purchasing and ensuring productivity yields comparable to Union levels;
(c) employment restructuring shall be implemented; levels of productivity comparable to those
obtained by Union steel industry product groups shall be reached as at 31 December 2006, based
on consolidated figures including indirect employment in the wholly owned service companies;
(d) any privatisation shall be on a basis that respects the need for transparency and fully respects the
commercial value of PHS. No further State aid shall be granted as part of the sale.
9.
The business plan for the other benefiting companies shall be implemented. In particular:
(a) for all of the other benefiting companies, restructuring efforts shall concentrate on the following:
(i) evolving the strategic focus from being production‑oriented to being marketing‑oriented,
(ii) improving the efficiency and effectiveness of the companies' business management and also
ensuring better control of direct sales,
(iii) reviewing, on the basis of sound economic considerations, the strategy of spin‑off companies
and, where appropriate, reintegrating services into the parent companies;
(b) for Huta Bankowa, implementing the cost savings programme;
(c) for Huta Buczek, obtaining the necessary financial support from creditors and local financial
institutions and implementing the cost savings programme, including reducing the investment
cost by adapting existing production facilities;
(d) for Huta Łabędy, implementing the cost savings programme and reducing reliance on the mining
industry;
(e) for Huta Pokój, achieving international productivity standards in the subsidiaries, implementing
energy consumption savings and cancelling the proposed investment in the processing and
construction department;954393_TRAITE_EN_301_350
12-01-2005
15:57
Pagina 338
338
Part IV
(f) for Huta Batory, reaching agreement with creditors and financial institutions on debt
rescheduling and investment loans. The company shall also ensure substantial additional cost
savings associated with employment restructuring and improved yields;
(g) for Huta Andrzej, securing a stable financial base for its development by negotiating an
agreement between the company's current lenders, long‑term creditors, trade creditors and
financial institutions. Additional investments in the hot tube mill as well as the implementation
of the staff reduction programme must take place;
(h) for Huta L.W., carrying out investments in relation to the company's hot‑rolling mills project,
lifting equipment, and environmental standing. This company shall also achieve higher
productivity levels, through staff restructuring and reducing the costs of external services.
10. Any subsequent changes in the overall restructuring plan and the individual plans must be
agreed by the Commission and, where appropriate, by the Council.
11. The implementation of the restructuring shall take place under conditions of full transparency
and on the basis of sound market economy principles.
12. The Commission and the Council shall closely monitor the implementation of the restructuring
and the fulfilment of the conditions set out in this Title concerning viability, State aid and capacity
reductions before and after 1 May 2004, until the end of the restructuring period, in accordance with
paragraphs 13 to 18. For this purpose the Commission shall report to the Council.
13. In addition to the monitoring of State aid, the Commission and the Council shall monitor the
restructuring benchmarks set out in Annex 3 to Protocol 8 to the Act of Accession of 16 April 2003.
References made in that Annex to paragraph 14 of the Protocol shall be construed as being made to
paragraph 14 of this Article.
14. Monitoring shall include an independent evaluation to be carried out in 2003, 2004, 2005 and
2006. The Commission's viability test shall be applied and productivity shall be measured as part of
the evaluation.
15.
Poland shall cooperate fully with all the arrangements for monitoring. In particular:
(a) Poland shall supply the Commission with six‑monthly reports concerning the restructuring of
the benefiting companies, no later than 15 March and 15 September of each year, until the end of
the restructuring period;
(b) the first report shall reach the Commission by 15 March 2003 and the last report by 15 March
2007, unless the Commission decides otherwise;
(c) the reports shall contain all the information necessary to monitor the restructuring process, the
State aid and the reduction and use of capacity and shall provide sufficient financial data to allow
an assessment to be made of whether the conditions and requirements contained in this Title have been fulfilled. The reports shall at the least contain the information set out in Annex 4 to
Protocol 8 to the Act of Accession of 16 April 2003, which the Commission reserves the right to
modify in line with its experiences during the monitoring process. In Annex 4 to Protocol 8 to
the Act of Accession of 16 April 2003, the reference to paragraph 14 of the Protocol shall be
construed as being to paragraph 14 of this Article. In addition to the individual business reports
of the benefiting companies there shall also be a report on the overall situation of the Polish steel
sector, including recent macroeconomic developments;
(d) all additional information necessary for the independent evaluation provided for in paragraph 14
must, furthermore, be provided by Poland;
(e) Poland shall oblige the benefiting companies to disclose all relevant data which might, under
other circumstances, be considered as confidential. In its reporting to the Council, the
Commission shall ensure that company‑specific confidential information is not disclosed.
16. The Commission may at any time decide to mandate an independent consultant to evaluate the
monitoring results, undertake any research necessary and report to the Commission and the Council.
17. If the Commission establishes, on the basis of the monitoring, that substantial deviations from
the financial data on which the viability assessment has been made have occurred, it may require
Poland to take appropriate measures to reinforce or modify the restructuring measures of the
benefiting companies concerned.
18.
Should the monitoring show that:
(a) the conditions for the transitional arrangements contained in this Title have not been fulfilled, or
that
(b) the commitments made in the framework of the extension of the period during which Poland
may exceptionally grant State support for the restructuring of its steel industry under the Europe
Agreement establishing an association between the European Communities and their Member
States, of the one part, and Poland, of the other part, have not been fulfilled, or that
(c) Poland in the course of the restructuring period has granted additional incompatible State aid to
the steel industry and to the benefiting companies in particular, the transitional arrangements
contained in this Title shall not have effect.
The Commission shall take appropriate steps requiring any company concerned to reimburse any aid
granted in breach of the conditions laid down in this Title.

Part IV
TITLE IX
PROVISIONS ON UNIT 1 AND UNIT 2 OF THE BOHUNICE V1 NUCLEAR POWER PLANT IN SLOVAKIA
Article 64
Slovakia has undertaken to close Unit 1 of the Bohunice V1 nuclear power plant by 31 December
2006 at the latest and Unit 2 of this plant by 31 December 2008 at the latest and subsequently
decommission these units.
Article 65
1. During the period 2004‑2006, the Union shall provide Slovakia with financial assistance in
support of its efforts to decommission, and to address the consequences of the closure and
decommissioning of, Unit 1 and Unit 2 of the Bohunice V1 nuclear power plant (hereinafter referred
to as ‘the Assistance’).
2. The Assistance shall be decided and implemented in accordance with the provisions laid down in
Council Regulation (EEC) No 3906/89 of 18 December 1989 on economic aid to certain countries of
Central and Eastern Europe ( 1 ).
3. For the period 2004‑2006 the Assistance shall amount to 90 million euro in commitment
appropriations, to be committed in equal annual tranches.
4. The Assistance or parts thereof may be made available as a Union contribution to the Bohunice
International Decommissioning Support Fund, managed by the European Bank for Reconstruction
and Development.
Article 66
The Union acknowledges that the decommissioning of the Bohunice V1 Nuclear Power plant must
continue beyond the Financial Perspective as defined in the Interinstitutional Agreement of 6 May
1999, and that this effort represents for Slovakia a significant financial burden. Decisions on the
continuation of Union assistance in this field after 2006 will take the situation into account.
Article 67
The provisions of this Title shall apply in the light of the Declaration on Unit 1 and Unit 2 of the
Bohunice V1 nuclear power plant in Slovakia which incorporates, without altering its legal effect, the
wording of the preamble to Protocol 9 to the Act of Accession of 16 April 2003.

TITLE X

PROVISIONS ON CYPRUS

Article 68
1. The application of the Community and Union acquis shall be suspended in those areas of the
Republic of Cyprus in which the Government of the Republic of Cyprus does not exercise effective
control.
2. The Council, on the basis of a proposal from the Commission, shall decide on the withdrawal of
the suspension referred to in paragraph 1. It shall act unanimously.
Article 69
1. The Council, on the basis of a proposal from the Commission, shall define the terms under
which the provisions of Union law shall apply to the line between those areas referred to in Article
68 and the areas in which the Government of the Republic of Cyprus exercises effective control. The
Council shall act unanimously.
2. The boundary between the Eastern Sovereign Base Area and those areas referred to in Article 68
shall be treated as part of the external borders of the Sovereign Base Areas for the purpose of Part
Four of the Annex to Protocol 3 to the Act of Accession of 16 April 2003 on the Sovereign Base
Areas of the United Kingdom of Great Britain and Northern Ireland in Cyprus for the duration of the
suspension of the application of the Community and Union acquis according to Article 68.
Article 70
1. Nothing in this Title shall preclude measures with a view to promoting the economic
development of the areas referred to in Article 68.
2. Such measures shall not affect the application of the Community and Union acquis under the
conditions set out in this Protocol in any other part of the Republic of Cyprus.
Article 71
In the event of settlement of the Cyprus problem, the Council, on the basis of a proposal from the
Commission, shall decide on the adaptations to the terms concerning the accession of Cyprus to the
Union with regard to the Turkish Cypriot Community. The Council shall act unanimously.
Article 72
This Title shall apply in the light of the declaration on Cyprus which incorporates, without altering its
legal effect, the wording of the preamble to Protocol 10 to the Act of Accession of 16 April 2003.

