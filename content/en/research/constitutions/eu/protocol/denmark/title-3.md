TITLE III
PROVISIONS ON THE SOVEREIGN BASE AREAS OF THE UNITED KINGDOM OF GREAT BRITAIN
AND NORTHERN IRELAND IN CYPRUS
Article 43
1. The Sovereign Base Areas shall be included within the customs territory of the Union and, for
this purpose, the customs and common commercial policy acts of the Union listed in Part One of the
Annex to Protocol 3 to the Act of Accession of 16 April 2003 shall apply to the Sovereign Base Areas
with the amendments set out in that Annex. In that Annex, reference to ‘this Protocol’ shall be
construed as being to this Title.
2. The Union acts on turnover taxes, excise duties and other forms of indirect taxation listed in Part
Two of the Annex to Protocol 3 to the Act of Accession of 16 April 2003 shall apply to the Sovereign
Base Areas with the amendments set out in that Annex as well as the relevant provisions applying to
Cyprus as set out in this Protocol.
3. The Union acts listed in Part Three of the Annex to Protocol 3 to the Act of Accession of
16 April 2003 shall be amended as set out in that Annex to enable the United Kingdom to maintain
the reliefs and exemptions from duties and taxes on supplies to its forces and associated personnel
which are granted by the Treaty concerning the Establishment of the Republic of Cyprus (hereinafter
referred to as the ‘Treaty of Establishment’).
Article 44
Articles III‑225 to III‑232 of the Constitution, together with the provisions adopted on that basis,
and the provisions adopted in accordance with Article III‑278(4)(b) of the Constitution shall apply to
the Sovereign Base Areas.
Article 45
Persons resident or employed in the territory of the Sovereign Base Areas who, under arrangements
made pursuant to the Treaty of Establishment and the associated Exchange of Notes dated 16 August
1960, are subject to the social security legislation of the Republic of Cyprus shall be treated for the
purposes of Council Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social
security schemes to employed persons, to self‑employed persons and to members of their families
moving within the Community ( 1 ) as if they were resident or employed in the territory of the
Republic of Cyprus.
( 1 )
OJ L 149, 5.7.1971, p. 2.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 329
Treaty establishing a Constitution for Europe
329
Article 46
1. The Republic of Cyprus shall not be required to carry out checks on persons crossing its land and
sea boundaries with the Sovereign Base Areas and any Union restrictions on the crossing of external
borders shall not apply in relation to such persons.
2. The United Kingdom shall exercise controls on persons crossing the external borders of the
Sovereign Base Areas in accordance with the undertakings set out in Part Four of the Annex to
Protocol 3 to the Act of Accession of 16 April 2003.
Article 47
The Council, on a proposal from the Commission, may, in order to ensure effective implementation
of the objectives of this Title, adopt a European decision amending Articles 43 to 46, including the
Annex to Protocol 3 to the Act of Accession of 16 April 2003, or applying other provisions of the
Constitution and Union acts to the Sovereign Base Areas on such terms and subject to such
conditions as it may specify. The Council shall act unanimously. The Commission shall consult the
United Kingdom and the Republic of Cyprus before bringing forward a proposal.
Article 48
1. Subject to paragraph 2, the United Kingdom shall be responsible for the implementation of this
Title in the Sovereign Base Areas. In particular:
(a) the United Kingdom shall be responsible for the application of the Union measures specified in
this Title in the fields of customs, indirect taxation and the common commercial policy in
relation to goods entering or leaving the island of Cyprus through a port or airport within the
Sovereign Base Areas;
(b) customs controls on goods imported into or exported from the island of Cyprus by the forces of
the United Kingdom through a port or airport in the Republic of Cyprus may be carried out
within the Sovereign Base Areas;
(c) the United Kingdom shall be responsible for issuing any licences, authorisations or certificates
which may be required under any applicable Union measure in respect of goods imported into or
exported from the island of Cyprus by the forces of the United Kingdom.
2. The Republic of Cyprus shall be responsible for the administration and payment of any Union
funds to which persons in the Sovereign Base Areas may be entitled pursuant to the application of the
common agricultural policy in the Sovereign Base Areas under Article 44, and the Republic of
Cyprus shall be accountable to the Commission for such expenditure.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 330
330
Part IV
3. Without prejudice to paragraphs 1 and 2, the United Kingdom may delegate to the competent
authorities of the Republic of Cyprus, in accordance with arrangements made pursuant to the Treaty
of Establishment, the performance of any functions imposed on a Member State by or under any
provision referred to in Articles 43 to 46.
4. The United Kingdom and the Republic of Cyprus shall cooperate to ensure the effective
implementation of this Title in the Sovereign Base Areas and, where appropriate, shall conclude
further arrangements concerning the delegation of the implementation of any of the provisions
referred to in Articles 43 to 46. A copy of any such arrangements shall be submitted to the
Commission.
Article 49
The arrangements provided for in this Title shall have the sole purpose of regulating the particular
situation of the Sovereign Base Areas of the United Kingdom in Cyprus and shall not apply to any
other territory of the Union, nor serve as a precedent, in whole or in part, for any other special
arrangements which either already exist or which might be set up in another European territory
provided for in Article IV‑440 of the Constitution.
Article 50
The Commission shall report to the European Parliament and the Council every five years as from
1 May 2004 on the implementation of the provisions of this Title.
Article 51
The provisions of this Title shall apply in the light of the Declaration on the Sovereign Base Areas of
the United Kingdom of Great Britain and Northern Ireland in Cyprus, which incorporates, without
altering its legal effect, the wording of the preamble to Protocol 3 to the Act of Accession of 16 April
2003.
TITLE IV
PROVISIONS ON THE IGNALINA NUCLEAR POWER PLANT IN LITHUANIA
Article 52
Acknowledging the readiness of the Union to provide adequate additional assistance to the efforts by
Lithuania to decommission the Ignalina nuclear power plant and highlighting this expression of
solidarity, Lithuania has undertaken to close Unit 1 of the Ignalina nuclear power plant before 2005
and Unit 2 of this plant by 31 December 2009 at the latest and subsequently decommission these
units.
Article 53
1. During the period 2004—2006, the Union shall provide Lithuania with additional financial
assistance in support of its efforts to decommission, and to address the consequences of the closure954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 331
Treaty establishing a Constitution for Europe
331
and decommissioning of, the Ignalina nuclear power plant (hereinafter ‘the Ignalina Programme’).
2. Measures under the Ignalina Programme shall be decided and implemented in accordance with
the provisions laid down in Council Regulation (EEC) No 3906/89 of 18 December 1989 on
economic aid to certain countries of Central and Eastern Europe ( 1 ).
3. The Ignalina Programme shall, inter alia, cover: measures in support of the decommissioning of
the Ignalina nuclear power plant; measures for the environmental upgrading in line with the acquis
and modernisation measures of conventional production capacity to replace the production capacity
of the two Ignalina nuclear power plant reactors; and other measures which are consequential to the
decision to close and decommission this plant and which contribute to the necessary restructuring,
environmental upgrading and modernisation of the energy production, transmission and distribution
sectors in Lithuania as well as to enhancing the security of energy supply and improving energy
efficiency in Lithuania.
4. The Ignalina Programme shall include measures to support plant personnel in maintaining a
high level of operational safety at the Ignalina nuclear power plant in the periods prior to the closure
and during the decommissioning of the said reactor units.
5. For the period 2004—2006 the Ignalina Programme shall amount to 285 million euro in
commitment appropriations, to be committed in equal annual tranches.
6. The contribution under the Ignalina Programme may, for certain measures, amount to up to
100 % of the total expenditure. Every effort should be made to continue the co‑financing practice
established under the pre‑accession assistance for Lithuania's decommissioning effort as well as to
attract co‑financing from other sources, as appropriate.
7. The assistance under the Ignalina Programme, or parts thereof, may be made available as a Union
contribution to the Ignalina International Decommissioning Support Fund, managed by the
European Bank for Reconstruction and Development.
8.
Public aid from national, Union and international sources:
(a) for the environmental upgrading in line with the acquis and modernisation measures of the
Lithuanian Thermal Power Plant in Elektrenai as the key replacement for the production capacity
of the two Ignalina nuclear power plant reactors; and
(b) for the decommissioning of the Ignalina nuclear power plant
shall be compatible with the internal market as defined in the Constitution.
( 1 )
OJ L 375, 23.12.1989, p. 11.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 332
332
Part IV
9. Public aid from national, Union and international sources in support of Lithuania's efforts to
address the consequences of the closure and of the decommissioning of the Ignalina nuclear power
plant may, on a case by case basis, be considered to be compatible — under the Constitution — with
the internal market, in particular public aid provided for enhancing the security of energy supply.
Article 54
1. Recognising that the decommissioning of the Ignalina nuclear power plant is of a long‑term
nature and represents for Lithuania an exceptional financial burden not commensurate with its size
and economic strength, the Union shall, in solidarity with Lithuania, provide adequate additional
assistance to the decommissioning effort beyond 2006.
2. The Ignalina Programme shall be, for this purpose, seamlessly continued and extended beyond
2006. Implementing provisions for the extended Ignalina Programme shall be adopted in accordance
with the procedure laid down in Article 35 of this Protocol and enter into force, at the latest, by the
date of expiry of the Financial Perspective as defined in the Interinstitutional Agreement of 6 May
1999.
3. The Ignalina Programme, as extended in accordance with the provisions of paragraph 2, shall be
based on the same elements and principles as described in Article 53.
4. For the period of the subsequent Financial Perspective, the overall average appropriations under
the extended Ignalina Programme shall be appropriate. Programming of these resources will be based
on actual payment needs and absorption capacity.
Article 55
Without prejudice to the provisions of Article 52, the general safeguard clause referred to in Article
26 shall apply until 31 December 2012 if energy supply is disrupted in Lithuania.
Article 56
This Title shall apply in the light of the Declaration on the Ignalina nuclear power plant in Lithuania
which incorporates, without altering its legal effect, the wording of the preamble to Protocol 4 to the
Act of Accession of 16 April 2003.

TITLE 5: PROVISIONS ON THE TRANSIT OF PERSONS BY LAND BETWEEN THE REGION OF KALININGRAD
AND OTHER PARTS OF THE RUSSIAN FEDERATION

Article 57
The Union rules and arrangements on transit of persons by land between the region of Kaliningrad
and other parts of the Russian Federation, and in particular the Council Regulation (EC) No 693/
2003 of 14 April 2003 establishing a specific Facilitated Transit Document (FTD), a Facilitated Rail Transit Document (FRTD) and amending the Common Consular Instructions and the Common
Manual ( 1 ), shall not in themselves delay or prevent the full participation of Lithuania in the
Schengen acquis, including the removal of internal border controls.
Article 58
The Union shall assist Lithuania in implementing the rules and arrangements for the transit of
persons between the region of Kaliningrad and the other parts of the Russian Federation with a view
to Lithuania's full participation in the Schengen area as soon as possible.
The Union shall assist Lithuania in managing the transit of persons between the region of Kaliningrad
and the other parts of the Russian Federation and shall, notably, bear any additional costs incurred by
implementing the specific provisions of the acquis provided for such transit.
Article 59
Without prejudice to the sovereign rights of Lithuania, any further act concerning the transit of
persons between the region of Kaliningrad and other parts of the Russian Federation shall be adopted
by the Council on a proposal from the Commission. The Council shall act unanimously.
Article 60
This Title shall apply in the light of the Declaration on the transit of persons by land between the
region of Kaliningrad and other parts of the Russian Federation, which incorporates, without altering
its legal affect, the wording of the preamble to Protocol 5 to the Act of Accession of 16 April 2003.

TITLE 6: PROVISIONS ON THE ACQUISITION OF SECONDARY RESIDENCES IN MALTA

Article 61

Bearing in mind the very limited number of residences in Malta and the very limited land available for
construction purposes, which can only cover the basic needs created by the demographic
development of the present residents, Malta may on a non‑discriminatory basis maintain in force the
rules on the acquisition and holding of immovable property for secondary residence purposes by
nationals of the Member States who have not legally resided in Malta for at least five years laid down
in the Immovable Property (Acquisition by Non‑Residents) Act (Chapter 246).
( 1 ) Malta shall apply authorisation procedures for the acquisition of immovable property for secondary
residence purposes in Malta, which shall be based on published, objective, stable and transparent
criteria. These criteria shall be applied in a non‑discriminatory manner and shall not differentiate
between nationals of Malta and of other Member States. Malta shall ensure that in no instance shall a
national of a Member State be treated in a more restrictive way than a national of a third country.
In the event that the value of one such property bought by a national of a Member State exceeds the
thresholds provided for in Malta's legislation, namely 30 000 Maltese lira for apartments and 50 000
Maltese lira for any type of property other than apartments and property of historical importance,
authorisation shall be granted. Malta may revise the thresholds established by such legislation to
reflect changes in prices in the property market in Malta.

