TITLE II
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS OF ACCESSION OF THE
KINGDOM OF DENMARK, IRELAND AND THE UNITED KINGDOM OF GREAT BRITAIN
AND NORTHERN IRELAND
SECTION 1
Provisions on Gibraltar
Article 6
1. Acts of the institutions relating to the products in Annex I to the Constitution and the products
subject, on importation into the Union, to specific rules as a result of the implementation of the
common agricultural policy, as well as the acts on the harmonisation of legislation of Member States
concerning turnover taxes, shall not apply to Gibraltar unless the Council adopts a European decision
which provides otherwise. The Council shall act unanimously on a proposal from the Commission.
2. The situation of Gibraltar defined in point VI of Annex II ( 1 ) to the Act concerning the
conditions of accession of the Kingdom of Denmark, Ireland and the United Kingdom of
Great Britain and Northern Ireland shall be maintained.
SECTION 2
Provisions on the Faroe Islands
Article 7
Danish nationals resident in the Faroe Islands shall be considered to be nationals of a Member State
within the meaning of the Constitution only from the date on which the Constitution becomes
applicable to those islands.
( 1 )
OJ L 73, 27.3.1972, p. 47.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 277
Treaty establishing a Constitution for Europe
277
SECTION 3
Provisions on the Channel Islands and the Isle of Man
Article 8
1. The Union rules on customs matters and quantitative restrictions, in particular customs duties,
charges having equivalent effect and the Common Customs Tariff, shall apply to the Channel Islands
and the Isle of Man under the same conditions as they apply to the United Kingdom.
2. In respect of agricultural products and products processed therefrom which are the subject of a
special trade regime, the levies and other import measures laid down in Union rules and applicable by
the United Kingdom shall be applied to third countries.
Such provisions of Union rules as are necessary to allow free movement and observance of normal
conditions of competition in trade in these products shall also be applicable.
The Council, on a proposal from the Commission, shall adopt the European regulations or decisions
establishing the conditions under which the provisions referred to in the first and second
subparagraphs shall be applicable to these territories.
Article 9
The rights enjoyed by Channel Islanders or Manxmen in the United Kingdom shall not be affected by
Union law. However, such persons shall not benefit from provisions of Union law relating to the free
movement of persons and services.
Article 10
The provisions of the Treaty establishing the European Atomic Energy Community applicable to
persons or undertakings within the meaning of Article 196 of that Treaty shall apply to those persons
or undertakings when they are established in the territories referred to in Article 8 of this Protocol.
Article 11
The authorities of the territories referred to in Article 8 shall apply the same treatment to all natural
and legal persons of the Union.
Article 12
If, during the application of the arrangements defined in this Section, difficulties appear on either side
in relations between the Union and the territories referred to in Article 8, the Commission shall
without delay propose to the Council such safeguard measures as it believes necessary, specifying
their terms and conditions of application.
The Council shall adopt the appropriate European regulations or decisions within one month.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 278
278
Part IV
Article 13
In this Section, Channel Islander or Manxman shall mean any British citizen who holds that
citizenship by virtue of the fact that he, a parent or grandparent was born, adopted, naturalised or
registered in the island in question; but such a person shall not for this purpose be regarded as a
Channel Islander or Manxman if he, a parent or a grandparent was born, adopted, naturalised or
registered in the United Kingdom. Nor shall he be so regarded if he has at any time been ordinarily
resident in the United Kingdom for five years.
The administrative arrangements necessary to identify these persons will be notified to the
Commission.
SECTION 4
Provisions on the implementation of the policy of industrialisation
and economic development in Ireland
Article 14
The Member States take note of the fact that the Government of Ireland has embarked upon the
implementation of a policy of industrialisation and economic development designed to align the
standards of living in Ireland with those of the other Member States and to eliminate
underemployment while progressively evening out regional differences in levels of development.
They recognise it to be in their common interest that the objectives of this policy be so attained and
agree to recommend to this end that the institutions implement all the means and procedures laid
down by the Constitution, particularly by making adequate use of the Union resources intended for
the realisation of its objectives.
The Member States recognise in particular that, in the application of Articles III‑167 and III‑168 of
the Constitution, it will be necessary to take into account the objectives of economic expansion and
the raising of the standard of living of the population.
SECTION 5
Provisions on the exchange of information with Denmark in the field
of nuclear energy
Article 15
1. From 1 January 1973, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of Denmark, which shall give it limited
distribution within its territory under the conditions laid down in that Article.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 279
Treaty establishing a Constitution for Europe
279
2. From 1 January 1973, Denmark shall place at the disposal of the European Atomic Energy
Community an equivalent volume of information in the sectors specified in paragraph 3. This
information shall be set forth in detail in a document transmitted to the Commission. The
Commission shall communicate this information to Community undertakings under the conditions
laid down in Article 13 of the Treaty establishing the European Atomic Energy Community.
3. The sectors in which Denmark shall make information available to the European Atomic Energy
Community are as follows:
(a) DOR heavy water moderated organic cooled reactor;
(b) DT‑350, DK‑400 heavy water pressure vessel reactors;
(c) high temperature gas loop;
(d) instrumentation systems and special electronic equipment;
(e) reliability;
(f) reactor physics, reactor dynamics and heat exchange;
(g) in‑pile testing of materials and equipment.
4. Denmark shall undertake to supply the European Atomic Energy Community with any
information complementary to the reports which it shall communicate, in particular during visits by
European Atomic Energy Community personnel or personnel from the Member States to the
Risö Centre, under conditions to be determined by mutual agreement in each case.
Article 16
1. In those sectors in which Denmark places information at the disposal of the European Atomic
Energy Community, the competent authorities shall grant upon request licences on commercial
terms to Member States, persons and undertakings of the Community where they possess exclusive
rights to patents filed in Member States and insofar as they have no obligation or commitment in
respect of third parties to grant or offer to grant an exclusive or partially exclusive licence to the
rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, Denmark shall encourage and
facilitate the granting of sublicences on commercial terms to Member States, persons and
undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 280
280
Part IV
SECTION 6
Provisions on the exchange of information with Ireland in the field of nuclear energy
Article 17
1. From 1 January 1973, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of Ireland, which shall give it limited distribution
within its territory under the conditions laid down in that Article.
2. From 1 January 1973, Ireland shall place at the disposal of the European Atomic Energy
Community information obtained in the nuclear field in Ireland, which is given limited distribution,
insofar as strictly commercial applications are not involved. The Commission shall communicate this
information to Community undertakings under the conditions laid down in Article 13 of the Treaty
establishing the European Atomic Energy Community.
3. The information referred to in paragraphs 1 and 2 shall mainly concern studies for the
development of a power reactor and work on radioisotopes and their application in medicine,
including the problems of radiation protection.
Article 18
1. In those sectors in which Ireland places information at the disposal of the European Atomic
Energy Community, the competent authorities shall grant upon request licences on commercial
terms to Member States, persons and undertakings of the Community where they possess exclusive
rights to patents filed in Member States and insofar as they have no obligation or commitment in
respect of third parties to grant or offer to grant an exclusive or partially exclusive licence to the
rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, Ireland shall encourage and
facilitate the granting of sublicences on commercial terms to Member States, persons and
undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
SECTION 7
Provisions on the exchange of information with the United Kingdom in the field
of nuclear energy
Article 19
1. From 1 January 1973, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 281
Treaty establishing a Constitution for Europe
281
Energy Community, shall be placed at the disposal of the United Kingdom, which shall give it limited
distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1973, the United Kingdom shall place at the disposal of the European Atomic
Energy Community an equivalent volume of information in the sectors set out in the list contained in
the Annex ( 1 ) to Protocol No 28 to the Act concerning the conditions of accession of the Kingdom
of Denmark, Ireland and the United Kingdom of Great Britain and Northern Ireland. This
information shall be set forth in detail in a document transmitted to the Commission. The
Commission shall communicate this information to Community undertakings under the conditions
laid down in Article 13 of the Treaty establishing the European Atomic Energy Community.
3. In view of the European Atomic Energy Community's greater interest in certain sectors, the
United Kingdom shall lay special emphasis on the transmission of information in the following
sectors:
(a) fast reactor research and development (including safety);
(b) fundamental research (applicable to reactor types);
(c) reactor safety (other than fast reactors);
(d) metallurgy, steel, zirconium alloys and concrete;
(e) compatibility of structural materials;
(f) experimental fuel fabrication;
(g) thermohydrodynamics;
(h) instrumentation.
Article 20
1. In those fields in which the United Kingdom places information at the disposal of the European
Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in the Member States of the Community and insofar as they
have no obligation or commitment in respect of third parties to grant or offer to grant an exclusive
or partially exclusive licence to the rights in these patents.
( 1 )
OJ L 73, 27.3.1972, p. 84.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 282
282
Part IV
2. Where an exclusive or partially exclusive licence has been granted, the United Kingdom shall
encourage and facilitate the granting of sublicences on commercial terms to the Member States,
persons and undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
TITLE III
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS
OF ACCESSION OF THE HELLENIC REPUBLIC
SECTION 1
Provisions on the granting by Greece of exemption from customs duties
on the import of certain goods
Article 21
Article III‑151 of the Constitution shall not prevent the Hellenic Republic from maintaining measures
of exemption granted before 1 January 1979 pursuant to:
(a) Law No 4171/61 (General measures to aid development of the country's economy),
(b) Decree Law No 2687/53 (Investment and protection of foreign capital),
(c) Law No 289/76 (Incentives with a view to promoting the development of frontier regions and
governing all pertinent questions),
until the expiry of the agreements concluded by the Hellenic Government with those persons
benefiting from these measures.
SECTION 2
Provisions on taxation
Article 22
The acts listed in point II.2 of Annex VIII ( 1 ) to the Act concerning the conditions of accession of the
Hellenic Republic shall apply in respect of the Hellenic Republic under the conditions laid down in
that Annex, with the exception of the references to points 9 and 18(b) thereof.
( 1 )
OJ L 291, 19.11.1979, p. 163.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 283
Treaty establishing a Constitution for Europe
283
SECTION 3
Provisions on cotton
Article 23
1. This Section concerns cotton, not carded or combed, falling within subheading 5201 00 of the
Combined Nomenclature.
2.
A system shall be introduced in the Union particularly to:
(a) support the production of cotton in regions of the Union where it is important for the
agricultural economy,
(b) permit the producers concerned to earn a fair income,
(c) stabilise the market by structural improvements at the level of supply and marketing.
3.
The system referred to in paragraph 2 shall include the grant of an aid to production.
4. In order to allow cotton producers to concentrate supply and to adapt production to market
requirements, a system shall be introduced to encourage the formation of producer groups and
federations of such groups.
This system shall provide for the grant of aids with a view to providing incentives for the formation
and facilitating the functioning of producer groups.
The only groups that may benefit from this system must:
(a) be formed on the initiative of the producers themselves,
(b) offer a sufficient guarantee for the duration and effectiveness of their action,
(c) be recognised by the Member State concerned.
5. The Union trading system with third countries shall not be affected. In this respect, in particular,
no measure restricting imports may be laid down.
6. A European law of the Council shall establish the adjustments necessary to the system
introduced pursuant to this Section.
The Council, on a proposal from the Commission, shall adopt the European regulations and
decisions establishing the general rules necessary for implementing the provisions of this Section.
The Council shall act after consulting the European Parliament.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 284
284
Part IV
SECTION 4
Provisions on the economic and industrial development of Greece
Article 24
The Member States take note of the fact that the Hellenic Government has embarked upon the
implementation of a policy of industrialisation and economic development designed to align the
standards of living in Greece with those of the other Member States and to eliminate
underemployment while progressively evening out regional differences in levels of development.
They recognise it to be in their common interest that the objectives of this policy be so attained.
To this end, the institutions shall implement all the means and procedures laid down by the
Constitution, particularly by making adequate use of the Union resources intended for the realisation
of its objectives.
In particular, in the application of Articles III‑167 and III‑168 of the Constitution, it will be necessary
to take into account the objectives of economic expansion and the raising of the standard of living of
the population.
SECTION 5
Provisions on the exchange of information with Greece in the field of nuclear energy
Article 25
1. From 1 January 1981, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of the Hellenic Republic, which shall give it limited
distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1981, the Hellenic Republic shall place at the disposal of the European Atomic
Energy Community information obtained in the nuclear field in Greece which is given limited
distribution, insofar as strictly commercial applications are not involved. The Commission shall
communicate this information to Community undertakings under the conditions laid down in
Article 13 of the Treaty establishing the European Atomic Energy Community.
3.
The information referred to in paragraphs 1 and 2 shall mainly concern:
(a) studies on the application of radioisotopes in the following fields: medicine, agriculture,
entomology and environmental protection,
(b) the application of nuclear technology to archeometry,
(c) the development of electronic medical apparatus,
(d) the development of methods of radioactive ore prospecting.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 285
Treaty establishing a Constitution for Europe
285
Article 26
1. In those sectors in which the Hellenic Republic places information at the disposal of the
European Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in Member States of the Community and insofar as they have
no obligation or commitment in respect of third parties to grant or offer to grant an exclusive or
partially exclusive licence to the rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, the Hellenic Republic shall
encourage and facilitate the granting of sublicences on commercial terms to Member States, persons
and undertakings of the European Atomic Energy Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
TITLE IV
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS OF ACCESSION
OF THE KINGDOM OF SPAIN AND THE PORTUGUESE REPUBLIC
SECTION 1
Financial provisions
Article 27
The own resources accruing from value added tax shall be calculated and checked as if the Canary
Islands and Ceuta and Melilla were included in the territorial field of application of Sixth Council
Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States
relating to turnover taxes — Common system of value added tax: uniform basis of assessment.
SECTION 2
Provisions on patents
Article 28
The provisions of Spanish national law relating to the burden of proof, which were adopted under
paragraph 2 of Protocol No 8 to the Act concerning the conditions of accession of the Kingdom of
Spain and the Portuguese Republic, shall not apply if the infringement proceedings are brought
against the holder of another process patent for the manufacture of a product identical to that954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 286
286
Part IV
obtained as the result of the patented process of the plaintiff, if that other patent was issued before
1 January 1986.
In cases where shifting the burden of proof does not apply, the Kingdom of Spain shall continue to
require the patent holder to adduce proof of infringement. In all these cases the Kingdom of Spain
shall apply a judicial procedure known as ‘distraint‑description’.
‘Distraint‑description’ means a procedure forming part of the system referred to in the first and
second paragraphs by which any person entitled to bring an action for infringement may, after
obtaining a court order, granted on his application, cause a detailed description to be made, at the
premises of the alleged infringer, by a bailiff assisted by experts, of the processes in question, in
particular by photocopying technical documents, with or without actual distraint. This court order
may order the payment of a security, intended to grant damages to the alleged infringer in case of
injury caused by the ‘distraint‑description’.
Article 29
The provisions of Portuguese national law relating to the burden of proof, which were adopted under
paragraph 2 of Protocol No 19 to the Act concerning the conditions of accession of the Kingdom of
Spain and the Portuguese Republic, shall not apply if the infringement proceedings are brought
against the holder of another process patent for the manufacture of a product identical to that
obtained as the result of the patented process of the plaintiff, if that other patent was issued before
1 January 1986.
In cases where shifting the burden of proof does not apply, the Portuguese Republic shall continue to
require the patent holder to adduce proof of infringement. In all these cases, the Portuguese Republic
shall apply a judicial procedure known as ‘distraint‑description’.
‘Distraint‑description’ means a procedure forming part of the system referred to in the first and
second paragraphs by which any person entitled to bring an action for infringement may, after
obtaining a court order, granted on his application, cause a detailed description to be made, at the
premises of the alleged infringer, by a bailiff assisted by experts, of the processes in question, in
particular by photocopying technical documents, with or without actual distraint. This court order
may order the payment of a security, intended to grant damages to the alleged infringer in case of
injury caused by the ‘distraint‑description’.
SECTION 3
Provisions on the mechanism for additional responsibilities within the framework
of fisheries agreements concluded by the Union with third countries
Article 30
1. A specific system is hereby established for the execution of operations carried out as a
complement to fishing activities undertaken by vessels flying the flag of a Member State in waters
falling under the sovereignty or within the jurisdiction of a third country within the framework of
responsibilities created under fisheries agreements concluded by the Union with the third countries in
question.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 287
Treaty establishing a Constitution for Europe
287
2. Operations considered likely to occur by way of addition to fishing activities subject to the
conditions and within the limits referred to in paragraphs 3 and 4 relate to:
(a) the processing, in the territory of the third country concerned, of fishery products caught by
vessels flying the flag of a Member State in the waters of that third country in the course of
fishing activities carried out by virtue of a fisheries agreement, with a view to those products
being put on the Union market under tariff headings falling within Chapter 3 of the Common
Customs Tariff,
(b) the loading or transhipment aboard a vessel flying the flag of a Member State occurring within
the framework of activities provided for under such a fisheries agreement, of fishery products
falling within Chapter 3 of the Common Customs Tariff with a view to their transport and any
processing for the purpose of being put on the Union market.
3. The import into the Union of products having been the subject of the operations referred to in
paragraph 2 shall be carried out subject to suspension, in part or in whole, of the Common Customs
Tariff duties or subject to a special system of charges, under the conditions and within the limits of
additionality fixed annually in relation to the volume of fishing possibilities deriving from the
agreements in question and from their accompanying detailed rules.
4. European laws or framework laws shall lay down the general rules of application of this system
and in particular the criteria for fixing and apportioning the quantities concerned.
The detailed implementing rules of this system and the quantities concerned shall be adopted in
accordance with the procedure laid down in Article 37 of Regulation (EC) No 104/2000.
SECTION 4
Provisions on Ceuta and Melilla
Subsection 1
General provisions
Article 31
1. The Constitution and the acts of the institutions shall apply to Ceuta and to Melilla, subject to the
derogations referred to in paragraphs 2 and 3 and to the other provisions of this Section.
2. The conditions under which the provisions of the Constitution concerning the free movement of
goods, and the acts of the institutions concerning customs legislation and commercial policy, shall
apply to Ceuta and to Melilla are set out in Subsection 3 of this Section.
3. Without prejudice to the specific provisions of Article 32, the acts of the institutions concerning
the common agricultural policy and the common fisheries policy shall not apply to Ceuta or to
Melilla.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 288
288
4.
Part IV
At the request of the Kingdom of Spain, a European law or framework law of the Council may:
(a) include Ceuta and Melilla in the customs territory of the Union;
(b) define the appropriate measures aimed at extending to Ceuta and to Melilla the provisions of
Union law in force.
On a proposal from the Commission acting on its own initiative or at the request of a Member State,
the Council may adopt a European law or framework law adjusting the arrangements applicable to
Ceuta and to Melilla if necessary.
The Council shall act unanimously after consulting the European Parliament.
Subsection 2
Provisions relating to the Common Fisheries Policy
Article 32
1. Subject to paragraph 2 and without prejudice to Subsection 3, the common fisheries policy shall
not apply to Ceuta or to Melilla.
2. The Council, on a proposal from the Commission, shall adopt the European laws, framework
laws, regulations or decisions which:
(a) determine the structural measures which may be adopted in favour of Ceuta and Melilla;
(b) determine the procedures appropriate to take into consideration all or part of the interests of
Ceuta and Melilla when it adopts acts, case by case, with a view to the negotiations by the Union
aimed at the resumption or conclusion of fisheries agreements with third countries and to the
specific interests of Ceuta and Melilla within international conventions concerning fisheries, to
which the Union is a contracting party.
3. The Council, on a proposal from the Commission, shall adopt the European laws, framework
laws, regulations or decisions which determine, where appropriate, the possibilities and conditions of
mutual access to respective fishing zones and to the resources thereof. It shall act unanimously.
4. The European laws and framework laws referred to in paragraphs 2 and 3 shall be adopted after
consultation of the European Parliament.
Subsection 3
Provisions on free movement of goods, customs legislation and commercial policy
Article 33
1. Products originating in Ceuta or in Melilla and products coming from third countries imported
into Ceuta or into Melilla under the arrangements which are applicable there to them shall not be954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 289
Treaty establishing a Constitution for Europe
289
deemed, when released for free circulation in the customs territory of the Union, to be goods
fulfilling the conditions of paragraphs 1 to 3 of Article III‑151 of the Constitution.
2.
The customs territory of the Union shall not include Ceuta and Melilla.
3. Except where otherwise provided for in this Subsection, the acts of the institutions regarding
customs legislation for foreign trade shall apply under the same conditions to trade between the
customs territory of the Union, on the one hand, and Ceuta and Melilla, on the other.
4. Except where otherwise provided for in this Subsection, the acts of the institutions regarding the
common commercial policy, be they autonomous or enacted by agreement, directly linked to the
import or export of goods, shall not be applicable to Ceuta or to Melilla.
5. Except where otherwise provided for in this Title, the Union shall apply in its trade with Ceuta
and Melilla, for products falling within Annex I to the Constitution, the general arrangements which
it applies in its foreign trade.
Article 34
Subject to Article 35, customs duties on the import into the customs territory of the Union of
products originating in Ceuta or in Melilla shall be abolished.
Article 35
1. Fishery products falling within headings 0301, 0302, 0303, 1604, 1605 and subheadings
0511 91 and 2301 20 of the Common Customs Tariff and originating in Ceuta or in Melilla, shall,
within the limit of tariff quotas calculated by product and on the average quantities actually disposed
of during 1982, 1983 and 1984, qualify for exemption from customs duties throughout the customs
territory of the Union.
The release for free circulation of products imported into the customs territory of the Union, under
these tariff quotas, shall be subject to compliance with the rules laid down by the common
organisation of markets and in particular with respect to reference prices.
2. The Council, on a proposal from the Commission, shall each year adopt European regulations or
decisions opening and allocating tariff quotas in accordance with the detailed rules laid down in
paragraph 1.
Article 36
1. Where application of Article 34 could lead to a substantial increase in the import of certain
products originating in Ceuta or in Melilla such as might prejudice Union producers, the Council, on
a proposal from the Commission, may adopt European regulations or decisions to subject the access
of these products to the customs territory of the Union to special conditions.
2. Where, because the common commercial policy and the Common Customs Tariff are not
applied to the import of raw materials or intermediate products into Ceuta or into Melilla, imports of
a product originating in Ceuta or in Melilla cause, or may cause, serious injury to a producer activity954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 290
290
Part IV
exercised in one or more Member States, the Commission, at the request of a Member State or on its
own initiative, may take the appropriate measures.
Article 37
The customs duties on import into Ceuta and into Melilla of products originating in the customs
territory of the Union, and charges having equivalent effect, shall be abolished.
Article 38
The customs duties and charges having an effect equivalent to such duties and the trade arrangements
applied on the import into Ceuta and into Melilla of goods coming from a third country may not be
less favourable than those applicable by the Union in accordance with its international commitments
or its preferential arrangements with regard to such third country, providing that the same
third country grants, to imports from Ceuta and from Melilla, the same treatment as that which it
grants to the Union. However, the arrangements applied to imports into Ceuta and into Melilla with
regard to goods coming from such third country may not be more favourable than those applied
with regard to the imports of products originating in the customs territory of the Union.
Article 39
The Council, on a proposal from the Commission, shall adopt European regulations or decisions
laying down the rules for the application of this Subsection and in particular the rules of origin
applicable to trade, as referred to in Articles 34, 35 and 37, including the provisions concerning the
identification of originating products and the control of origin.
The rules will include, in particular, provisions on marking and/or labelling of products, on the
conditions of registration of vessels, on the application of the rule on mixed origin for fishery
products, and also provisions enabling the origin of products to be determined.
SECTION 5
Provisions on the regional development of Spain
Article 40
The Member States take note of the fact that the Spanish Government has embarked upon the
implementation of a policy of regional development designed in particular to stimulate economic
growth in the less‑developed regions and areas of Spain.
They recognise it to be in their common interest that the objectives of this policy be attained.
They agree, in order to help the Spanish Government to accomplish this task, to recommend that the
institutions use all the means and procedures laid down by the Constitution, particularly by making
adequate use of the Union resources intended for the realisation of its objectives.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 291
Treaty establishing a Constitution for Europe
291
The Member States recognise in particular that, in the application of Articles III‑167 and III‑168 of
the Constitution, it will be necessary to take into account the objectives of economic expansion and
the raising of the standard of living of the population of the less‑developed regions and areas of
Spain.
SECTION 6
Provisions on the economic and industrial development of Portugal
Article 41
The Member States take note of the fact that the Portuguese Government has embarked upon the
implementation of a policy of industrialisation and economic development designed to align the
standard of living in Portugal with that of the other Member States and to eliminate
underemployment while progressively evening out regional differences in levels of development.
They recognise it to be in their common interest that the objectives of this policy be attained.
They agree to recommend to this end that the institutions use all the means and procedures laid
down by the Constitution, particularly by making adequate use of the Union resources intended for
the realisation of its objectives.
The Member States recognise in particular that, in the application of Articles III-167 and III-168 of
the Constitution, it will be necessary to take into account the objectives of economic expansion and
the raising of the standard of living of the population.
SECTION 7
Provisions on the exchange of information with the Kingdom of Spain
in the field of nuclear energy
Article 42
1. From 1 January 1986, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of the Kingdom of Spain, which shall give it
limited distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1986, the Kingdom of Spain shall place at the disposal of the European Atomic
Energy Community information obtained in the nuclear field in Spain which is given limited
distribution, insofar as strictly commercial applications are not involved. The Commission shall
communicate this information to Community undertakings under the conditions laid down in
Article 13 of the Treaty establishing the European Atomic Energy Community.
3.
The information referred to in paragraphs 1 and 2 shall mainly concern:
(a) nuclear physics (low- and high-energy);
(b) radiation protection;954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 292
292
Part IV
(c) isotope applications, in particular those of stable isotopes;
(d) research reactors and relevant fuels;
(e) research into the field of the fuel cycle (more especially the mining and processing of low‑grade
uranium ore; optimisation of fuel elements for power reactors).
Article 43
1. In those sectors in which the Kingdom of Spain places information at the disposal of the
European Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in Member States and insofar as they have no obligation or
commitment in respect of third parties to grant or offer to grant an exclusive or partially exclusive
licence to the rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, the Kingdom of Spain shall
encourage and facilitate the granting of sublicences on commercial terms to Member States, persons
and undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
SECTION 8
Provisions on the exchange of information with the Portuguese Republic in the field
of nuclear energy
Article 44
1. From 1 January 1986, such information as has been communicated to Member States, persons
and undertakings, in accordance with Article 13 of the Treaty establishing the European Atomic
Energy Community, shall be placed at the disposal of the Portuguese Republic, which shall give it
limited distribution within its territory under the conditions laid down in that Article.
2. From 1 January 1986, the Portuguese Republic shall place at the disposal of the European
Atomic Energy Community information obtained in the nuclear field in Portugal which is given
limited distribution, insofar as strictly commercial applications are not involved. The Commission
shall communicate this information to Community undertakings under the conditions laid down in
Article 13 of the Treaty establishing the European Atomic Energy Community.
3.
The information referred to in paragraphs 1 and 2 shall mainly concern:
(a) reactor dynamics;
(b) radiation protection;954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 293
Treaty establishing a Constitution for Europe
293
(c) application of nuclear measuring techniques (in the industrial, agricultural, archaeological and
geological fields);
(d) atomic physics (effective measuring of cross sections, pipeline techniques);
(e) extractive metallurgy of uranium.
Article 45
1. In those sectors in which the Portuguese Republic places information at the disposal of the
European Atomic Energy Community, the competent authorities shall grant upon request licences on
commercial terms to Member States, persons and undertakings of the Community where they
possess exclusive rights to patents filed in Member States and insofar as they have no obligation or
commitment in respect of third parties to grant or offer to grant an exclusive or partially exclusive
licence to the rights in these patents.
2. Where an exclusive or partially exclusive licence has been granted, the Portuguese Republic shall
encourage and facilitate the granting of sublicences on commercial terms to Member States, persons
and undertakings of the Community by the holders of such licences.
Such exclusive or partially exclusive licences shall be granted on a normal commercial basis.
TITLE V
PROVISIONS TAKEN FROM THE ACT CONCERNING THE CONDITIONS OF ACCESSION OF THE
REPUBLIC OF AUSTRIA, THE REPUBLIC OF FINLAND AND THE KINGDOM OF SWEDEN
SECTION 1
Financial provisions
Article 46
Own resources accruing from value added tax shall be calculated and checked as though
the Åland Islands were included in the territorial scope of Sixth Council Directive 77/388/EEC of
17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes —
Common system of value added tax: uniform basis of assessment.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 294
294
Part IV
SECTION 2
Provisions on agriculture
Article 47
Where there are serious difficulties resulting from accession which remain after full utilisation of
Article 48 and of the other measures resulting from the rules existing in the Union, the Commission
may adopt a European decision authorising Finland to grant national aids to producers so as to
facilitate their full integration into the common agricultural policy.
Article 48
1. The Commission shall adopt European decisions authorising Finland and Sweden to grant
long‑term national aids with a view to ensuring that agricultural activity is maintained in specific
regions. These regions should cover the agricultural areas situated to the north of the 62nd Parallel
and some adjacent areas south of that parallel affected by comparable climatic conditions rendering
agricultural activity particularly difficult.
2. The regions referred to in paragraph 1 shall be determined by the Commission, taking into
consideration in particular:
(a) the low population density;
(b) the portion of agricultural land in the overall surface area;
(c) the portion of agricultural land devoted to arable crops intended for human consumption, in the
agricultural surface area used.
3. The national aids provided for in paragraph 1 may be related to physical factors of production,
such as hectares of agricultural land or heads of animal taking account of the relevant limits laid
down in the common organisations of the market, as well as the historical production patterns of
each farm, but must not:
(a) be linked to future production;
(b) or lead to an increase in production or in the level of overall support recorded during a reference
period preceding 1 January 1995, to be determined by the Commission.
These aids may be differentiated by region.
These aids must be granted in particular in order to:
(a) maintain traditional primary production and processing naturally suited to the climatic
conditions of the regions concerned;
(b) improve the structures for the production, marketing and processing of agricultural products;954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 295
Treaty establishing a Constitution for Europe
295
(c) facilitate the disposal of the said products;
(d) ensure that the environment is protected and the countryside preserved.
Article 49
1. The aids provided for in Articles 47 and 48 and any other national aid subject to Commission
authorisation under this Title shall be notified to the Commission. They may not be applied until
such authorisation has been given.
2. As regards the aids provided for in Article 48, the Commission shall submit to the Council every
five years as from 1 January 1996 a report on:
(a) the authorisations granted;
(b) the results of the aid granted under such authorisations.
In preparation for drawing up such reports, Member States in receipt of such authorisations shall
supply the Commission in good time with information on the effects of the aids granted, illustrating
the development noted in the agricultural economy of the regions in question.
Article 50
In the field of the aids provided for in Articles III‑167 and III‑168 of the Constitution:
(a) among the aids applied in Austria, Finland and Sweden prior to 1 January 1995, only those
notified to the Commission by 30 April 1995 will be deemed to be existing aids within the
meaning of Article III‑168(1) of the Constitution;
(b) existing aids and plans intended to grant or alter aids which were notified to the Commission
prior to 1 January 1995 shall be deemed to have been notified on that date.
Article 51
1. Unless otherwise stipulated in specific cases, the Council, on a proposal from the Commission,
shall adopt the necessary European regulations or decisions to implement this Section.
2. A European law of the Council may make the adaptations to the provisions appearing in this
Section which may prove necessary as a result of a modification in Union law. The Council shall act
unanimously after consulting the European Parliament.
Article 52
1. If transitional measures are necessary to facilitate the transition from the existing regime in
Austria, Finland and Sweden to that resulting from application of the common organisation of the
markets under the conditions set out in the Act concerning the conditions of accession of the
Republic of Austria, the Republic of Finland and the Kingdom of Sweden, such measures shall be
adopted in accordance with the procedure laid down in Article 38 of Regulation No 136/66/EEC or,
as appropriate, in the corresponding Articles of the other Regulations on the common organisation954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 296
296
Part IV
of agricultural markets. These measures may be taken during a period expiring on 31 December 1997
and their application shall be limited to that date.
2. A European law of the Council may extend the period referred to in paragraph 1. The Council
shall act unanimously after consulting the European Parliament.
Article 53
Articles 51 and 52 shall be applicable to fishery products.
SECTION 3
Provisions on transitional measures
Article 54
The Acts listed in points VII.B.I, VII.D.1, VII.D.2.c, IX.2.b, c, f, g, h, i, j, l, m, n, x, y, z and aa, and X.a, b
and c of Annex XV ( 1 ) to the Act concerning the conditions of accession of the Republic of Austria,
the Republic of Finland and the Kingdom of Sweden shall apply in respect of Austria, Finland and
Sweden under the conditions laid down in that Annex.
With regard to point IX.2.x of Annex XV referred to in the first paragraph, the reference to the
provisions of the Treaty establishing the European Community, in particular to Articles 90 and 91
thereof, must be understood as referring to the provisions of the Constitution, in particular to
Article III‑170(1) and (2) thereof.
SECTION 4
Provisions on the applicability of certain acts
Article 55
1. Any individual exemption decisions taken and negative clearance decisions taken before
1 January 1995 under Article 53 of the Agreement on the European Economic Area (EEA) or
Article 1 of Protocol 25 to that Agreement, whether by the Surveillance Authority of the European
Free Trade Association (EFTA) or the Commission, and which concern cases which fall under
Article 81 of the Treaty establishing the European Community as a result of accession shall remain
valid for the purposes of Article III‑161 of the Constitution until the time limit specified therein
expires or until the Commission adopts a duly motivated European decision to the contrary, in
accordance with Union law.
2. All decisions taken by the EFTA Surveillance Authority before 1 January 1995 pursuant to
Article 61 of the EEA Agreement and which fall under Article 87 of the Treaty establishing the
European Community as a result of accession shall remain valid with respect to Article III‑167 of the
Constitution unless the Commission adopts a European decision to the contrary pursuant to
( 1 )
OJ C 241, 29.8.1994, p. 322.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 297
Treaty establishing a Constitution for Europe
297
Article III‑168 of the Constitution. This paragraph shall not apply to decisions subject to the
proceedings provided for in Article 64 of the EEA Agreement.
3. Without prejudice to paragraphs 1 and 2, the decisions taken by the EFTA Surveillance Authority
remain valid after 1 January 1995 unless the Commission takes a duly motivated decision to the
contrary in accordance with Union law.
SECTION 5
Provisions on the Åland Islands
Article 56
The provisions of the Constitution shall not preclude the application of the existing provisions in
force on 1 January 1994 on the Åland islands on:
(a) restrictions, on a non-discriminatory basis, on the right of natural persons who do not enjoy
hembygdsrätt/kotiseutuoikeus (regional citizenship) in Åland, and for legal persons, to acquire
and hold real property on the Åland islands without permission by the competent authorities of
the Åland islands;
(b) restrictions, on a non-discriminatory basis, on the right of establishment and the right to provide
services by natural persons who do not enjoy hembygdsrätt/kotiseutuoikeus (regional citizenship)
in Åland, or by legal persons without permission by the competent authorities of the Åland
islands.
Article 57
1. The territory of the Åland Islands — being considered as a third territory, as defined in the third
indent of Article 3(1) of Council Directive 77/388/EEC, and as a national territory falling outside the
field of application of the excise harmonisation directives as defined in Article 2 of Council Directive
92/12/EEC — shall be excluded from the territorial application of Union law in the fields of
harmonisation of the laws of the Member States on turnover taxes and on excise duties and other
forms of indirect taxation.
This paragraph shall not apply to the provisions of Council Directive 69/335/EEC relating to capital
duty.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 298
298
Part IV
2. The derogation provided for in paragraph 1 is aimed at maintaining a viable local economy in
the islands and shall not have any negative effects on the interests of the Union nor on its common
policies. If the Commission considers that the provisions in paragraph 1 are no longer justified,
particularly in terms of fair competition or own resources, it shall submit appropriate proposals to
the Council, which shall adopt the necessary acts in accordance with the pertinent articles of the
Constitution.
Article 58
The Republic of Finland shall ensure that the same treatment applies to all natural and legal persons
of the Member States in the Åland islands.
Article 59
The provisions of this Section shall apply in the light of the Declaration on the Åland Islands, which
incorporates, without altering its legal effect, the wording of the preamble to Protocol No 2 to the Act
concerning the conditions of accession of the Republic of Austria, the Republic of Finland and the
Kingdom of Sweden.
SECTION 6
Provisions on the Sami people
Article 60
Notwithstanding the provisions of the Constitution, exclusive rights to reindeer husbandry within
traditional Sami areas may be granted to the Sami people.
Article 61
This Section may be extended to take account of any further development of exclusive Sami rights
linked to their traditional means of livelihood. A European law of the Council may make the
necessary amendments to this Section. The Council shall act unanimously after consulting the
European Parliament and the Committee of the Regions.
Article 62
The provisions of this Section shall apply in the light of the Declaration on the Sami people, which
incorporates, without altering its legal effect, the wording of the preamble to Protocol 3 to the Act
concerning the conditions of accession of the Republic of Austria, the Republic of Finland and the
Kingdom of Sweden.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 299
Treaty establishing a Constitution for Europe
299
SECTION 7
Special provisions in the framework of the Structural Funds in Finland and Sweden
Article 63
Areas covered by the objective of promoting the development and structural adjustment of regions
with an extremely low population density shall in principle represent or belong to regions at NUTS
level II with a population density of 8 persons per km 2 or less. Union assistance may, subject to the
requirement of concentration, also extend to adjacent and contiguous smaller areas fulfilling the same
population density criterion. The regions and areas referred to in this Article, are listed in Annex 1 ( 1 )
to Protocol 6 to the Act concerning the conditions of accession of the Republic of Austria, the
Republic of Finland and the Kingdom of Sweden.
SECTION 8
Provisions on rail and combined transport in Austria
Article 64
1.
For the purposes of this Section, the following definitions shall apply:
(a) ‘heavy goods vehicle’ shall mean any motor vehicle with a maximum authorised weight of over
7,5 tonnes registered in a Member State designed to carry goods or haul trailers, including
semi‑trailer tractor units, and trailers with a maximum authorised weight of over 7,5 tonnes and
hauled by a motor vehicle registered in a Member State with a maximum authorised weight of
7,5 tonnes or less;
(b) ‘combined transport’ shall mean the carriage of goods by heavy goods vehicles or loading units
which complete part of their journey by rail and either begin or end the journey by road,
whereby transit traffic may under no circumstances cross Austrian territory on its way to or from
a rail terminal by road alone.
2. Articles 65 to 71 shall apply to measures relating to the provision of rail and combined transport
crossing the territory of Austria.
Article 65
The Union and the Member States concerned shall, within the framework of their respective
competences, adopt and closely coordinate measures for the development and promotion of rail and
combined transport for the trans‑Alpine carriage of goods.
( 1 )
OJ C 241, 29.8.1994, p. 355.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 300
300
Part IV
Article 66
When establishing the guidelines provided for in Article III‑247 of the Constitution, the Union shall
ensure that the axes defined in Annex 1 ( 1 ) to Protocol 9 to the Act concerning the conditions of
accession of the Republic of Austria, the Republic of Finland and the Kingdom of Sweden form part
of the trans‑European networks for rail and combined transport and are furthermore identified as
projects of common interest.
Article 67
The Union and the Member States concerned shall, within the framework of their respective
competences, implement the measures listed in Annex 2 ( 2 ) to Protocol 9 to the Act concerning the
conditions of accession of the Republic of Austria, the Republic of Finland and the Kingdom of
Sweden.
Article 68
The Union and the Member States concerned shall use their best endeavours to develop and utilise
the additional railway capacity referred to in Annex 3 ( 3 ) to Protocol 9 to the Act concerning the
conditions of accession of the Republic of Austria, the Republic of Finland and the Kingdom of
Sweden.
Article 69
The Union and the Member States concerned shall take measures to enhance the provision of rail and
combined transport. Where appropriate, and subject to the provisions of the Constitution, such
measures shall be established in close consultation with railway companies and other railway service
providers. Priority should be given to those measures set out in the provisions of Union law on
railways and combined transport. In implementing any measures, particular attention shall be
attached to the competitiveness, effectiveness and cost transparency of rail and combined transport.
In particular, the Member States concerned shall endeavour to take such measures so as to ensure that
prices for combined transport are competitive with those for other modes of transport. Any aid
granted to these ends shall comply with Union law.
Article 70
The Union and the Member States concerned shall, in the event of a serious disturbance in rail transit,
such as a natural disaster, take all possible concerted action to maintain the flow of traffic. Priority
shall be given to sensitive loads, such as perishable foods.
( 1 ) OJ C 241, 29.8.1994, p. 364.
( 2 ) OJ C 241, 29.8.1994, p. 365.
( 3 ) OJ C 241, 29.8.1994, p. 367.954393_TRAITE_EN_251_300
12-01-2005
15:21
Pagina 301
Treaty establishing a Constitution for Europe
301
Article 71
The Commission, acting in accordance with the procedure laid down in Article 73(2), shall review the
operation of this Section.
Article 72
1. This Article shall apply to the carriage of goods by road on journeys carried out within the
territory of the Community.
2. For journeys which involve transit of goods by road through Austria, the regime established for
journeys on own account and for journeys for hire or reward under the First Council Directive of
23 July 1962 and Council Regulation (EEC) No 881/92 shall apply subject to the provisions of this
Article.
3.
Until 1 January 1998, the following provisions shall apply:
(a) The total of NOx emissions from heavy goods vehicles crossing Austria in transit shall be reduced
by 60 % in the period between 1 January 1992 and 31 December 2003, according to the table in
Annex 4.
(b) The reductions in total NOx emissions from heavy goods vehicles shall be administered
according to an ecopoints system. Under that system any heavy goods vehicle crossing Austria in
transit shall require a number of ecopoints equivalent to its NOx emissions (authorised under the
Conformity of Production (COP) value or type‑approval value). The method of calculation and
administration of such points is described in Annex 5.
(c) If the number of transit journeys in any year exceeds the reference figure established for 1991 by
more than 8 %, the Commission, acting in accordance with the procedure laid down in
Article 16, shall adopt appropriate measures in accordance with paragraph 3 of Annex 5.
(d) Austria shall issue and make available in good time the ecopoints cards required for the
administration of the ecopoints system, pursuant to Annex 5, for heavy goods vehicles crossing
Austria in transit.
(e) The ecopoints shall be distributed by the Commission among Member States in accordance with
provisions to be established in accordance with paragraph 7.
4. Before 1 January 1998, the Council, on the basis of a report by the Commission, shall review the
operation of provisions concerning transit of goods by road through Austria. The review shall take
place in conformity with basic principles of Community law, such as the proper functioning of the
internal market, in particular the free movement of goods and freedom to provide services,
protection of the environment in the interest of the Community as a whole, and traffic safety. Unless
the Council, acting unanimously on a proposal from the Commission and after consulting the
European Parliament, decides otherwise, the transitional period shall be extended to 1 January 2001,
during which the provisions of paragraph 3 shall apply.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 302
302
Part IV
5. Before 1 January 2001, the Commission, in cooperation with the European Environment
Agency, shall make a scientific study of the degree to which the objective concerning reduction of
pollution set out in paragraph 3(a) has been achieved. If the Commission concludes that this objective
has been achieved on a sustainable basis, the provisions of paragraph 3 shall cease to apply on
1 January 2001. If the Commission concludes that this objective has not been achieved on a
sustainable basis, the Council, acting in accordance with Article 75 of the EC Treaty, may adopt
measures, within a Community framework, which ensure equivalent protection of the environment,
in particular a 60 % reduction of pollution. If the Council does not adopt such measures, the
transitional period shall be automatically extended for a final period of three years, during which the
provisions of paragraph 3 shall apply.
6.
At the end of the transitional period, the Community acquis in its entirety shall be applied.
7. The Commission, acting in accordance with the procedure laid down in Article 16, shall adopt
detailed measures concerning the procedures relating to the ecopoints system, the distribution of
ecopoints and technical questions concerning the application of this Article, which shall enter into
force on the date of accession of Austria.
The measures referred to in the first subparagraph shall ensure that the factual situation for the
present Member States resulting from the application of Council Regulation (EEC) No 3637/92 and
of the Administrative Arrangement, signed on 23 December 1992, setting the date of entry into force
and the procedures for the introduction of the ecopoints system referred to in the Transit Agreement,
is maintained. All necessary efforts shall be made to ensure that the share of ecopoints allocated to
Greece takes sufficient account of Greek needs in this context.
Article 73
1.
The Commission shall be assisted by a Committee.
2. In cases where reference is made to this paragraph, Articles 3 and 7 of Decision 1999/468/EC
shall apply.
3.
The Committee shall adopt its Rules of Procedure.
SECTION 9
Provisions on the use of specific Austrian terms of the German language in the framework
of the European Union
Article 74
1. The specific Austrian terms of the German language contained in the Austrian legal order and
listed in the Annex ( 1 ) to Protocol No 10 to the Act concerning the conditions of accession of the
Republic of Austria, the Republic of Finland and the Kingdom of Sweden shall have the same status
( 1 )
OJ C 241, 29.8.1994, p. 370.954393_TRAITE_EN_301_350
12-01-2005
15:56
Treaty establishing a Constitution for Europe
Pagina 303
303
and may be used with the same legal effect as the corresponding terms used in Germany listed in that
Annex.
2. In the German language version of new legal acts the specific Austrian terms referred to in the
Annex to Protocol No 10 to the Act concerning the conditions of accession of the Republic of
Austria, the Republic of Finland and the Kingdom of Sweden shall be added in appropriate form to
the corresponding terms used in Germany.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 304
304
9.
Part IV
PROTOCOL ON THE TREATY AND THE ACT OF ACCESSION OF THE CZECH REPUBLIC,
THE REPUBLIC OF ESTONIA, THE REPUBLIC OF CYPRUS, THE REPUBLIC OF LATVIA,
THE REPUBLIC OF LITHUANIA, THE REPUBLIC OF HUNGARY, THE REPUBLIC OF MALTA,
THE REPUBLIC OF POLAND, THE REPUBLIC OF SLOVENIA AND THE SLOVAK REPUBLIC
THE HIGH CONTRACTING PARTIES,
RECALLING that the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the
Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia
and the Slovak Republic acceded to the European Communities and to the European Union established by the Treaty on
European Union on 1 May 2004;
CONSIDERING that Article IV‑437(2)(e) of the Constitution provides that the Treaty of 16 April 2003 concerning the
accessions referred to above shall be repealed;
CONSIDERING that many of the provisions of the Act annexed to that Treaty of Accession remain relevant; that Article
IV‑437(2) of the Constitution provides that those provisions must be set out or referred to in a Protocol, so that they
remain in force and their legal effects are preserved;
CONSIDERING that some of those provisions require the technical adjustments necessary to bring them into line with
the Constitution without altering their legal effect,
HAVE AGREED UPON the following provisions, which shall be annexed to the Treaty establishing a Constitution for
Europe and to the Treaty establishing the European Atomic Energy Community:
PART ONE
PROVISIONS RELATING TO THE ACT OF ACCESSION OF 16 APRIL 2003
TITLE I
PRINCIPLES
Article 1
For the purposes of this Protocol:
(a) the expression ‘Act of Accession of 16 April 2003’ means the Act concerning the conditions of
accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of
Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of
Poland, the Republic of Slovenia and the Slovak Republic and the adjustments to the Treaties on
which the European Union is founded;954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 305
Treaty establishing a Constitution for Europe
305
(b) the expressions ‘Treaty establishing the European Community’ (EC Treaty) and ‘Treaty
establishing the European Atomic Energy Community’ (EAEC Treaty) mean those Treaties as
supplemented or amended by treaties or other acts which entered into force before 1 May 2004;
(c) the expression ‘Treaty on European Union’ (EU Treaty) means that Treaty as supplemented or
amended by treaties or other acts which entered into force before 1 May 2004;
(d) the expression ‘the Community’ means one or both of the Communities referred to in (b) as the
case may be;
(e) the expression ‘present Member States’ means the following Member States: the Kingdom of
Belgium, the Kingdom of Denmark, the Federal Republic of Germany, the Hellenic Republic, the
Kingdom of Spain, the French Republic, Ireland, the Italian Republic, the Grand Duchy of
Luxembourg, the Kingdom of the Netherlands, the Republic of Austria, the Portuguese Republic,
the Republic of Finland, the Kingdom of Sweden and the United Kingdom of Great Britain and
Northern Ireland;
(f) the expression ‘new Member States’ means the following Member States: the Czech Republic, the
Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the
Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and
the Slovak Republic.
Article 2
The rights and obligations resulting from the Treaty on the Accession of the Czech Republic, the
Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the
Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the
Slovak Republic, referred to in Article IV‑437(2)(e) of the Constitution, took effect, under the
conditions laid down in that Treaty, as from 1 May 2004.
Article 3
1. The provisions of the Schengen acquis integrated into the framework of the Union by the
Protocol annexed to the Treaty establishing a Constitution for Europe (hereinafter referred to as the
‘Schengen Protocol’) and the acts building upon it or otherwise related to it, listed in Annex I to the
Act of Accession of 16 April 2003, as well as any further such acts adopted before 1 May 2004, shall
be binding on and applicable in the new Member States from 1 May 2004.
2. Those provisions of the Schengen acquis as integrated into the framework of the Union and the
acts building upon it or otherwise related to it not referred to in paragraph 1, while binding on the
new Member States from 1 May 2004, shall apply in a new Member State only pursuant to a
European decision of the Council to that effect after verification in accordance with the applicable
Schengen evaluation procedures that the necessary conditions for the application of all parts of the
acquis concerned have been met in that new Member State.
The Council shall take its decision, after consulting the European Parliament, acting with the
unanimity of its members representing the Governments of the Member States in respect of which
the provisions referred to in the present paragraph have already been put into effect and of the
representative of the Government of the Member State in respect of which those provisions are to be954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 306
306
Part IV
put into effect. The members of the Council representing the Governments of Ireland and of the
United Kingdom of Great Britain and Northern Ireland shall take part in such a decision insofar as it
relates to the provisions of the Schengen acquis and the acts building upon it or otherwise related to it
in which these Member States participate.
3. The Agreements concluded by the Council under Article 6 of the Schengen Protocol shall be
binding on the new Member States from 1 May 2004.
4. The new Member States shall be required in respect of those conventions or instruments in the
field of justice and home affairs which are inseparable from the attainment of the objectives of the EU
Treaty:
(a) to accede to those which, by 1 May 2004, have been opened for signature by the present Member
States, and to those which have been drawn up by the Council in accordance with Title VI of the
EU Treaty and recommended to the Member States for adoption;
(b) to introduce administrative and other arrangements, such as those adopted by 1 May 2004 by the
present Member States or by the Council, to facilitate practical cooperation between the Member
States' institutions and organisations working in the field of justice and home affairs.
Article 4
Each of the new Member States shall participate in Economic and Monetary Union from 1 May 2004
as a Member State with a derogation within the meaning of Article III‑197 of the Constitution.
Article 5
1. The new Member States, which have acceded by the Act of Accession of 16 April 2003 to the
decisions and agreements adopted by the Representatives of the Governments of the Member States,
meeting within the Council, shall be required to accede to all other agreements concluded by the
present Member States relating to the functioning of the Union or connected with the activities
thereof.
2. The new Member States shall be required to accede to the conventions provided for in Article
293 of the EC Treaty and to those that are inseparable from the attainment of the objectives of the EC
Treaty, insofar as they are still in force, and also to the protocols on the interpretation of those
conventions by the Court of Justice of the European Communities, signed by the present Member
States, and to this end they shall be required to enter into negotiations with the present Member
States in order to make the necessary adjustments thereto.
Article 6
1. The new Member States shall be required to accede, under the conditions laid down in this
Protocol, to the agreements or conventions concluded or provisionally applied by the present
Member States and the Union or the European Atomic Energy Community, acting jointly, and to the
agreements concluded by those States which are related to those agreements or conventions.954393_TRAITE_EN_301_350
12-01-2005
15:56
Treaty establishing a Constitution for Europe
Pagina 307
307
The accession of the new Member States to the agreements or conventions mentioned in paragraph
4, as well as the agreements with Belarus, China, Chile, Mercosur and Switzerland which have been
concluded or signed by the Community and its present Member States jointly shall be agreed by the
conclusion of a protocol to such agreements or conventions between the Council, acting
unanimously on behalf of the Member States, and the third country or countries or international
organisation concerned. This procedure is without prejudice to the Union's and the European Atomic
Energy Community's own competences and does not affect the allocation of powers between the
Union and the European Atomic Energy Community and the Member States as regards the
conclusion of such agreements in the future or any other amendments not related to accession. The
Commission shall negotiate these protocols on behalf of the Member States on the basis of
negotiating directives approved by the Council, acting by unanimity, and in consultation with a
committee comprised of the representatives of the Member States. It shall submit a draft of the
protocols for conclusion to the Council.
2. Upon acceding to the agreements and conventions referred to in paragraph 1 the new Member
States shall acquire the same rights and obligations under those agreements and conventions as the
present Member States.
3. The new Member States shall be required to accede, under the conditions laid down in this
Protocol, to the Agreement on the European Economic Area ( 1 ), in accordance with Article 128 of
that Agreement.
4. As from 1 May 2004, and, where appropriate, pending the conclusion of the necessary protocols
referred to in paragraph 1, the new Member States shall apply the provisions of the Agreements
concluded by the present Member States and, jointly, the Community, with Algeria, Armenia,
Azerbaijan, Bulgaria, Croatia, Egypt, the former Yugoslav Republic of Macedonia, Georgia, Israel,
Jordan, Kazakhstan, Kyrgyzstan, Lebanon, Mexico, Moldova, Morocco, Romania, the Russian
Federation, San Marino, South Africa, South Korea, Syria, Tunisia, Turkey, Turkmenistan, Ukraine and
Uzbekistan as well as the provisions of other agreements concluded jointly by the present Member
States and the Community before 1 May 2004.
Any adjustments to these Agreements shall be the subject of protocols concluded with the
co‑contracting countries in conformity with the provisions of the second subparagraph of paragraph
1. Should the protocols not have been concluded by 1 May 2004, the Union, the European Atomic
Energy Community and the Member States shall take, in the framework of their respective
competences, the necessary measures to deal with that situation.
5. As from 1 May 2004, the new Member States shall apply the bilateral textile agreements and
arrangements concluded by the Community with third countries.
The quantitative restrictions applied by the Union on imports of textile and clothing products shall
be adjusted to take account of the accession of the new Member States.
Should the amendments to the bilateral textile agreements and arrangements not have entered into
force by 1 May 2004, the Union shall make the necessary adjustments to its rules for the import of
textile and clothing products from third countries to take into account the accession of the new
Member States.
( 1 )
OJ L 1, 3.1.1994, p. 3.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 308
308
Part IV
6. The quantitative restrictions applied by the Union on imports of steel and steel products shall be
adjusted on the basis of imports by new Member States during the years immediately preceding the
signing of the Accession Treaty of steel products originating in the supplier countries concerned.
7. Fisheries agreements concluded before 1 May 2004 by the new Member States with third
countries shall be managed by the Union.
The rights and obligations resulting for the new Member States from those agreements shall not be
affected during the period in which the provisions of those agreements are provisionally maintained.
As soon as possible, and in any event before the expiry of the agreements referred to in the first
subparagraph, appropriate European decisions for the continuation of fishing activities resulting from
those agreements shall be adopted in each case by the Council on a proposal from the Commission,
including the possibility of extending certain agreements for periods not exceeding one year.
8. With effect from 1 May 2004, the new Member States shall withdraw from any free trade
agreements with third countries, including the Central European Free Trade Agreement.
To the extent that agreements between one or more of the new Member States on the one hand, and
one or more third countries on the other, are not compatible with the obligations arising from the
Constitution and in particular from this Protocol, the new Member States shall take all appropriate
steps to eliminate the incompatibilities established. If a new Member State encounters difficulties in
adjusting an agreement concluded with one or more third countries before accession, it shall,
according to the terms of the agreement, withdraw from that agreement.
9. The new Member States shall take appropriate measures, where necessary, to adjust their
position in relation to international organisations, and in relation to those international agreements
to which the Union or the European Atomic Energy Community or other Member States are also
parties, to the rights and obligations arising from their accession to the Union.
They shall in particular withdraw at 1 May 2004 or the earliest possible date thereafter from
international fisheries agreements and organisations to which the Union is also a party, unless their
membership relates to matters other than fisheries.
Article 7
Acts adopted by the institutions to which the transitional provisions laid down in this Protocol relate
shall retain their status in law; in particular, the procedures for amending those acts shall continue to
apply.
Article 8
Provisions of the Act of Accession of 16 April 2003, as interpreted by the Court of Justice of the
European Communities and the Court of First Instance, the purpose or effect of which is to repeal or
amend, otherwise than as a transitional measure, acts adopted by the institutions, bodies, offices or
agencies of the Community or of the European Union established by the Treaty on European Union
shall remain in force subject to the application of the second paragraph.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 309
Treaty establishing a Constitution for Europe
309
These provisions shall have the same status in law as the acts which they repeal or amend and shall be
subject to the same rules as those acts.
Article 9
The texts of the acts of the institutions, bodies, offices and agencies of the Community or of the
European Union established by the Treaty on European Union and the texts of acts of the European
Central Bank which were adopted before 1 May 2004 and which were drawn up in the Czech,
Estonian, Latvian, Lithuanian, Hungarian, Maltese, Polish, Slovenian and Slovak languages shall be
authentic from that date, under the same conditions as the texts drawn up and authentic in the other
languages.
Article 10
A European law of the Council may repeal the transitional provisions set out in this Protocol, when
they are no longer applicable. The Council shall act unanimously after consulting the European
Parliament.
Article 11
The application of the Constitution and acts adopted by the institutions shall, as a transitional
measure, be subject to the derogations provided for in this Protocol.
TITLE II
PERMANENT PROVISIONS
Article 12
The adaptations to the acts listed in Annex III to the Act of Accession of 16 April 2003 made
necessary by accession shall be drawn up in conformity with the guidelines set out in that Annex and
in accordance with the procedure and under the conditions laid down in Article 36.
Article 13
The measures listed in Annex IV to the Act of Accession of 16 April 2003 shall be applied under the
conditions laid down in that Annex.
Article 14
A European law of the Council may make the adaptations to the provisions of this Protocol relating
to the common agricultural policy which may prove necessary as a result of a modification of Union
law. The Council shall act unanimously after consulting the European Parliament.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 310
310
Part IV
TITLE III
TEMPORARY PROVISIONS
Article 15
The measures listed in Annexes V, VI, VII, VIII, IX, X, XI, XII, XIII and XIV to the Act of Accession of
16 April 2003 shall apply in respect of the new Member States under the conditions laid down in
those Annexes.
Article 16
1. The revenue designated as ‘Common Customs Tariff duties and other duties’ referred to in Article
2(1)(b) of Council Decision 2000/597/EC, Euratom of 29 September 2000 on the system of the
European Communities' own resources ( 1 ), or the corresponding provision in any Decision replacing
it, shall include the customs duties calculated on the basis of the rates resulting from the Common
Customs Tariff and any tariff concession relating thereto applied by the Union in the new Member
States' trade with third countries.
2. For the year 2004, the harmonised VAT assessment base and the GNI (gross national income)
base of each new Member State, referred to in Article 2(1)(c) and (d) of Council Decision
2000/597/EC, Euratom shall be equal to two thirds of the annual base. The GNI base of each new
Member State to be taken into account for the calculation of the financing of the correction in
respect of budgetary imbalances granted to the United Kingdom, referred to in Article 5(1) of Council
Decision 2000/597/EC, Euratom shall likewise be equal to two thirds of the annual base.
3. For the purposes of determining the frozen rate for 2004 according to Article 2(4)(b) of Council
Decision 2000/597/EC, Euratom the capped VAT bases of the new Member States shall be calculated
on the basis of two thirds of their uncapped VAT base and two thirds of their GNI.
Article 17
1. The budget of the Union for the financial year 2004 shall be adapted to take into account the
accession of the new Member States through an amending budget taking effect on 1 May 2004.
2. The twelve monthly twelfths of VAT and GNI‑based resources to be paid by the new Member
States under the amending budget referred to in paragraph 1, as well as the retroactive adjustment of
the monthly twelfths for the period January—April 2004 that only apply to the present Member
States, shall be converted into eighths to be called during the period May—December 2004. The
retroactive adjustments that result from any subsequent amending budget adopted in 2004 shall
likewise be converted into equal parts to be called during the remainder of the year.
( 1 )
OJ L 253, 7.10.2000, p. 42.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 311
Treaty establishing a Constitution for Europe
311
Article 18
On the first working day of each month the Union shall pay the Czech Republic, Cyprus, Malta and
Slovenia, as an item of expenditure under the Union budget, one eighth in 2004, as of 1 May 2004,
and one twelfth in 2005 and 2006 of the following amounts of temporary budgetary compensation:
(EUR million, 1999 prices)
Czech Republic
2004 2005
2006
125,4 178,0 85,1
Cyprus 68,9 119,2 112,3
Malta 37,8 65,6 62,9
Slovenia 29,5 66,4 35,5
Article 19
On the first working day of each month the Union shall pay the Czech Republic, Estonia, Cyprus,
Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, as an item of expenditure under the
Union budget, one eighth in 2004, as of 1 May 2004, and one twelfth in 2005 and 2006 of the
following amounts of a special lump‑sum cash‑flow facility:
(EUR million, 1999 prices)
2004 2005 2006
174,70 91,55 91,55
Estonia 15,80 2,90 2,90
Cyprus 27,70 5,05 5,05
Latvia 19,50 3,40 3,40
Lithuania 34,80 6,30 6,30
Hungary 155,30 27,95 27,95
Malta 12,20 27,15 27,15
Poland 442,80 550,00 450,00
Slovenia 65,40 17,85 17,85
Slovakia 63,20 11,35 11,35
Czech Republic954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 312
312
Part IV
One thousand million euro for Poland and 100 million euro for the Czech Republic included in the
special lump‑sum cash‑flow facility shall be taken into account for any calculations on the
distribution of Structural Funds for the years 2004, 2005 and 2006.
Article 20
1. The new Member States listed below shall pay the following amounts to the Research Fund for
Coal and Steel referred to in Decision 2002/234/ECSC of the Representatives of the Governments of
the Member States, meeting within the Council, of 27 February 2002 on the financial consequences
of the expiry of the ECSC Treaty and on the Research Fund for Coal and Steel ( 1 ):
(EUR million, current prices)
Czech Republic
39,88
Estonia 2,50
Latvia 2,69
Hungary 9,93
Poland
92,46
Slovenia 2,36
Slovakia 20,11
2. The contributions to the Research Fund for Coal and Steel shall be made in four instalments
starting in 2006 and paid as follows, in each case on the first working day of the first month of each
year:
2006: 15 %
2007: 20 %
2008: 30 %
2009: 35 %
Article 21
1. Save as otherwise provided for in this Protocol, no financial commitments shall be made under
the Phare programme ( 2 ), the Phare cross‑border cooperation programme ( 3 ), pre‑accession funds
for Cyprus and Malta ( 4 ), the ISPA programme ( 5 ) and the Sapard programme ( 6 ) in favour of the
( 1 )
2
OJ L 79, 22.3.2002, p. 42.
( ) Regulation (EEC) No 3906/89 (OJ L 375, 23.12.1989, p. 11).
( 3 ) Regulation (EC) No 2760/98 (OJ L 345, 19.12.1998, p. 49).
( 4 ) Regulation (EC) No 555/2000 (OJ L 68, 16.3.2000, p. 3).
( 5 ) Regulation (EC) No 1267/1999 (OJ L 161, 26.6.1999, p. 73).
( 6 ) Regulation (EC) No 1268/1999 (OJ L 161, 26.6.1999, p. 87).954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 313
Treaty establishing a Constitution for Europe
313
new Member States after 31 December 2003. The new Member States shall receive the same
treatment as the present Member States as regards expenditure under the first three Headings of the
Financial Perspective, as defined in the Interinstitutional Agreement of 6 May 1999 ( 1 ), as from
1 January 2004, subject to the individual specifications and exceptions below or as otherwise
provided for in this Protocol. The maximum additional appropriations for headings 1, 2, 3 and 5 of
the Financial Perspective related to enlargement are set out in Annex XV to the Act of Accession of
16 April 2003. However, no financial commitment under the 2004 budget for any programme or
agency concerned may be made before the accession of the relevant new Member State has taken
place.
2. Paragraph 1 shall not apply to expenditure under the European Agricultural Guidance and
Guarantee Fund, Guarantee Section, according to Articles 2(1), 2(2), and 3(3) of Council Regulation
(EC) No 1258/1999 of 17 May 1999 on the financing of the common agricultural policy ( 2 ), which
will become eligible for Community funding only from 1 May 2004, in accordance with Article 2 of
this Protocol.
However, paragraph 1 of this Article shall apply to expenditure for rural development under the
European Agricultural Guidance and Guarantee Fund, Guarantee Section, according to Article 47a of
Council Regulation (EC) No 1257/1999 of 17 May 1999 on support for rural development from the
European Agricultural Guidance and Guarantee Fund (EAGGF) and amending and repealing certain
regulations ( 3 ), subject to the conditions set out in the amendment of that Regulation in Annex II to
the Act of Accession of 16 April 2003.
3. Subject to the last sentence of paragraph 1, as of 1 January 2004, the new Member States shall
participate in Union programmes and agencies according to the same terms and conditions as the
present Member States with funding from the general budget of the Union.
4. If any measures are necessary to facilitate the transition from the pre‑accession regime to that
resulting from the application of this Article, the Commission shall adopt the required measures.
Article 22
1. Tendering, contracting, implementation and payments for pre‑accession assistance under the
Phare programme, the Phare cross-border cooperation programme and pre‑accession funds for
Cyprus and Malta shall be managed by implementing agencies in the new Member States as from
1 May 2004.
The Commission shall adopt European decisions to waive the ex ante control by the Commission over
tendering and contracting following a positively assessed Extended Decentralised Implementation
( 1 )
Interinstitutional Agreement of 6 May 1999 between the European Parliament, the Council and the Commission on budgetary discipline and
improvement of the budgetary procedure (OJ C 172, 18.6.1999, p. 1).
( 2 ) OJ L 160, 26.6.1999, p. 103.
( 3 ) OJ L 160, 26.6.1999, p. 80.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 314
314
Part IV
System (EDIS) in accordance with the criteria and conditions laid down in the Annex to Council
Regulation (EC) No 1266/1999 of 21 June 1999 on coordinating aid to the applicant countries in the
framework of the pre‑accession strategy and amending Regulation (EEC) No 3906/89 ( 1 ).
If these decisions to waive ex ante control have not been adopted before 1 May 2004, any contracts
signed between 1 May 2004 and the date on which the Commission decisions are taken shall not be
eligible for pre‑accession assistance.
However, exceptionally, if the Commission decisions to waive ex‑ante control are delayed beyond
1 May 2004 for reasons not attributable to the authorities of a new Member State, the Commission
may accept, in duly justified cases, eligibility for pre‑accession assistance of contracts signed between
1 May 2004 and the date of these decisions, and the continued implementation of pre‑accession
assistance for a limited period, subject to ex ante control by the Commission over tendering and
contracting.
2. Global budget commitments made before 1 May 2004 under the pre‑accession financial
instruments referred to in paragraph 1, including the conclusion and registration of subsequent
individual legal commitments and payments made after 1 May 2004, shall continue to be governed
by the rules and regulations of the pre‑accession financing instruments and be charged to the
corresponding budget chapters until closure of the programmes and projects concerned.
Notwithstanding this, public procurement procedures initiated after 1 May 2004 shall be carried
out in accordance with the relevant Union acts.
3. The last programming exercise for the pre‑accession assistance referred to in paragraph 1 shall
take place in the last full calendar year preceding 1 May 2004. Actions under these programmes will
have to be contracted within the following two years and disbursements made as provided for in the
Financing Memorandum ( 2 ), usually by the end of the third year after the commitment. No
extensions shall be granted for the contracting period. Exceptionally and in duly justified cases,
limited extensions in terms of duration may be granted for disbursement.
4. In order to ensure the necessary phasing out of the pre‑accession financial instruments referred
to in paragraph 1 as well as the ISPA programme, and a smooth transition from the rules applicable
before and after 1 May 2004, the Commission may take all appropriate measures to ensure that the
necessary statutory staff is maintained in the new Member States for a maximum of fifteen months
following that date.
During this period, officials assigned to posts in the new Member States before accession and who are
required to remain in service in those States after 1 May 2004 shall benefit, as an exception, from the
same financial and material conditions as were applied by the Commission before 1 May 2004
in accordance with Annex X to the Staff Regulations of officials and the conditions of employment
of other servants of the European Communities laid down in Regulation (EEC, Euratom, ECSC)
( 1 ) OJ L 232, 2.9.1999, p. 34.
( 2 ) As set out in the Phare Guidelines (SEC (1999) 1596, updated on 6.9.2002 by C 3303/2).954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 315
Treaty establishing a Constitution for Europe
315
No 259/68 ( 1 ). The administrative expenditure, including salaries for other staff, necessary for the
management of the pre‑accession assistance shall be covered, for all of 2004 and until the end of July
2005, under the heading ‘support expenditure for operations’ (former part B of the budget) or
equivalent headings for the financial instruments referred to in paragraph 1 as well as the ISPA
programme, of the relevant pre‑accession budgets.
5. Where projects approved under Regulation (EC) No 1258/1999 can no longer be funded under
that instrument, they may be integrated into rural development programming and financed under the
European Agricultural Guidance and Guarantee Fund. Should specific transitional measures be
necessary in this regard, these shall be adopted by the Commission in accordance with the procedures
laid down in Article 50(2) of Council Regulation (EC) No 1260/1999 of 21 June 1999 laying down
general provisions on the Structural Funds ( 2 ).
Article 23
1. Between 1 May 2004 and the end of 2006, the Union shall provide temporary financial
assistance, hereinafter referred to as the ‘Transition Facility’, to the new Member States to develop and
strengthen their administrative capacity to implement and enforce Union and European Atomic
Energy Community law and to foster exchange of best practice among peers.
2. Assistance shall address the continued need for strengthening institutional capacity in certain
areas through action which cannot be financed by the Structural Funds, in particular in the following
areas:
(a) justice and home affairs (strengthening of the judicial system, external border controls,
anti‑corruption strategy, strengthening of law enforcement capacities);
(b) financial control;
(c) protection of the financial interests of the Union and of the European Atomic Energy
Community and the fight against fraud;
(d) internal market, including customs union;
(e) environment;
(f) veterinary services and administrative capacity‑building relating to food safety;
(g) administrative and control structures for agriculture and rural development, including the
Integrated Administration and Control System (IACS);
( 1 ) OJ L 56, 4.3.1968, p. 1.
( 2 ) OJ L 161, 26.6.1999, p. 1.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 316
316
Part IV
(h) nuclear safety (strengthening the effectiveness and competence of nuclear safety authorities and
their technical support organisations as well as public radioactive waste management agencies);
(i) statistics;
(j) strengthening public administration according to needs identified in the Commission's
comprehensive monitoring report which are not covered by the Structural Funds.
3. Assistance under the Transition Facility shall be decided in accordance with the procedure laid
down in Article 8 of Council Regulation (EEC) No 3906/89 of 18 December 1989 on economic aid
to certain countries of Central and Eastern Europe ( 1 ).
4. The programme shall be implemented in accordance with Article 53(1)(a) and (b) of the
Financial Regulation applicable to the general budget of the European Communities ( 2 ) or the
European law replacing it. For twinning projects between public administrations for the purpose of
institution‑building, the procedure for call for proposals through the network of contact points in the
Member States shall continue to apply, as established in the Framework Agreements with the present
Member States for the purpose of pre‑accession assistance.
The commitment appropriations for the Transition Facility, at 1999 prices, shall be 200 million euro
in 2004, 120 million euro in 2005 and 60 million euro in 2006. The annual appropriations shall be
authorised by the budgetary authority within the limits of the Financial Perspective as defined by the
Interinstitutional Agreement of 6 May 1999.
Article 24
1. A Schengen Facility is hereby created as a temporary instrument to help beneficiary Member
States between 1 May 2004 and the end of 2006 to finance actions at the new external borders of the
Union for the implementation of the Schengen acquis and external border control.
In order to address the shortcomings identified in the preparation for participation in Schengen, the
following types of action shall be eligible for financing under the Schengen Facility:
(a) investment in construction, renovation or upgrading of border‑crossing infrastructure and
related buildings;
(b) investments in any kind of operating equipment (e.g. laboratory equipment, detection tools,
Schengen Information System — SIS II hardware and software, means of transport);
(c) training of border guards;
( 1 ) OJ L 375, 23.12.1989, p. 11.
( 2 ) Regulation (EC, Euratom) No 1605/2002 (OJ L 248, 16.9.2002, p. 1).954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 317
Treaty establishing a Constitution for Europe
317
(d) support to costs for logistics and operations.
2. The following amounts shall be made available under the Schengen Facility in the form of
lump‑sum grant payments as of 1 May 2004 to the beneficiary Member States listed below:
(million euro, 1999 prices)
2004 2005 2006
Estonia 22,90 22,90 22,90
Latvia 23,70 23,70 23,70
Lithuania 44,78 61,07 29,85
Hungary 49,30 49,30 49,30
Poland 93,34 93,33 93,33
Slovenia 35,64 35,63 35,63
Slovakia 15,94 15,93 15,93
3. The beneficiary Member States shall be responsible for selecting and implementing individual
operations in compliance with this Article. They shall also be responsible for coordinating use of the
Schengen Facility with assistance from other Union instruments, ensuring compatibility with Union
policies and measures and compliance with the Financial Regulation applicable to the general budget
of the European Communities or with the European law replacing it.
The lump‑sum grant payments shall be used within three years from the first payment and any
unused or unjustifiably spent funds shall be recovered by the Commission. The beneficiary Member
States shall submit, no later than six months after expiry of the three‑year deadline, a comprehensive
report on the financial execution of the lump‑sum grant payments with a statement justifying the
expenditure.
The beneficiary State shall exercise this responsibility without prejudice to the Commission's
responsibility for the implementation of the Union's budget and in accordance with the provisions
applicable to decentralised management in the said Financial Regulation or in the European law
replacing it.
4. The Commission retains the right of verification, through the Anti‑Fraud Office (OLAF). The
Commission and the Court of Auditors may also carry out on‑the‑spot checks in accordance with the
appropriate procedures.
5. The Commission may adopt any technical provisions necessary for the operation of the
Schengen Facility.
Article 25
The amounts referred to in Articles 18, 19, 23 and 24 shall be adjusted each year, as part of the
technical adjustment provided for in paragraph 15 of the Interinstitutional Agreement of 6 May
1999.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 318
318
Part IV
Article 26
1. If, until the end of a period of up to three years after 1 May 2004, difficulties arise which are
serious and liable to persist in any sector of the economy or which could bring about serious
deterioration in the economic situation of a given area, a new Member State may apply for
authorisation to take protective measures in order to rectify the situation and adjust the sector
concerned to the economy of the internal market.
In the same circumstances, any present Member State may apply for authorisation to take protective
measures with regard to one or more of the new Member States.
2. Upon request by the State concerned, the Commission shall, by emergency procedure, adopt the
European regulations or decisions establishing the protective measures which it considers necessary,
specifying the conditions and modalities under which they are to be put into effect.
In the event of serious economic difficulties and at the express request of the Member State
concerned, the Commission shall act within five working days of the receipt of the request
accompanied by the relevant background information. The measures thus decided on shall be
applicable forthwith, shall take account of the interests of all parties concerned and shall not entail
frontier controls.
3. The measures authorised under paragraph 2 may involve derogations from the rules of the
Constitution, and in particular from this Protocol, to such an extent and for such periods as are
strictly necessary in order to attain the objectives referred to in paragraph 1. Priority shall be given to
such measures as will least disturb the functioning of the internal market.
Article 27
If a new Member State has failed to implement commitments undertaken in the context of the
accession negotiations, causing a serious breach of the functioning of the internal market, including
any commitments in all sectoral policies which concern economic activities with cross‑border effect,
or an imminent risk of such breach, the Commission may, until the end of a period of up to three
years after 1 May 2004, upon the motivated request of a Member State or on its own initiative, adopt
European regulations or decisions establishing appropriate measures.
Measures shall be proportional and priority shall be given to measures which least disturb the
functioning of the internal market and, where appropriate, to the application of the existing sectoral
safeguard mechanisms. Such safeguard measures shall not be invoked as a means of arbitrary
discrimination or a disguised restriction on trade between Member States. The measures shall be
maintained no longer than strictly necessary, and, in any case, will be lifted when the relevant
commitment is implemented. They may however be applied beyond the period specified in the first
paragraph as long as the relevant commitments have not been fulfilled. In response to progress made
by the new Member State concerned in fulfilling its commitments, the Commission may adapt the
measures as appropriate. The Commission shall inform the Council in good time before revoking the
European regulations or decisions establishing the safeguard measures, and it shall take duly into
account any observations of the Council in this respect.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 319
Treaty establishing a Constitution for Europe
319
Article 28
If there are serious shortcomings or any imminent risks of such shortcomings in a new Member State
in the transposition, state of implementation or the application of the framework decisions or any
other relevant commitments, instruments of cooperation and decisions relating to mutual
recognition in the area of criminal law under Title VI of the EU Treaty, Directives and Regulations
relating to mutual recognition in civil matters under Title IV of the EC Treaty, and European laws and
framework laws adopted on the basis of Sections 3 and 4 of Chapter IV of Title III of Part III of the
Constitution, the Commission may, until the end of a period of up to three years after 1 May 2004,
upon the motivated request of a Member State or on its own initiative and after consulting the
Member States, adopt European regulations or decisions establishing appropriate measures and
specify the conditions and modalities under which these measures are put into effect.
These measures may take the form of temporary suspension of the application of relevant provisions
and decisions in the relations between a new Member State and any other Member State or Member
States, without prejudice to the continuation of close judicial cooperation. The measures shall be
maintained no longer than strictly necessary, and, in any case, will be lifted when the shortcomings
are remedied. They may however be applied beyond the period specified in the first paragraph as long
as these shortcomings persist. In response to progress made by the new Member State concerned in
rectifying the identified shortcomings, the Commission may adapt the adopted measures as
appropriate after consulting the Member States. The Commission shall inform the Council in good
time before revoking safeguard measures, and it shall take duly into account any observations of the
Council in this respect.
Article 29
In order not to hamper the proper functioning of the internal market, the enforcement of the new
Member States' national rules during the transitional periods referred to in Annexes V to XIV to the
Act of Accession of 16 April 2003 shall not lead to border controls between Member States.
Article 30
If transitional measures are necessary to facilitate the transition from the existing regime in the new
Member States to that resulting from the application of the common agricultural policy under the
conditions set out in this Protocol, such measures shall be adopted by the Commission in accordance
with the procedure referred to in Article 42(2) of Council Regulation (EC) No 1260/2001 of 19 June
2001 on the common organisation of the markets in the sugar sector ( 1 ), or as appropriate, in the
corresponding Articles of the other Regulations on the common organisation of agricultural markets
or of the European laws replacing them or the relevant procedure as determined in the applicable
legislation. The transitional measures referred to in this Article may be adopted during a period of
three years after 1 May 2004 and their application shall be limited to that period. A European law of
the Council may extend this period. The Council shall act unanimously after consulting the European
Parliament.
( 1 )
OJ L 178, 30.6.2001, p. 1.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 320
320
Part IV
Article 31
If transitional measures are necessary to facilitate the transition from the existing regime in the new
Member States to that resulting from the application of the Union veterinary and phytosanitary rules,
such measures shall be adopted by the Commission in accordance with the relevant procedure as
determined in the applicable legislation. These measures shall be taken during a period of three years
after 1 May 2004 and their application shall be limited to that period.
Article 32
1. The terms of office of the new members of the Committees, groups and other bodies listed in
Annex XVI to the Act of Accession of 16 April 2003 shall expire at the same time as those of the
members in office on 1 May 2004.
2. The terms of office of the new members of the Committees and groups set up by the
Commission which are listed in Annex XVII to the Act of Accession of 16 April 2003 shall expire at
the same time as those of the members in office on 1 May 2004.
TITLE IV
APPLICABILITY OF THE ACTS OF THE INSTITUTIONS
Article 33
As from 1 May 2004, the new Member States shall be considered as being addressees of directives
and decisions within the meaning of Article 249 of the EC Treaty and of Article 161 of the EAEC
Treaty, provided that those directives and decisions have been addressed to all the present Member
States. Except with regard to directives and decisions which enter into force pursuant to Article 254
(1) and (2) of the EC Treaty, the new Member States shall be considered as having received
notification of such directives and decisions upon 1 May 2004.
Article 34
The new Member States shall put into effect the measures necessary for them to comply, from 1 May
2004, with the provisions of directives and decisions within the meaning of Article 249 of the EC
Treaty and of Article 161 of the EAEC Treaty, unless another time‑limit is provided for in the Annexes
referred to in Article 15 or in any other provisions of this Protocol.
Article 35
Unless otherwise stipulated, the Council, on a proposal from the Commission, shall adopt the
necessary European regulations and decisions to implement the provisions contained in Annexes III
and IV to the Act of Accession of 16 April 2003 referred to in Articles 12 and 13 of this Protocol.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 321
Treaty establishing a Constitution for Europe
321
Article 36
1. Where acts of the institutions prior to 1 May 2004 require adaptation by reason of accession,
and the necessary adaptations have not been provided for in this Protocol, those adaptations shall be
made in accordance with the procedure laid down by paragraph 2. Those adaptations shall enter into
force as from 1 May 2004.
2. The Council, on a proposal from the Commission, or the Commission, according to which of
these two institutions adopted the original acts, shall to this end adopt the necessary acts.
Article 37
Provisions laid down by law, regulation or administrative action designed to ensure the protection of
the health of workers and the general public in the territory of the new Member States against the
dangers arising from ionising radiations shall, in accordance with Article 33 of the EAEC Treaty, be
communicated by those States to the Commission within three months from 1 May 2004.
PART TWO
PROVISIONS ON THE PROTOCOLS
ANNEXED TO THE ACT OF ACCESSION OF 16 APRIL 2003
TITLE I
TRANSITIONAL PROVISIONS ON THE EUROPEAN INVESTMENT BANK
Article 38
The Kingdom of Spain shall pay the amount of EUR 309 686 775 as its share of the capital paid in
for the subscribed capital increase. This contribution shall be paid in eight equal instalments
falling due on 30 September 2004, 30 September 2005, 30 September 2006, 31 March 2007,
30 September 2007, 31 March 2008, 30 September 2008 and 31 March 2009.
The Kingdom of Spain shall contribute, in eight equal instalments falling due on those dates, to the
reserves and provisions equivalent to reserves, as well as to the amount still to be appropriated to the
reserves and provisions, comprising the balance of the profit and loss account, established at the end
of the month of April 2004, as entered on the balance sheet of the Bank, in amounts corresponding
to 4,1292 % of the reserves and provisions.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 322
322
Part IV
Article 39
From 1 May 2004, the new Member States shall pay the following amounts corresponding to their
share of the capital paid in for the subscribed capital as defined in Article 4 of the Statute of the
European Investment Bank.
Poland
EUR 170 563 175
Czech Republic EUR 62 939 275
Hungary EUR 59 543 425
Slovakia EUR 21 424 525
Slovenia EUR 19 890 750
Lithuania EUR 12 480 875
Cyprus EUR 9 169 100
Latvia EUR 7 616 750
Estonia EUR 5 882 000
Malta EUR 3 490 200
These contributions shall be paid in eight equal instalments falling due on 30 September 2004,
30 September 2005, 30 September 2006, 31 March 2007, 30 September 2007, 31 March 2008,
30 September 2008 and 31 March 2009.
Article 40
The new Member States shall contribute, in eight equal instalments falling due on the dates referred
to in Article 39, to the reserves and provisions equivalent to reserves, as well as to the amount still to
be appropriated to the reserves and provisions, comprising the balance of the profit and loss account,
established at the end of the month of April 2004, as entered on the balance sheet of the European
Investment Bank, in amounts corresponding to the following percentages of the reserves and
provisions:
Poland 2,2742 %
Czech Republic 0,8392 %
Hungary 0,7939 %
Slovakia 0,2857 %
Slovenia 0,2652 %
Lithuania 0,1664 %
Cyprus 0,1223 %
Latvia 0,1016 %
Estonia 0,0784 %
Malta 0,0465 %954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 323
Treaty establishing a Constitution for Europe
323
Article 41
The capital and payments provided for in Articles 38, 39 and 40 shall be paid in by the Kingdom of
Spain and the new Member States in cash in euro, save by way of derogation decided unanimously by
the Board of Governors.
TITLE II
PROVISIONS ON THE RESTRUCTURING OF THE CZECH STEEL INDUSTRY
Article 42
1. Notwithstanding Articles III‑167 and III‑168 of the Constitution, State aid granted by the Czech
Republic for restructuring purposes to specified parts of the Czech steel industry from 1997 to 2003
shall be deemed to be compatible with the internal market provided that:
(a) the period provided for in Article 8(4) of Protocol 2 on ECSC products to the Europe Agreement
establishing an association between the European Communities and their Member States, of the
one part, and the Czech Republic, of the other part ( 1 ), has been extended until 1 May 2004;
(b) the terms set out in the restructuring plan on the basis of which the abovementioned Protocol
was extended are adhered to throughout the period 2002—2006;
(c) the conditions set out in this Title are met, and
(d) no State aid for restructuring is to be paid to the Czech steel industry after 1 May 2004.
2. Restructuring of the Czech steel sector, as described in the individual business plans of the
companies listed in Annex 1 to Protocol 2 to the Act of Accession of 16 April 2003 (hereinafter
referred to as ‘benefiting companies’), and in line with the conditions set out in this Title, shall be
completed no later than 31 December 2006 (hereinafter referred to as ‘the end of the restructuring
period’).
3. Only benefiting companies shall be eligible for State aid in the framework of the Czech steel
restructuring programme.
4.
A benefiting company may not:
(a) in the case of a merger with a company not included in Annex 1 to Protocol 2 to the Act of
Accession of 16 April 2003, pass on the benefit of the aid granted to the benefiting company;
( 1 )
OJ L 360, 31.12.1994, p. 2.954393_TRAITE_EN_301_350
324
12-01-2005
15:56
Pagina 324
Part IV
(b) take over the assets of any company not included in Annex 1 to Protocol 2 to the Act of
Accession of 16 April 2003 which is declared bankrupt in the period up to 31 December 2006.
5. Any subsequent privatisation of any of the benefiting companies shall respect the conditions and
principles regarding viability, State aid and capacity reduction defined in this Title.
6. The total restructuring aid to be granted to the benefiting companies shall be determined by the
justifications set out in the approved Czech steel restructuring plan and individual business plans as
approved by the Council. But in any case, the aid paid out in the period 1997—2003 is limited to a
maximum amount of CZK 14 147 425 201. Of this total figure, Nová Huť receives a maximum of
CZK 5 700 075 201, Vítkovice Steel receives a maximum of CZK 8 155 350 000 and Válcovny
Plechu Frýdek Místek receives a maximum of CZK 292 000 000 depending on the requirements as
set out in the approved restructuring plan. The aid shall only be granted once. No further State aid
shall be granted by the Czech Republic for restructuring purposes to the Czech steel industry.
7. The net capacity reduction to be achieved by the Czech Republic for finished products during the
period 1997—2006 shall be 590 000 tonnes.
Capacity reduction shall be measured only on the basis of permanent closure of production facilities
by physical destruction such that the facilities cannot be restored to service. A declaration of
bankruptcy of a steel company shall not qualify as capacity reduction.
The above level of net capacity reduction, together with any other capacity reductions identified as
necessary in the restructuring programmes, shall be completed in line with the timetable in Annex 2
to Protocol 2 to the Act of Accession of 16 April 2003.
8. The Czech Republic shall remove trade barriers in the coal market in accordance with the acquis
by accession, enabling Czech steel companies to obtain access to coal at international market prices.
9.
The business plan for the benefiting company Nová Huť shall be implemented. In particular:
(a) the Vysoké Pece Ostrava (VPO) plant shall be brought into the organisational framework of Nová
Huť by acquisition of full ownership. A target date shall be set for this merger, including
assignation of responsibility for its implementation;954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 325
Treaty establishing a Constitution for Europe
325
(b) restructuring efforts shall concentrate on the following:
(i) evolving Nová Huť from being production‑oriented to being marketing‑oriented and
improving the efficiency and effectiveness of its business management, including greater
transparency on costs;
(ii) Nová Huť reviewing its product mix and entry into higher added‑value markets;
(iii) Nová Huť making the necessary investments in order to achieve a higher quality of finished
products in the short term;
(c) employment restructuring shall be implemented; levels of productivity comparable to those
obtained by the Union's steel industry product groups shall be reached as at 31 December 2006,
on the basis of the consolidated figures of the benefiting companies concerned;
(d) compliance with the relevant Community acquis in the field of environmental protection shall be
achieved by 1 May 2004 including the necessary investments addressed in the business plan. In
accordance with the business plan the necessary future IPPC‑related investment shall also be
made, in order to ensure compliance with Council Directive 96/61/EC of 24 September 1996
concerning integrated pollution prevention and control ( 1 ) by 1 November 2007.
10. The business plan for the benefiting company Vítkovice Steel shall be implemented. In
particular:
(a) the Duo Mill shall be permanently closed no later than 31 December 2006. In the event of
purchase of the company by a strategic investor, the purchase contract shall be made conditional
on this closure by this date;
(b) restructuring efforts shall concentrate on the following:
(i) an increase in direct sales and a greater focus on cost reduction, this being essential for more
efficient business management,
(ii) adapting to market demand and shifting towards higher value‑added products,
(iii) bringing forward the proposed investment in the secondary steel‑making process from 2004
to 2003, in order to allow the company to compete on quality rather than on price;
(c) compliance with the relevant Community acquis in the field of environmental protection shall be
achieved by 1 May 2004 including the necessary investments addressed in the business plan,
which include the need for future IPPC‑related investment.
( 1 )
OJ L 257, 10.10.1996, p. 26.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 326
326
Part IV
11. The business plan for the benefiting company Válcovny Plechu Frýdek Místek (VPFM) shall be
implemented. In particular:
(a) Hot Rolling Mills Nos 1 and 2 shall be permanently closed at the end of 2004;
(b) restructuring efforts shall concentrate on the following:
(i) making the necessary investment in order to reach a higher quality of finished product in the
short term after the signing of the Treaty of Accession,
(ii) giving priority to the implementation of key identified profit improvement opportunities
(including employment restructuring, cost reductions, yield improvements and distribution
reorientation).
12. Any subsequent changes in the overall restructuring plan and the individual plans must be
agreed by the Commission and, where appropriate, by the Council.
13. The implementation of the restructuring shall take place under conditions of full transparency
and on the basis of sound market economy principles.
14. The Commission and the Council shall closely monitor the implementation of the restructuring
and the fulfilment of the conditions set out in this Title concerning viability, State aid and capacity
reductions before and after 1 May 2004 until the end of the restructuring period, in accordance with
paragraphs 15 to 18. For this purpose the Commission shall report to the Council.
15. The Commission and the Council shall monitor the restructuring benchmarks set out in Annex
3 to Protocol 2 to the Act of Accession of 16 April 2003. The references in that Annex to paragraph
16 of the said Protocol shall be construed as being made to paragraph 16 of this Article.
16. Monitoring shall include an independent evaluation to be carried out in 2003, 2004, 2005 and
2006. The Commission's viability test shall be an important element in ensuring that viability is
achieved.
17. The Czech Republic shall cooperate fully with all the arrangements for monitoring. In
particular:
(a) the Czech Republic shall supply the Commission with six‑monthly reports concerning the
restructuring of the benefiting companies, no later than 15 March and 15 September of each year,
until the end of the restructuring period,
(b) the first report shall reach the Commission by 15 March 2003 and the last report by 15 March
2007, unless the Commission decides otherwise,954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 327
Treaty establishing a Constitution for Europe
327
(c) the reports shall contain all the information necessary to monitor the restructuring process and
the reduction and use of capacity and shall provide sufficient financial data to allow an
assessment to be made of whether the conditions and requirements contained in this Title have
been fulfilled. The reports shall at the least contain the information set out in Annex 4 to
Protocol 2 to the Act of Accession of 16 April 2003, which the Commission reserves the right to
modify in line with its experiences during the monitoring process. In addition to the individual
business reports of the benefiting companies, there shall also be a report on the overall situation
of the Czech steel sector, including recent macroeconomic developments,
(d) the Czech Republic shall oblige the benefiting companies to disclose all relevant data which
might, under other circumstances, be considered as confidential. In its reporting to the Council,
the Commission shall ensure that company‑specific confidential information is not disclosed.
18. The Commission may at any time decide to mandate an independent consultant to evaluate the
monitoring results, undertake any research necessary and report to the Commission and the Council.
19. If the Commission establishes, on the basis of the reports referred to in paragraph 17, that
substantial deviations from the financial data on which the viability assessment has been made have
occurred, it may require the Czech Republic to take appropriate measures to reinforce the
restructuring measures of the benefiting companies concerned.
20.
Should the monitoring show that:
(a) the conditions for the transitional arrangements contained in this Title have not been fulfilled, or
that
(b) the commitments made in the framework of the extension of the period during which the Czech
Republic may exceptionally grant State support for the restructuring of its steel industry under
the Europe Agreement establishing an association between the European Communities and their
Member States, of the one part, and the Czech Republic, of the other part ( 1 ) have not been
fulfilled, or that
(c) the Czech Republic in the course of the restructuring period has granted additional incompatible
State aid to the steel industry and to the benefiting companies in particular,
the transitional arrangements contained in this Title shall not have effect.
The Commission shall take appropriate steps requiring any company concerned to reimburse any aid
granted in breach of the conditions laid down in this Title.
( 1 )

