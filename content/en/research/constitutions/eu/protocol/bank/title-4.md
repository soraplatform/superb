TITLE IV
PROVISIONS ON THE IGNALINA NUCLEAR POWER PLANT IN LITHUANIA
Article 52
Acknowledging the readiness of the Union to provide adequate additional assistance to the efforts by
Lithuania to decommission the Ignalina nuclear power plant and highlighting this expression of
solidarity, Lithuania has undertaken to close Unit 1 of the Ignalina nuclear power plant before 2005
and Unit 2 of this plant by 31 December 2009 at the latest and subsequently decommission these
units.
Article 53
1. During the period 2004—2006, the Union shall provide Lithuania with additional financial
assistance in support of its efforts to decommission, and to address the consequences of the closure954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 331
Treaty establishing a Constitution for Europe
331
and decommissioning of, the Ignalina nuclear power plant (hereinafter ‘the Ignalina Programme’).
2. Measures under the Ignalina Programme shall be decided and implemented in accordance with
the provisions laid down in Council Regulation (EEC) No 3906/89 of 18 December 1989 on
economic aid to certain countries of Central and Eastern Europe ( 1 ).
3. The Ignalina Programme shall, inter alia, cover: measures in support of the decommissioning of
the Ignalina nuclear power plant; measures for the environmental upgrading in line with the acquis
and modernisation measures of conventional production capacity to replace the production capacity
of the two Ignalina nuclear power plant reactors; and other measures which are consequential to the
decision to close and decommission this plant and which contribute to the necessary restructuring,
environmental upgrading and modernisation of the energy production, transmission and distribution
sectors in Lithuania as well as to enhancing the security of energy supply and improving energy
efficiency in Lithuania.
4. The Ignalina Programme shall include measures to support plant personnel in maintaining a
high level of operational safety at the Ignalina nuclear power plant in the periods prior to the closure
and during the decommissioning of the said reactor units.
5. For the period 2004—2006 the Ignalina Programme shall amount to 285 million euro in
commitment appropriations, to be committed in equal annual tranches.
6. The contribution under the Ignalina Programme may, for certain measures, amount to up to
100 % of the total expenditure. Every effort should be made to continue the co‑financing practice
established under the pre‑accession assistance for Lithuania's decommissioning effort as well as to
attract co‑financing from other sources, as appropriate.
7. The assistance under the Ignalina Programme, or parts thereof, may be made available as a Union
contribution to the Ignalina International Decommissioning Support Fund, managed by the
European Bank for Reconstruction and Development.
8.
Public aid from national, Union and international sources:
(a) for the environmental upgrading in line with the acquis and modernisation measures of the
Lithuanian Thermal Power Plant in Elektrenai as the key replacement for the production capacity
of the two Ignalina nuclear power plant reactors; and
(b) for the decommissioning of the Ignalina nuclear power plant
shall be compatible with the internal market as defined in the Constitution.
( 1 )
OJ L 375, 23.12.1989, p. 11.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 332
332
Part IV
9. Public aid from national, Union and international sources in support of Lithuania's efforts to
address the consequences of the closure and of the decommissioning of the Ignalina nuclear power
plant may, on a case by case basis, be considered to be compatible — under the Constitution — with
the internal market, in particular public aid provided for enhancing the security of energy supply.
Article 54
1. Recognising that the decommissioning of the Ignalina nuclear power plant is of a long‑term
nature and represents for Lithuania an exceptional financial burden not commensurate with its size
and economic strength, the Union shall, in solidarity with Lithuania, provide adequate additional
assistance to the decommissioning effort beyond 2006.
2. The Ignalina Programme shall be, for this purpose, seamlessly continued and extended beyond
2006. Implementing provisions for the extended Ignalina Programme shall be adopted in accordance
with the procedure laid down in Article 35 of this Protocol and enter into force, at the latest, by the
date of expiry of the Financial Perspective as defined in the Interinstitutional Agreement of 6 May
1999.
3. The Ignalina Programme, as extended in accordance with the provisions of paragraph 2, shall be
based on the same elements and principles as described in Article 53.
4. For the period of the subsequent Financial Perspective, the overall average appropriations under
the extended Ignalina Programme shall be appropriate. Programming of these resources will be based
on actual payment needs and absorption capacity.
Article 55
Without prejudice to the provisions of Article 52, the general safeguard clause referred to in Article
26 shall apply until 31 December 2012 if energy supply is disrupted in Lithuania.
Article 56
This Title shall apply in the light of the Declaration on the Ignalina nuclear power plant in Lithuania
which incorporates, without altering its legal effect, the wording of the preamble to Protocol 4 to the
Act of Accession of 16 April 2003.
TITLE V
PROVISIONS ON THE TRANSIT OF PERSONS BY LAND BETWEEN THE REGION OF KALININGRAD
AND OTHER PARTS OF THE RUSSIAN FEDERATION
Article 57
The Union rules and arrangements on transit of persons by land between the region of Kaliningrad
and other parts of the Russian Federation, and in particular the Council Regulation (EC) No 693/
2003 of 14 April 2003 establishing a specific Facilitated Transit Document (FTD), a Facilitated Rail954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 333
Treaty establishing a Constitution for Europe
333
Transit Document (FRTD) and amending the Common Consular Instructions and the Common
Manual ( 1 ), shall not in themselves delay or prevent the full participation of Lithuania in the
Schengen acquis, including the removal of internal border controls.
Article 58
The Union shall assist Lithuania in implementing the rules and arrangements for the transit of
persons between the region of Kaliningrad and the other parts of the Russian Federation with a view
to Lithuania's full participation in the Schengen area as soon as possible.
The Union shall assist Lithuania in managing the transit of persons between the region of Kaliningrad
and the other parts of the Russian Federation and shall, notably, bear any additional costs incurred by
implementing the specific provisions of the acquis provided for such transit.
Article 59
Without prejudice to the sovereign rights of Lithuania, any further act concerning the transit of
persons between the region of Kaliningrad and other parts of the Russian Federation shall be adopted
by the Council on a proposal from the Commission. The Council shall act unanimously.
Article 60
This Title shall apply in the light of the Declaration on the transit of persons by land between the
region of Kaliningrad and other parts of the Russian Federation, which incorporates, without altering
its legal affect, the wording of the preamble to Protocol 5 to the Act of Accession of 16 April 2003.
TITLE VI
PROVISIONS ON THE ACQUISITION OF SECONDARY RESIDENCES IN MALTA
Article 61
Bearing in mind the very limited number of residences in Malta and the very limited land available for
construction purposes, which can only cover the basic needs created by the demographic
development of the present residents, Malta may on a non‑discriminatory basis maintain in force the
rules on the acquisition and holding of immovable property for secondary residence purposes by
nationals of the Member States who have not legally resided in Malta for at least five years laid down
in the Immovable Property (Acquisition by Non‑Residents) Act (Chapter 246).
( 1 )
OJ L 99, 17.4.2003, p. 8.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 334
334
Part IV
Malta shall apply authorisation procedures for the acquisition of immovable property for secondary
residence purposes in Malta, which shall be based on published, objective, stable and transparent
criteria. These criteria shall be applied in a non‑discriminatory manner and shall not differentiate
between nationals of Malta and of other Member States. Malta shall ensure that in no instance shall a
national of a Member State be treated in a more restrictive way than a national of a third country.
In the event that the value of one such property bought by a national of a Member State exceeds the
thresholds provided for in Malta's legislation, namely 30 000 Maltese lira for apartments and 50 000
Maltese lira for any type of property other than apartments and property of historical importance,
authorisation shall be granted. Malta may revise the thresholds established by such legislation to
reflect changes in prices in the property market in Malta.

TITLE VII

PROVISIONS ON ABORTION IN MALTA

Article 62
Nothing in the Treaty establishing a Constitution for Europe or in the Treaties and Acts modifying or
supplementing it shall affect the application in the territory of Malta of national legislation relating to
abortion.
TITLE VIII
PROVISIONS ON THE RESTRUCTURING OF THE POLISH STEEL INDUSTRY
Article 63
1. Notwithstanding Articles III‑167 and III‑168 of the Constitution, State aid granted by Poland for
restructuring purposes to specified parts of the Polish steel industry shall be deemed to be compatible
with the internal market provided that:
(a) the period provided for in Article 8(4) of Protocol 2 on ECSC products to the Europe Agreement
establishing an association between the European Communities and their Member States, of the
one part, and Poland, of the other part ( 1 ), has been extended until 1 May 2004,
(b) the terms set out in the restructuring plan, on the basis of which the abovementioned Protocol
was extended are adhered to throughout the period 2002—2006,
(c) the conditions set out in this Title are met, and
( 1 )
OJ L 348, 31.12.1993, p. 2.954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 335
Treaty establishing a Constitution for Europe
335
(d) no State aid for restructuring is to be paid to the Polish steel industry after 1 May 2004.
2. Restructuring of the Polish steel sector, as described in the individual business plans of the
companies listed in Annex 1 to Protocol 8 to the Act of Accession of 16 April 2003 (hereinafter
referred to as ‘benefiting companies’), and in line with the conditions set out in this Title, shall be
completed no later than 31 December 2006 (hereinafter referred to as ‘the end of the restructuring
period’).
3. Only benefiting companies shall be eligible for State aid in the framework of the Polish steel
restructuring programme.
4.
A benefiting company may not:
(a) in the case of a merger with a company not included in Annex 1 to Protocol 8 to the Act of
Accession of 16 April 2003, pass on the benefit of the aid granted to the benefiting company;
(b) take over the assets of any company not included in Annex 1 to Protocol 8 to the Act of
Accession of 16 April 2003 which is declared bankrupt in the period up to 31 December 2006.
5. Any subsequent privatisation of any of the benefiting companies shall take place on a basis that
respects the need for transparency and shall respect the conditions and principles regarding viability,
State aids and capacity reduction defined in this Title. No further State aid shall be granted as part of
the sale of any company or individual assets.
6. The restructuring aid granted to the benefiting companies shall be determined by the
justifications set out in the approved Polish steel restructuring plan and individual business plans as
approved by the Council. But, in any case, the aid paid out in the period of 1997‑2003 in its total
amount shall not exceed PLN 3 387 070 000.
Of this total figure:
(a) as regards Polskie Huty Stali (hereinafter referred to as ‘PHS’), the restructuring aid already granted
or to be granted from 1997 until the end of 2003 shall not exceed PLN 3 140 360 000. PHS has
already received PLN 62 360 000 of restructuring aid in the period 1997‑2001; it shall receive
further restructuring aid of no more than PLN 3 078 000 000 in 2002 and 2003 depending on
the requirements set out in the approved restructuring plan (to be entirely paid out in 2002 if the
extension of the grace period under Protocol 2 of the Europe Agreement establishing an
association between the European Communities and their Member States, of the one part, and
Poland, of the other part, is granted by the end of 2002, or otherwise in 2003);
(b) as regards Huta Andrzej S.A., Huta Bankowa Sp. z o.o., Huta Batory S.A., Huta Buczek S.A., Huta
L.W. Sp. z o.o., Huta Łabędy S.A., and Huta Pokój S.A. (hereinafter referred to as ‘other benefiting
companies’), the steel restructuring aid already granted or to be granted from 1997 until the end
of 2003 shall not exceed PLN 246 710 000. These firms have already received PLN 37 160 000
of restructuring aid in the period 1997‑2001; they shall receive further restructuring aid of no
more than PLN 210 210 000 depending on the requirements set out in the approved954393_TRAITE_EN_301_350
12-01-2005
15:56
Pagina 336
336
Part IV
restructuring plan (of which PLN 182 170 000 in 2002 and PLN 27 380 000 in 2003 if the
extension of the grace period under Protocol 2 of the Europe Agreement establishing an
association between the European Communities and their Member States, of the one part, and
Poland, of the other part, is granted by the end of 2002, or otherwise PLN 210 210 000 in
2003).
No further State aid shall be granted by Poland for restructuring purposes to the Polish steel
industry.
7. The net capacity reduction to be achieved by Poland for finished products during the period
1997‑2006 shall be a minimum of 1 231 000 tonnes. This overall amount includes net capacity
reductions of at least 715 000 tpy in hot‑rolled products and 716 000 tpy in cold‑rolled products, as
well as an increase of at most 200 000 tpy of other finished products.
Capacity reduction shall be measured only on the basis of permanent closure of production facilities
by physical destruction such that the facilities cannot be restored to service. A declaration of
bankruptcy of a steel company shall not qualify as capacity reduction.
The net capacity reductions shown in Annex 2 to Protocol 8 to the Act of Accession of 16 April
2003 are minima and actual net capacity reductions to be achieved and the time frame for doing so
shall be established on the basis of Poland's final restructuring programme and individual business
plans under the Europe Agreement establishing an association between the European Communities
and their Member States, of the one part, and Poland, of the other part, taking into account the
objective to ensure the viability of benefiting companies as at 31 December 2006.
8.
The business plan for the benefiting company PHS shall be implemented. In particular:
(a) restructuring efforts shall concentrate on the following:
(i) reorganising PHS production facilities on a product basis and ensuring horizontal
organisation by function (purchasing, production, sales),
(ii) establishing in PHS a unified management structure enabling full realisation of synergies in
the consolidation,
(iii) evolving the strategic focus of PHS from being production‑oriented to being
marketing‑oriented,
(iv) improving the efficiency and effectiveness of PHS business management and also ensuring
better control of direct sales,
(v) PHS reviewing, on the basis of sound economic considerations, the strategy of spin‑off
companies and, where appropriate, reintegrating services into the parent company,954393_TRAITE_EN_301_350
12-01-2005
15:57
Pagina 337
Treaty establishing a Constitution for Europe
337
(vi) PHS reviewing its product mix, reducing over‑capacity on long semi‑finished products and
generally moving further into the higher value-added product market,
(vii) PHS investing in order to achieve a higher quality of finished products; special attention shall
be given to attaining by the date set in the timetable for the implementation of the PHS
restructuring programme and at the latest by the end of 2006 3‑Sigma production quality
level at the PHS site in Kraków;
(b) cost savings shall be maximised in PHS during the restructuring period through energy efficiency
gains, improved purchasing and ensuring productivity yields comparable to Union levels;
(c) employment restructuring shall be implemented; levels of productivity comparable to those
obtained by Union steel industry product groups shall be reached as at 31 December 2006, based
on consolidated figures including indirect employment in the wholly owned service companies;
(d) any privatisation shall be on a basis that respects the need for transparency and fully respects the
commercial value of PHS. No further State aid shall be granted as part of the sale.
9.
The business plan for the other benefiting companies shall be implemented. In particular:
(a) for all of the other benefiting companies, restructuring efforts shall concentrate on the following:
(i) evolving the strategic focus from being production‑oriented to being marketing‑oriented,
(ii) improving the efficiency and effectiveness of the companies' business management and also
ensuring better control of direct sales,
(iii) reviewing, on the basis of sound economic considerations, the strategy of spin‑off companies
and, where appropriate, reintegrating services into the parent companies;
(b) for Huta Bankowa, implementing the cost savings programme;
(c) for Huta Buczek, obtaining the necessary financial support from creditors and local financial
institutions and implementing the cost savings programme, including reducing the investment
cost by adapting existing production facilities;
(d) for Huta Łabędy, implementing the cost savings programme and reducing reliance on the mining
industry;
(e) for Huta Pokój, achieving international productivity standards in the subsidiaries, implementing
energy consumption savings and cancelling the proposed investment in the processing and
construction department;954393_TRAITE_EN_301_350
12-01-2005
15:57
Pagina 338
338
Part IV
(f) for Huta Batory, reaching agreement with creditors and financial institutions on debt
rescheduling and investment loans. The company shall also ensure substantial additional cost
savings associated with employment restructuring and improved yields;
(g) for Huta Andrzej, securing a stable financial base for its development by negotiating an
agreement between the company's current lenders, long‑term creditors, trade creditors and
financial institutions. Additional investments in the hot tube mill as well as the implementation
of the staff reduction programme must take place;
(h) for Huta L.W., carrying out investments in relation to the company's hot‑rolling mills project,
lifting equipment, and environmental standing. This company shall also achieve higher
productivity levels, through staff restructuring and reducing the costs of external services.
10. Any subsequent changes in the overall restructuring plan and the individual plans must be
agreed by the Commission and, where appropriate, by the Council.
11. The implementation of the restructuring shall take place under conditions of full transparency
and on the basis of sound market economy principles.
12. The Commission and the Council shall closely monitor the implementation of the restructuring
and the fulfilment of the conditions set out in this Title concerning viability, State aid and capacity
reductions before and after 1 May 2004, until the end of the restructuring period, in accordance with
paragraphs 13 to 18. For this purpose the Commission shall report to the Council.
13. In addition to the monitoring of State aid, the Commission and the Council shall monitor the
restructuring benchmarks set out in Annex 3 to Protocol 8 to the Act of Accession of 16 April 2003.
References made in that Annex to paragraph 14 of the Protocol shall be construed as being made to
paragraph 14 of this Article.
14. Monitoring shall include an independent evaluation to be carried out in 2003, 2004, 2005 and
2006. The Commission's viability test shall be applied and productivity shall be measured as part of
the evaluation.
15.
Poland shall cooperate fully with all the arrangements for monitoring. In particular:
(a) Poland shall supply the Commission with six‑monthly reports concerning the restructuring of
the benefiting companies, no later than 15 March and 15 September of each year, until the end of
the restructuring period;
(b) the first report shall reach the Commission by 15 March 2003 and the last report by 15 March
2007, unless the Commission decides otherwise;
(c) the reports shall contain all the information necessary to monitor the restructuring process, the
State aid and the reduction and use of capacity and shall provide sufficient financial data to allow
an assessment to be made of whether the conditions and requirements contained in this Title have been fulfilled. The reports shall at the least contain the information set out in Annex 4 to
Protocol 8 to the Act of Accession of 16 April 2003, which the Commission reserves the right to
modify in line with its experiences during the monitoring process. In Annex 4 to Protocol 8 to
the Act of Accession of 16 April 2003, the reference to paragraph 14 of the Protocol shall be
construed as being to paragraph 14 of this Article. In addition to the individual business reports
of the benefiting companies there shall also be a report on the overall situation of the Polish steel
sector, including recent macroeconomic developments;
(d) all additional information necessary for the independent evaluation provided for in paragraph 14
must, furthermore, be provided by Poland;
(e) Poland shall oblige the benefiting companies to disclose all relevant data which might, under
other circumstances, be considered as confidential. In its reporting to the Council, the
Commission shall ensure that company‑specific confidential information is not disclosed.
16. The Commission may at any time decide to mandate an independent consultant to evaluate the
monitoring results, undertake any research necessary and report to the Commission and the Council.
17. If the Commission establishes, on the basis of the monitoring, that substantial deviations from
the financial data on which the viability assessment has been made have occurred, it may require
Poland to take appropriate measures to reinforce or modify the restructuring measures of the
benefiting companies concerned.
18.
Should the monitoring show that:
(a) the conditions for the transitional arrangements contained in this Title have not been fulfilled, or
that
(b) the commitments made in the framework of the extension of the period during which Poland
may exceptionally grant State support for the restructuring of its steel industry under the Europe
Agreement establishing an association between the European Communities and their Member
States, of the one part, and Poland, of the other part, have not been fulfilled, or that
(c) Poland in the course of the restructuring period has granted additional incompatible State aid to
the steel industry and to the benefiting companies in particular, the transitional arrangements
contained in this Title shall not have effect.
The Commission shall take appropriate steps requiring any company concerned to reimburse any aid
granted in breach of the conditions laid down in this Title.

Part IV
TITLE IX
PROVISIONS ON UNIT 1 AND UNIT 2 OF THE BOHUNICE V1 NUCLEAR POWER PLANT IN SLOVAKIA
Article 64
Slovakia has undertaken to close Unit 1 of the Bohunice V1 nuclear power plant by 31 December
2006 at the latest and Unit 2 of this plant by 31 December 2008 at the latest and subsequently
decommission these units.
Article 65
1. During the period 2004‑2006, the Union shall provide Slovakia with financial assistance in
support of its efforts to decommission, and to address the consequences of the closure and
decommissioning of, Unit 1 and Unit 2 of the Bohunice V1 nuclear power plant (hereinafter referred
to as ‘the Assistance’).
2. The Assistance shall be decided and implemented in accordance with the provisions laid down in
Council Regulation (EEC) No 3906/89 of 18 December 1989 on economic aid to certain countries of
Central and Eastern Europe ( 1 ).
3. For the period 2004‑2006 the Assistance shall amount to 90 million euro in commitment
appropriations, to be committed in equal annual tranches.
4. The Assistance or parts thereof may be made available as a Union contribution to the Bohunice International Decommissioning Support Fund, managed by the European Bank for Reconstruction and Development.

Article 66
The Union acknowledges that the decommissioning of the Bohunice V1 Nuclear Power plant must
continue beyond the Financial Perspective as defined in the Interinstitutional Agreement of 6 May
1999, and that this effort represents for Slovakia a significant financial burden. Decisions on the
continuation of Union assistance in this field after 2006 will take the situation into account.

Article 67

The provisions of this Title shall apply in the light of the Declaration on Unit 1 and Unit 2 of the
Bohunice V1 nuclear power plant in Slovakia which incorporates, without altering its legal effect, the
wording of the preamble to Protocol 9 to the Act of Accession of 16 April 2003.

TITLE X

PROVISIONS ON CYPRUS

Article 68
1. The application of the Community and Union acquis shall be suspended in those areas of the
Republic of Cyprus in which the Government of the Republic of Cyprus does not exercise effective
control.
2. The Council, on the basis of a proposal from the Commission, shall decide on the withdrawal of
the suspension referred to in paragraph 1. It shall act unanimously.
Article 69
1. The Council, on the basis of a proposal from the Commission, shall define the terms under
which the provisions of Union law shall apply to the line between those areas referred to in Article
68 and the areas in which the Government of the Republic of Cyprus exercises effective control. The
Council shall act unanimously.
2. The boundary between the Eastern Sovereign Base Area and those areas referred to in Article 68
shall be treated as part of the external borders of the Sovereign Base Areas for the purpose of Part
Four of the Annex to Protocol 3 to the Act of Accession of 16 April 2003 on the Sovereign Base
Areas of the United Kingdom of Great Britain and Northern Ireland in Cyprus for the duration of the
suspension of the application of the Community and Union acquis according to Article 68.
Article 70
1. Nothing in this Title shall preclude measures with a view to promoting the economic
development of the areas referred to in Article 68.
2. Such measures shall not affect the application of the Community and Union acquis under the
conditions set out in this Protocol in any other part of the Republic of Cyprus.
Article 71
In the event of settlement of the Cyprus problem, the Council, on the basis of a proposal from the
Commission, shall decide on the adaptations to the terms concerning the accession of Cyprus to the
Union with regard to the Turkish Cypriot Community. The Council shall act unanimously.
Article 72
This Title shall apply in the light of the declaration on Cyprus which incorporates, without altering its
legal effect, the wording of the preamble to Protocol 10 to the Act of Accession of 16 April 2003.

