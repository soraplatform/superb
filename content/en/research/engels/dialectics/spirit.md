---
title: Natural Science and the Spirit World
heading: Dialectics of Nature
date: 2022-02-01
description: Dialectics of Nature
# image: "/graphics/mao.jpg"
linkb: "/research/engels/dialectics/"
linkbtext: "Index"
linkf: "/research/engels/dialectics/"
linkftext: "Index"
author: "Frederick Engels"
icon: /icons/engels.png
---


Modern dialectics is split into 2 extremes. 

One is based on  <!-- In accordance with this we should hardly err in looking for the most extreme degree of fantasy, credulity, and superstition, not in that trend of  -->natural science which, like the German philosophy of nature, tries to force the objective world into the framework of its subjective thought. 

Another is based on mere experience. It disdains thought and prefers <!--  with sovereign  and really has gone to the furthest extreme in --> the extreme emptiness of thought.  This school prevails in England. 

Its father was Francis Bacon. He pushed the empirical-inductive method to attain:
- <!--  should be pursued to attain by its means, above all,  -->longer life
- rejuvenation
- alteration of stature and features
- transformation of one body into another
- the production of new species
- power over the air
- the production of storms. 

He complains that such investigations have been abandoned. In his natural history, he gives recipes for making gold and performing various miracles. 

Similarly, Isaac Newton in his old age expounded the revelation of St. John.

This is why English empiricists recently <!-- in the person of some of its representatives – and not the worst of them – should seem to --> have fallen as hopeless victims to the <!-- spirit-rapping and --> spirit-seeing imported from America.

The first natural scientist of this kind <!--  belonging here --> is Alfred Russell Wallace. He, simultaneously with Darwin, advanced the theory of natural selection.

In On Miracles and Modern Spiritualism of 1875, he says that his first experiences in this branch of natural knowledge was in 1844 when he attended the lectures of Mr. Spencer Hall on mesmerism. <!--  and as a result carried out similar experiments on his pupils. “I was extremely interested in the subject and pursued it with ardour.”  -->

Mr. Hall produced:
- magnetic sleep together with the phenomena of articular rigidity
- local loss of sensation
- confirmed the correctness of Gall’s map of the skull
  - on touching any one of Gall’s organs, the corresponding activity was aroused in the magnetised patient and exhibited by appropriate and lively gestures. 

He established that his patient, merely by being touched, partook of all the sensations of the operator; he made him drunk with a glass of water as soon as he told him that it was brandy. 

He could make one of the young men so stupid, even in the waking condition, that he no longer knew his own name, a feat, however, that other schoolmasters are capable of accomplishing without any mesmerism. And so on.

I also saw Mr. Spencer Hall in the winter of 1843-4 in Manchester. 

He was a very mediocre charlatan, who travelled the country under the patronage of some parsons and undertook magnetico-phrenological performances with a young girl to prove:
- the existence of God
- the immortality of the soul
- the incorrectness of the materialism being preached by the Owenites in all big towns. 

The lady was sent into a magnetico-sleep and then, as soon as the operator touched any part of the skull corresponding to one of Gall’s organs, she gave a bountiful display of theatrical, demonstrative gestures and poses representing the activity of the organ concerned. 

For instance, for the organ of philoprogenitiveness she fondled and kissed an imaginary baby, etc. Moreover, the good Mr. Hall had enriched Gall’s geography of the skull with a new island of Barataria: right at the top of the skull he had discovered an organ of veneration, on touching which his hypnotic miss sank on to her knees, folded her hands in prayer, and depicted to the astonished, philistine audience an angel wrapt in veneration. 

That was the climax and conclusion of the exhibition. The existence of God had been proved.

The effect on me and one of my acquaintances was exactly the same as on Mr. Wallace. 

The phenomena interested us. We tried to find out how far we could reproduce them. 

A 12 year old boy offered himself as subject. Gently gazing into his eyes, or stroking, sent him without difficulty into the hypnotic condition. 

But since we were rather less credulous than Mr. Wallace and set to work with rather less fervour, we arrived at quite different results. 

Apart from muscular rigidity and loss of sensation, which were easy to produce, we found also a state of complete passivity of the will bound up with a peculiar hypersensitivity of sensation. 

The patient, when aroused from his lethargy by any external stimulus, exhibited very much greater liveliness than in the waking condition. There was no trace of any mysterious relation to the operator; anyone else could just as easily set the sleeper into activity. 

To set Gall’s cranial organs into action was the least that we achieved; we went much further, we could not only exchange them for one another, or make their seat anywhere in the whole body, but we also fabricated any amount of other organs, organs of singing, whistling, piping, dancing, boxing, sewing, cobbling, tobacco-smoking, etc., and we could make their seat wherever we wanted. 

Wallace made his patients drunk on water, but we discovered in the great toe an organ of drunkenness which only had to be touched in order to cause the finest drunken comedy to be enacted. 

But no organ showed a trace of action until the patient was given to understand what was expected of him; the boy soon perfected himself by practice to such an extent that the merest indication sufficed.

The organs produced in this way then retained their validity for later occasions of putting to sleep, as long as they were not altered in the same way. The patient had even a double memory, one for the waking state and a second quite separate one for the hypnotic condition., 

As regards the passivity of the will and its absolute subjection to the will of a third person, this loses all its miraculous appearance when we bear in mind that the whole condition began with the subjection of the will of the patient to that of the operator, and cannot be restored without it. The most powerful magician of a magnetiser in the world will come to the end of his resources as soon as his patient laughs him in the face.

While we with our frivolous scepticism thus found that the basis of magnetico- phrenological charlatanry lay in a series of phenomena which for the most part differ only in degree from those of the waking state and require no mystical interpretation, Mr. Wallace’s “ardour” led him into a series of self-deceptions, in virtue of which he confirmed Gall’s map of the skull in all its details and noted a mysterious relation between operator and patient.[2] 

Everywhere in Mr. Wallace’s account, the sincerity of which reaches the degree of naivété, it becomes apparent that he was much less concerned in investigating the factual background of charlatanry than in reproducing all the phenomena at all costs. 

Only this frame of mind is needed for the man who was originally a scientist to be quickly converted into an “adept” by means of simple and facile self-deception. Mr. Wallace ended up with faith in magnetico-phrenological miracles and so already stood with one foot in the world of spirits.

He drew the other foot after him in 1865. On returning from his twelve years of travel in the tropical zone, experiments in table-turning introduced him to the society of various “mediums.” How rapid his progress was, and how complete his mastery of the subject, is testified to by the above-mentioned booklet. He expects us to take for good coin not only all the alleged miracles of Home, the brothers Davenport, and other “mediums” who all more or less exhibit themselves for money and who have for the most part been frequently exposed as impostors, but also a whole series of allegedly authentic spirit histories from early times. 

The Pythonesses of the Greek oracle, the witches of the Middle Ages, were all “mediums,” and Iamblichus[3] in his De divinatione already described quite accurately “the most astonishing phenomena of modern spiritualism."

Just one example to show how lightly Mr. Wallace deals with the scientific corroboration and authentication of these miracles. 

It is certainly a strong assumption that we should believe that the aforesaid spirits should allow themselves to be photographed, and we have surely the right to demand that such spirit photographs should be authenticated in the most indubitable manner before we accept them as genuine. 

Now Mr. Wallace recounts on p.187 that in March, 1872, a leading medium, Mrs. Guppy, née Nicholls, had herself photographed together with her husband and small boy at Mr. Hudson’s in Notting Hill, and on two different photographs a tall female figure, finely draped in white gauze robes, with somewhat Eastern features, was to be seen behind her in a pose as if giving a benediction. 

“Here, then, one of two things are absolutely certain.[4] Either there was a living intelligent, but invisible being present, or Mr. and Mrs. Guppy, the photographer, and some fourth person planned a wicked imposture and have maintained it ever since. 

Knowing Mr. and Mrs. Guppy so well as I do, I feel an absolute conviction that they are as incapable of an imposture of this kind as any earnest inquirer after truth in the department of natural science."[5]

Consequently, either deception or spirit photography. Quite so. And, if deception, either the spirit was already on the photographic plates, or four persons must have been concerned, or three if we leave out as weak-minded or duped old Mr. Guppy who died in January, 1875, at the age of 84 (it only needed that he should be sent behind the Spanish screen of the background). 

That a photographer could obtain a “model” for the spirit without difficulty does not need to be argued. But the photographer Hudson, shortly afterwards, was publicly prosecuted for habitual falsification of spirit photographs, so Mr. Wallace remarks in mitigation: 

“One thing is clear, if an imposture has occurred, it was at once detected by spiritualists themselves.” Hence there is not much reliance to be placed on the photographer. Remains Mrs. Guppy, and for her there is only the “absolute conviction” of our friend Wallace and nothing more. 

Nothing more? Not at all. 

The absolute trustworthiness of Mrs. Guppy is evidenced by her assertion that one evening, early in June, 1871, she was carried through the air in a state of unconsciousness from her house in Highbury Hill Park to 69, Lamb’s Conduit Street – three English miles as the crow flies – and deposited in the said house of No. 69 on the table in the midst of a spiritualistic séance. 

The doors of the room were closed, and although Mrs. Guppy was one of the stoutest women in London, which is certainly saying a good deal, nevertheless her sudden incursion did not leave behind the slightest hole either in the doors or in the ceiling. (Reported in the London .Echo, June 8, 1871.)

If anyone still does not believe in the genuineness of spirit photography, there’s no helping him.

The second eminent adept among English natural scientists is Mr. William Crookes. He discovered the chemical element thallium and the radiometer. 

Mr. Crookes began to investigate spiritualistic manifestations about 1871, and employed for this purpose a number of physical and mechanical appliances, spring balances, electric batteries, etc. 

Whether he brought to his task the main apparatus required, a sceptically critical mind, or whether he remained to the end in a fit state for working, we shall see. At any rate, within a not very long period, Mr. Crookes was just as completely captivated as Mr. Wallace. “For some years,” he relates, “a young lady, Miss Florence Cook, has exhibited remarkable mediumship, which latterly culminated in the production of an entire female form purporting to be of spiritual origin, and which appeared barefooted and in white flowing robes while she lay entranced in dark clothing and securely bound in a cabinet or adjoining room.” 

This spirit called itself Katie and looked remarkably like Miss Cook, was one evening suddenly seized round the waist by Mr. Volckmann – the present husband of Mrs. Guppy – and held fast in order to see whether it was not indeed Miss Cook in another edition. 

The spirit proved to be a quite sturdy damsel, it defended itself vigorously, the onlookers intervened, the gas was turned out, and when, after some scuffling, peace was reestablished and the room re-lit, the spirit had vanished and Miss Cook lay bound and unconscious in her corner. 

Nevertheless, Mr. Volckmann is said to maintain up to the present day that he had seized hold of Miss Cook and nobody else. In order to establish this scientifically, Mr. Varley, a well-known electrician, on the occasion of a new experiment, arranged for the current from a battery to flow through the medium, Miss Cook, in such a way that she could not play the part of the spirit without interrupting the current. Nevertheless, the spirit made its appearance. 

It was, therefore, indeed a being different from Miss Cook. To establish this further was the task of Mr. Crookes. His first step was to win the confidence of the spiritualistic lady. This confidence, so he says himself in the Spiritualist, June 5, 1874, “increased gradually to such an extent that she refused to give a séance unless I made the arrangements. She said that she always wanted me to be near her and in the neighbourhood of the cabinet; 

I found that – when this confidence had been established and she was sure that I would not break any promise made to her – the phenomena increased considerably in strength and there was freely forthcoming evidence that would have been unobtainable in any other way. 

She frequently consulted me in regard to the persons present at the séances and the places to be given them, for she had recently become very nervous as a result of certain ill-advised suggestions that, besides other more scientific methods of investigation, force also should be applied.”


The spirit lady rewarded this confidence, which was as kind as it was scientific, in the highest measure. 

She even made her appearance – which can no longer surprise us – in Mr. Crookes’ house, played with his children and told them “anecdotes from her adventures in India,” treated Mr. Crookes to an account of “some of the bitter experiences of her past life,” allowed him to take her by the arm so that he could convince himself of her evident materiality, allowed him to take her pulse and count the number of her respirations per minute, and finally allowed herself to be photographed next to Mr. Crookes.

“This figure,” says Mr. Wallace, “after she had been seen, touched, photographed, and conversed with, vanished absolutely out of a small room from which there was no other exit than an adjoining room filled with spectators” – which was not such a great feat, provided that the spectators were polite enough to show as much faith in Mr. Crookes, in whose house this happened, as Mr. Crookes did in the spirit.

Unfortunately, these “fully authenticated phenomena” are not immediately credible even for spiritualists. We saw above how the very spiritualistic Mr. Volckmann permitted himself to make a very material grab. 

And now a clergyman, a member of the committee of the “British National Association of Spiritualists,” has also been present at a séance with Miss Cook, and he established the fact without difficulty that the room through the door of which the spirit came and disappeared communicated with the outer world by a second door. The behaviour of Mr. Crookes, who was also present, gave “the final death blow to my belief that there might be something in the manifestations.” (Mystic London, by the Rev. C. Maurice Davies, London, Tinsley Brothers).[6] And, over and above that, it came to light in America how “Katies” were “materialised.” A married couple named Holmes held séances in Philadelphia in which likewise a “Katie” appeared and received bountiful presents from the believers. However, one sceptic refused to rest until he got on the track of the said Katie, who, anyway, had already gone on strike once because of lack of pay; he discovered her in a boarding-house as a young lady of unquestionable flesh and bone, and in possession of all the presents that had been given to the spirit.

Meanwhile the Continent also had its scientific spiritseers. A scientific association at St. Petersburg – I do not know exactly whether the University or even the Academy itself – charged the Councillor of State, Aksakov, and the chemist, Butlerov, to examine the basis of the spiritualistic phenomena, but it dbes not seem that very much came of this. On the other hand – if the noisy announcements of the spiritualists are to be believed – Germany has now also put forward its man in the person of Professor Zöllner in Leipzig.

For years, Herr Zöllner has been hard at work on the “fourth dimension” of space. He has discovered that many things that are impossible in 3D space are a simple matter 4D space. 

Thus, in 4D space:
- a closed metal sphere can be turned inside out like a glove, without making a hole in it
- a knot can be tied in an endless string or one which has both ends fastened
- 2 separate closed rings can be interlinked without opening either of them
- etc

According to the recent triumphant reports from the spirit world, it is said now that Professor Zöllner has addressed himself to one or more mediums in order with their aid to determine more details of the locality of the fourth dimension. 

After the session the arm of the chair, on which he rested his arm while his hand never left the table, was found to have become interlocked with his arm, a string that had both ends sealed to the table was found tied into four knots, and so on. 

In short, all the miracles of the 4th dimension were performed by the spirits with the utmost ease. I believe in the correctness of the spirit bulletin. <!-- , and if it should contain any inaccuracy, Herr Zöllner ought to be thankful that I am giving him the opportunity to make a correction.  -->

If, however, it reproduces the experiences of Herr Zöllner without falsification, then it obviously signifies a new era both in the science of spiritualism and that of mathematics. 

The spirits prove the existence of the 4th dimension, just as the 4th dimension vouches for the existence of spirits. 

And this once established, an entirely new, immeasurable field is opened to science. 

All previous mathematics and natural science will be only a preparatory school for the mathematics of the fourth and still higher dimensions, and for the mechanics, physics, chemistry, and physiology of the spirits dwelling in these higher dimensions. 

Has not Mr. Crookes scientifically determined how much weight is lost by tables and other articles of furniture on their passage into the fourth dimension – as we may now well be permitted to call it – and does not Mr. Wallace declare it proven that fire there does no harm to the human body?

And now we have even the physiology of the spirit bodies! They breathe, they have a pulse, therefore lungs, heart, and a circulatory apparatus, and in consequence are at least as admirably equipped as our own in regard to the other bodily organs. 

Breathing requires carbohydrates which undergo combustion in the lungs. These carbohydrates can only be supplied from outside. hence, stomach, intestines, and their accessories – and if we have once established so much, the rest follows without difficulty. 

The existence of such organs, however, implies the possibility of their falling a prey to disease, hence it may still come to pass that Herr Virchow will have to compile a cellular pathology of the spirit world. And since most of these spirits are very handsome young ladies, who are not to be distinguished in any respect whatsoever from terrestrial damsels, other than by their supra-mundane beauty, it could not be very long before they come into contact with “men who feel the passion of love"; and since, as established by Mr. Crookes from the beat of the pulse, “the female heart is not absent,” natural selection also has opened before it the prospect of a fourth dimension, one in which it has no longer any need to fear of being confused with wicked social-democracy.

 

The most certain path from natural science to mysticism is not the extravagant theorising of the philosophy of nature, but the shallowest empiricism that spurns all theory and distrusts all thought. 

It is not a priori necessity that proves the existence .of spirits, but the empirical observations of Messrs. Wallace, Crookes, and Co. 

If we trust the spectrum-analysis observations of Crookes, which led to the discovery of the metal thallium, or the rich zoological discoveries of Wallace in the Malay Archipelago, we are asked to place the same trust in the spiritualistic experiences and discoveries of these two scientists. 

We say that we can verify the one but not the other. But the spirit-seers argue that they can let us verify also the spirit phenomena.

Dialectics cannot be despised with impunity. 

However great one’s contempt for all theoretical thought, nevertheless one cannot bring two natural facts into relation with one another, or understand the connection existing between them, without theoretical thought. The only question is whether one’s thinking is correct or not, and contempt of theory is evidently the most certain way to think naturalistically, and therefore incorrectly. 

But, according to an old and well-known dialectic law, incorrect thinking, carried to its logical conclusion, inevitably arrives at the opposite of its point of departure. Hence, the empirical contempt of dialectics on the part of some of the most sober empiricists is punished by their being led into the most barren of all superstitions, into modern spiritualism.

It is the same with mathematics. The ordinary metaphysical mathematicians boast with enormous pride of the absolute irrefutability of the results of their science. 

But these results include also imaginary magnitudes, which thereby acquire a certain reality. When one has once become accustomed to ascribe some kind of reality outside of our minds to √-1, or to the fourth dimension, then it is not a matter of much importance if one goes a step further and also accepts the spirit world of the mediums. It is as Ketteler said about Döllinger[7]: “The man has defended so much nonsense in his life, he really could have accepted infallibility into the bargain!”

In fact, mere empiricism is incapable of refuting the spiritualists because:

1. The “higher” phenomena always show themselves only when the “investigator” concerned is already so far in the toils that he now only sees what he is meant to see or wants to see – as Crookes himself describes with such inimitable naivété. 

2. The spiritualist cares nothing that hundreds of alleged facts are exposed as imposture and dozens of alleged mediums as ordinary tricksters. 

As long as every single alleged miracle has not been explained away, they have still room enough to carry on, as indeed Wallace says clearly enough in connection with the falsified spirit photographs. 

The existence of falsifications proves the genuineness of the genuine ones.

Empiricism is compelled to refute the importunate spirit-seers not through empirical experiments, but by theoretical considerations. , and to say, with Huxley[8]: “The only good that I can see in the demonstration of the truth of ‘spiritualism’ is to furnish an additional argument against suicide. 

Better live a crossing-sweeper than die and be made to talk twaddle by a ‘medium’ hired at a guinea a séance!"


## Notes

1. From a manuscript of Engels probably written in 1878, and first published in the “Illustrierter Neue Welt-Kalender für das Jahr 1898.”

2. As already said, the patients perfect themselves by practice. It is therefore quite possible that, when the subjection of the will has become habitual, the relation of the participants becomes more intimate, individual phenomena are intensified and are reflected weakly even in the waking state. [Note by F. Engels.]

3. See Appendix II, p. 368.

4. The spirit world is superior to grammar. A joker once caused the spirit of the grammarian Lindley Murray to testify. To the question whether he was there, he answered: “I are.” (American for I am.) The medium was from America. [Note by F. Engels.]

5. See Appendix II, p. 369.

6. See Appendix II, p. 370.

7. A catholic scholar who did not accept the dogma of papal infallibility.

8. See Appendix II, p. 370.
