+++
title= "The Natural Order of Society"
heading= "The Simple Republic by Plato Book 3 Chapter 4"
# date= 2015-09-09
date= 2015-09-26
image= "/covers/republic.jpg"
description= "The guardians should be tested regularly for their position"
linkf= "/research/socrates/simple-republic/book-3/chapter-4/"
linkftext= "Chapter 4"
linkb= "/research/socrates/simple-republic/book-3/chapter-3/"
linkbtext= "Chapter 3"
icon= "/icons/soc.png"
# linkbook= https://play.google.com/store/books/details?id=WlkBEAAAQBAJ
# linkbooktext= Support Superphysics by buying the ebook version
+++

## Lawmakers and Lawyers Must Be Virtuous

{{< r a="Glaucon" >}}
All that, Socrates, is excellent. But I shouldn't there be good physicians in a State who will treat the people, both healthy and unhealthy?
{{< /r >}}

{{< l a="Socrates" >}}
The best judges know all sorts of moral natures. The most skilful physicians have the greatest experience with diseases, from their youth upwards. They should have had all manner of diseases in themselves. <!-- I think that the body is not the instrument with which they cure the body. In that case we could not allow them ever to be or to have been sickly. But  --> They cure their body with their mind. The sick mind can cure nothing. But it is otherwise with the judge, since he governs mind by mind.

The judge should not:
<ul>
  <li>be trained among vicious minds nor associate with them from youth upwards</li>
  <li>go through the whole calendar of crime just to be able to infer the crimes of others as the physician might infer bodily diseases to certain habits.</li>
</ul>

{{< /l >}}



{{< l a="Socrates" >}}
The honourable judge should have had no experience or contamination of evil habits when young. This is why good young men often appear to be simple and easily victimized by the dishonest because they have no idea of what evil is in their own souls.

Therefore, the judge should not be young. He should have learned to know evil, not from his own soul, but from a long observation of the nature of evil in others.
Knowledge should be his guide, not personal experience. The ideal judge will have a good soul. 

A master criminal is very careful when he is with fellow criminals because he compares them to himself. But when is with men of virtue, he appears to be a fool due to his unseasonable suspicions. He cannot recognise an honest man, because he has no pattern of honesty in himself. The bad are more numerous than the good. He meets bad people more often. This makes him, and others, think that he himself is wise than foolish.
<!-- Then the good and wise judge whom we are seeking is not this man, but the other.
For vice cannot know virtue. -->
<!-- I think that the virtuous, and not the vicious, has wisdom. -->

A virtuous nature, educated by time, will acquire a knowledge both of virtue and vice. This is the sort of medicine and law which you will sanction in your state. They will minister to better natures, giving health both of soul and of body. But those who are diseased in their bodies they will leave to die. The corrupt and incurable souls they will put an end to themselves.

Thus our youth, having been educated only in that simple music which inspires temperance, will be reluctant to go to law. The musician, who keeps to the same track, is content to practise the simple gymnastic. He will have nothing to do with medicine unless in some extreme case.

The very exercises and tolls which he undergoes are intended to stimulate the spirited element of his nature, and not to increase his strength. He will not, like common athletes, use exercise and regimen to develop his muscles.

The current art of music is not really designed to train the soul. The curret art of gymnastic is not designed to train the body. 
{{< /l >}}

{{< r a="Glaucon" >}}
What then is their real object?
{{< /r >}}

{{< l a="Socrates" >}}
I believe that the teachers of both aimed for the improvement of the soul. The exclusive devotion to gymnastic or music have opposite effects on the mind. Gymnastic produces a temper of hardness and ferocity. But this ferocity only comes from spirit and leads to courage if rightly educated. If too intensified, it is liable to become hard and brutal.

Music produces softness and effeminacy. The philosopher will have the quality of gentleness which will turn to softness if it is too much indulged.

<!-- But if educated rightly, will be gentle and moderate. -->
The guardians should have both these qualities and both should be in harmony. The harmonious soul is both temperate and courageous. The inharmonious is cowardly and boorish.
{{< /l >}}



{{< l a="Socrates" >}}
When a man allows music to play in him and to pour into his soul through the funnel of his ears those sweet and soft and melancholy airs of which we were just now speaking, and his whole life is passed in warbling and the delights of song.

In the first stage of the process, his passion or spirit is tempered like iron. It is made useful, instead of being brittle and useless.

But if he carries on the softening and soothing process, he begins to melt and waste, until he has wasted away his spirit and cut out the sinews of his soul. He becomes a feeble warrior.

If his spirit is naturally weak, the change is competed quickly. But if his spirit is strong, then the weakening power of music makes him excitable. He flames up on the least provocation. Instead of having spirit, he grows irritable. <!--  and passionate and is quite impracticable. -->

And so in gymnastics, if a man takes violent exercise and eats a lot, his body fills him with pride and spirit. He becomes twice the man that he was. He will become uncivilized and a hater of philosophy. He will never use the weapon of persuasion. His intelligence will grow feeble, dull, and blind from having no taste in learning, enquiry, thought or culture. He is like a wild beast, all violence and fierceness, and knows no other way of dealing.
{{< /l >}}
<!-- is a great feeder, and the reverse of a great student of music and philosophy, at first the high condition of 
, and  -->
<!-- , if he does nothing else and holds no converse with the Muses. -->
<!-- His mind will never wake up or receive nourishment.
His senses will not be purged of their mists. -->

<!-- He lives in all ignorance and evil conditions.
He has no sense of propriety and grace.
 -->


{{< l a="Socrates" >}}
There are two principles of human nature:
<ul>
  <li>The spirited</li>
  <li>The philosophical</li>
</ul>

Some god has given these indirectly for the soul and the body. These two principles can be adjusted, like the strings of an instrument, until they are duly harmonized.

The true musician is one who mingles music with gymnastic in the fairest proportions and best tunes them to the soul. He is far higher than the tuner of the strings. Such a genius will be always required in our State if the government is to last.
{{< /l >}}



{{< l a="Socrates" >}}
Those are the principles of our nurture and education. The details of the rules for our dances, hunting, and gymnastic and equestrian contests follow this general principle. This general principle helps us create those rules.

<ul>
  <li>The elder must rule the younger.</li>
  <li>The best of these must rule.</li>
  <li>The best husbandmen are those most devoted to husbandry.</li>
</ul>

We should have the best of guardians for our city. They should be wise, efficient, and have a special care of the State. A man will be most likely to care about:
<ul>
  <li>that which he loves,</li>
  <li>that which he regards as having the same interests with himself,</li>
  <li>that of which the good or evil fortune is supposed by him at any time most to affect his own.</li>
</ul>
{{< /l >}}


{{< r a="Glaucon" >}}
Then there must be a selection.
{{< /r >}}


{{< l a="Socrates" >}}
Let us note who among our guardians show the greatest eagerness to do what is good for their country, and the greatest repugnance to do what is against her interests.

They will have to be watched at every age so that we may see whether they preserve their resolution and never forget their sense of duty to the State.
A person can forget his resolution either:
<ul>
  <li>willingly, or
    <ul>
      <li>This happens when he gets rid of a falsehood and learns better.</li>
    </ul>
  </li>
  <li>against his will.
    <ul>
      <li>This happens whenever he is deprived of a truth.</li>
    </ul>
  </li>
</ul>
{{< /l >}}

{{< r a="Glaucon" >}}
I understand the willing loss of a resolution. But I still have to learn the meaning of the unwilling.
{{< /r >}}

{{< l a="Socrates" >}}
Do you not see that men are unwillingly deprived of good, and willingly deprived of evil? Is not to have lost the truth an evil, and to possess the truth a good? You would agree that to conceive things as they are is to possess the truth?
{{< /l >}}

{{< r a="Glaucon" >}}
Yes, I agree that people are deprived of the truth against their will.
{{< /r >}}


{{< l a="Socrates" >}}
This is not this involuntary deprivation caused by theft, or force, or enchantment. I mean that some men are changed by persuasion, and that others forget.
<!-- "Theft" is in argument stealing stealing away the hearts of one class, and in time stealing those of the other.
The victims are forced to change their opinion, compelled by violence of pain or grief.
The enchanted are those who change their minds under:
the softer influence of pleasure, or
the sterner influence of fear. -->

Therefore, we must enquire who are the best guardians of their own conviction, so that the interest of the State will be the rule of their lives.

1. Our first test is to watch them from their youth upwards, and make them perform actions which will make them forget or be deceived. We will select those who remember and are not deceived.

2. Our second test are the toils, pains, and conflicts prescribed for them.

3. Our third test is to try them with enchantments.

<!-- These will give further proof of the same qualities.
We will see whether their behaviour will be like those who take colts amid tumult to see if they timid. -->
We must take our youth amid some terrors and then pass them into pleasures. We can then prove them more thoroughly than gold is proved in the furnace. We may discover whether they are:
<ul>
  <li>armed against all enchantments,</li>
  <li>of a noble bearing always,</li>
  <li>good guardians of themselves and of the music which they have learned, and</li>
  <li>retaining under all circumstances a rhythmical and harmonious nature, such as will be most serviceable to the individual and to the State.</li>
</ul>

Those who come out of the trial victorious and pure at every age, from boyhood to adulthood, shall be appointed as ruler and guardian of the State. He shall be honoured in life and death. He shall receive sepulture and other memorials of honour, the greatest that we have to give. But we must reject those who fail.
<!-- Our rulers and guardians should generally be chosen this way. -->
Perhaps the word 'guardian' in the fullest sense should be applied to only this higher class which:
<ul>
  <li>preserves us against foreign enemies, and</li>
  <li>maintains peace among our citizens at home so that they cannot harm us.</li>
</ul>

The young men whom we before called 'guardians' may be more properly designated as the rulers' auxiliaries and supporters. 

An old Phoenician tale told of a useful lie. It said that the earth is our mother where we were manufactured in, and from it we were sent up. We should communicate this useful lie gradually, first to the rulers, then to the soldiers, and lastly to the people. We will tell them that:
<ul>
  <li>their youth was a dream,</li>
  <li>the education and training which they received from us, an appearance only.</li>
  <li>their land is their mother and nurse which they should defend</li>
  <li>her citizens are the children of the land and their own brothers.</li>
</ul>
{{< /l >}}


## Plato's Four Castes= Gold (Brahmin), Silver (Ksattriya), Brass (Vaesha), Iron (Shudra)
<!-- The human species will generally be preserved in the children.
But all are of the same original stock. -->

{{< l a="Socrates" >}}
The tale also said that God created each citizen differently:
<ul>
  <li>The golden citizens have the power of command and consequently have the greatest honour.</li>
  <li>The silver ones are the auxiliaries.</li>
  <li>The brass and iron citizens are the husbandmen and craftsmen.</li>
  <li></li>
</ul>        

A golden parent will sometimes have a silver son, or a silver parent a golden son. God proclaims as a first principle to the rulers, that he should guard most the purity of the race. They should observe what elements mingle in their offspring.

If the son of a golden or silver parent has an admixture of brass and iron, then nature orders a transposition of ranks. The ruler must not pity the child that descends to become a husbandman or artisan just as there may be sons of artisans who are raised to honour by having an admixture of gold or silver to become guardians or auxiliaries.

An oracle says that when a man of brass or iron guards the State, it will be destroyed. 
{{< /l >}}

{{< r a="Glaucon" >}}
There is no way for  of accomplishing this. 

But their sons and their descendants may be made to believe in the tale. Such a belief will make them care more for the city and for one another. We can let this fiction fly abroad as a rumour.

In the meantime, let us arm our earth-born heroes and lead them under their rulers. Let them select a spot from where they can best suppress insurrection. There let them encamp, sacrifice to the proper Gods, and prepare their dwellings. Their houses must shield them against the cold of winter and the heat of summer. They must be the houses of soldiers, and not of shop-keepers. 

A shepherd is monstrous if he keeps watch-dogs which act like wolves and turn on the sheep because of hunger, evil habit, or lack of discipline. Therefore, every care must be taken that our auxiliaries, being stronger than our citizens, will not grow to be too much for them and become savage tyrants. A really good education will furnish the best safeguard. 
{{< /r >}}


{{< r a="Glaucon" >}}
But they are well-educated already.
{{< /r >}}


{{< l a="Socrates" >}}
Not yet, but I think that they should be. True education will civilize and humanize them. Their education, habitations, and all that belongs to them:
<ul>
  <li>should not impair their virtue as guardians, nor</li>
  <li>tempt them to prey on the other citizens.</li>
</ul>

First, none of them should have any property of his own beyond what is absolutely necessary. They should not have a private house or store closed to the public. Their provisions should be only such as are required by trained warriors, who are men of temperance and courage. They should agree to receive from the citizens a fixed rate of pay, enough to meet the expenses of the year and no more. They will go to mess and live together like soldiers in a camp.

We will tell them that they have gold and silver from God and that the diviner metal is within them. They have therefore no need of the dross which is current among men. They should not pollute the divine by any such earthly admixture. <!-- For that commoner metal has been the source of many unholy deeds, but their own is undefiled. -->

Of the citizens, they alone cannot touch or handle silver or gold, be under the same roof with them, wear them, or drink from them. This will be their salvation, and they will be the saviours of the State.

But if they ever acquire homes, lands, or money of their own:
<ul>
  <li>They will become housekeepers and husbandmen instead of guardians, enemies and tyrants instead of allies of the other citizens</li>
  <li>They will hate and be hated, plot and be plotted against.</li>
<!--   <li>They will pass their whole life in much greater terror of internal than of external enemies, and the hour of ruin, both to themselves and to the rest of the State, will be at hand.  -->
</ul>

These shall be the regulations appointed by us for guardians on their houses and all other matters.
{{< /l >}}
