---
title: "The True Nature"
heading: Chapter 5 Book 10 of The Republic by Plato Simplified
date: 2022-01-31
description: "Let each of us leave every other kind of knowledge and seek and follow one thing only"
image: "/covers/republic.jpg"
linkb: "/research/socrates/simple-republic/book-10/chapter-4/"
linkbtext: "Chapter 4"
linkf: "/research/socrates/simple-republic/"
linkftext: "Index"
icon: "/icons/soc.png"
---


{{< l a="Socrates" >}}
Here, my dear Glaucon, is the supreme peril of our human state. Therefore, the utmost care should be taken.

**Let each of us leave every other kind of knowledge and seek and follow one thing only.**

This will help us learn and discern between good and evil. In this way, we can always and everywhere choose the better life.

We should consider the bearing of all these things on virtue. We should know:
<ul>
  <li>what the effect of beauty is when combined with poverty or wealth in a particular soul,</li>
  <li>what are the good and evil consequences of:
    <ul>
      <li>noble and humble birth,</li>
      <li>private and public station,</li>
      <li>strength and weakness,</li>
      <li>cleverness and dullness, and</li>
      <li>all the natural and acquired gifts of the soul, and</li>
      <li>the operation of them when conjoined.</li>
    </ul>
  </li>
</ul>
{{< /l >}}



{{< l a="Socrates" >}}
These will make us look at the nature of the soul and be able to determine which is the better and the worse. We will give the name of:
<ul>
  <li>"evil" to the life which will make souls more unjust, and</li>
  <li>good to the life which will make souls more just.</li>
</ul>

All else we will disregard.

We have seen and known that this is the best choice both in this life and in the next life.

A man must take with him after death an adamantine faith in truth and right so that in the next life he may be undazzled by the desire of wealth or the other allurements of evil.

If not, he will come upon tyrannies and similar villainies. He will do irremediable wrongs to others and suffer worse himself.

But let him know how to choose the mean and avoid the extremes on either side not only in this life but in all lives to come, for this is the way of happiness.

According to Er, the prophet said:

{{< q >}}
Even for the last comer, if he chooses wisely and will live diligently, there is appointed a happy and not undesirable existence.<br><br>The first chooser should not be careless. The last chooser should not despair.
{{< /q >}}

The first chooser came forward and chose the greatest tyranny.
<ul>
  <li>His mind had been darkened by folly and sensuality.</li>
  <li>He had not thought out the whole matter before he chose.</li>
  <li>He did not at first sight perceive that he was fated, among other evils, to devour his own children.</li>
  <li>But when he had time to reflect and saw what was in the lot, he began to beat his breast and lament over his choice, forgetting the prophet's proclamation.</li>
  <li>For, instead of throwing the blame of his misfortune on himself, he accused chance and the gods, and everything rather than himself.</li>
  <li>He was one of those who came from heaven.</li>
  <li>In a former life, he had dwelt in a well-ordered State.</li>
  <li>But his virtue was a matter of habit only, and he had no philosophy.</li>
</ul>

This was true of others who were similarly overtaken. Most of them came from heaven and therefore they had never been schooled by trial.

Whereas the pilgrims who came from earth having themselves suffered and seen others suffer, were not in a hurry to choose.

Due to this inexperience and also because the lot was a chance, many of the souls exchanged a good destiny for an evil or an evil for a good.

A man might be happy here if, from his arrival in this world, he 
<ul>
  <li>had always dedicated himself to sound philosophy and</li>
  <li>had been moderately fortunate in the number of the lot<!-- , as Er reported, --> .</li>
</ul>

His journey to another life and return to this would be smooth and heavenly, instead of being rough and underground.
{{< /l >}}

{{< r a="Glaucon" >}}
Most curious was the spectacle—sad, laughable, and strange.
{{< /r >}}


{{< l a="Socrates" >}}
The choice of the souls was in most cases based on their experience of a previous life. He saw:

<ul>
  <li>the soul who had once been Orpheus choosing the life of a swan because of his enmity to women. He hated to be born of a woman because they had been his murderers.</li>
  <li>the soul of Thamyras choosing the life of a nightingale. Like swan and other musicians, it is a bird that wants to be male.</li>
  <li>Ajax, the son of Telamon, obtaining the 20th lot and choosing the life of a lion. He remembered the injustice which was done him in the judgment about the arms.</li>
  <li>Agamemnon taking the life of an eagle because, like Ajax, he hated human nature by reason of his sufferings.</li>
  <li>Atalanta choosing to the great fame of an athlete and was unable to resist the temptation.</li>
  <li>the soul of Epeus, the son of Panopeus, choosing the nature of a woman cunning in the arts</li>
  <li>the soul of the jester Thersites was putting on the form of a monkey</li>
  <li>lastly, the soul of Odysseus who remembered his former toils which then disenchanted him of ambition. And so he spent a considerable time searching the life of a private man who had no cares. He had some difficulty in finding this, which was lying around neglected by everybody else. When he saw it, he said that he would have done the same had his lot been first instead of last, and that he was delighted to have it.</li>
</ul>
{{< /l >}}



{{< l a="Socrates" >}}
Not only did men pass into animals. There were animals tame and wild who also changed into one another and into corresponding human natures. 

The good changed into the gentle and the evil into the savage, in all sorts of combinations*. All the souls had now chosen their lives.


> *Superphysics note: The analysis of such evolutionary combinations for a soul can be done by machine learning 


They went in the order of their choice to Lachesis [past] who sent with them the genius that they had chosen, to be:
<ul>
  <li>the guardian of their lives and</li>
  <li>the fulfiller of the choice.</li>
</ul>
{{< /l >}}



{{< l a="Socrates" >}}
This genius led the souls first to Clotho [present] and drew them within the revolution of the spindle impelled by her hand, thus ratifying the destiny* of each.


> *Superphysics note: This explains free-will being bound by predestination. In app development, this is similar to the compilation process of code. In Superphysics, the life-patterns are compiled into the chakras of the metaphysical soul, just as biological patterns are compiled into the DNA of a physical body


Then, when they were fastened to this, carried them to Atropos [future]. He spun the threads and made them irreversible* and they passed beneath the throne of Necessity.

> *Superphysics note: This is similar to the deploy phase of an app onto the Play store or App store


When they had all passed, they marched on in a scorching heat to the plain of Forgetfulness*, which was a barren waste destitute of trees and verdure.


> *Superphysics note: This is the main property of Maya that generates the illusion of dualistic existence. In apps, this prevents the app or user input from knowing the app's uncompiled code or blueprint


{{< l a="Socrates" >}}
Then towards evening, they encamped by the river of Unmindfulness, whose water no vessel can hold. They were all obliged to drink a certain quantity of this.

Those who were not saved by wisdom drank more than was necessary. Those who drank forgot all things.

After they had gone to rest, at the middle of the night there was a thunderstorm and earthquake. Instantly, they were driven upwards in all manner of ways to their birth, like shooting stars.

Er was hindered from drinking the water. He could not say how he returned to the body except that he awoke suddenly and found himself lying on the pyre.
{{< /l >}}


{{< l a="Socrates" >}}
Thus, Glaucon, the tale has been saved and has not perished.

It will save us if we are obedient to it. We shall pass safely over the river of Forgetfulness and our soul will not be defiled.

My counsel is that we hold fast ever to the heavenly way and follow after justice and virtue always.

We should consider that the soul is immortal and able to endure every sort of good and every sort of evil.

Thus, we shall live dear to one another and to the gods, both while remaining here and when, like conquerors in the games who go round to gather gifts, we receive our reward. 

It shall be well with us both in this life and in the pilgrimage of a thousand years in the afterlife.
{{< /l >}}

END
