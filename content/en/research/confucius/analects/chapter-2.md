+++
title=  "Good Government, Filial Piety, and The Superior Man"
description=  "Filial piety is in not being perverse. We should always follow the Rules of Propriety with regard to our parents"
image=  "/covers/analects.png"
date = 2020-01-30
linkf=  "/research/confucius/analects/chapter-1"
linkftext=  "Chapter 1"
linkb=  "/research/confucius/analects/chapter-3"
linkbtext=  "Chapter 3"
heading=  "Chapter 2"
+++ 


{{< l a="conf" >}}
Confucius=  "Let a ruler base his government upon virtuous principles, and he will be like the pole-star, which remains steadfast in its place, while all the host of stars turn towards it.

The 'Book of Odes' has 300 pieces.
But one expression in it can cover the purport of all, namely unswerving mindfulness.
The people will become evasive and devoid of any sense of shame if they are governed simply by statute, pains and penalties.
They will develop a sense of shame and know their errors if they are governed by principles of virtue and the Rules of Propriety.
When I reached 15, I became bent on study.

<ul>
<li>At 30, I was a confirmed student.</li>
<li>At 40, nothing could move me from my course.</li>
<li>At 50, I comprehended the will and decrees of Heaven.</li>
<li>At 60, my ears were attuned to them.</li>
<li>At 70, I could follow my heart's desires, without overstepping the lines of rectitude.</li>"
</ul>
{{< /l >}}

<div class="right mang">
Mang-i=  What does filial piety consist in?
</div>

<div class="left conf">
Filial piety is in not being perverse. 

We should always follow the Rules of Propriety with regard to our parents= 
<ul>
<li>in ministering to them while living,</li>
<li>in burying them when dead, and</li>
<li>in offering sacrificial gifts to them.</li>
</ul>

Parents should bear only one trouble=  their own sickness. The filial piety of the present day simply means the being able to support one's parents.

This extends even to the case of dogs and horses. All of these can support them.
</div>

<div class="right mang">
If there is no reverential feeling, what is there to distinguish between the cases?
</div>

<div class="left conf">
It is the difficulty.
</div>

<div class="right mang">
Does filial piety mean that young people should just do whatever is told of them? <!--  folks simply take upon themselves the toil of it. --> Or that, in the matter of meat and drink, they simply set these before their elders?
</div>


<div class="left conf">
I talked to Hwi all day. He has controverted nothing that I have said, as if he were without wits.
But when his back was turned, and I looked attentively at his conduct<!--  apart from me, I --> and found it satisfactory in all its issues.
</div>

<div class="right mang">
No, indeed! Hwi is not without his wits.
</div>

<div class="left conf">
If you observe<!--  what things people (usually) take in hand, watch --> the motives of people and note what gives them satisfaction, then they will not be able to hide their character from you. <!--  shall they be able to conceal from you what they are? Conceal themselves, indeed! -->

Be versed in ancient lore, and familiarize yourself with the modern; then may you become teachers. The great man is not a mere receptacle.
</div>


<div class="left conf">
In reply to Tsz-kung respecting the great man=  "What he first says, as a result of his experience, he afterwards follows up. "The great man is catholic-minded, and not one-sided. The common man is the reverse. "Learning, without thought, is a snare; thought, without learning, is a danger. "Where the mind is set much upon heterodox principles there truly and indeed is harm." 
</div>

+++

<div class="right tsz-lu">
What is knowledge? 
</div>

<div class="left conf">
Shall I give you a lesson about knowledge? When you know a thing, maintain that you know it. When you do not, acknowledge your ignorance. This is characteristic of knowledge.
</div>

+++


<div class="right tsz-chang">
I am studying official incomes
</div>

<div class="left conf">
You will make few mistakes if you speak guardedly and stay away from doubtful things. <!-- Of the many things you hear hold aloof from those that are , and  with reference to the rest; your mistakes will then be few. --> Avoid <!-- lso, of the many courses you see adopted, hold aloof from those that are --> risky courses, and carefully follow the others. <!-- ; you will then seldom have occasion for regret. --> You will be seldom mistaken in your utterances. This will aid you <!-- , and having few occasions for regret in the line you take, you are --> on the high road to your preferment.
</div>

+++

<div class="right ngai">
Duke Ngai=  What should be done to render the people submissive to authority?
</div>

<div class="left conf">
Promote the straightforward, and reject those whose courses are crooked, and the thing will be effected. Promote the crooked and reject the straightforward, and the effect will be the reverse." 
</div>

+++

<div class="right kikang">
Ki K'ang=  How can the people be induced to show respect, loyalty, and willingness to be led? 
</div>

<div class="left conf">
Their leader should have a grave dignity. <!--  in him who has the oversight of them, and they will show him respect; --> He should be seen to be good to his own parents and kind. This will make them loyal to him. 

He should promote those who have ability and see to the instruction of those who have none. This will make them willing to be led.
</div>


<div class="right kikang">
Someone=  Why are you not an administrator of government?
</div>


<div class="left conf">
The 'Book of the Annals' says about filial duty=  

{{< quote >}}
'Make it a point to be dutiful to your parents and amicable with your brethren. These same duties extend to an administrator.' 
</div>

Therefore I am an administrator in my own way.
<!-- If these, then, also make an administrator, how am I to take your words about being an administrator?"  -->

People whose word has no reliability are good for nothing How should your carriages, large or little, get along without your whipple-trees or swing-trees?
</div>

<div class="right tsz-chang">
Tsz-chang=  Is it possible to forecast the state of the country 10 generations from now?
</div>

<div class="left conf">
The Yin dynasty adopted the rules and manners of the Hu line of kings. It is possible to tell whether it retrograded or advanced. 

The Chow line has followed the Yin and adopted its ways. Whether there has been deterioration or improvement may also be determined. 

Some other line may take up in turn those of Chow. If this process goes on for 100 generations, then the result may be known.
</div>

+++

<div class="left conf">
It is but flattery to make sacrificial offerings to departed spirits not belonging to one's own family. 

It is moral cowardice to leave undone what one perceives to be right to do. 
</div>

[Footnote 2=  Of Lu (Confucius's native State).] 

[Footnote 3=  Head of one of the "Three Families" of Lu.]
